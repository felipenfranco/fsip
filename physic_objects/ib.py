'''
Created on Mar 9, 2012

@author: felipe
'''
import utils
import numpy
import physic_objects
from math_objects.lagrangian_points import LagrangianPoints
from math_objects.cartesian_grid import *
from linear_algebra.linear_system import LinearSystem
from fluid import StokesFluid
import fast_ib
import utils
from physic_objects.fluid import NavierStokesFluid, PolimericNavierStokesFluid


class IB(LagrangianPoints):
    '''
        Abstract Class for the Immersed Boundary.
    '''


    def __init__(self, numberOfPoints):
        '''
        Constructor
        '''
        LagrangianPoints.__init__(self, numberOfPoints)
        self.velocity = LagrangianPoints(numberOfPoints)
        self.force = LagrangianPoints(numberOfPoints)
        self.ID = "IB"
        self.h = 1. / numberOfPoints
        self.u, self.v = [], []

    def updateData(self, currentTime):
        pass

    def setVelocity(self, dX):
        self.u.append(dX[:self.numberOfPoints].mean())
        self.v.append(dX[self.numberOfPoints:].mean())

    def getVelocity(self):
        return numpy.array(self.u), numpy.array(self.v)

    def getPeriodicPositions(self, sizeX, sizeY, x, y):
        '''
        This function acts like the origin is (0,0)
        '''
        periodicX = numpy.copy(x)
        periodicY = numpy.copy(y)
        fast_ib.ib.enforce_periodicity(periodicX, periodicY, sizeX, sizeY)
        return periodicX, periodicY

    def interpolateVelocity(self, velocityField):
        '''
        This function makes the translation so that the origin is (0,0), to work with the periodic function above
        '''
        u = velocityField.u
        v = velocityField.v
        x , y = self.getX()
        uib, vib = self.velocity.getX()
        ux, uy = u.getX(), u.getY()
        vx, vy = v.getX(), v.getY()
        sx, ex, sy, ey = u.grid.getBounds(Fortran=False)  # <- this False is correct!!!
        nx, ny = u.grid.nx, u.grid.ny
        dx, dy = u.grid.getSpacing()
        A0, B0 = u.grid.origin.x , u.grid.origin.y
        A1, B1 = A0 + u.grid.sizeX , B0 + u.grid.sizeY

        periodicX, periodicY = self.getPeriodicPositions(A1, B1, x, y)
        fast_ib.ib.interpolate_velocity(periodicX, periodicY, uib, vib, u.data, v.data, ux, vx, uy, vy, sx, sy, ey, nx, ny, dx, dy, A0, B0)

    def spreadForce(self, rhsU, rhsV, laggedPositions=None):
        '''
        This function makes the translation so that the origin is (0,0), to work with the periodic function above
        In order to make the Fortran implementation easier, this is routine
        that sends sx,ex,ey from the python point of view
        '''
        x , y = self.getX()
        fx, fy = self.force.getX()
        ux, uy = rhsU.getX(), rhsU.getY()
        vx, vy = rhsV.getX(), rhsV.getY()
        sx, ex, sy, ey = rhsU.grid.getBounds(Fortran=False)  # <- this False is correct!!!
        nx, ny = rhsU.grid.nx, rhsU.grid.ny
        dx, dy = rhsU.grid.getSpacing()
        A0, B0 = rhsU.grid.origin.x , rhsU.grid.origin.y
        A1, B1 = A0 + rhsU.grid.sizeX , B0 + rhsU.grid.sizeY
        h = self.h

        if laggedPositions != None:
            xLagged, yLagged = laggedPositions[:self.numberOfPoints], laggedPositions[self.numberOfPoints:]
            periodicX, periodicY = self.getPeriodicPositions(A1, B1, xLagged, yLagged)
        else:
            periodicX, periodicY = self.getPeriodicPositions(A1, B1, x, y)

        fast_ib.ib.spread_force(periodicX, periodicY, fx, fy, rhsU.data, rhsV.data, ux, vx, uy, vy, sx, sy, ey, nx, ny, dx, dy, A0, B0, h)

    def computeForce(self, x=None, y=None):
        if x != None and y != None:
            return self.boundaryForce.computeForce(self, x, y)
        else:
            self.boundaryForce.computeForce(self)

    def computeHessian(self):
        self.boundaryForce.computeHessian(self)

    def computeEnergy(self):
        pass

    def getHessian(self):
        return self.hessian

    def rateOfWork(self):
        u, v = self.velocity.getX()
        fx, fy = self.force.getX()
        return (numpy.dot(u, fx) + numpy.dot(v, fy)) * self.ds

    def explicitUpdate(self, dt, currentTime=None):
        u, v = self.velocity.getX()
        self.x += u * dt
        self.y += v * dt

    def computeMaxDt(self):
        dx, dy = self.grid.getSpacing()
        return self.boundaryForce.computeMaxDt(dx, dy)

    def setX(self, X):
        self.x = X[:self.numberOfPoints]
        self.y = X[self.numberOfPoints:]

    def updateX(self, dX):
        self.x += dX[:self.numberOfPoints]
        self.y += dX[self.numberOfPoints:]

    def saveState(self):
        raise NotImplementedError("Only the swimmer implement this method!")

    def loadState(self, currentTime):
        raise NotImplementedError("Only the swimmer implement this method!")


class Ellipse(IB):
    def __init__(self, numberOfPoints, centerPoint, sizeX, sizeY, grid, boundaryForce):
        IB.__init__(self, numberOfPoints)
        self.centerPoint = centerPoint
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.grid = grid
        self.h = 1. / numberOfPoints
        self.boundaryForce = boundaryForce
        # generate the ellipse
        sin, cos, pi = numpy.sin, numpy.cos, numpy.pi
        self.x[:] = [sizeX * cos(2 * pi * self.h * i) + centerPoint.x for i in range(numberOfPoints)]
        self.y[:] = [sizeY * sin(2 * pi * self.h * i) + centerPoint.y for i in range(numberOfPoints)]



class Wall(IB):
    def __init__(self, ds, startPoint, endPoint, grid, boundaryForce):
        '''Only works for horizontal walls, for now'''
        length = ((endPoint.x - startPoint.x) ** 2 + (endPoint.y - startPoint.y) ** 2) ** .5
        numberOfPoints = int(ceil(length / ds))
        IB.__init__(self, numberOfPoints)
        self.grid = grid
        self.boundaryForce = boundaryForce
        self.h = ds
        self.x[:] = [i * ds + startPoint.x for i in range(numberOfPoints)]
        self.y[:] = [startPoint.y for i in range(numberOfPoints)]


class RandomPoints(IB):
    def __init__(self, sx, ex, sy, ey, numberOfPoints, grid, boundaryForce):
        self.sx = sx
        self.ex = ex
        self.sy = sy
        self.ey = ey
        self.grid = grid
        self.numberOfPoints = numberOfPoints
        self.boundaryForce = boundaryForce
        IB.__init__(self, numberOfPoints)

        self.h = 1. / numberOfPoints
        self.x[:] = sx + (ex - sx) * numpy.random.random(numberOfPoints)
        self.y[:] = sy + (ey - sy) * numpy.random.random(numberOfPoints)


if __name__ == '__main__':
    from numpy import pi, sqrt
    import pylab
    from force import EllasticForce, TheterForce

#===============================================================================
# Bubble
#===============================================================================
    n = 64
    nbp = 2 * n
    ellasticConst = 1000.0
    visc = 1.
    dens = 1.

    grid = Grid(Point(0., 0.), 1., 1., n, n, 2)
    fluid = StokesFluid(grid, visc)
#     fluid = NavierStokesFluid(grid, visc, dens)
    bubble = Ellipse(nbp, Point(0.5, 0.5), 0.3, 0.1, grid, EllasticForce(ellasticConst))

    dx , dy = grid.getSpacing()
    dt = 0.01 * sqrt(dx / ellasticConst)

    pylab.ion()
    while fluid.currentTime < 1.:
        print fluid.currentTime
        bubble.computeForce()

        fluid.clearRhs()
        bubble.spreadForce(fluid.systemU.rhs, fluid.systemV.rhs)
        fluid.advanceInTime(dt)

        bubble.interpolateVelocity(fluid.velocity)
        bubble.explicitUpdate(dt)

        pylab.clf()
        pylab.axis([0, 1, 0, 1])
        pylab.plot(bubble.x, bubble.y)
        pylab.draw()


#===============================================================================
# Cylinder
#===============================================================================
#    n = 32
#    nbp = 2 * n
#    ellasticConst = 5000.0
#    cylinder_diameter = 0.3
#    reynolds = 250
#    dens = 1.
#    u_inf = 1.
#    visc = dens * u_inf * cylinder_diameter / reynolds
#
#    beta = 0.5
#    de = 5.0
#
#    grid = Grid(Point(0., 0.), 8., 8., n, n, 2)
#    fluid = NavierStokesFluid(grid, visc, dens)
# #    fluid = PolimericNavierStokesFluid(grid, visc, dens, beta, de)
#    cylinder = Ellipse(nbp, Point(1.85, 4.), cylinder_diameter, cylinder_diameter, grid, TheterForce(ellasticConst))
#    cylinder.boundaryForce.setTheterPoints(cylinder.x.copy(), cylinder.y.copy())
#
#    dx , dy = grid.getSpacing()
#    r = 1.
#
#    fRhsU = lambda x, y: .02
#    fRhsV = lambda x, y: 0.
#
#    pylab.ion()
#    snapshot = 0.05
#
#    while fluid.currentTime < 100.:
#        print fluid.currentTime
#        dt = 0.5 * min(fluid.CFL(r), (grid.dx ** r) * sqrt(dens / ellasticConst))
#        cylinder.computeForce()
#
#        fluid.clearRhs()
#        fluid.initializeRhs(fRhsU, fRhsV)
#        cylinder.spreadForce(fluid.systemU.rhs, fluid.systemV.rhs)
#        fluid.advanceInTime(dt)
#
#        cylinder.interpolateVelocity(fluid.velocity)
#        cylinder.explicitUpdate(dt)
#
#        print "Max velocity:", fluid.velocity.u.data.max()
#
#        if snapshot < fluid.currentTime:
#            snapshot += 10.0
#            pylab.clf()
#            pylab.axis([0., 8., 0., 8.])
#            x, y = cylinder.getPeriodicPositions(2 * pi, 2 * pi, cylinder.x, cylinder.y)
#            pylab.plot(x, y)
# #            pylab.quiver(grid.getX(), grid.getY(), fluid.velocity.u.data.T, fluid.velocity.v.data.T)
#            pylab.pcolor(grid.getX(), grid.getY(), fluid.getVorticity().data.T)
# #            pylab.pcolor(grid.getX(), grid.getY(), fluid.pressure.data.T)
#            pylab.colorbar()
#            pylab.draw()
#    raw_input()
