'''
Created on Apr 7, 2013

@author: felipe
'''
import utils
from physic_objects.ib import IB
from math_objects.lagrangian_points import LagrangianPoints
import numpy
import pylab

class IBCollection(IB):
    '''
    A collection of immersed boundaries.
    This class should handle the assemble of forces and hessians.
    '''
    def __init__(self, ibList, ds):
        self.ibList = ibList
        self.grid = ibList[0].grid

        numberOfPoints = 0
        for ib in ibList:
            ib.myShift = numberOfPoints
            numberOfPoints += ib.numberOfPoints

        LagrangianPoints.__init__(self, numberOfPoints)
        self.velocity = LagrangianPoints(numberOfPoints)
        self.force = LagrangianPoints(numberOfPoints)
        self.ID = "IBCollection"
        self.h = ds
        self.u, self.v = [], []

    def getX(self):
        x = []
        y = []
        for ib in self.ibList:
            x.append(ib.x)
            y.append(ib.y)
        self.x[:] = numpy.concatenate(x)
        self.y[:] = numpy.concatenate(y)
        return self.x, self.y

    def updateX(self, dX):
        nbp = self.numberOfPoints
        self.x += dX[:nbp]
        self.y += dX[nbp:]

        for ib in self.ibList:
            ib.x = self.x[ib.myShift:ib.myShift + ib.numberOfPoints]
            ib.y = self.y[ib.myShift:ib.myShift + ib.numberOfPoints]

    def computeForce(self, fluidID=None):
        fx = []
        fy = []
        for ib in self.ibList:
            ib.computeForce(fluidID)
            fx.append(ib.force.x)
            fy.append(ib.force.y)

        self.force.x[:] = numpy.concatenate(fx)
        self.force.y[:] = numpy.concatenate(fy)


    def computeHessian(self):
        nbp = self.numberOfPoints
        hessian = numpy.zeros((2 * nbp, 2 * nbp))

        xyShift = nbp
        for i in range(len(self.ibList)):
            ib = self.ibList[i]
            ib.computeHessian()
            H = ib.getHessian()

            sx = ib.myShift
            ex = ib.myShift + ib.numberOfPoints
            sy = ib.myShift + xyShift
            ey = ib.myShift + ib.numberOfPoints + xyShift

            Hxx = H[0:ib.numberOfPoints, 0:ib.numberOfPoints]
            Hyy = H[ib.numberOfPoints:, ib.numberOfPoints:]
            Hxy = H[0:ib.numberOfPoints, ib.numberOfPoints:]
            Hyx = H[ib.numberOfPoints:, 0:ib.numberOfPoints]

            hessian[sx:ex, sx:ex] = Hxx
            hessian[sy:ey, sy:ey] = Hyy
            hessian[sx:ex, sy:ey] = Hxy
            hessian[sy:ey, sx:ex] = Hyx

        self.hessian = hessian

    def getHessian(self):
        return self.hessian

    def explicitUpdate(self, dt, currentTime=None):
        for ib in self.ibList:
            ib.explicitUpdate(dt, currentTime)

    def updateData(self, currentTime):
        for ib in self.ibList:
            ib.updateData(currentTime)

    def computeEnergy(self):
        energies = []
        for ib in self.ibList:
            if ib.ID == "Swimmer":
                energies.append(ib.computeEnergy())
        return energies

    def interpolateVelocity(self, velocityField):
        velx = []
        vely = []
        
        for ib in self.ibList:
            ib.interpolateVelocity(velocityField)
            velx.append(ib.velocity.x)
            vely.append(ib.velocity.y)
        self.velocity.x = numpy.concatenate(velx)
        self.velocity.y = numpy.concatenate(vely)



if __name__ == '__main__':
    from swimmer.swimmer_class import Swimmer
    from physic_objects.ib import Wall
    from math_objects.cartesian_grid import Grid, Point
    from physic_objects.fluid import StokesFluid, NavierStokesFluid
    from physic_objects.fluid_structure import FluidStructure
    from physic_objects.force import TheterForce

    n = 64
    domainSize = 1.
    grid = Grid(Point(0, 0), domainSize, domainSize, n, n, 2)

    fluid = StokesFluid(grid)

    funcS1 = ".05*sin(2*pi*x-2*pi*t) + .5"

    S1 = 1e5
    S2 = 1e3
    ellasticConstant = 1e6
    ds = grid.dx / 2
    s1 = Swimmer(S1, S2, ds, funcS1, 'x', (0, 1.0), grid, True)
    w1 = Wall(ds, Point(0., .7), Point(1., .7), grid, TheterForce(ellasticConstant))
    w2 = Wall(ds, Point(0., .3), Point(1., .3), grid, TheterForce(ellasticConstant))

    w1.boundaryForce.setTheterPoints(w1.x, w1.y)
    w2.boundaryForce.setTheterPoints(w2.x, w2.y)

    ibs = IBCollection([s1, w1, w2], ds)

    pylab.ioff()
    x, y = ibs.getX()
    pylab.axis([0, 1, 0, 1])
    pylab.plot(x, y, '.')
    pylab.show()


    dt = ds
    finalTime = 100.
    system = FluidStructure(fluid, ibs, dt, 'Implicit')

    pylab.ion()
    snapshot = 0.1
    while fluid.currentTime < finalTime:
        system.advanceInTime(dt)
        if snapshot < fluid.currentTime:
            snapshot += .1
            pylab.clf()
            pylab.axis([0, 1, 0, 1])
            x, y = ibs.getX()
            px, py = ibs.getPeriodicPositions(1., 1., x, y)
            pylab.plot(px, py, '.')
            pylab.streamplot(grid.getX(), grid.getY(), fluid.velocity.u.data.T, fluid.velocity.v.data.T)
            pylab.draw()

