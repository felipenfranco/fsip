'''
Created on Mar 9, 2012

@author: felipe
'''
import utils
import numpy as np
from pycppad import independent, adfun
# import adolc
from swimmer import energy
import fast_energy
import numpy

concatenate = np.concatenate
sqrt = np.sqrt



class TheterForce:
    def __init__(self, ellasticConstant):
        self.ellasticConstant = ellasticConstant
        self.type = "NonLinear"
        self.theterSet = False

    def setTheterPoints(self, theterX, theterY):
        self.theterSet = True
        self.theterX = np.array(theterX)
        self.theterY = np.array(theterY)

    def computeForce(self, ib, x=None, y=None):
        if x != None and y != None:
            raise NotImplementedError("Must implement this!")
        if self.theterSet:
            x, y = ib.getX()
            theterX = self.theterX
            theterY = self.theterY
            fx, fy = ib.force.getX()
            fx[:] = -self.ellasticConstant * (x[:] - theterX[:])
            fy[:] = -self.ellasticConstant * (y[:] - theterY[:])
        else:
            raise RuntimeError("When using Theter Forces, you must set the theter points before " +
                               "the simulation starts using the setTheterPoints method.")

    def computeHessian(self, ib):
        nbp = ib.numberOfPoints
        hessian = np.zeros((2 * nbp, 2 * nbp), order='F')
        kI = -self.ellasticConstant * numpy.identity(nbp)
        hessian[:nbp, :nbp] = kI
        hessian[nbp:, nbp:] = kI
        ib.hessian = hessian

    def computeMaxDt(self, dx, dy):
        return 0.5 * (sqrt(min(dx, dy)) / sqrt(self.ellasticConstant))



class EllasticForce:
    def __init__(self, ellasticConstant):
        self.ellasticConstant = ellasticConstant
        self.type = "NonLinear"

    def computeForce(self, ib, x=None, y=None):
        if x != None and y != None:
            raise NotImplementedError("Must implement this!")
        x, y = ib.getX()
        fx, fy = ib.force.getX()
        nbp = ib.numberOfPoints
        for i in range(-1, nbp - 1):
            fx[i] = x[i - 1] - 2.*x[i] + x[i + 1]
            fy[i] = y[i - 1] - 2.*y[i] + y[i + 1]
        fx *= self.ellasticConstant / (ib.h ** 2)
        fy *= self.ellasticConstant / (ib.h ** 2)

    def computeMaxDt(self, dx, dy):
        return 0.01 * sqrt(min(dx, dy) / self.ellasticConstant)

    def computeHessian(self, ib):
        h = ib.h
        nbp = ib.numberOfPoints
        const = self.ellasticConstant / (h * h)
        hessian = np.zeros((2 * nbp, 2 * nbp), order='F')

        for i in range(nbp):
            hessian[i , np.mod(i - 1, nbp)] = 1.
            hessian[i , i] = -2.
            hessian[i , np.mod(i + 1, nbp)] = 1.

        hessian[nbp:, nbp:] = hessian[0:nbp, 0:nbp]
        ib.hessian = hessian * const


class EnergyForce:
    def __init__(self, computationType='fortran'):
        '''
            computationType = ad or fortran or sparse
        '''
        self.type = "NonLinear"
        self.computationType = computationType
        self.doOnce = True

    def computeEnergy(self, ib, printTerms=False, splitTerms=False):
        return energy(concatenate(ib.getX()), ib.c, ib.ds, ib.numberOfPoints, ib.grid.sizeX, ib.grid.sizeY, ib.S1, ib.S2, ib.isPeriodic, printTerms, splitTerms)

    def computeForce(self, ib, x=None, y=None):
        if x != None and y != None:
            # store the old values
            oldforcex = ib.force.x.copy()
            oldforcey = ib.force.y.copy()
            oldx = ib.x.copy()
            oldy = ib.y.copy()
            ib.x[:] = x
            ib.y[:] = y

        if self.computationType == "ad":
            self._ad_grad(ib)
        elif self.computationType == "fortran" or "sparse":
            self._fortran_grad(ib)
        else:
            raise TypeError("ComputationType %s not available." % self.computationType)

        if x != None and y != None:
            newF = numpy.concatenate([ib.force.x, ib.force.y])
            ib.force.x[:] = oldforcex
            ib.force.y[:] = oldforcey
            ib.x[:] = oldx
            ib.y[:] = oldy
            return newF


    def computeHessian(self, ib):
        if self.doOnce:
            self.doOnce = False
            if self.computationType == "sparse":
                nnz = 11 * ib.numberOfPoints * 2  # number of non zeros: this is based on the structure of the hessian
                self.rows = np.zeros(nnz, dtype=int)
                self.cols = np.zeros(nnz, dtype=int)
                self.vals = np.zeros(nnz, dtype=float)
            else:
                self.H = np.zeros((ib.numberOfPoints * 2, ib.numberOfPoints * 2), order='F')


        if self.computationType == "ad":
            self._ad_hessian(ib)
        elif self.computationType == "fortran":
            self._fortran_hessian(ib)
        elif self.computationType == "sparse":
            self._sparse_hessian(ib)
        else:
            raise TypeError("ComputationType %s not available." % self.computationType)

    def _ad_grad(self, ib):
        nbp = ib.numberOfPoints
        X = concatenate(ib.getX())
        ax = independent(X)
        ay = np.array([energy(ax, ib.c, ib.ds, nbp, ib.grid.sizeX, ib.grid.sizeY, ib.S1, ib.S2, ib.isPeriodic), ])
        function = adfun(ax, ay)
        function.optimize()
        grad = function.jacobian(X)[0]
        # force = -grad
        ib.force.x[:] = -1.*grad[:nbp]
        ib.force.y[:] = -1.*grad[nbp:]

    def _fortran_grad(self, ib):
        fast_energy.hessian.compute_grad(ib.x, ib.y, ib.c, ib.force.x, ib.force.y, ib.ds, ib.S1, ib.S2)
        ib.force.x *= -1.
        ib.force.y *= -1.
        small_gx, small_gy = self._small_ad_grad(ib)
        ib.force.x[0:3] = small_gx[0:3]
        ib.force.y[0:3] = small_gy[0:3]
        ib.force.x[-3:] = small_gx[-3:]
        ib.force.y[-3:] = small_gy[-3:]

    def _ad_hessian(self, ib):
        X = concatenate(ib.getX())
        nbp = ib.numberOfPoints
        ax = independent(X)
        ay = np.array([energy(ax, ib.c, ib.ds, nbp, ib.grid.sizeX, ib.grid.sizeY, ib.S1, ib.S2, ib.isPeriodic), ])
        function = adfun(ax, ay)
        function.optimize()
        ib.hessian = function.hessian(X, np.array([1.]))

    def _fortran_hessian(self, ib):
        nbp = ib.numberOfPoints
        H = self.H
        fast_energy.hessian.compute_hessian(ib.x, ib.y, ib.c, H, ib.ds, ib.S1, ib.S2)

        small_H, nbps = self._small_ad_hessian(ib)

        # x part
        H[0, 0:4] = small_H[0, 0:4]
        H[1, 0:4] = small_H[1, 0:4]
        H[nbp - 2, 0:4] = small_H[nbps - 2, 0:4]
        H[nbp - 1, 0:4] = small_H[nbps - 1, 0:4]
        H[nbp - 2, nbp - 1 - 4:nbp] = small_H[nbps - 2, nbps - 1 - 4:nbps]
        H[nbp - 1, nbp - 1 - 4:nbp] = small_H[nbps - 1, nbps - 1 - 4:nbps]

        # mixed part
        H[nbp, 0:4] = small_H[nbps, 0:4]
        H[nbp + 1, 0:4] = small_H[nbps + 1, 0:4]
        H[nbp, nbp - 1 - 4:nbp] = small_H[nbps, nbps - 1 - 4:nbps]
        H[nbp + 1, nbp - 1 - 4:nbp] = small_H[nbps + 1, nbps - 1 - 4:nbps]

        H[2 * nbp - 2, 0:4] = small_H[2 * nbps - 2, 0:4]
        H[2 * nbp - 1, 0:4] = small_H[2 * nbps - 1, 0:4]
        H[2 * nbp - 2, nbp - 1 - 4:nbp] = small_H[2 * nbps - 2, nbps - 1 - 4:nbps]
        H[2 * nbp - 1, nbp - 1 - 4:nbp] = small_H[2 * nbps - 1, nbps - 1 - 4:nbps]

        # y part
        H[nbp, nbp:nbp + 4] = small_H[nbps, nbps:nbps + 4]
        H[nbp + 1, nbp:nbp + 4] = small_H[nbps + 1, nbps:nbps + 4]
        H[2 * nbp - 2, nbp:nbp + 4] = small_H[2 * nbps - 2, nbps:nbps + 4]
        H[2 * nbp - 1, nbp:nbp + 4] = small_H[2 * nbps - 1, nbps:nbps + 4]
        H[2 * nbp - 2, 2 * nbp - 1 - 4:2 * nbp] = small_H[2 * nbps - 2, 2 * nbps - 1 - 4:2 * nbps]
        H[2 * nbp - 1, 2 * nbp - 1 - 4:2 * nbp] = small_H[2 * nbps - 1, 2 * nbps - 1 - 4:2 * nbps]

        fast_energy.hessian.fill_upper_part(H)
        ib.hessian = H




    def _small_ad_grad(self, ib):
        nbp = 10
        x = concatenate([ib.x[0:nbp / 2], ib.x[-nbp / 2:]])
        y = concatenate([ib.y[0:nbp / 2], ib.y[-nbp / 2:]])
        c = concatenate([ib.c[0:nbp / 2], ib.c[-nbp / 2:]])

        X = concatenate([x, y])
        ax = independent(X)
        ay = np.array([energy(ax, c, ib.ds, nbp, ib.grid.sizeX, ib.grid.sizeY, ib.S1, ib.S2, ib.isPeriodic), ])
        function = adfun(ax, ay)
        function.optimize()
        grad = function.jacobian(X)[0]
        # force = -grad
        return -1.*grad[:nbp], -1.*grad[nbp:]


    def _small_ad_hessian(self, ib):
        nbp = 14
        x = concatenate([ib.x[0:nbp / 2], ib.x[-nbp / 2:]])
        y = concatenate([ib.y[0:nbp / 2], ib.y[-nbp / 2:]])
        c = concatenate([ib.c[0:nbp / 2], ib.c[-nbp / 2:]])

        X = concatenate([x, y])
        ax = independent(X)
        ay = np.array([energy(ax, c, ib.ds, nbp, ib.grid.sizeX, ib.grid.sizeY, ib.S1, ib.S2, ib.isPeriodic), ])
        function = adfun(ax, ay)
        function.optimize()
        return function.hessian(X, np.array([1.])), nbp


    def _sparse_hessian(self, ib):
        nbp = ib.numberOfPoints

        rows = self.rows
        cols = self.cols
        vals = self.vals

        rows[:] = 0
        cols[:] = 0
        vals[:] = 0.

        last_index = fast_energy.hessian.compute_sparse_hessian(ib.x, ib.y, ib.c, vals, rows, cols, ib.ds, ib.S1, ib.S2)

        small_H, nbps = self._small_ad_hessian(ib)

        r, c = numpy.nonzero(small_H)
        lines = []
        for line in [0, 1, 2, 3, nbps - 4, nbps - 3, nbps - 2, nbps - 1, nbps, nbps + 1, 2 * nbps - 4, 2 * nbps - 3, 2 * nbps - 2, 2 * nbps - 1]:
            lines.append(numpy.where(r == line)[0])

        # this is ugly, fix some problems with the fortran part filling the symmetric part, minor overhead, so...
        for line in [0, 1, 2, 3, nbp - 4, nbp - 3, nbp - 2, nbp - 1, nbp, nbp + 1, 2 * nbp - 4, 2 * nbp - 3, 2 * nbp - 2, 2 * nbp - 1]:
            vals[numpy.where(rows == line)] = 0.0

        indicesSmall = numpy.concatenate(lines)
        rBig = []
        cBig = []

        def toBig(index):
            if 0 <= index < nbps / 2:
                return index
            if nbps / 2 <= index < 3 * nbps / 2:
                return nbp - (nbps - index)
            if 3 * nbps / 2 <= index < 2 * nbps:
                return 2 * nbp - (2 * nbps - index)

        for index in indicesSmall:
            rBig.append(toBig(r[index]))
            cBig.append(toBig(c[index]))

        for k in range(len(rBig)):
            posBig = last_index + k
            rows[posBig] = rBig[k]
            cols[posBig] = cBig[k]
            rSmall = r[indicesSmall[k]]
            cSmall = c[indicesSmall[k]]
            vals[posBig] = small_H[rSmall, cSmall]

        #this is faster then creating the csc matrix!!!
        import scipy.sparse as sparse
        coo = sparse.coo_matrix((vals, (rows, cols)), shape=(2 * nbp, 2 * nbp))
        csc = coo.tocsc()
        ib.hessian = csc
