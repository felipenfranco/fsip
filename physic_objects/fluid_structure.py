# -*- coding: utf-8 -*-
'''
Created on Mar 15, 2012

@author: felipe
'''
import utils
from math_objects.implicit_method import newton, FluidStructureInteraction
from math_objects.lookup_table import LookupTable
import numpy
from scipy.linalg import solve
from numpy.core.numeric import concatenate
from physic_objects.fluid import PolimericNavierStokesFluid


class FluidStructure:
    '''
    This class holds a Fluid type class and a Immersed Boundary.
    '''

    def __init__(self, fluidIn, ibIn, dt, systemType, computationType="Matrix"):
        '''
        systemType = "Explicit" or "Implicit" string
        '''
        self.fluid = fluidIn
        self.ib = ibIn
        self.systemType = systemType
        self.dt = dt
        self.computationType = computationType

        if systemType is "Explicit":
            self.advanceInTime = self.advanceExplicit
        elif systemType is "Implicit":
#             if ibIn.boundaryForce.type == "Linear":
#                 self.advanceInTime = self.advanceImplicitLinear
#             else:
#                 self.advanceInTime = self.advanceImplicitNonLinear
            self.advanceInTime = self.advanceImplicitNonLinear
            self.FSInteraction = FluidStructureInteraction(LookupTable(self.fluid, dt), computationType)
        else:
            raise NotImplementedError("Type %s is not implemented. It must be Explicit or Implicit" % (systemType))

        self.I = numpy.identity(self.ib.numberOfPoints * 2)

    def changeDt(self, new_dt):
        if self.dt != new_dt:
            self.FSInteraction = FluidStructureInteraction(LookupTable(self.fluid, new_dt), 'Matrix')
            self.dt = new_dt

    def advanceExplicit(self, dt):
        self.ib.computeForce(self.fluid.ID)
        self.ib.spreadForce(self.fluid.systemU.rhs, self.fluid.systemV.rhs)
        self.fluid.advanceInTime(dt)
        self.ib.interpolateVelocity(self.fluid.velocity)
        self.ib.explicitUpdate(dt, self.fluid.currentTime)
        self.fluid.clearRhs()
        print "Current Time:", self.fluid.currentTime

    def advanceImplicitLinear(self, dt):
        nbp = self.ib.numberOfPoints
        FSI = self.FSInteraction
        self.ib.computeHessian()
        A = self.ib.hessian
        FSI.performPreComputations(self.fluid, self.ib, dt)
        laggedPositions = numpy.concatenate([self.ib.x, self.ib.y])

        L = self.I - FSI.computeInteraction(A)

        X = solve(L, FSI.explicitTerm)
        self.ib.x = X[:nbp]
        self.ib.y = X[nbp:]

        self.fluid.clearRhs()
        self.ib.computeForce()
        self.ib.spreadForce(self.fluid.systemU.rhs, self.fluid.systemV.rhs, laggedPositions)

        self.fluid.advanceInTime(dt)
        print "Current Time:", self.fluid.currentTime

    def advanceImplicitNonLinear(self, dt, withReport=False):
        FSI = self.FSInteraction

        self.ib.updateData(self.fluid.currentTime)

        FSI.performPreComputations(self.fluid, self.ib, dt)

        laggedPositions = numpy.concatenate([self.ib.x, self.ib.y])
        newton(self.ib, FSI, dt, withReport)
#        steepestDescent(self.ib, FSI, dt)

        self.ib.computeForce()
        self.ib.spreadForce(self.fluid.systemU.rhs, self.fluid.systemV.rhs, laggedPositions)
        self.fluid.advanceInTime(dt)
        self.ib.interpolateVelocity(self.fluid.velocity)
        self.fluid.clearRhs()
        print "Current Time (%s):" % (self.computationType), self.fluid.currentTime

    def saveState(self, stateName):
        self.fluid.saveState(stateName + "fluid")
        self.ib.saveState(stateName + "ib")

    def loadState(self, stateName):
        self.fluid.loadState(stateName + "fluid.fluid")
        self.ib.loadState(stateName + "ib")


if __name__ == '__main__':
    from fluid import PolimericStokesFluid, StokesFluid, NavierStokesFluid
    from ib import Ellipse
    from force import EllasticForce
    from math_objects.cartesian_grid import *
    from math_objects.lagrangian_points import LagrangianPoints
    import pylab

    n = 64
    grid = Grid(Point(0., 0.), 1.0, 1.0, n, n, 2)
    visc = .01
    dens = 1.
    ellasticConst = 10e3
    de = 50.
    beta = .5

#    fluid = StokesFluid(grid, visc)
#    fluid2 = StokesFluid(grid, visc)
#    fluid = PolimericStokesFluid(grid, 1.0, 0.5, 5.0)
#    fluid2 = PolimericStokesFluid(grid, 1.0, 0.5, 5.0)
    fluid = NavierStokesFluid(grid, visc, dens)
    fluid2 = NavierStokesFluid(grid, visc, dens)
#    fluid = PolimericNavierStokesFluid(grid, visc, dens, beta, de)
#    fluid2 = PolimericNavierStokesFluid(grid, visc, dens, beta, de)

    ib = Ellipse(n * 2, Point(0.5, 0.5), 0.3, 0.1, grid, EllasticForce(ellasticConst))
    ib2 = Ellipse(n * 2, Point(0.5, 0.5), 0.3, 0.1, grid, EllasticForce(ellasticConst))

    dt = 0.00025 * grid.dx

    #dt *= 0.01 #if its stokes!!!
    implicitSystem = FluidStructure(fluid, ib, dt, "Implicit")
    explicitSystem = FluidStructure(fluid2, ib2, dt, "Explicit")

    pylab.ion()
    snapshot = 10 * dt
    while fluid.currentTime < 10.:
        explicitSystem.advanceInTime(dt)
        implicitSystem.advanceInTime(dt)


        implicitX, implicitY = implicitSystem.ib.getX()
        explicitX, explicitY = explicitSystem.ib.getX()
        diffx = implicitX - explicitX
        diffy = implicitY - explicitY

        if snapshot < fluid.currentTime:
            snapshot += 50 * dt
            pylab.clf()
            pylab.axis([0, 1, 0, 1])
            pylab.plot(implicitX, implicitY, label='Implicit')
            pylab.plot(explicitX, explicitY, label='Explicit')
#            pylab.contourf(grid.getX(), grid.getY(), fluid2.polimer.Sxx.data.T)
#            pylab.colorbar()
            pylab.legend()
            pylab.draw()
        print max(abs(diffx).max(), abs(diffy).max()), 1. / n

