from os.path import realpath
import utils
from math_objects import updateGhostsCode

diracDelta = '''!    -*- f90 -*-
subroutine compute_delta(x,y,dx,dy,delta_out,pi)
    double precision :: delta_out
    double precision , intent(in) :: x , y, dx , dy , pi
    double precision :: d1 , d2

    d1 = 0.0
    d2 = 0.0

    !for debugging purpouse only... comment when not needed
    if (x > 3*dx) then
        print *, "Error, dirac delta distance_x!!!"
        stop
    end if
    if (y > 3*dy) then
        print *, "Error, dirac delta distance_y!!!"
        stop
    end if


    if (x <= dx) then
        d1 = (1.0/(8.0*dx))*(3.0 - (2.0*x)/(dx) + sqrt(1.0+(4.0*x)/(dx)-(4.0*x*x)/(dx*dx)))
    else if (x > dx .and. x <= 2.0*dx) then
        d1 = (1.0/(8.0*dx))*(5.0 - (2.0*x)/(dx) - sqrt(-7.0+(12.0*x)/(dx)-(4.0*x*x)/(dx*dx)))
    else
        d1 = 0.0
    end if

    if (y <= dy) then
        d2 = (1.0/(8.0*dy))*(3.0 - (2.0*y)/(dy) + sqrt(1.0+(4.0*y)/(dy)-(4.0*y*y)/(dy*dy)))
    else if (y > dy .and. y <= 2.0*dy) then
        d2 = (1.0/(8.0*dy))*(5.0 - (2.0*y)/(dy) - sqrt(-7.0+(12.0*y)/(dy)-(4.0*y*y)/(dy*dy)))
    else
        d2 = 0.0
    end if

    delta_out = d1*d2
end subroutine
'''

enforcePeriodicity = '''!    -*- f90 -*-
subroutine enforce_periodicity(x,y,n,x_size,y_size)
    implicit none
    integer, intent(in) :: n
    double precision , dimension(n) , intent(inout) :: x , y
    double precision , intent(in) :: x_size, y_size
    integer :: fiber

    !f2py intent(in) :: x_size, y_size
    !f2py intent(inplace) :: x , y
    !f2py intent(hide) :: n
    !f2py threadsafe

    do fiber = 1, n
        x(fiber) = modulo(x(fiber)+x_size , x_size)
        y(fiber) = modulo(y(fiber)+y_size , y_size)

        if (x(fiber)==0.0) then
            x(fiber)=x_size
        end if
    end do
end subroutine
'''

spreadForceIB = '''!    -*- f90 -*-
subroutine spread_force(x_fiber,y_fiber,fx,fy,nbp,urhs,vrhs,n,m,ux,vx,k,uy,vy,l,sx,sy,ey,nx,ny,dx,dy,A0,B0,h)
    implicit none
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: urhs , vrhs
    double precision , dimension(0:(nbp-1)) , intent(inout) :: x_fiber , y_fiber , fx , fy
    double precision , dimension(0:(k-1)) , intent(inout) :: ux , vx
    double precision , dimension(0:(l-1)) , intent(inout) :: uy , vy
    double precision , dimension(-2:2) :: dx_matrix, dy_matrix
    integer  , intent(in) :: sx , sy , ey , n , m , l , nbp , nx , ny, k
    double precision , intent(in) :: dx , dy , A0, B0,h
    integer :: i , j , fiber , ipos , jpos , imodulo , jmodulo
    double precision :: distance_x , distance_y , x, y , delta
    double precision :: pi = 4.0*datan(1.0d0)
    !f2py intent(in) :: sx , sy , ey , n , m , nbp , nx , ny , dx , dy, A0, B0,h
    !f2py intent(inplace) :: urhs , vrhs , x_fiber , y_fiber , fx , fy , ux , uy , vx , vy
    !f2py intent(hide) :: n , m , nbp , l, k


    !creates the matrix of distances
    do i = -2,2
        dx_matrix(i)=dx*i
        dy_matrix(i)=dy*i
    end do


    do fiber = 0 , nbp-1
        x = x_fiber(fiber)
        y = y_fiber(fiber)
        ipos = int(ceiling((x-A0)/dx)) - 1 + sx
        jpos = int(floor((y-B0)/dy)) + sy

        do j = jpos-2 , jpos+2
            do i = ipos-2 , ipos+2
                imodulo = modulo(i-sx,nx)+sx
                jmodulo = modulo(j-sy,ny)+sy

                distance_x = abs( x - (ux(ipos)+dx_matrix(i-ipos)) )
                distance_y = abs( y - (uy(jpos)+dy_matrix(j-jpos)) )
                call compute_delta(distance_x,distance_y,dx,dy,delta,pi)
                urhs(imodulo,jmodulo) = urhs(imodulo,jmodulo) + fx(fiber)*delta*h

                distance_x = abs( x - (vx(ipos)+dx_matrix(i-ipos)) )
                distance_y = abs( y - (vy(jpos)+dy_matrix(j-jpos)) )
                call compute_delta(distance_x,distance_y,dx,dy,delta,pi)
                vrhs(imodulo,jmodulo) = vrhs(imodulo,jmodulo) + fy(fiber)*delta*h
            end do
        end do
    end do
end subroutine
'''

interpolateVelocityIB = '''!    -*- f90 -*-
subroutine interpolate_velocity(x_fiber,y_fiber,u,v,nbp,ufluid,vfluid,n,m,ux,vx,k,uy,vy,l,sx,sy,ey,nx,ny,dx,dy,A0,B0)
    implicit none
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: ufluid , vfluid
    double precision , dimension(0:(nbp-1)) , intent(inout) :: x_fiber , y_fiber , u , v
    double precision , dimension(0:(k-1)) , intent(inout) :: ux , vx
    double precision , dimension(0:(l-1)) , intent(inout) :: uy , vy
    double precision , dimension(-2:2) :: dx_matrix, dy_matrix
    integer , intent(in) :: sx, sy, ey , n , m , l , nbp , nx , ny, k
    double precision , intent(in) :: dx , dy ,A0,B0
    integer :: i , j , fiber , ipos , jpos , imodulo , jmodulo
    double precision :: distance_x , distance_y , x , y , delta
    double precision :: pi = 4.0*datan(1.0d0)
    !f2py intent(in) :: sx, sy, ey , n , m , nbp , nx , ny , dx , dy , A0 , B0, ux , uy , vx , vy
    !f2py intent(inplace) :: ufluid , vfluid , x_fiber , y_fiber , u , v
    !f2py intent(hide) :: n , m , nbp , l, k

    u = 0.0
    v = 0.0

    !creates the matrix of distances
    do i = -2,2
        dx_matrix(i)=dx*i
        dy_matrix(i)=dy*i
    end do

    do fiber = 0 , nbp-1
        x = x_fiber(fiber)
        y = y_fiber(fiber)
        ipos = int(ceiling((x-A0)/dx)) - 1 + sx
        jpos = int(floor((y-B0)/dy)) + sy

        do j = jpos-2 , jpos+2
            do i = ipos-2 , ipos+2
                imodulo = modulo(i-sx,nx)+sx
                jmodulo = modulo(j-sy,ny)+sy
                distance_x = abs( x - (ux(ipos)+dx_matrix(i-ipos)) )
                distance_y = abs( y - (uy(jpos)+dy_matrix(j-jpos)) )
                call compute_delta(distance_x,distance_y,dx,dy,delta,pi)
                u(fiber) = u(fiber) + ufluid(i,j)*delta*dx*dy

                distance_x = abs( x - (vx(ipos)+dx_matrix(i-ipos)) )
                distance_y = abs( y - (vy(jpos)+dy_matrix(j-jpos)) )
                call compute_delta(distance_x,distance_y,dx,dy,delta,pi)
                v(fiber) = v(fiber) + vfluid(i,j)*delta*dx*dy
            end do
        end do
    end do
end subroutine
'''

spreadForcePolimer = '''!    -*- f90 -*-
subroutine spread_force_polimer(Sxx, Syy, Sxy, n, m, urhs, vrhs, dx, dy, beta, sx, ex, sy, ey, ngc)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: Sxx, Syy, Sxy, urhs, vrhs
    double precision , intent(in) :: dx, dy, beta
    integer , intent(in) :: n , m, sx, ex, sy, ey, ngc
    double precision :: divu, divv
    integer :: i, j

    !f2py intent(inplace) :: Sxx, Syy, Sxy, urhs, vrhs
    !f2py intent(hide) :: n, m
    !f2py intent(in) :: dx, dy, beta, sx, ex, sy, ey, ngc
    !f2py threadsafe
    call update_ghosts(Sxx,n,m,sx,ex,sy,ey,ngc)
    call update_ghosts(Syy,n,m,sx,ex,sy,ey,ngc)
    call update_ghosts(Sxy,n,m,sx,ex,sy,ey,ngc)

    do j = sy , ey
        do i = sx , ex
            divu = (Sxx(i+1,j)-Sxx(i,j))/dx + (Sxy(i,j+1)-Sxy(i,j))/dy
            divv = (Sxy(i,j)-Sxy(i-1,j))/dx + (Syy(i,j)-Syy(i,j-1))/dy
            urhs(i,j) = urhs(i,j) + beta*divu
            vrhs(i,j) = vrhs(i,j) + beta*divv
        end do
    end do
end subroutine
'''

advanceOperatorSxx = '''!    -*- f90 -*-
!the act of appplying advance_operator to the stress tensor is returned in the Gxx variable
subroutine advance_operator_sxx(Gxx, Sxx, Syy, Sxy, rhs, D1x, D2x, D3x, D1y, D2y, D3y, &
                                diffx, diffy, n, m, u, v, dx, dy, we, sx, ex, sy, ey, ngc, dt)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: Gxx, Sxx, Syy, Sxy, u, v, rhs, &
                                                         D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy
    double precision , intent(in) :: dx, dy, we, dt
    integer , intent(in) :: n , m, sx, ex, sy, ey, ngc
    integer :: i, j
    double precision :: u_center, v_center, Sxx_dx, Sxx_dy, u_dx, u_dy, Sxy_center, Sxx_dxx, Sxx_dyy
    double precision :: uSxx_dx , vSxx_dy
    !f2py threadsafe
    !f2py intent(inplace) :: Gxx, Sxx, Syy, Sxy, u, v, rhs, D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy
    !f2py intent(hide) :: n, m
    !f2py intent(in) :: dx, dy, we, sx, ex, sy, ey, ngc, dt

    call divided_diffs_x(Sxx, D1x, D2x, D3x, diffx, u, n, m, dx, sx, ex, sy, ey, ngc)
    call divided_diffs_y(Sxx, D1y, D2y, D3y, diffy, v, n, m, dx, sx, ex, sy, ey, ngc)

    do j = sy, ey
        do i = sx , ex
            u_center    = ( u(i,j) + u(i-1,j) ) / 2.0
            v_center    = ( v(i,j) + v(i,j+1) ) / 2.0

            u_dx        = (u(i,j)-u(i-1,j)) / dx
            u_dy        = (u(i,j+1)-u(i,j-1)+u(i-1,j+1)-u(i-1,j-1)) / (4.0*dy)
            Sxy_center  = (Sxy(i,j)+Sxy(i-1,j)+Sxy(i-1,j+1)+Sxy(i,j+1)) / 4.0
            Sxx_dxx     = (Sxx(i-1,j)-2.0*Sxx(i,j)+Sxx(i+1,j))/(dx*dx)
            Sxx_dyy     = (Sxx(i,j-1)-2.0*Sxx(i,j)+Sxx(i,j+1))/(dy*dy)

            uSxx_dx = u_center*diffx(i,j)
            vSxx_dy = v_center*diffy(i,j)
            !uSxx_dx = u_center*(-Sxx(i+2,j) + 8.0d0*Sxx(i+1,j) -8.0d0*Sxx(i-1,j) + Sxx(i-2,j))/(12.0d0*dx)
            !vSxx_dy = v_center*(-Sxx(i,j+2) + 8.0d0*Sxx(i,j+1) -8.0d0*Sxx(i,j-1) + Sxx(i,j-2))/(12.0d0*dy)
            !uSxx_dx = u_center*(Sxx(i+1,j) -Sxx(i-1,j))/(2.0d0*dx)
            !vSxx_dy = v_center*(Sxx(i,j+1) -Sxx(i,j-1))/(2.0d0*dy)

            !Gxx(i,j)    = -(uSxx_dx + vSxx_dy) + 2.0*(u_dx*Sxx(i,j)+u_dy*Sxy_center) -(1.0/we)*(Sxx(i,j)-1.0) &
            !              + rhs(i,j)

            Gxx(i,j) = Sxx(i,j) + dt*( 2.0*(u_dx*Sxx(i,j)+u_dy*Sxy_center) + (1.0/we)*(1.0-Sxx(i,j)) &
                                       -(uSxx_dx + vSxx_dy) + rhs(i,j) )

        end do
    end do
    call update_ghosts(Gxx,n,m,sx,ex,sy,ey,ngc)
end subroutine advance_operator_sxx
'''


advanceOperatorSyy = '''!    -*- f90 -*-
!the act of appplying advance_operator to the stress tensor is returned in the Gxx variable
subroutine advance_operator_syy(Gyy, Sxx, Syy, Sxy, rhs, D1x, D2x, D3x, D1y, D2y, D3y, &
                                diffx, diffy, n, m, u, v, dx, dy, we, sx, ex, sy, ey, ngc, dt)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: Gyy, Sxx, Syy, Sxy, u, v, rhs, &
                                                         D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy
    double precision , intent(in) :: dx, dy, we, dt
    integer , intent(in) :: n , m, sx, ex, sy, ey, ngc
    integer :: i, j
    double precision :: u_center, v_center, Syy_dx, Syy_dy, v_dx, v_dy, Sxy_center, Syy_dxx, Syy_dyy
    double precision :: uSyy_dx, vSyy_dy
    !f2py threadsafe
    !f2py intent(inplace) :: Gyy, Sxx, Syy, Sxy, u, v, rhs, D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy
    !f2py intent(hide) :: n, m
    !f2py intent(in) :: dx, dy, we, sx, ex, sy, ey, ngc, dt

    call divided_diffs_x(Syy, D1x, D2x, D3x, diffx, u, n, m, dx, sx, ex, sy, ey, ngc)
    call divided_diffs_y(Syy, D1y, D2y, D3y, diffy, v, n, m, dx, sx, ex, sy, ey, ngc)

    do j = sy, ey
        do i = sx , ex
            u_center    = ( u(i,j) + u(i-1,j) ) / 2.0d0
            v_center    = ( v(i,j+1) + v(i,j) ) / 2.0d0

            v_dx        = (v(i+1,j)-v(i-1,j)+v(i+1,j+1)-v(i-1,j+1)) / (4.0d0*dx)
            v_dy        = (v(i,j+1)-v(i,j)) / dy
            Sxy_center  = (Sxy(i,j)+Sxy(i-1,j)+Sxy(i-1,j+1)+Sxy(i,j+1)) / 4.0d0
            Syy_dxx     = (Syy(i-1,j)-2.0d0*Syy(i,j)+Syy(i+1,j))/(dx*dx)
            Syy_dyy     = (Syy(i,j-1)-2.0d0*Syy(i,j)+Syy(i,j+1))/(dy*dy)

            uSyy_dx = u_center*diffx(i,j)
            vSyy_dy = v_center*diffy(i,j)
            !uSyy_dx = u_center*(-Syy(i+2,j) + 8.0d0*Syy(i+1,j) -8.0d0*Syy(i-1,j) + Syy(i-2,j))/(12.0d0*dx)
            !vSyy_dy = v_center*(-Syy(i,j+2) + 8.0d0*Syy(i,j+1) -8.0d0*Syy(i,j-1) + Syy(i,j-2))/(12.0d0*dy)
            !uSyy_dx = u_center*(Syy(i+1,j) -Syy(i-1,j))/(2.0d0*dx)
            !vSyy_dy = v_center*(Syy(i,j+1) -Syy(i,j-1))/(2.0d0*dy)

            !Gyy(i,j)    = -(uSyy_dx + vSyy_dy) + 2.0d0*(v_dx*Sxy_center+v_dy*Syy(i,j)) -(1.0/we)*(Syy(i,j)-1.0d0) &
            !              + rhs(i,j)
            Gyy(i,j) = Syy(i,j) + dt*( 2.0d0*(v_dx*Sxy_center+v_dy*Syy(i,j)) + (1.0/we)*(1.0-Syy(i,j)) &
                                       -(uSyy_dx + vSyy_dy) + rhs(i,j) )

        end do
    end do
    call update_ghosts(Gyy,n,m,sx,ex,sy,ey,ngc)
end subroutine advance_operator_syy
'''


advanceOperatorSxy = '''!    -*- f90 -*-
!the act of appplying advance_operator to the stress tensor is returned in the Gxx variable
subroutine advance_operator_sxy(Gxy, Sxx, Syy, Sxy, rhs, D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy, &
                                n, m, u, v, dx, dy, we, sx, ex, sy, ey, ngc, dt)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: Gxy, Sxx, Syy, Sxy, u, v, rhs, &
                                                        D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy
    double precision , intent(in) :: dx, dy, we, dt
    integer , intent(in) :: n , m, sx, ex, sy, ey, ngc
    integer :: i, j
    double precision :: u_node, v_node, Sxy_dx, Sxy_dy, v_dx, u_dy, Sxx_node, Syy_node, Sxy_dxx, Sxy_dyy
    double precision :: uSxy_dx, vSxy_dy
    !f2py threadsafe
    !f2py intent(inplace) :: Gxy, Sxx, Syy, Sxy, u, v, rhs, D1x, D2x, D3x, D1y, D2y, D3y, diffx, diffy
    !f2py intent(hide) :: n, m
    !f2py intent(in) :: dx, dy, we, sx, ex, sy, ey, ngc, dt

    call divided_diffs_x(Sxy, D1x, D2x, D3x, diffx, u, n, m, dx, sx, ex, sy, ey, ngc)
    call divided_diffs_y(Sxy, D1y, D2y, D3y, diffy, v, n, m, dx, sx, ex, sy, ey, ngc)

    do j = sy, ey
        do i = sx , ex
            u_node      = ( u(i,j) + u(i,j-1) ) / 2.0d0
            v_node      = ( v(i,j) + v(i+1,j) ) / 2.0d0

            v_dx        = (v(i+1,j)-v(i,j)) / dx
            u_dy        = (u(i,j)-u(i,j-1)) / dy
            Sxx_node    = (Sxx(i,j)+Sxx(i+1,j)+Sxx(i+1,j-1)+Sxx(i,j-1)) / 4.0d0
            Syy_node    = (Syy(i,j)+Syy(i+1,j)+Syy(i+1,j-1)+Syy(i,j-1)) / 4.0d0
            Sxy_dxx     = (Sxy(i-1,j)-2.0d0*Sxy(i,j)+Sxy(i+1,j))/(dx*dx)
            Sxy_dyy     = (Sxy(i,j-1)-2.0d0*Sxy(i,j)+Sxy(i,j+1))/(dy*dy)

            uSxy_dx = u_node*diffx(i,j)
            vSxy_dy = v_node*diffy(i,j)
            !uSxy_dx = u_node*(-Sxy(i+2,j) + 8.0d0*Sxy(i+1,j) -8.0d0*Sxy(i-1,j) + Sxy(i-2,j))/(12.0d0*dx)
            !vSxy_dy = v_node*(-Sxy(i,j+2) + 8.0d0*Sxy(i,j+1) -8.0d0*Sxy(i,j-1) + Sxy(i,j-2))/(12.0d0*dy)
            !uSxy_dx = u_node*(Sxy(i+1,j) -Sxy(i-1,j))/(2.0d0*dx)
            !vSxy_dy = v_node*(Sxy(i,j+1) -Sxy(i,j-1))/(2.0d0*dy)

            !Gxy(i,j)    = -(uSxy_dx + vSxy_dy) + (v_dx*Sxx_node+u_dy*Syy_node) -(1.0/we)*(Sxy(i,j)) &
            !              + rhs(i,j)
            Gxy(i,j) = Sxy(i,j) + dt*( (v_dx*Sxx_node+u_dy*Syy_node) + (1.0/we)*(-Sxy(i,j)) &
                                       -(uSxy_dx + vSxy_dy) + rhs(i,j) )

        end do
    end do
    call update_ghosts(Gxy,n,m,sx,ex,sy,ey,ngc)
end subroutine advance_operator_sxy
'''

computeDividedDifferencesX = '''
subroutine divided_diffs_x(S, D1x, D2x, D3x, diffx, u, n, m, dx, sx, ex, sy, ey, ngc)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: S, D1x, D2x, D3x, diffx, u
    integer, intent(in) :: n, m, sx, ex, sy, ey, ngc
    double precision, intent(in) :: dx
    integer :: i, j, k, ks
    double precision :: c, cs
    !f2py threadsafe
    !f2py intent(inplace) :: S, D1x, D2x, D3x, diffx, u
    !f2py intent(hide) :: n, m
    !f2py intent(in) :: dx, sx, ex, sy, ey, ngc

    D1x = 0.0d0
    D2x = 0.0d0
    D3x = 0.0d0
    diffx = 0.0d0

    do j = sy, ey
        do i = sx, ex
            D1x(i,j) = (S(i+1,j)-S(i,j))/dx
        end do
    end do

    call update_ghosts(D1x,n,m,sx,ex,sy,ey,ngc)

    do j = sy, ey
        do i = sx, ex
            D2x(i,j) = (D1x(i+1,j)-D1x(i,j))/(2.0*dx)
        end do
    end do

    call update_ghosts(D2x,n,m,sx,ex,sy,ey,ngc)

    do j = sy, ey
        do i = sx, ex
            D3x(i,j) = (D2x(i+1,j)-D2x(i,j))/(3.0*dx)
        end do
    end do

    call update_ghosts(D3x,n,m,sx,ex,sy,ey,ngc)

    do j = sy, ey
        do i = sx, ex
            !decide for upwinding
             if( u(i,j) >= 0.0d0) then
                k=i-1
             else
                k=i
             endif
            !choose smallest second difference
            if (abs(D2x(k,j)) <= abs(D2x(k+1,j))) then
                c=D2x(k,j)
                ks=k-1
            else
                c=D2x(k+1,j)
                ks=k
            endif
            !choose smallest third difference
            If(abs(D3x(ks,j)) <= abs(D3x(ks+1,j))) then
                cs=D3x(ks,j)
            else
                cs=D3x(ks+1,j)
            end if

             diffx(i,j) = D1x(k,j) + c*dble( (2*(i-k)-1) )*dx + &
                           cs*dble((3*((i-ks)**2)-6*(i-ks)+2))*dx*dx
        end do
    end do
end subroutine divided_diffs_x
'''


computeDividedDifferencesY = '''
subroutine divided_diffs_y(S, D1y, D2y, D3y, diffy, v, n, m, dy, sx, ex, sy, ey, ngc)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: S, D1y, D2y, D3y, diffy, v
    integer, intent(in) :: n, m, sx, ex, sy, ey, ngc
    double precision, intent(in) :: dy
    integer :: i, j, k, ks
    double precision :: c, cs
    !f2py threadsafe
    !f2py intent(inplace) :: S, D1y, D2y, D3y, diffy, v
    !f2py intent(hide) :: n, m
    !f2py intent(in) :: dy, sx, ex, sy, ey, ngc

    D1y = 0.0d0
    D2y = 0.0d0
    D3y = 0.0d0
    diffy = 0.0d0

    do j = sy, ey
        do i = sx, ex
            D1y(i,j) = (S(i,j+1)-S(i,j))/dy
        end do
    end do

    call update_ghosts(D1y,n,m,sx,ex,sy,ey,ngc)

    do j = sy, ey
        do i = sx, ex
            D2y(i,j) = (D1y(i,j+1)-D1y(i,j))/(2.0*dy)
        end do
    end do

    call update_ghosts(D2y,n,m,sx,ex,sy,ey,ngc)

    do j = sy, ey
        do i = sx, ex
            D3y(i,j) = (D2y(i,j+1)-D2y(i,j))/(3.0*dy)
        end do
    end do

    call update_ghosts(D3y,n,m,sx,ex,sy,ey,ngc)

    do j = sy, ey
        do i = sx, ex
            !decide for upwinding
             if( v(i,j) >= 0.0d0) then
                k=j-1
             else
                k=j
             endif
            !choose smallest second difference
            if (abs(D2y(i,k)) <= abs(D2y(i,k+1))) then
                c=D2y(i,k)
                ks=k-1
            else
                c=D2y(i,k+1)
                ks=k
            endif
            !choose smallest third difference
            If(abs(D3y(i,ks)) <= abs(D3y(i,ks+1))) then
                cs=D3y(i,ks)
            else
                cs=D3y(i,ks+1)
            end if

             diffy(i,j) = D1y(i,k) + c*dble( (2*(j-k)-1) )*dy + &
                           cs*dble((3*((j-ks)**2)-6*(j-ks)+2))*dy*dy
        end do
    end do
end subroutine divided_diffs_y
'''


codeIbHeader = '''!    -*- f90 -*-
module ib
include "omp_lib.h"
contains
'''

codePolimerHeader = '''!    -*- f90 -*-
module polimer
include "omp_lib.h"
contains
'''

codeIb = codeIbHeader + diracDelta + enforcePeriodicity + spreadForceIB + interpolateVelocityIB + "end module"
codePolimer = codePolimerHeader + updateGhostsCode + spreadForcePolimer
codePolimer += advanceOperatorSxx + advanceOperatorSyy + advanceOperatorSxy
codePolimer += computeDividedDifferencesX + computeDividedDifferencesY + "end module"


def retrieveCode():
    name_of_current_file = realpath(__file__)
    codeInfo = [(codeIb, 'fast_ib', name_of_current_file)]
    codeInfo.append((codePolimer, 'fast_polimer', name_of_current_file))
    return codeInfo

