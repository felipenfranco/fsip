# -*- coding: utf-8 -*-
'''
Created on Mar 8, 2012

@author: felipe
'''
import sys
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')
import utils
from math_objects.cartesian_grid import *
from math_objects.scalar_field import CenterScalarField
from math_objects.vector_field import VectorField
from linear_algebra.linear_system import LinearSystem
from linear_algebra.biconjugate_gradient import bicgstab, TempVarsBicgstab
from math_objects.projection_method import projectionMethod, TempVarsProjection, \
    computeConvection
from polimer import Polimer
import fast_laplace
from time import time
from cPickle import dump, load

class Fluid(object):
    '''
    Abstract class of a Fluid.
    '''


    def __init__(self, grid, visc, dens):
        self.grid = grid
        self.pressure = CenterScalarField(grid)
        self.velocity = VectorField(grid)
        self.visc, self.dens = visc, dens
        self.currentTime = 0.

#        nx, ny = self.grid.nx, self.grid.ny
#        originX, originY = self.grid.origin.getTuple()
#        sizeX, sizeY = self.grid.sizeX, self.grid.sizeY
#        self.nameParameters = [nx, ny, originX, sizeX, originY, sizeY, self.visc,dens]
#        self.fluidName = '_nx%d_ny%d_%g_%g_%g_%g_%g_%g'


    def saveState(self, stateName):
#        fluidName = self.fluidName % tuple((self.nameParameters + [self.currentTime, ]))
        stateName = stateName + ".fluid"
        data = {}
        data['u'] = self.velocity.u.data
        data['v'] = self.velocity.v.data
        data['p'] = self.pressure.data
        data['currentTime'] = self.currentTime

        with open(stateName, 'w') as dataFile:
            dump(data, dataFile)


    def loadState(self, stateName):
#        fluidName = self.fluidName % tuple((self.nameParameters + [currentTime, ]))
        with open(stateName, 'r') as dataFile:
            data = load(dataFile)

        self.velocity.u.data = data['u']
        self.velocity.v.data = data['v']
        self.pressure.data = data['p']
        self.currentTime = data['currentTime']


    def advanceInTime(self, dt):
        raise NotImplementedError("Only the derived classes must have this implemented.")


    def initializeRhs(self, fRhsU, fRhsV, t = 0.):
        self.systemU.rhs.initialize(fRhsU, t)
        self.systemV.rhs.initialize(fRhsV, t)


    def clearRhs(self):
        self.systemU.rhs[:] = 0.
        self.systemV.rhs[:] = 0.


    def clear(self):
        self.velocity.u[:] = 0.
        self.velocity.v[:] = 0.
        self.pressure[:] = 0.
        self.clearRhs()


    def getVorticity(self):
        u, v = self.velocity.u[:], self.velocity.v[:]
        sx, ex, sy, ey = self.grid.getBounds()
        dx, dy = self.grid.getSpacing()
        u_dy = (u[sx:ex, sy:ey] - u[sx:ex, (sy - 1):(ey - 1)]) / dy
        v_dx = (v[(sx + 1):(ex + 1), sy:ey] - v[sx:ex, sy:ey]) / dx
        vorticity = self.velocity.u.generateNewScalarField(self.grid)
        vorticity[sx:ex, sy:ey] = u_dy - v_dx
        return vorticity
    
    def dissipation(self):
        u = self.velocity.u.data
        v = self.velocity.v.data
        sx, ex, sy, ey = self.grid.getBounds()
        dx, dy = self.grid.getSpacing()
        u_dy = (u[sx:ex, sy:ey] - u[sx:ex, sy - 1:ey - 1]) / dy
        v_dx = (v[sx + 1:ex + 1, sy:ey] - v[sx:ex, sy:ey]) / dx
        
        u_dx = (u[sx:ex, sy:ey] - u[sx - 1:ex - 1, sy:ey]) / dx
        v_dy = (v[sx:ex, sy + 1:ey + 1] - v[sx:ex, sy:ey]) / dy
        
        temp = ((u_dy + v_dx) ** 2).flatten() 
        return self.visc * temp.sum() * dx * dy


class StokesFluid(Fluid):
    '''
    0 = -grad(p) + visc*Laplacian(u) + force
    '''

    def __init__(self, grid, visc = 1.):
        dens = 0.
        Fluid.__init__(self, grid, visc, dens)
        updateFunction = fast_laplace.laplace.laplace_update_function_stokes
        residualFunction = fast_laplace.laplace.laplace_residual_function_stokes

        self.systemP = LinearSystem(updateFunction, residualFunction, self.pressure)
        self.systemU = LinearSystem(updateFunction, residualFunction, self.velocity.u)
        self.systemV = LinearSystem(updateFunction, residualFunction, self.velocity.v)
        self.tempVars = TempVarsBicgstab() #for the biconjugate gradient
        self.tempVars.initialize(self)
        self.ID = "StokesFluid"

    def advanceInTime(self, dt):
        self.currentTime += dt
        bicgstab(self, self.visc, self.dens, dt)



class PolimericStokesFluid(StokesFluid):
    '''
    0 = -grad(p) + visc*Laplacian(u) + force + beta*div(S)
    '''
    def __init__(self, grid, visc, beta, de):
        '''
            de = Weissenberg number
        '''
        self.beta = beta
        self.de = de
        StokesFluid.__init__(self, grid, visc)
        self.polimer = Polimer(grid, beta, de)
        self.polimer.initialize()
        self.ID = "PolimericStokesFluid"

    def advanceInTime(self, dt):
        self.currentTime += dt
        self.polimer.spreadForce(self.systemU.rhs, self.systemV.rhs)
        bicgstab(self, self.visc, self.dens, dt)
        self.polimer.advanceInTime(self.velocity, dt)

    def saveState(self, stateName):
#        fluidName = self.fluidName % tuple((self.nameParameters + [self.currentTime, self.beta, self.de]))
        stateName = stateName + ".fluid"
        data = {}
        data['u'] = self.velocity.u.data
        data['v'] = self.velocity.v.data
        data['p'] = self.pressure.data
        data['currentTime'] = self.currentTime
        data['Sxx'] = self.polimer.Sxx.data
        data['Sxy'] = self.polimer.Sxy.data
        data['Syy'] = self.polimer.Syy.data
        data['x'] = self.grid.getX()
        data['y'] = self.grid.getY()

        with open(stateName, 'w') as dataFile:
            dump(data, dataFile)


    def loadState(self, stateName):
#        fluidName = self.fluidName % tuple((self.nameParameters + [currentTime, self.beta, self.de]))
        with open(stateName, 'r') as dataFile:
            data = load(dataFile)

        self.velocity.u.data = data['u']
        self.velocity.v.data = data['v']
        self.pressure.data = data['p']
        self.currentTime = data['currentTime']
        self.polimer.Sxx.data = data['Sxx']
        self.polimer.Sxy.data = data['Sxy']
        self.polimer.Syy.data = data['Syy']
        self.polimer.started = True


class NavierStokesFluid(Fluid):
    '''
    dens*(du/dt + (u.nabla)u) = -grad(p) + visc*Laplacian(u) + force
    '''

    def __init__(self, grid, visc = 1., dens = 1.):
        Fluid.__init__(self, grid, visc, dens)

        updateFunctionVelocity = fast_laplace.laplace.laplace_update_function_ns
        residualFunctionVelocity = fast_laplace.laplace.laplace_residual_function_ns

        updateFunctionPressure = fast_laplace.laplace.laplace_update_function_stokes
        residualFunctionPressure = fast_laplace.laplace.laplace_residual_function_stokes

        self.systemP = LinearSystem(updateFunctionPressure, residualFunctionPressure, self.pressure)
        self.systemU = LinearSystem(updateFunctionVelocity, residualFunctionVelocity, self.velocity.u)
        self.systemV = LinearSystem(updateFunctionVelocity, residualFunctionVelocity, self.velocity.v)
        self.tempVars = TempVarsProjection()
        self.tempVars.initialize(self)
        self.ID = "NavierStokesFluid"

    def advanceInTime(self, dt):
        self.currentTime += dt
        projectionMethod(self, dt)
        
    def CFL(self, dt):
        dx, dy = self.grid.getSpacing()
        u, v = self.velocity.u.data, self.velocity.v.data
        cfl = 0.5 / (abs(u).max() / dx + abs(v).max() / dy)
        if cfl > dt:
            return dt
        return cfl
    
    def getConvection(self):
        convectionU = self.tempVars.convectionU
        convectionV = self.tempVars.convectionV
        computeConvection(self.velocity.u, self.velocity.v, convectionU, convectionV)
        
        return convectionU.data.copy(), convectionV.data.copy()

class PolimericNavierStokesFluid(NavierStokesFluid):
    '''
    dens*(du/dt + (u.nabla)u) = -grad(p) + visc*Laplacian(u) + force + beta*div(S)
    '''
    def __init__(self, grid, visc, dens, beta, de):
        self.beta = beta
        self.de = de
        NavierStokesFluid.__init__(self, grid, visc, dens)
        self.polimer = Polimer(grid, beta, de)
        self.polimer.initialize()
        self.ID = "PolimericNavierStokesFluid"

    def advanceInTime(self, dt):
        self.currentTime += dt
        self.polimer.spreadForce(self.systemU.rhs, self.systemV.rhs)
        projectionMethod(self, dt)
        self.polimer.advanceInTime(self.velocity, dt)
        
    def saveState(self, stateName):
        stateName = stateName + ".fluid"
        data = {}
        data['u'] = self.velocity.u.data
        data['v'] = self.velocity.v.data
        data['p'] = self.pressure.data
        data['currentTime'] = self.currentTime
        data['Sxx'] = self.polimer.Sxx.data
        data['Sxy'] = self.polimer.Sxy.data
        data['Syy'] = self.polimer.Syy.data

        with open(stateName, 'w') as dataFile:
            dump(data, dataFile)


    def loadState(self, stateName):
        with open(stateName, 'r') as dataFile:
            data = load(dataFile)

        self.velocity.u.data = data['u']
        self.velocity.v.data = data['v']
        self.pressure.data = data['p']
        self.currentTime = data['currentTime']
        self.polimer.Sxx.data = data['Sxx']
        self.polimer.Sxy.data = data['Sxy']
        self.polimer.Syy.data = data['Syy']
        
        

if __name__ == '__main__':
    from sympy import var, sin, cos, lambdify, diff, exp
    import numpy
    import pylab
    pi = numpy.pi
    norm = numpy.linalg.norm
    inf = numpy.inf
    
    visc = 1.
    dens = 1.
    
#===================================================================================================
# Stokes Test
#===================================================================================================
    print "\n\n\n\n\n\n"
    print "Stokes Test"
    lastu = 0.
    lastv = 0.
    lastp = 0.
    for n in [512, ]:  # (16, 32, 64, 128, 256, 512, 1024):
        visc = 1.0
        print "(%d x %d)" % (n, n)
        grid = Grid(Point(0., 0.), 1., 1., n, n, 2)
        myFluid = StokesFluid(grid, visc)
        exactFluid = StokesFluid(grid, visc)

        x, y = var('x y')

        p = cos(2 * pi * (x + y))
        u = sin(2 * pi * (x + y)) ** 2
        v = cos(2 * pi * (x + y)) ** 2

        fu = lambdify([x, y] , p.diff(x) - visc * (u.diff(x, x) + u.diff(y, y)) , numpy)
        fv = lambdify([x, y] , p.diff(y) - visc * (v.diff(x, x) + v.diff(y, y)) , numpy)

        u = lambdify([x, y], u, numpy)
        v = lambdify([x, y], v, numpy)
        p = lambdify([x, y], p, numpy)

        exactFluid.velocity.initialize(u, v)
        exactFluid.pressure.initialize(p)


        while myFluid.currentTime < 1.0:
            print myFluid.currentTime
            myFluid.initializeRhs(fu, fv)
            myFluid.advanceInTime(0.1)

        sx, ex, sy, ey = grid.getBounds()

        # the solutions are given up to a constant
        # we find the constant and subtract to obtain the true error

        constantU = myFluid.velocity.u.data.max() - exactFluid.velocity.u.data.max()
        constantV = myFluid.velocity.v.data.max() - exactFluid.velocity.v.data.max()
        constantP = myFluid.pressure.data.max() - exactFluid.pressure.data.max()

        currentNormU = norm((myFluid.velocity.u[sx:ex, sy:ey] - exactFluid.velocity.u[sx:ex, sy:ey] - constantU).flatten(), inf)
        currentNormV = norm((myFluid.velocity.v[sx:ex, sy:ey] - exactFluid.velocity.v[sx:ex, sy:ey] - constantV).flatten(), inf)
        currentNormP = norm((myFluid.pressure[sx:ex, sy:ey] - exactFluid.pressure[sx:ex, sy:ey] - constantP).flatten(), inf)
        ratioU = lastu / currentNormU
        ratioV = lastv / currentNormV
        ratioP = lastp / currentNormP
        lastu, lastv, lastp = currentNormU, currentNormV, currentNormP

        print "U (max norm, ratio):(%g,\t%g)" % (currentNormU, ratioU)
        print "V (max norm, ratio):(%g,\t%g)" % (currentNormV, ratioV)
        print "P (max norm, ratio):(%g,\t%g)" % (currentNormP, ratioP)


#===================================================================================================
# PolimericStokes Test
#===================================================================================================
#     for n in [128]:
#         for de in [.6]:
#             print "(%d x %d)" % (n, n)
#             grid = Grid(Point(0., 0.), 2 * pi, 2 * pi, n, n, 2)
#             beta = .5 / de
#             myFluid = PolimericStokesFluid(grid, visc, beta, de)
#             dt = myFluid.grid.dx
#
#             x, y = var('x y')
#
#             u = -sin(x) * cos(y)
#             v = cos(x) * sin(y)
#
#             fu = lambdify([x, y] , -2.*sin(x) * cos(y) , numpy)
#             fv = lambdify([x, y] , 2.*cos(x) * sin(y) , numpy)
#
#             u = lambdify([x, y], u, numpy)
#             v = lambdify([x, y], v, numpy)
#
#             while myFluid.currentTime < 6.:
#                 print "\nCurrent Time:", myFluid.currentTime
#                 myFluid.initializeRhs(fu, fv)
#                 myFluid.advanceInTime(dt)
#
#
# 	    myFluid.saveState("teste_catalina")
#             raw_input()
#             #pylab.ioff()
#
# #            myFluid.velocity.initialize(u, v, 0)
# #            pylab.contourf(grid.getX(), grid.getY(), myFluid.getVorticity().data.T, 30)
# #            pylab.colorbar()
# #            pylab.show()
#
#             pylab.contourf(myFluid.polimer.getTrace().T, 20)
#             pylab.colorbar()
#             pylab.show()
#
#             pylab.contourf(myFluid.polimer.Sxy.data.T, 20)
#             pylab.colorbar()
#             pylab.show()
#
#             myFluid.clearRhs()
#             rhsU = myFluid.systemU.rhs
#             rhsV = myFluid.systemV.rhs
#             myFluid.polimer.spreadForce(rhsU, rhsV)
#             pylab.contourf(rhsU.data.T / beta, 20)
#             pylab.colorbar()
#             pylab.show()
#===================================================================================================
# Navier Stokes Test
#===================================================================================================
#     for dtsqrd in [False]:
#         lastu = 0.
#         lastv = 0.
#         lastp = 0.
#         dens = 1.0
#         visc = 1.0
#
#         print "\n\n\n\n\n\n"
#         print "Navier Stokes Test, dtsqrd=", dtsqrd
#         for n in [16, 32, 64, 128, 256, 512, 1024]:
#             sys.stdout.flush()
#             print "(%d x %d)" % (n, n)
#             grid = Grid(Point(0., 0.), 1. , 1., n, n, 2)
#             if dtsqrd:
#                 dt = grid.dx ** 2
#                 FinalTime = 5.0
#             else:
#                 dt = grid.dx
#                 FinalTime = 5.0
#
#             myFluid = NavierStokesFluid(grid, visc, dens)
#             exactFluid = NavierStokesFluid(grid, visc, dens)
#
#             x, y, t = var('x y t')
#
#             p = cos(2 * pi * (x + y))
#             u = sin(2 * pi * (x + y) + t) ** 2
#             v = cos(2 * pi * (x + y) + t) ** 2
#
#             fu = lambdify([x, y, t] , dens * (u.diff(t) + (u * u.diff(x) + v * u.diff(y))) + p.diff(x) - (visc) * (u.diff(x, x) + u.diff(y, y)) , numpy)
#             fv = lambdify([x, y, t] , dens * (v.diff(t) + (u * v.diff(x) + v * v.diff(y))) + p.diff(y) - (visc) * (v.diff(x, x) + v.diff(y, y)) , numpy)
#
#             u = lambdify([x, y, t], u, numpy)
#             v = lambdify([x, y, t], v, numpy)
#             p = lambdify([x, y, t], p, numpy)
#
#             myFluid.velocity.initialize(u, v, 0.)
#             myFluid.pressure.initialize(p, 0.)
#
#             while myFluid.currentTime < FinalTime:
#                 cfl_dt = myFluid.CFL(dt)
#                 if FinalTime - myFluid.currentTime < cfl_dt:
#                     cfl_dt = FinalTime - myFluid.currentTime
#                 myFluid.initializeRhs(fu, fv, myFluid.currentTime + cfl_dt)
#                 myFluid.advanceInTime(cfl_dt)
#
#             print "Final Time:", myFluid.currentTime
#
#             exactFluid.velocity.initialize(u, v, myFluid.currentTime)
#             exactFluid.pressure.initialize(p, myFluid.currentTime)
#             sx, ex, sy, ey = grid.getBounds()
# #            pressureConst = (myFluid.pressure[sx:ex, sy:ey]).max() / (exactFluid.pressure[sx:ex, sy:ey]).max()
#             dx, dy = grid.getSpacing()
#             currentNormU = norm((myFluid.velocity.u[sx:ex, sy:ey] - exactFluid.velocity.u[sx:ex, sy:ey]).flatten() * sqrt(dx * dy), 2)
#             currentNormV = norm((myFluid.velocity.v[sx:ex, sy:ey] - exactFluid.velocity.v[sx:ex, sy:ey]).flatten() * sqrt(dx * dy), 2)
#             currentNormP = norm((myFluid.pressure[sx:ex, sy:ey] - exactFluid.pressure[sx:ex, sy:ey]).flatten() * sqrt(dx * dy), 2)
#             ratioU = lastu / currentNormU
#             ratioV = lastv / currentNormV
#             ratioP = lastp / currentNormP
#             lastu, lastv, lastp = currentNormU, currentNormV, currentNormP
#
#             print "U (max norm, ratio):(%g,\t%g)" % (currentNormU, ratioU)
#             print "V (max norm, ratio):(%g,\t%g)" % (currentNormV, ratioV)
#             print "P (max norm, ratio):(%g,\t%g)" % (currentNormP, ratioP)
#
#             print "Inf norm U:", norm((myFluid.velocity.u[sx:ex, sy:ey] - exactFluid.velocity.u[sx:ex, sy:ey]).flatten(), inf)
#             print "Inf norm V:", norm((myFluid.velocity.v[sx:ex, sy:ey] - exactFluid.velocity.v[sx:ex, sy:ey]).flatten(), inf)
#             print "Inf norm P:", norm((myFluid.pressure[sx:ex, sy:ey] - exactFluid.pressure[sx:ex, sy:ey]).flatten(), inf)
#===================================================================================================
# PolimericNavierStokes Test
#===================================================================================================
#    for n in [64]:
#        print "(%d x %d)" % (n, n)
#        grid = Grid(Point(0., 0.), 2 * pi, 2 * pi, n, n, 2)
#        de = 5.
#        beta = .5 / de
#        myFluid = PolimericNavierStokesFluid(grid, visc, dens, beta, de)
#        dt = myFluid.grid.dx * .1
#
#        x, y = var('x y')
#
#        u = sin(x) * cos(y)
#        v = -cos(x) * sin(y)
#
#        fu = lambdify([x, y] , 2.*sin(x) * cos(y) , numpy)
#        fv = lambdify([x, y] , -2.*cos(x) * sin(y) , numpy)
#
#        u = lambdify([x, y], u, numpy)
#        v = lambdify([x, y], v, numpy)
#        
#        pylab.ion()
#        while myFluid.currentTime < 6.:
#            print "\nCurrent Time:", myFluid.currentTime
#            t = time()
#            myFluid.initializeRhs(fu, fv)
#            myFluid.advanceInTime(dt)
#            print "Time to Compute:", time() - t
#            
#            print "Max Sxy:", numpy.abs(myFluid.polimer.Sxy.data).max()
#            print "Max trace:" , numpy.abs(myFluid.polimer.getTrace()).max()
#
#        pylab.ioff()            
#        pylab.contourf(myFluid.polimer.getTrace().T)
#        pylab.colorbar()
#        pylab.show()
#        pylab.contourf(myFluid.polimer.Sxy.data.T)
#        pylab.colorbar()
#        pylab.show()
