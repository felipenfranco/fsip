# -*- coding: utf-8 -*-
'''
Created on Mar 11, 2012

@author: felipe
'''
import utils
from math_objects.scalar_field import CenterScalarField, NodeScalarField, ScalarField
import physic_objects
import fast_polimer
import numpy as np
from matplotlib.patches import Ellipse


import math

def dotproduct(v1, v2):
    return sum((a * b) for a, b in zip(v1, v2))

def norm2(v):
    return math.sqrt(dotproduct(v, v))

def angle(v1, v2):
    return math.acos(dotproduct(v1, v2) / (norm2(v1) * norm2(v2)))
  

class Polimer(object):
    '''        
    classdocs
    '''


    def __init__(self, grid, beta, de):
        '''
            de = Deborah number
        '''
        self.grid = grid
        self.beta = beta
        self.de = de

        self.Sxx = CenterScalarField(grid)
        self.Syy = CenterScalarField(grid)
        self.Sxy = NodeScalarField(grid)

        self.Gxx = CenterScalarField(grid)
        self.Gyy = CenterScalarField(grid)
        self.Gxy = NodeScalarField(grid)

        self.Txx = CenterScalarField(grid)
        self.Tyy = CenterScalarField(grid)
        self.Txy = NodeScalarField(grid)

        self.rhsSxx = CenterScalarField(grid)
        self.rhsSyy = CenterScalarField(grid)
        self.rhsSxy = NodeScalarField(grid)

        self.D1x = CenterScalarField(grid)
        self.D2x = CenterScalarField(grid)
        self.D3x = CenterScalarField(grid)
        self.diffX = CenterScalarField(grid)

        self.D1y = CenterScalarField(grid)
        self.D2y = CenterScalarField(grid)
        self.D3y = CenterScalarField(grid)
        self.diffY = CenterScalarField(grid)

        self.ID = "Polimer"
        self.started = False

    def copyTo(self, newPolimer):
        newPolimer.Sxx.data = np.array(self.Sxx.data, order = 'F')
        newPolimer.Syy.data = np.array(self.Syy.data, order = 'F')
        newPolimer.Sxy.data = np.array(self.Sxy.data, order = 'F')

    def initialize(self, fxx = lambda x, y: 1., fyy = lambda x, y: 1., fxy = lambda x, y: 0.):
        pairs = [(self.Sxx, fxx), (self.Syy, fyy), (self.Sxy, fxy)]
        for pair in pairs:
            scalarField , function = pair
            x, y = scalarField.getX(), scalarField.getY()
            for j in range(scalarField.dimy):
                scalarField.data[:, j] = function(x[:], y[j])
            scalarField.updateGhosts()

    def spreadForce(self, rhsU, rhsV):
        Sxx, Syy, Sxy = self.Sxx.data, self.Syy.data, self.Sxy.data
        urhs, vrhs = rhsU.data, rhsV.data
        dx, dy = self.grid.getSpacing()
        sx, ex, sy, ey = self.grid.getBounds(Fortran = True)
        ngc = self.grid.ngc
        fast_polimer.polimer.spread_force_polimer(Sxx, Syy, Sxy, urhs, vrhs, dx, dy, self.beta, sx, ex, sy, ey, ngc)

    def advanceOperatorSxx(self, velocity, dt, xx, yy, xy, Gxx):
        '''The act of appplying advance_operator to the stress tensor (xx,yy,xy) is returned in the Gxx array'''
        rhsSxx = self.rhsSxx.data
        u, v = velocity.u.data, velocity.v.data
        dx, dy = self.grid.getSpacing()
        sx, ex, sy, ey = self.grid.getBounds(Fortran = True)
        ngc = self.grid.ngc
        we = self.de
        D1x, D2x, D3x = self.D1x.data, self.D2x.data, self.D3x.data
        D1y, D2y, D3y = self.D1y.data, self.D2y.data, self.D3y.data
        diffx, diffy = self.diffX.data, self.diffY.data
        fast_polimer.polimer.advance_operator_sxx(Gxx, xx, yy, xy, rhsSxx, D1x, D2x, D3x, D1y, D2y,
                                                  D3y, diffx, diffy, u, v, dx, dy, we, sx, ex, sy, ey, ngc, dt)

    def advanceOperatorSyy(self, velocity, dt, xx, yy, xy, Gyy):
        '''The act of appplying advance_operator to the stress tensor is returned in the Gyy array'''
        rhsSyy = self.rhsSyy.data
        u, v = velocity.u.data, velocity.v.data
        dx, dy = self.grid.getSpacing()
        sx, ex, sy, ey = self.grid.getBounds(Fortran = True)
        ngc = self.grid.ngc
        we = self.de
        D1x, D2x, D3x = self.D1x.data, self.D2x.data, self.D3x.data
        D1y, D2y, D3y = self.D1y.data, self.D2y.data, self.D3y.data
        diffx, diffy = self.diffX.data, self.diffY.data
        fast_polimer.polimer.advance_operator_syy(Gyy, xx, yy, xy, rhsSyy, D1x, D2x, D3x, D1y, D2y,
                                                  D3y, diffx, diffy, u, v, dx, dy, we, sx, ex, sy, ey, ngc, dt)

    def advanceOperatorSxy(self, velocity, dt, xx, yy, xy, Gxy):
        '''The act of appplying advance_operator to the stress tensor is returned in the Gxy array'''
        rhsSxy = self.rhsSxy.data
        u, v = velocity.u.data, velocity.v.data
        dx, dy = self.grid.getSpacing()
        sx, ex, sy, ey = self.grid.getBounds(Fortran = True)
        ngc = self.grid.ngc
        we = self.de
        D1x, D2x, D3x = self.D1x.data, self.D2x.data, self.D3x.data
        D1y, D2y, D3y = self.D1y.data, self.D2y.data, self.D3y.data
        diffx, diffy = self.diffX.data, self.diffY.data
        fast_polimer.polimer.advance_operator_sxy(Gxy, xx, yy, xy, rhsSxy, D1x, D2x, D3x, D1y, D2y,
                                                  D3y, diffx, diffy, u, v, dx, dy, we, sx, ex, sy, ey, ngc, dt)

    def rungeKutta2(self, velocity, dt):
        Sxx, Syy, Sxy = self.Sxx, self.Syy, self.Sxy
        Gxx, Gyy, Gxy = self.Gxx, self.Gyy, self.Gxy
        Txx, Tyy, Txy = self.Txx, self.Tyy, self.Txy

        Sxx.updateGhosts()
        Syy.updateGhosts()
        Sxy.updateGhosts()

        self.advanceOperatorSxx(velocity, dt, Sxx.data, Syy.data, Sxy.data, Gxx.data)
        self.advanceOperatorSyy(velocity, dt, Sxx.data, Syy.data, Sxy.data, Gyy.data)
        self.advanceOperatorSxy(velocity, dt, Sxx.data, Syy.data, Sxy.data, Gxy.data)

        self.advanceOperatorSxx(velocity, dt, Gxx.data, Gyy.data, Gxy.data, Txx.data)
        self.advanceOperatorSyy(velocity, dt, Gxx.data, Gyy.data, Gxy.data, Tyy.data)
        self.advanceOperatorSxy(velocity, dt, Gxx.data, Gyy.data, Gxy.data, Txy.data)

        Sxx.data = (Sxx + Txx) / 2.0
        Syy.data = (Syy + Tyy) / 2.0
        Sxy.data = (Sxy + Txy) / 2.0

        Sxx.updateGhosts()
        Syy.updateGhosts()
        Sxy.updateGhosts()
        self.started = True

    def advanceInTime(self, velocity, dt):
        self.rungeKutta2(velocity, dt)

    def getTrace(self):
        return self.Sxx.data + self.Syy.data

    def getDivU(self):
        Sxx , Sxy = self.Sxx.data, self.Sxy.data
        sx, ex, sy, ey = self.grid.getBounds()
        dx, dy = self.grid.getSpacing()
        divx = (Sxx[(sx + 1):(ex + 1), sy:ey] - Sxx[sx:ex, sy:ey]) / dx
        divy = (Sxy[sx:ex, (sy + 1):(ey + 1)] - Sxy[sx:ex, sy:ey]) / dy
        return divx + divy

    def getDivV(self):
        Syy , Sxy = self.Syy.data, self.Sxy.data
        sx, ex, sy, ey = self.grid.getBounds()
        dx, dy = self.grid.getSpacing()
        divx = (Sxy[(sx + 1):(ex + 1), sy:ey] - Sxy[sx:ex, sy:ey]) / dx
        divy = (Syy[sx:ex, sy:ey] - Syy[sx:ex, (sy - 1):(ey - 1)]) / dy
        return divx + divy

    def getEllipses(self):
        sx, ex, sy, ey = self.grid.getBounds()
        x , y = self.grid.getX(), self.grid.getY()
        ells = []
        print "Computing ellipses..."
        for i in range(sx, ex):
            for j in range(sy, ey):
                sxx = self.Sxx[i, j]
                syy = self.Syy[i, j]
                sxy = self.Sxy[i, j]
                matrix = np.array([[sxx, sxy], [sxy, syy]])
                e, v = np.linalg.eig(matrix)
                v[:, 0] = v[:, 0] / norm2(v[:, 0])
                v[:, 1] = v[:, 1] / norm2(v[:, 1])

                if e[0] > e[1]:
                    bige = e[0]
                    bigv = v[:, 0]
                    smalle = e[1]
                    smallv = v[:, 1]
                else:
                    bige = e[1]
                    bigv = v[:, 1]
                    smalle = e[0]
                    smallv = v[:, 0]
                
                smalle *= self.grid.dx * .1
                bige *= self.grid.dx * .1
                
                xy = [x[i], y[j]]
                width = bige
                height = smalle
                ang = angle([1, 0], bigv)
                
                ells.append(Ellipse(xy, width, height, ang, fill = False))
        print "Done!"
        return ells

if __name__ == '__main__':
    from sympy import var, sin, cos, lambdify, diff, exp
    import numpy, sympy
    from numpy.linalg import norm
    from numpy import inf
    from math_objects.cartesian_grid import *
    from fluid import PolimericStokesFluid
    from time import sleep

    pi = numpy.pi
    norm = numpy.linalg.norm
    inf = numpy.inf

    de = 1.0
    x , y, t = var('x y t')
    u = sin(x) * cos(y)
    v = cos(x + y) * sin(x - y)
    Sxx = (t ** 2) * cos(x) * sin(y)
    Syy = (t ** 2) * sin(x * y) * cos(y)
    Sxy = (t ** 2) * cos(x) * sin(x + y)

    rhsSxx = Sxx.diff(t) + u * Sxx.diff(x) + v * Sxx.diff(y) - 2.*(u.diff(x) * Sxx + u.diff(y) * Sxy) + (1. / de) * (Sxx - 1.)
    rhsSyy = Syy.diff(t) + u * Syy.diff(x) + v * Syy.diff(y) - 2.*(v.diff(x) * Sxy + v.diff(y) * Syy) + (1. / de) * (Syy - 1.)
    rhsSxy = Sxy.diff(t) + u * Sxy.diff(x) + v * Sxy.diff(y) - v.diff(x) * Sxx - u.diff(y) * Syy + (1. / de) * (Syy)

    rhsSxx = lambdify([x, y, t], rhsSxx, numpy)
    rhsSyy = lambdify([x, y, t], rhsSyy, numpy)
    rhsSxy = lambdify([x, y, t], rhsSxy, numpy)
    u = lambdify([x, y, t], u, numpy)
    v = lambdify([x, y, t], v, numpy)
    Sxx = lambdify([x, y, t], Sxx, numpy)
    Syy = lambdify([x, y, t], Syy, numpy)
    Sxy = lambdify([x, y, t], Sxy, numpy)

    oldNorm = [0., 0., 0.]
    for n in [2 ** i for i in range(6, 9)]:
        grid = Grid(Point(0., 0.), 2 * pi, 2 * pi, n, n, 2)
        myFluid = PolimericStokesFluid(grid, 1., 0.5, de)
        myFluid.polimer.initialize(lambda x, y: 0., lambda x, y: 0., lambda x, y: 0.)
        myFluid.velocity.initialize(u, v)
        dt = myFluid.grid.dx ** 2
        finalTime = 2 * dt

        currentTime = 0.
        exactPolimer = Polimer(grid, 0.5, de)
        while (currentTime - finalTime) < 10e-10:
            currentTime += dt
            myFluid.polimer.rhsSxx.initialize(rhsSxx, t = currentTime)
            myFluid.polimer.rhsSyy.initialize(rhsSyy, t = currentTime)
            myFluid.polimer.rhsSxy.initialize(rhsSxy, t = currentTime)
            myFluid.polimer.advanceInTime(myFluid.velocity, dt)

            exactPolimer.Sxx.initialize(Sxx, t = currentTime)
            exactPolimer.Syy.initialize(Syy, t = currentTime)
            exactPolimer.Sxy.initialize(Sxy, t = currentTime)

            print norm((exactPolimer.Syy.data - myFluid.polimer.Syy.data).flatten(), inf)





        sx, ex, sy, ey = myFluid.grid.getBounds()
        sxxNorm = norm((exactPolimer.Sxx - myFluid.polimer.Sxx)[sx:ex, sy:ey].flatten(), inf)
        syyNorm = norm((exactPolimer.Syy - myFluid.polimer.Syy)[sx:ex, sy:ey].flatten(), inf)
        sxyNorm = norm((exactPolimer.Sxy - myFluid.polimer.Sxy)[sx:ex, sy:ey].flatten(), inf)
        print "Sxx (norm, ratio) = (%g,%g)" % (sxxNorm, oldNorm[0] / sxxNorm)
        print "Syy (norm, ratio) = (%g,%g)" % (syyNorm, oldNorm[1] / syyNorm)
        print "Sxy (norm, ratio) = (%g,%g) \n\n" % (sxyNorm, oldNorm[2] / sxyNorm)
        oldNorm = [sxxNorm, syyNorm, sxyNorm]

