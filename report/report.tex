\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Re=0 Infinite Sheet}
Infinite sheet, Stokes equation. The sheet is of the form:
\begin{equation}
y(x) = b sin(\kappa x + \omega t).
\end{equation}

Taylor \cite{taylor} predicted that the mean swim velocity of the sheet would be
\begin{equation}
U(b,\kappa,\omega) = \frac{\omega}{\kappa} \frac{b^2 \kappa^2(1-\frac{19}{16}b^2 k^2)}{2},
\end{equation}
and that, if looked from the frame of reference moving with the sheet, material points
of the sheet would move in paths like 8 figures.

Parameters for the numerical simmulations:
\newline \newline
\begin{center}
\begin{tabular}{c|c}
\hline
$\Omega$ & $[0, 1]^2$ \\
\hline
$\Delta s$ & $0.5 \Delta x$ \\ 
\hline 
$\Delta t$ & $\Delta s$ \\ 
\hline 
S1 & $\frac{10^6}{\Delta s}$ \\ 
\hline 
S2 & $10^4\Delta s$ \\ 
\hline 
$\kappa$ & $2\pi$ \\ 
\hline 
$\omega$ & $2\pi$ \\ 
\hline 
Final Time & 1 \\
\hline
Amplitudes & 0.0005, 0.001, 0.002, 0.004,  \\ 
& 0.008, 0.016, 0.032, 0.064 \\
\hline 
\end{tabular} 
\end{center}

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{taylor.png}
Horizontal axis: amplitudes,
Vertical axis: mean velocity over a period
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{last_point_8.png}
Path of the last point of the sheet, in the frame of reference moving with the sheet.
Amplitude=0.016, 2 periods of motion.
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Re=2.5 Infinite Sheet}
Infinite sheet, Navier Stokes equations. The sheet is of the form:
\begin{equation}
y(x) = b sin(\kappa x + \omega t).
\end{equation}

The Reynolds number is given by:
\begin{equation}
Re = \frac{\rho \omega}{\mu \kappa^2}.
\end{equation}

Tuck \cite{tuck} predicted that the mean swim velocity of the sheet would be
\begin{equation}
U(b,\kappa,\omega,Re) = \frac{\omega b^2 \kappa^2 \left( 1+\frac{1}{F[Re]} \right)}{4\kappa},
\end{equation}
where
\begin{equation}
F[Re] = \sqrt{0.5 \left( 1+\sqrt{1+Re^2} \right)}
\end{equation}

Parameters for the numerical simmulations:
\newline \newline
\begin{center}
\begin{tabular}{c|c}
\hline
$\Omega$ & $[0, 1]^2$ \\
\hline
$\mu$ & $\frac{1}{5 \pi}$ \\
\hline
$\rho$ & 1 \\
\hline
$\Delta s$ & $0.5 \Delta x$ \\ 
\hline 
$\Delta t$ & $0.5 \Delta s$ \\ 
\hline 
S1 & $\frac{10^6}{\Delta s}$ \\ 
\hline 
S2 & $10^4\Delta s$ \\ 
\hline 
$\kappa$ & $2\pi$ \\ 
\hline 
$\omega$ & $2\pi$ \\ 
\hline 
Final Time & 2 \\
\hline
Amplitudes & 0.0005, 0.001, 0.002, 0.004,  \\ 
& 0.008, 0.016, 0.032, 0.064 \\
\hline 
\end{tabular} 
\end{center}

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{tuck.png}
Horizontal axis: amplitudes,
Vertical axis: mean velocity over a period
\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Re=0 Infinite Sheet with Walls}
Infinite sheet, Stokes equations. The sheet is of the form:
\begin{equation}
y(x) = b sin(\kappa x + \omega t).
\end{equation}

Katz \cite{katz} predicts that for an infinite sheet, inside a channel and with small amplitudes b,
the mean velocity is given by:

\begin{equation}
U(\omega,\kappa,b,h) = \frac{3 \omega}{\kappa} \left[ \left( \frac{h}{b} \right)^2 +2 \right],
\end{equation}
where $2h$ is the height of the channel.

Parameters for the numerical simmulations:
\newline \newline
\begin{center}
\begin{tabular}{c|c}
\hline
$\Omega$ & $[0, 1]^2$ \\
\hline
$\Delta s$ & $0.5 \Delta x$ \\ 
\hline 
$\Delta t$ & $0.5 \Delta s$ \\ 
\hline 
S1 & $\frac{10^6}{\Delta s}$ \\ 
\hline 
S2 & $10^4\Delta s$ \\ 
\hline
Ellastic constant of the channel walls & $10^8$ \\
\hline 
$\kappa$ & $2\pi$ \\ 
\hline 
$\omega$ & $2\pi$ \\ 
\hline 
Final Time & 1 \\
\hline
Amplitude (b) & 0.025 \\ 
\hline
Channel half-height (h) & 0.125, 0.15, 0.175, 0.2, 0.225,  \\
& 0.25, 0.275, 0.3, 0.325, 0.35, 0.375 \\
\hline 
\end{tabular} 
\end{center}

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{katz.png}
Horizontal axis: ratio $\frac{h}{b}$,
Vertical axis: mean velocity over a period
\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%\section{Re=0 Infinite Sheet Polimeric}
%running with new setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Re=0 Finite Swimmer Polimeric}
Finite swimmer, Polimeric Stokes equations. The swimmer comes from the 
curvature function:

\begin{equation}
c(s)=-A(s-L)\kappa^2 sin(\kappa s + \omega t), ~~ s \in [0,L]
\end{equation}
where A is the amplitude and L is the swimmer length.

To obtain the initial positions we solve the following system:
\begin{eqnarray}
\frac{\partial \theta}{\partial s} &= c(s),\label{curv} \\
\frac{\partial x}{\partial s} &= cos(\theta),\label{xeq} \\
\frac{\partial y}{\partial s} &= sin(\theta),\label{yeq}
\end{eqnarray}
in the discrete points $s_i=i*\Delta s$, $i=0,1,...,integer\left(\frac{L}{\Delta s}\right)$.
To do this, we integrate \ref{curv} analytically, plug it in \ref{xeq} and \ref{yeq} and then
integrate numerically \ref{xeq} and \ref{yeq} to obtain the $(x_i,y_i)$ positions of the swimmer,
that is
$$ x_i = \int_0^{s_i} cos(\theta(t))dt $$
$$ y_i = \int_0^{s_i} sin(\theta(t))dt $$

General parameters for the numerical simmulations:
\newline \newline
\begin{center}
\begin{tabular}{c|c}
\hline
$\Omega$ & $[0, 2]^2$ \\
\hline
$\Delta s$ & $0.5 \Delta x$ \\ 
\hline 
$\Delta t$ & $\Delta s$ \\ 
\hline 
S1 & $\frac{10^8}{\Delta s}$ \\ 
\hline 
S2 & $10^6\Delta s$ \\ 
\hline
length of the swimmer (L) & 0.6 \\
\hline 
$\kappa$ & $\frac{2\pi}{L}$ \\ 
\hline 
$\omega$ & $2\pi$ \\ 
\hline 
Final Time & 20 \\
\hline
Amplitude (A) & 0.08 \\ 
\hline
Deborah (De) & 5 \\ 
\hline
\end{tabular} 
\end{center}

\clearpage
\subsection{Domain Test}
In this test, we fixed the resolution and changed the domain size.
The parameters that changed are:
\newline
\begin{center}
\begin{tabular}{c|c}
\hline
N & Domain \\
\hline
$128x128$ & $[0, 1]^2$ \\
\hline
$256x256$ & $[0, 2]^2$ \\
\hline
$512x512$ & $[0, 4]^2$ \\
\hline
\end{tabular} 
\end{center}


\begin{figure}
\begin{center}
\includegraphics[scale=.6]{domain_polimeric.png}
Domain Test. Horizontal axis: time,
Vertical axis: mean x position of the swimmer. \\
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{errordomain_polimeric.png}
Domain Test Error Polimeric. Horizontal axis: time,
Vertical axis: error of the mean x position of the swimmer. \\
\end{center}
\end{figure}

\clearpage
\subsection{Resolution}
In this test, we changed the resolution and fixed all other parameters.
The resolutions used are $128\times 128$, $256\times 256$, $512\times 512$.

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{resolution_polimeric.png}
Resolution Test. Horizontal axis: time,
Vertical axis: mean x position of the swimmer. \\
\end{center}
\end{figure}

\clearpage
\subsection{S2 varying}
In this test we fixed the resolution at $256\times 256$ and changed the values of S2.

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{s2_varying_polimeric.png}
S2 varying Test. N=256, Horizontal axis: time,
Vertical axis: mean x position of the swimmer. \\
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=.6]{s2_varying_err_polimeric.png}
S2 varying Test. N=256, Horizontal axis: time,
Vertical axis: Error of mean x position of the swimmer. \\
\end{center}
\end{figure}


\clearpage
\subsection{De varying}
In this test we fixed the resolution at $256\times 256$ and changed the Deborah number.
\begin{figure}
\begin{center}
\includegraphics[scale=.6]{de_varying.png}
De varying Test. N=256, Horizontal axis: time,
Vertical axis: mean x position of the swimmer. \\
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\begin{thebibliography}{9}

\bibitem{taylor}
  Taylor, G. I.
  \emph{Analysis of the swimming of microscopic organisms}.
  R. Soc. London, Ser. A 209, 447  1951
  
\bibitem{tuck}
  Tuck, E. O.
  \emph{A note on a swimming problem}.
  J. Fluid Mech. 31, 1968

\bibitem{katz}
  Katz, D. F.
  \emph{On the propulsion of micro-organisms near solid boundaries}.
  J. Fluid Mech. 64, 3349. 1974.

\end{thebibliography}

\end{document}