# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
import pylab
import numpy as np

if __name__ == '__main__':
    amplitudeList = [0.0005 * (2 ** i) for i in range(8)]
    S1 , S2 = 1e6, 1e4
    wavelength = 2 * np.pi
    Re = 2.5
    for n in [256, 128, 64, 32]:
	    x = []
	    y = []
	    
	    for amplitude in amplitudeList:
		name = "tuck_re%g_n%d_amp%g_S1%g_S2%g.npz" % (Re, n, amplitude, S1, S2)
		temp=np.load(name)
		x.append(amplitude)
		y.append(temp['y'].mean())
	    
	    pylab.plot(x,y,'s',label='n=%d'%(n))

    def F(Re):
	return (.5 * (1 + (1 + Re ** 2) ** .5)) ** .5
    
    a0 = amplitudeList[0]
    a1 = amplitudeList[-1]
    nplot = 10000
    amplitudeList = np.arange(nplot)*((a1-a0)/nplot)+a0
    tuckList = [.25 * (amplitude ** 2) * (wavelength ** 2) * (1. + 1. / F(Re)) for amplitude in amplitudeList]
    
    pylab.plot(amplitudeList,tuckList,label='Tuck Formula')
    pylab.legend(loc='upper left')
    pylab.savefig('tuck.png')
