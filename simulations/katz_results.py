# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
import pylab
import numpy as np

if __name__ == '__main__':
    amplitudeList = [0.0005 * (2 ** i) for i in range(8)]
    S1 , S2 = 1e6, 1e4
    wavelength = 2 * np.pi
    amplitude = 0.025
    channelList = np.arange(5, 16) * amplitude
    
    
    for reynolds in [0]:
	if reynolds == 0:
	    symb = 's'
	elif reynolds == 0.02:
	    symb = 'x'
	else:
	    symb = 'o'
	for n in [128, 64, 32]:
		x = []
		y = []
		
		for channel in channelList:
		    name = "katz_re%g_n%d_amp%g_S1%g_S2%g_chan%g.npz" % (reynolds, n, amplitude, S1, S2, channel)
		    temp=np.load(name)
		    x.append(channel)
		    y.append(temp['y'].mean())
		
		pylab.plot(x,y,symb,label='n=%d'%(n))

    
    a0 = channelList[0]
    a1 = channelList[-1]
    nplot = 10000
    channelList = np.arange(nplot)*((a1-a0)/nplot)+a0
    katzList = [3. / ((channel / amplitude) ** 2 + 2) for channel in channelList]
    
    pylab.plot(channelList,katzList,label='Katz Formula')
    pylab.legend(loc='upper right')
    pylab.savefig('katz.png')
