'''
Created on Mar 27, 2013

@author: felipe
'''
# import matplotlib
# matplotlib.use('Agg')
import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid, PolimericStokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints



def complete(x, n):
    if len(x) > n:
        completed = x[:n]
    else:
        completed = numpy.concatenate([x, [x[-1] for _ in range(n - len(x))]])
    assert (len(completed) == n)
    return completed



if __name__ == '__main__':
    n = 128
    de = .5

    beta = .5 / de

    # grid parameters
    sizeX, sizeY = 1., 1.
    nx, ny = int(n * sizeX), int(n * sizeY)
    grid = Grid(Point(0., 0.), sizeX, sizeY, nx, ny, 2)
    finalTime = 7.0  # 4 * de + 2.

    # swimmer parameter
    amplitude = 0.0125
    ds = grid.dx / 2.
    S1 , S2 = 1e5, 1e1
    wavelength = 2 * pi
    waveVelocity = 2 * pi
    dt = grid.dx
    func = '%g * sin(%g*x - %g*t) + 0.5' % (amplitude, wavelength, waveVelocity)

    # Newtonian
    visc = 1.
    fluidNewt = StokesFluid(grid, visc)
    swimmerNewt = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, True)
    systemNewt = FluidStructure(fluidNewt, swimmerNewt, dt, 'Implicit')
    velocityNewt = LagrangianPoints(0)

    time = utils.Time(.1)
#     pylab.ion()
    snapshot = dt
    while systemNewt.fluid.currentTime < .1:
        time.start()
        print "Energy:", swimmerNewt.computeEnergy()
        swimmerNewt.checkDistance()
        systemNewt.advanceInTime(dt)
        velocityNewt.addPoint(systemNewt.fluid.currentTime, systemNewt.ib.velocity.x.mean())
        time.stop()
        time.printTimeInfo(dt, systemNewt.fluid.currentTime)

#         if systemNewt.fluid.currentTime > snapshot:
#             snapshot += 10 * dt
#             pylab.clf()
# #             pylab.axis([0, 1, 0, 1])
# #             pylab.plot(swimmerNewt.x, swimmerNewt.y)
#             pylab.plot(velocityNewt.x, velocityNewt.y)
#             pylab.draw()

    pylab.ioff()
    pylab.plot(velocityNewt.x, velocityNewt.y)
    pylab.show()


    fluid = PolimericStokesFluid(grid, visc, beta, de)
    swimmer = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, True)
    system = FluidStructure(fluid, swimmer, dt, 'Implicit')
    velocity = LagrangianPoints(0)
#
#     time = utils.Time(finalTime)
#     snapshot = 0.1
    pylab.ion()
#
    steadyNewtVel = velocityNewt.y[-1]
    snap = 0
    while system.fluid.currentTime < finalTime:
        time.start()
        if finalTime - system.fluid.currentTime < dt:
            dt = finalTime - system.fluid.currentTime
            system.changeDt(dt)

        system.advanceInTime(dt)
        velocity.addPoint(system.fluid.currentTime, system.ib.velocity.x.mean())
#         time.stop()
#         time.printTimeInfo(dt, system.fluid.currentTime)
#
        if snapshot < system.fluid.currentTime:
            snapshot += .2
            pylab.clf()
            pylab.axis([0, 6, 0.6, 1.])
            pylab.plot(velocity.x, velocity.y / complete(velocityNewt.y, len(velocity.y)))
# #             pylab.axis([0, 1, 0, 1])
# #             pylab.plot(swimmer.x % 1, swimmer.y % 1)
# #             pylab.contourf(grid.getX(), grid.getY(), fluid.polimer.getTrace().T, cmap=pylab.cm.Oranges)
# #             pylab.colorbar()
            pylab.draw()
        print " RATIO!:" , velocity.y[-1] / velocityNewt.y[-1]
#         if system.fluid.currentTime > 6.5:
#             name = "paper_de1_125_%d"
#             system.saveState(name % (snap))
#             snap += 1
#             pylab.clf()
#             pylab.axis([0, 1, 0, 1])
#             pylab.plot(swimmer.x % 1, swimmer.y % 1)
#             pylab.contourf(grid.getX(), grid.getY(), fluid.polimer.getTrace().T, cmap=pylab.cm.Oranges)
#             pylab.colorbar()
#             pylab.savefig(name % (snap) + 'png')


    polName = "vel_periodic_pol_%g_%d_%g_%g_%g.npz" % (de, nx + ny, amplitude, S1, S2)
    velocity.save(polName)

#    pylab.ioff()
#    pylab.clf()
#    pylab.axis([0, 1, 0, 1])
#    pylab.plot(swimmer.x % 1, swimmer.y % 1)
#    pylab.contourf(grid.getX(), grid.getY(), fluid.polimer.getTrace().T, cmap = pylab.cm.Oranges)
#    pylab.colorbar()
#    pylab.show()



