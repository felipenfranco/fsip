'''
Created on Apr 19, 2012

@author: felipe
'''
import subprocess
import multiprocessing
import numpy
import pylab
import os.path



def plot_result(swimmerSize):
    for de in [0, .5, 1, 2, 3, 4, 5]:
        if os.path.exists("n128_size%g_de%g_x.txt" % (swimmerSize, de)):
            x = numpy.loadtxt("n128_size%g_de%g_x.txt" % (swimmerSize, de))
            y = numpy.loadtxt("n128_size%g_de%g_y.txt" % (swimmerSize, de))
            pylab.plot(x, y, label = str(de))
    pylab.legend(loc = "upper left")
    pylab.show()

def plot_result2(S1exp, S2exp, sizes, des, amp):
    for swimmerSize in sizes:
        for de in des:
            if os.path.exists("n128_size%g_de%g_S1_%g_S2_%d_amp_%g_x.txt" % (swimmerSize, de, S1exp, S2exp, amp)):
                print "Existe!"
                x = numpy.loadtxt("n128_size%g_de%g_S1_%g_S2_%d_amp_%g_x.txt" % (swimmerSize, de, S1exp, S2exp, amp))
                y = numpy.loadtxt("n128_size%g_de%g_S1_%g_S2_%d_amp_%g_y.txt" % (swimmerSize, de, S1exp, S2exp, amp))
                pylab.plot(x, y, label = "%g" % (de))
#                pylab.title("%g,%g" % (S1exp, S2exp))
                pylab.legend(loc = "upper left")
#            else:
#                print "Nao existe", "n128_size%g_de%g_S1_%g_S2_%d_amp_%g_x.txt" % (swimmerSize, de, S1exp, S2exp, amp)



if __name__ == '__main__':

    scriptToRun = ["python2.7", "/afs/labmap.ime.usp.br/usr/fnfranco/fsip/simulations/lisa_paper_nonperiodic.py", "128"]
#    scriptToRun = ["python2.7", "/home/felipe/Workspace/fsip/simulations/lisa_paper_nonperiodic.py", "128"]
    S1exp = 12
    S2exp = 11
    amp = .05

    simmulationTasks = []

    #create list of tasks to run
    for amp in [.05]:
        for swimmerSize in numpy.arange(.3, .47, .01):
            for de in [0, .5, 1, 2, 3, 4, 5]:
                simmulationTasks += [scriptToRun + [str(swimmerSize), str(de), str(S1exp), str(S2exp), str(amp)]]


    triedSimulations = set()
    pool = multiprocessing.Pool(5)

    while simmulationTasks:
        results = []
        r = pool.map_async(subprocess.call, simmulationTasks, callback = results.append)
        r.wait()
        results = results[0]

        #check if all simulations went well
        #if not, create a new simmulation list with modified parameter S1, S2
        #for the tasks that went bad.
        newTasks = []
        for i in range(len(results)):
            if results[i]:
                triedSimulations.add(str(simmulationTasks[i]))
                tryThis = []
                S1range = range(8, 13)
                S1range.reverse()
                for S1 in S1range:
                    S2range = range(S1 - 4, S1)
                    S2range.reverse()
                    for S2 in S2range:
                        newTask = list(simmulationTasks[i])
                        newTask[5] = str(S1)
                        newTask[6] = str(S2)
                        if str(newTask) not in triedSimulations:
                            tryThis = newTask
                            break
                    if tryThis:
                        break
                if tryThis:
                    newTasks.append(tryThis)
        simmulationTasks = list(newTasks)





