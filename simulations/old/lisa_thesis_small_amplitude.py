'''
Created on Aug 20, 2012

@author: fnfranco
'''
import sys
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid, NavierStokesFluid
from swimmer.swimmer_class import NonPeriodicSwimmer, PeriodicSwimmer, NonPeriodicSwimmerVarying
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints
import pylab; pylab.ioff()
import numpy

if __name__ == '__main__':
    wavelength = 10 * pi
    waveVelocity = 8 * pi
    n = 64
#    vel = waveVelocity / wavelength
    domain_size = .2
#    period = domain_size / vel
    visc, dens = .01, 1.
    print "Reynolds number of the flow:", (dens * waveVelocity) / (visc * (wavelength ** 2))
    
    grid = Grid(Point(0., 0.), domain_size, domain_size, n, n, 2)
    fluid = NavierStokesFluid(grid, visc, dens)
    
    amplitude = domain_size / 100.
    ds = grid.dx / 2.
    dt = ds#0.00025
    
    S1 = 10000.0
    S2 = 10.0
    finalTime = 1.#8.*pi / waveVelocity
    print " Final TIME:", finalTime, dt
    numberOfPoints = 2 * n
    startPoint = Point(0., domain_size / 2)
    
    swimmer = NonPeriodicSwimmer(S1, S2, ds, numberOfPoints, startPoint, amplitude, wavelength, waveVelocity, grid, True, -1)
#    swimmer = PeriodicSwimmer(S1, S2, ds, numberOfPoints, startPoint, amplitude, 2 * pi, waveVelocity, grid, -1)
    system = FluidStructure(fluid, swimmer, dt, 'Implicit')
    time = utils.Time(finalTime)
    
    first = LagrangianPoints(0)
    one_quarter = LagrangianPoints(0)
    middle = LagrangianPoints(0)
    last = LagrangianPoints(0)
    rateOfWork = LagrangianPoints(0)
    vel = LagrangianPoints(0)
    
#    pylab.ioff()
#    pylab.axis([0, 1., .09 * 5, .11 * 5])
#    pylab.plot(swimmer.x, swimmer.y)
#    pylab.show()
#
#    pylab.ion()
    snapshot = dt
    
    while system.fluid.currentTime < finalTime:
        time.start()
        
        first.addPoint(swimmer.x[0], swimmer.y[0])
        one_quarter.addPoint(swimmer.x[31], swimmer.y[31])
        middle.addPoint(swimmer.x[63], swimmer.y[63])
        last.addPoint(swimmer.x[-1], swimmer.y[-1])
        rateOfWork.addPoint(fluid.currentTime, swimmer.rateOfWork())
        vel.addPoint(fluid.currentTime, swimmer.velocity.x.mean())
        print "VELOCITY:", vel.y[-1]
        
        system.advanceInTime(dt)
        
        time.stop()
        time.printTimeInfo(dt, system.fluid.currentTime)
        
#        if system.fluid.currentTime > snapshot:
#            snapshot += 20 * dt
#            pylab.clf()
#            
#            pylab.plot(rateOfWork.x, rateOfWork.y)
#            pylab.plot(rateOfWork.x, swimmer.u, label = 'u')
#            pylab.plot(vel.x, vel.y, label = 'vel x')
            
#            pylab.axis([0, .2, 0, .2])
#            pylab.plot(swimmer.x, swimmer.y)

#            pylab.axis([-0.0015, 0.0, 0.094, 0.106])
#            pylab.plot(first.x, first.y)
#            pylab.axis([0.98, 1., .09 * 5, .11 * 5])
#            pylab.plot(last.x / 5 , last.y / 5)
#            pylab.axis([0, 1., .09 * 5, .11 * 5])
#            pylab.plot(swimmer.x, swimmer.y)
#
#            pylab.axis([0.10146 - 0.0015, 0.10146, 0.094, 0.106])
#            pylab.plot(middle.x, middle.y)
#        print middle.x[-1]; raw_input()
#            pylab.legend()
#            pylab.draw()


    first.save("first_small_amp_%d" % (n), True)
    one_quarter.save("one_quarter_small_amp_%d" % (n), True)
    middle.save("middle_small_amp_%d" % (n), True)
    last.save("last_small_amp_%d" % (n), True)
    rateOfWork.save("rate_small_amp_%d" % (n), True)
    vel.save("vel_small_amp_%d" % (n), True)

 
    
    pylab.clf()
    pylab.axis([-0.0015, 0.0, 0.094, 0.106])
    pylab.plot(first.x, first.y)
    pylab.savefig("first_small.png")
    
    
    pylab.clf()
    pylab.axis([0.04690, 0.04840, 0.094, 0.106])
    pylab.plot(one_quarter.x, one_quarter.y)
    pylab.savefig("one_quarter_small.png")
    
    pylab.clf()
    pylab.axis([0.09685, 0.09835, 0.094, 0.106])
    pylab.plot(middle.x, middle.y)
    pylab.savefig("middle_small.png")
    
    pylab.clf()
    pylab.axis([0.19675, 0.19825, 0.094, 0.106])
    pylab.plot(last.x, last.y)
    pylab.savefig("last_small.png")
    
    
    u = numpy.array(swimmer.u)
    times = numpy.array([i * dt for i in range(u.size)])
    displacement = u.mean() * times
    
    pylab.clf()
    pylab.axis([-0.00025, 0.00025, 0.094, 0.106])
    pylab.plot(first.x - displacement, first.y)
    pylab.savefig("first_small_lab.png")
    
    
    pylab.clf()
    pylab.axis([0.04815, 0.04865, 0.094, 0.106])
    pylab.plot(one_quarter.x - displacement, one_quarter.y)
    pylab.savefig("one_quarter_small_lab.png")
    
    
    pylab.clf()
    pylab.axis([0.09810, 0.09860, 0.094, 0.106])
    pylab.plot(middle.x - displacement, middle.y)
    pylab.savefig("middle_small_lab.png")
    
    pylab.clf()
    pylab.axis([0.198, 0.1985, 0.094, 0.106])
    pylab.plot(last.x - displacement, last.y)
    pylab.savefig("last_small_lab.png")
