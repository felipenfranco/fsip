'''
Created on Mar 28, 2012

@author: felipe
'''
import sys
import pylab
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid, PolimericStokesFluid
from swimmer.swimmer_class import NonPeriodicSwimmerVarying
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints

if __name__ == '__main__':
#    n = int(sys.argv[1])
#    de = float(sys.argv[2])
#    s_til = float(sys.argv[3])
    
    n = 32
    de = .5
    s_til = 3
    beta = .5
    
    #grid parameters
    sizeX, sizeY = 1., 1.
    nx, ny = int(n * sizeX), int(n * sizeY)
    grid = Grid(Point(0., 0.), sizeX, sizeY, nx, ny, 2)
    finalTime = 20.
    
    #swimmer parameter
    amplitude = 0.2
    ds = grid.dx / 2.
    swimmerSize = .5
    numberOfPoints = numpy.ceil(swimmerSize / ds) + 1
    S1 , S2 = 10 ** (s_til + 2), 10 ** s_til
    wavelength = (2.4 * pi) / swimmerSize
    waveVelocity = 2 * pi
    dt = ds * .1

    visc = 1.
    fluidNewt = StokesFluid(grid, visc)
    fluid = PolimericStokesFluid(grid, visc, beta, de)

    swimmerNewt = NonPeriodicSwimmerVarying(S1, S2, ds, numberOfPoints, Point(.0, sizeY / 2), amplitude, wavelength, waveVelocity, grid)
    swimmer = NonPeriodicSwimmerVarying(S1, S2, ds, numberOfPoints, Point(.0, sizeY / 2), amplitude, wavelength, waveVelocity, grid)
    
    system = FluidStructure(fluid, swimmer, dt, 'Implicit')
    systemNewt = FluidStructure(fluidNewt, swimmerNewt, dt, 'Implicit')
    
    time = utils.Time(finalTime)
    snapshot = 0.001
    pylab.ion()
    pylab.rcParams['figure.figsize'] = 2.2 * 4.8, 4.75

    
    centerOfMass = LagrangianPoints(0)
    centerOfMassNewt = LagrangianPoints(0)
    vel = LagrangianPoints(0)
    velNewt = LagrangianPoints(0)
    mean = LagrangianPoints(0)
    
    while system.fluid.currentTime < finalTime:
        time.start()
        system.advanceInTime(dt)
        systemNewt.advanceInTime(dt)
        centerOfMass.addPoint(system.fluid.currentTime, system.ib.x.mean())
        centerOfMassNewt.addPoint(systemNewt.fluid.currentTime, systemNewt.ib.x.mean())
        vel.addPoint(system.fluid.currentTime, system.ib.velocity.x.mean())
        velNewt.addPoint(systemNewt.fluid.currentTime, systemNewt.ib.velocity.x.mean())
        mean.addPoint(systemNewt.fluid.currentTime, vel.y.mean() / velNewt.y.mean())
        
        time.stop()
        time.printTimeInfo(dt, system.fluid.currentTime)
        
        if snapshot < system.fluid.currentTime:
            snapshot += .1
            pylab.clf()
#            pylab.axis([0, 20, 0.2, 1.])
#            pylab.plot(centerOfMassNewt.x, centerOfMassNewt.y, label = 'Newt')
#            pylab.plot(centerOfMass.x, centerOfMass.y, label = 'De')
            pylab.plot(mean.x, mean.y, label = 'Ratio De/Newt')
#            pylab.plot([0, 20], [centerOfMass.y[0], centerOfMass.y[0] + .5])
#            pylab.axis([0, .7, 0.35, .65])
#            pylab.plot(swimmerNewt.x, swimmerNewt.y, label = 'Newt')
#            pylab.plot(swimmer.x, swimmer.y, label = 'De')
            pylab.legend(loc = 'upper left')
            pylab.draw()
#        swimmer.checkDistance()

    polName = "center_mass_%g_%d_%g_%g_%g.npz" % (de, nx + ny, amplitude, S1, S2)
    centerOfMass.save(polName)
    
#    pylab.ioff()
#    pylab.plot(centerOfMass.x, centerOfMass.y)
#    pylab.show()
    
    

