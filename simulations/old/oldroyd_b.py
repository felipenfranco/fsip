# -*- coding: utf-8 -*-
'''
This script reproduces some results of the paper "Emergence of Singular Structures in Oldroyd-B Fluids
by Becca Thomases and Michael Shelley.
'''
import utils
from math_objects.cartesian_grid import *
from physic_objects.fluid import PolimericStokesFluid, StokesFluid
from numpy import pi, sin, cos
import pylab


if __name__ == '__main__':
    domainSize = 2 * pi
    n = 64
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)

    fx = lambda x, y:2 * sin(x) * cos(y)
    fy = lambda x, y:-2 * cos(x) * sin(y)

    dt = grid.dx
    finalTime = 6.

    # test for the values of Sxx in the (pi,y) plane
    if True:
        for tests in [[1., 2., 3., 4., 5.]]:  # [[.1, .2, .3, .4, .5], [.6, .7, .8, .9], [1., 2., 3., 4., 5.]]:
            for weissenbergNumber in tests:
                beta = 0.5 / weissenbergNumber
                fluid = PolimericStokesFluid(grid, 1., beta, weissenbergNumber)

                while fluid.currentTime < finalTime:
                    print fluid.currentTime
                    fluid.initializeRhs(fx, fy)
                    fluid.advanceInTime(dt)

                sx, ex, sy, ey = grid.getBounds()
                y = fluid.polimer.Sxx.getY()
                sxx = fluid.polimer.Sxx.data[(ex - sx) / 2, :]
                pylab.plot(y, sxx)
            pylab.show()

    # test for the qualitative view of the div(S), Trace(S), Sxy contour plots
    if True:
        for weissenbergNumber in (5., .3, .6):
            beta = 0.5 / weissenbergNumber
            fluid = PolimericStokesFluid(grid, 1., beta, weissenbergNumber)

            while fluid.currentTime < finalTime:
                print fluid.currentTime
                fluid.initializeRhs(fx, fy)
                fluid.advanceInTime(dt)
