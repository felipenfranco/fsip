'''
Created on Apr 19, 2012

@author: felipe
'''
import subprocess
import numpy
import pylab

def plot_result(swimmerSize):
    for de in ["0.0", "0.5", "1.0", "2.0", "3.0", "4.0", "5.0"]:
        x = numpy.loadtxt("n64_size%g_de%g_x.txt" % (swimmerSize, float(de)))
        y = numpy.loadtxt("n64_size%g_de%g_y.txt" % (swimmerSize, float(de)))
        pylab.plot(x, y, label = de)
    pylab.legend(loc = "upper left")
    pylab.show()


if __name__ == '__main__':
    scriptToRun = ["python2.7", "/afs/labmap.ime.usp.br/usr/fnfranco/fsip/simulations/lisa_paper_nonperiodic.py", "128"]
    swimmerSize = .35
    de = 0.
    amp = .5
    for S2exp in range(1, 14):
        processes = []
        for S1exp in range(1, 14):
            processes += [scriptToRun + [str(swimmerSize), str(de), str(S1exp), str(S2exp), str(amp)]]
        p = [subprocess.Popen(proc) for proc in processes]
        signals = [proc.wait() for proc in p]


