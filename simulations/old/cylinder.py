# -*- coding: utf-8 -*-
'''
Created on Apr 27, 2012

@author: fnfranco
'''
import utils
from math_objects.cartesian_grid import Grid, Point
from physic_objects.fluid import NavierStokesFluid
from physic_objects.ib import Ellipse
from physic_objects.force import TheterForce
from physic_objects.fluid_structure import FluidStructure
import pylab

if __name__ == '__main__':
    nx, ny = 64, 32
    nbp = nx * 2
    dens = 1.
    visc = 1.
    raio = 0.15
    reynoldsNumber = (dens * 2 * raio) / visc

    print "Reynolds Number of the flow:", reynoldsNumber

    sizeX = 16.
    sizeY = 8.
    ellasticConstant = 10e6
    FinalTime = 100.

    grid = Grid(Point(0., 0.), sizeX, sizeY, nx, ny, 2)
    dx, dy = grid.getSpacing()
    sx, ex, sy, ey = grid.getBounds()
    fluid = NavierStokesFluid(grid, visc, dens)
    ib = Ellipse(nbp, Point(4., 4.), 2 * raio, 2 * raio, grid, TheterForce(ellasticConstant))

    dt = dx
    system = FluidStructure(fluid, ib, dt, 'Implicit')

    fRhsU = lambda x, y: 1.
    fRhsV = lambda x, y: 0.

    ib.boundaryForce.setTheterPoints(ib.x, ib.y)
    snapshotTime = .01
    pylab.ion()
    x, y = grid.getX(), grid.getY()
    while fluid.currentTime < FinalTime:
        dt = ib.boundaryForce.computeMaxDt(dx, dy) * 100
        system.changeDt(dt)
#        fluid.initializeRhs(fRhsU, fRhsV)
#         fluid.systemU.rhs.data[:] = 0.
#         fluid.systemU.rhs.data[sx, :] = 100.
#         fluid.systemV.rhs.data[:] = 0.
        fluid.velocity.u[sx, :] = 1.

        system.advanceInTime(dt)
        print "DT: ", dt
        if fluid.currentTime > snapshotTime:
            snapshotTime += 1.
            pylab.clf()
            pylab.axis([0, 16, 0, 8])
            pylab.plot(ib.x, ib.y)
            pylab.quiver(x, y, fluid.velocity.u.data.T, fluid.velocity.v.data.T)
            pylab.contour(x, y , fluid.getVorticity().data.T)
#             pylab.contour(x, y , fluid.pressure.data.T)
            pylab.draw()
            

