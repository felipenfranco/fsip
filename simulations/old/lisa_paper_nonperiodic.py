'''
Created on Mar 28, 2012

@author: felipe
'''
import sys
import pylab
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import PolimericStokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import Point, Grid
from math_objects.lagrangian_points import LagrangianPoints

if __name__ == '__main__':
    n = int(sys.argv[1])
    S1 = float(sys.argv[2])
    S2 = float(sys.argv[3])
    domain = float(sys.argv[4])

    beta = .5

    # grid parameters
    grid = Grid(Point(0., 0.), domain, domain, n, n, 2)
    finalTime = 10.

    # swimmer parameter
    amplitude = 0.1
    ds = grid.dx / 2.
    swimmerSize = .6
    numberOfPoints = numpy.ceil(swimmerSize / ds) + 1
    S1 , S2 = 10 ** (s_til + 2), 10 ** s_til
    wavelength = (4. * pi)
    waveVelocity = 2 * pi
    dt = ds * .5

    visc = 1.
    if de == 0.:
        fluid = StokesFluid(grid, visc)
    else:
        fluid = PolimericStokesFluid(grid, visc, beta, de)

    swimmer = NonPeriodicSwimmerVarying(S1, S2, ds, numberOfPoints, Point(.0, sizeY / 2), amplitude, wavelength, waveVelocity, grid)
    system = FluidStructure(fluid, swimmer, dt, 'Implicit')

    time = utils.Time(finalTime)
    snapshot = 0.001
    pylab.ion()
    pylab.rcParams['figure.figsize'] = 2.2 * 4.8, 4.75


    centerOfMass = LagrangianPoints(0)
    while system.fluid.currentTime < finalTime:
        time.start()
        system.advanceInTime(dt)
        centerOfMass.addPoint(system.fluid.currentTime, system.ib.x.mean())
        time.stop()
        time.printTimeInfo(dt, system.fluid.currentTime)

        if snapshot < system.fluid.currentTime:
            snapshot += .001
            pylab.clf()
#            pylab.axis([0, 20, 0.2, 1.])
#            pylab.plot(centerOfMass.x, centerOfMass.y)
#            pylab.plot([0, 20], [centerOfMass.y[0], centerOfMass.y[0] + .5])
            pylab.axis([0, .7, 0.35, .65])
            pylab.plot(swimmer.x, swimmer.y)
            pylab.draw()
#        swimmer.checkDistance()

    polName = "center_mass_%g_%d_%g_%g_%g.npz" % (de, nx + ny, amplitude, S1, S2)
    centerOfMass.save(polName)

#    pylab.ioff()
#    pylab.plot(centerOfMass.x, centerOfMass.y)
#    pylab.show()



