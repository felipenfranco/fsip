# -*- coding: utf-8 -*-
'''
Created on Aug 20, 2012

@author: fnfranco
'''
# import matplotlib
# matplotlib.use('Agg') #comment if you want to see interactive plots on your machine

import sys
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid, NavierStokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints
import pylab; pylab.ioff()
import numpy



if __name__ == '__main__':
    wavelength = 10 * pi
    waveVelocity = 8 * pi
    n = 32
#    vel = waveVelocity / wavelength
    domain_size = .2
#    period = domain_size / vel
    visc, dens = .01, 1.
    print "Reynolds number of the flow:", (dens * waveVelocity) / (visc * (wavelength ** 2))
    
    grid = Grid(Point(0., 0.), domain_size, domain_size, n, n, 2)
    fluid = NavierStokesFluid(grid, visc, dens)
#    fluid = StokesFluid(grid, 0.01)
    
    amplitude = domain_size / 10.
    ds = grid.dx / 2.
    dt = grid.dx  # 0.0008
    
    S1 = 10000.0
    S2 = 10.0
    finalTime = 2.
    print " Final TIME:", finalTime, dt
    
    func = '%g * sin(%g*x - %g*t) + %g' % (amplitude, wavelength, waveVelocity, domain_size / 2)
    swimmer = Swimmer(S1, S2, ds, func, 'x', (0., domain_size), grid, True)
    system = FluidStructure(fluid, swimmer, dt, 'Implicit')
    time = utils.Time(finalTime)
    
    first = LagrangianPoints(0)
    one_quarters = LagrangianPoints(0)
    three_quarters = LagrangianPoints(0)
    last = LagrangianPoints(0)
    rateOfWork = LagrangianPoints(0)
    dissipation = LagrangianPoints(0)
    vel = LagrangianPoints(0)
    
#    pylab.ioff()
#    pylab.axis([0, 1., .09 * 5, .11 * 5])
#    pylab.plot(swimmer.x, swimmer.y)
#    pylab.show()
#
    pylab.ion()
    snapshot = dt
    doOnce = True
    while system.fluid.currentTime < finalTime:
#        if system.fluid.currentTime > 0.005 and doOnce:
#            dt = 0.0008
#            system.changeDt(dt)
#            doOnce = False
            
        time.start()
        
        first.addPoint(swimmer.x[0], swimmer.y[0])
        one_quarters.addPoint(swimmer.x[15], swimmer.y[15])
        three_quarters.addPoint(swimmer.x[47], swimmer.y[47])
        last.addPoint(swimmer.x[-1], swimmer.y[-1])
        
        rateOfWork.addPoint(fluid.currentTime, swimmer.rateOfWork())
        dissipation.addPoint(fluid.currentTime, fluid.dissipation())
        vel.addPoint(fluid.currentTime, swimmer.velocity.x.mean())
        print "VELOCITY:", vel.y[-1]
        
        system.advanceInTime(dt)
        
        time.stop()
        time.printTimeInfo(dt, system.fluid.currentTime)
        
#         if system.fluid.currentTime > snapshot:
#             snapshot += 10 * dt
#             pylab.clf()
            
#            pylab.plot(rateOfWork.x, rateOfWork.y)
#            pylab.plot(dissipation.x, dissipation.y)
            
#            cu, cv = fluid.getConvection()
#            pylab.contourf(cv.T)
#            pylab.colorbar()
            
#            pylab.plot(rateOfWork.x, rateOfWork.y)
#            pylab.plot(rateOfWork.x, swimmer.u, label = 'u')
#            pylab.plot(vel.x, vel.y, label = 'vel x')
            
#            pylab.axis([-0.1, .2, 0, .2])
#            pylab.plot(swimmer.x, swimmer.y)

#            pylab.axis([-0.12, 0.0, 0.04, 0.16])
#             pylab.plot(first.x, first.y)
#            pylab.axis([0.98, 1., .09 * 5, .11 * 5])
#            pylab.plot(last.x , last.y)
#            pylab.axis([0, 1., .09 * 5, .11 * 5])
#            pylab.plot(swimmer.x, swimmer.y)

#            pylab.axis([0.008, 0.14, 0.04, 0.16])
#            pylab.plot(three_quarters.x, three_quarters.y)

#            pylab.axis([0.098, 0.099, 0.094, 0.106])
#            pylab.plot(middle.x, middle.y)

#            pylab.legend()
#             pylab.draw()

    
    first.save("first_large_amp_%d" % (n), True)
    one_quarters.save("one_quarters_large_amp_%d" % (n), True)
    three_quarters.save("three_quarters_large_amp_%d" % (n), True)
    last.save("last_large_amp_%d" % (n), True)
    rateOfWork.save("rate_large_amp_%d" % (n), True)
    vel.save("vel_large_amp_%d" % (n), True)

    u = numpy.array(swimmer.u)
    times = numpy.array([i * dt for i in range(u.size)])
    #displacement = u.mean() * times
    displacement = numpy.zeros_like(u)
    for i in range(len(u)):
        displacement[i] = sum(u[:i] * dt)
    
    pylab.clf()
    #pylab.axis([-0.120, 0.001, 0.04, 0.16])
    pylab.plot(first.x, first.y)
    pylab.savefig("first_large.png")
    
    
    pylab.clf()
    #pylab.axis([-0.083, 0.042, 0.04, 0.16])
    pylab.plot(one_quarters.x, one_quarters.y)
    pylab.savefig("one_quarter_large.png")
    
    
    pylab.clf()
    #pylab.axis([0.008, 0.14, 0.04, 0.16])
    pylab.plot(three_quarters.x, three_quarters.y)
    pylab.savefig("three_quarter_large.png")
    
    
    pylab.clf()
    #pylab.axis([0.05, 0.2, 0.04, 0.16])
    pylab.plot(last.x, last.y)
    pylab.savefig("last_large.png")
    
    
    
    
    pylab.clf()
    #pylab.axis([-0.031, 0.031, 0.04, 0.16])
    pylab.plot(first.x - displacement, first.y)
    pylab.savefig("first_large_lab.png")
    
    
    pylab.clf()
    #pylab.axis([0.009, 0.071, 0.04, 0.16])
    pylab.plot(one_quarters.x - displacement, one_quarters.y)
    pylab.savefig("one_quarter_large_lab.png")
    
    
    pylab.clf()
    #pylab.axis([0.096, 0.16, 0.04, 0.16])
    pylab.plot(three_quarters.x - displacement, three_quarters.y)
    pylab.savefig("three_quarter_large_lab.png")
    
    
    pylab.clf()
    #pylab.axis([0.149, 0.211, 0.04, 0.16])
    pylab.plot(last.x - displacement, last.y)
    pylab.savefig("last_large_lab.png")
