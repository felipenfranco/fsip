# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
import pylab
import numpy as np
import sys
from os.path import exists

if __name__ == '__main__':
    amplitude = float(sys.argv[1])
    S_til = float(sys.argv[2])
    name = "finite_De%g_n%d_amp%g_S1%g_S2%g.npz"
    S1 , S2 = 10 ** (S_til + 2), 10 ** S_til

    for de in [0, 0.5, 1, 2, 3, 4, 5]:
        if exists(name % (de, 64, amplitude, S1, S2)):
            meanx = np.load(name % (de, 64, amplitude, S1, S2))
            pylab.plot(meanx['x'], meanx['y'], label='de=%g' % (de))

    pylab.legend(loc='upper left')
    pylab.savefig('finite_amp%g_s1%g_s2%g.png' % (amplitude, S1, S2))

