# -*- coding: utf-8 -*-
'''
Created on Apr 27, 2012

@author: fnfranco
'''
import utils
from math_objects.cartesian_grid import Grid, Point
from physic_objects.fluid import NavierStokesFluid
from physic_objects.ib import Wall
from physic_objects.force import TheterForce
from physic_objects.fluid_structure import FluidStructure
import pylab

if __name__ == '__main__':
    nx, ny = 64, 64
    nbp = nx * 2
    dens = 1.
    visc = 1.

    sizeX = 1.
    sizeY = 1.
    ellasticConstant = 10e4
    FinalTime = 100.

    grid = Grid(Point(0., 0.), sizeX, sizeY, nx, ny, 2)
    dx, dy = grid.getSpacing()
    sx, ex, sy, ey = grid.getBounds()
    fluid = NavierStokesFluid(grid, visc, dens)
    ib = Wall(dx / 2., Point(0., .5), Point(1., .5), grid, TheterForce(ellasticConstant))

#     pylab.plot(ib.x, ib.y)
#     pylab.show()

    dt = dx
    system = FluidStructure(fluid, ib, dt, 'Implicit')

    fRhsU = lambda x, y: 100.
    fRhsV = lambda x, y: 0.

    ib.boundaryForce.setTheterPoints(ib.x, ib.y)
    snapshotTime = .01
    pylab.ion()
    x, y = grid.getX(), grid.getY()
    while fluid.currentTime < FinalTime:
        dt = dx  # ib.boundaryForce.computeMaxDt(dx, dy) * 1
        system.changeDt(dt)
#         fluid.velocity.u[sx, :] = 1e-15
#         fluid.initializeRhs(fRhsU, fRhsV)
        fluid.systemU.rhs.data[:] = 0.
        fluid.systemU.rhs.data[sx:sx + 10, :] = 10.
        fluid.systemV.rhs.data[:] = 0.

        system.advanceInTime(dt)
        print "DT: ", dt
        if fluid.currentTime > snapshotTime:
            snapshotTime += 1.1
            pylab.clf()
#             pylab.axis([0, 1, 0, 1])
#             pylab.plot(ib.x, ib.y)
#             pylab.quiver(x, y, fluid.velocity.u.data.T, fluid.velocity.v.data.T)
#             pylab.plot(fluid.velocity.u[ny / 2, :])
#             pylab.contour(x, y , fluid.getVorticity().data.T)
#             pylab.contour(x, y , fluid.pressure.data.T)
            pylab.contourf(x, y , fluid.velocity.u.data.T)
            pylab.draw()

        print fluid.velocity.u.data.max()



