'''
Created on Mar 28, 2012

@author: felipe
'''
import matplotlib
# matplotlib.use('Agg')
import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')


if __name__ == '__main__':
    n = int(sys.argv[1])
    de = float(sys.argv[2])
    S_til = float(sys.argv[3])
    amplitude = float(sys.argv[4])

    simName = 'periodic_n%d_de%g_S%g_amp%g' % (n, de, S_til, amplitude)
    finalTime = 4 * de + 2.


    # load the newtonian
    simNameNewt = 'periodic_n%d_de0_S%g_amp%g' % (n, S_til, amplitude)
    newt = numpy.load("vel" + simNameNewt + ".npz")

    # load the polimeric
    pol = numpy.load("vel" + simName + ".npz")


    # repeat the period for the newtonian
    velnewt = newt['y']
    velpol = pol['y']

    period = len(velnewt) / 2
    numPeriods = len(velpol) / period
    newtFullVel = [velnewt[:period]]
    for _ in range(numPeriods - 1):
        newtFullVel.append(velnewt[period:])

    newtFullVel = numpy.concatenate(newtFullVel)

#     pylab.plot(pol['x'], velpol)
#     pylab.plot(pol['x'], newtFullVel)
    pylab.plot(pol['x'], velpol / newtFullVel)
    pylab.show()
    print (velpol / newtFullVel)[-1]





