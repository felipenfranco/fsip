'''
Created on Mar 27, 2013

@author: felipe
'''
# import matplotlib
# matplotlib.use('Agg')

# import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import NavierStokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints


'''
In this simulation, we try to reproduce the analitical results by Tuck for Re = (rho*w)/(mu*k^2).
He finds out that for an infinite sheet y = b*sin(kx-wt), and small amplitudes b, the following holds:

1- if U is the mean velocity, then U = V*(.25 b^2 k^2(1+1/F[Re])), where V = (w/k) and F[Re] = sqrt( .5*(1+sqrt(1+Re^2)) )
3 - The dissipation of energy, averaged over a period, is W = mu b^2 w^2 k (1+F[Re]), where mu is the viscosity.
'''


if __name__ == '__main__':
    visc = .2 / pi
    dens = 1.
    for n in [256, 128, 64, 32]:
        # grid parameters
        domainSize = 1.
        grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)


        ds = grid.dx / 2.
        S1 , S2 = 1e6, 1e4
        wavelength = 2 * pi
        waveVelocity = 2 * pi

        amplitudeList = [0.0005 * (2 ** i) for i in range(8)]
        amplitudeList.reverse()
        dt = ds * .5
        Re = (dens * waveVelocity) / (visc * wavelength ** 2)
        finalTime = 2.
        print "Reynolds:", Re

        for amplitude in amplitudeList:
            func = '%g * sin(%g*x + %g*t) + 0.5' % (amplitude, wavelength, waveVelocity)

            fluid = NavierStokesFluid(grid, visc, dens)
            sheet = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, True)
            system = FluidStructure(fluid, sheet, dt, 'Implicit')

            time = utils.Time(finalTime)
            snapshot = dt
            vel = LagrangianPoints(0)
            while system.fluid.currentTime < finalTime:
                time.start()
                print "Energy:", sheet.computeEnergy()
                system.advanceInTime(dt)
                time.stop()
                time.printTimeInfo(dt, system.fluid.currentTime)

                vel.addPoint(system.fluid.currentTime, sheet.velocity.x.mean())
            vel.save("tuck_re%g_n%d_amp%g_S1%g_S2%g" % (Re, n, amplitude, S1, S2))

#     def F(Re):
#         return (.5 * (1 + (1 + Re ** 2) ** .5)) ** .5
#
#     tuckList = [.25 * (amplitude ** 2) * (wavelength ** 2) * (1. + 1. / F(Re)) for amplitude in amplitudeList]
#     pylab.plot(amplitudeList, tuckList, label='Tuck Formula')
#     pylab.legend(loc='upper left')
#     pylab.ioff()
#     pylab.show()
#     pylab.savefig('tuck.png')

