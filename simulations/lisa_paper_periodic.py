'''
Created on Mar 28, 2012

@author: felipe
'''
# import matplotlib
# matplotlib.use('Agg')
import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/workspace/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid, PolimericStokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints


if __name__ == '__main__':
#     n = int(sys.argv[1])
#     de = float(sys.argv[2])
#     S_til = float(sys.argv[3])
#     amplitude = float(sys.argv[4])
    n = 64
    de = 1.
    S1 = 10000.0
    S2 = 50.0
    amplitude = .125
    amplitude2 = .0625
    amplitude3 = .03125
    q = 0

    simName = 'periodic_n%d_de%g_S1%g_S2%g_amp%g_q%g' % (n, de, S1, S2, amplitude, q)


    # grid parameters
    sizeX, sizeY = 1., 1.
    nx, ny = int(n * sizeX), int(n * sizeY)
    grid = Grid(Point(0., 0.), sizeX, sizeY, nx, ny, 2)
    finalTime = 4 * de + 2.

    # swimmer parameter
    ds = grid.dx / 2.
    wavelength = 2 * pi
    waveVelocity = 2 * pi
    dt = 1. * ds * (2 ** (-q))
    visc = 1.


    beta = .5 / de
    fluid = PolimericStokesFluid(grid, visc, beta, de)
    fluidNewt = StokesFluid(grid)
    fluid2 = PolimericStokesFluid(grid, visc, beta, de)
    fluidNewt2 = StokesFluid(grid)
    fluid3 = PolimericStokesFluid(grid, visc, beta, de)
    fluidNewt3 = StokesFluid(grid)

    func = '%g*sin(%g*x-%g*t)' % (amplitude, wavelength, waveVelocity)
    startPoint = Point(0., 0.5)

    swimmer = Swimmer(S1, S2, ds, func, 'x', (0, 1.), grid, True, startPoint)
    swimmerNewt = Swimmer(S1, S2, ds, func, 'x', (0, 1.), grid, True, startPoint)
    system = FluidStructure(fluid, swimmer, dt, 'Implicit')
    systemNewt = FluidStructure(fluidNewt, swimmerNewt, dt, 'Implicit')
    velocity = LagrangianPoints(0)
    velocityNewt = LagrangianPoints(0)

    func2 = '%g*sin(%g*x-%g*t)' % (amplitude2, wavelength, waveVelocity)
    swimmer2 = Swimmer(S1, S2, ds, func2, 'x', (0, 1.), grid, True, startPoint)
    swimmerNewt2 = Swimmer(S1, S2, ds, func2, 'x', (0, 1.), grid, True, startPoint)
    system2 = FluidStructure(fluid2, swimmer2, dt, 'Implicit')
    systemNewt2 = FluidStructure(fluidNewt2, swimmerNewt2, dt, 'Implicit')
    velocity2 = LagrangianPoints(0)
    velocityNewt2 = LagrangianPoints(0)

    func3 = '%g*sin(%g*x-%g*t)' % (amplitude3, wavelength, waveVelocity)
    swimmer3 = Swimmer(S1, S2, ds, func3, 'x', (0, 1.), grid, True, startPoint)
    swimmerNewt3 = Swimmer(S1, S2, ds, func3, 'x', (0, 1.), grid, True, startPoint)
    system3 = FluidStructure(fluid3, swimmer3, dt, 'Implicit')
    systemNewt3 = FluidStructure(fluidNewt3, swimmerNewt3, dt, 'Implicit')
    velocity3 = LagrangianPoints(0)
    velocityNewt3 = LagrangianPoints(0)

    time = utils.Time(finalTime)
    snap = 0
    pylab.ion()
    while system.fluid.currentTime < finalTime:
        if finalTime - system.fluid.currentTime < dt:
            dt = finalTime - system.fluid.currentTime
            system.changeDt(dt)

        time.start()
        system.advanceInTime(dt)
        systemNewt.advanceInTime(dt)

        system2.advanceInTime(dt)
        systemNewt2.advanceInTime(dt)

        system3.advanceInTime(dt)
        systemNewt3.advanceInTime(dt)


        velocity.addPoint(system.fluid.currentTime, system.ib.velocity.x.mean())
        velocityNewt.addPoint(systemNewt.fluid.currentTime, systemNewt.ib.velocity.x.mean())

        velocity2.addPoint(system2.fluid.currentTime, system2.ib.velocity.x.mean())
        velocityNewt2.addPoint(systemNewt2.fluid.currentTime, systemNewt2.ib.velocity.x.mean())

        velocity3.addPoint(system3.fluid.currentTime, system3.ib.velocity.x.mean())
        velocityNewt3.addPoint(systemNewt3.fluid.currentTime, systemNewt3.ib.velocity.x.mean())

        if snap < system.fluid.currentTime:
            snap += .1
            pylab.clf()
            pylab.axis([0, 6, .6, 1])
            pylab.plot(velocity.x, velocity.y / velocityNewt.y, label='.125')
            pylab.plot(velocity2.x, velocity2.y / velocityNewt2.y, label='.0625')
            pylab.plot(velocity3.x, velocity3.y / velocityNewt3.y, label='.03125')
            pylab.legend(loc='upper right')
            pylab.draw()
        swimmer.computeEnergy(True)
        time.stop()
        time.printTimeInfo(dt, system.fluid.currentTime)
        print "Current mean vel:", velocity.y[-1]
    raw_input()
    velocity.save("vel" + simName)
    swimmer.save("sheet" + simName)
    fluid.saveState("fluid" + simName)


