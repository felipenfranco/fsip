# -*- coding: utf-8 -*-
'''
Created on Mar 27, 2013

@author: felipe
'''
import matplotlib
matplotlib.use('Agg')

import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import PolimericStokesFluid, StokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import Grid, Point
from numpy import pi
from simulation_manager.manager import SimulationManager


def run_simulation(n, S1, S2, domainSize, q, De, amplitude, finalTime):
    assert(S1 >= S2)

    beta = 0.
    if De:
        beta = 0.5 / De

    simulationName = "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g" % (n, S1, S2, domainSize, De, beta, q, amplitude)

    # grid parameters
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)
    ds = grid.dx / 2.
    wavelength = 2 * pi
    waveVelocity = 2 * pi
    dt = 0.5 * ds * (2 ** (-q))
    visc = 1.

    if De:
        saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always", "saveSxxSup":"always",
                    "saveSyySup":"always", "saveSxySup":"always", "saveMeanXSpeed":"always", "saveEnergyE1":"always",
                    "saveEnergyE2":"always", "savePolimer":"end"}
    else:
        saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always", "saveMeanXSpeed":"always",
                    "saveEnergyE1":"always", "saveEnergyE2":"always"}


    if De:
        fluid = PolimericStokesFluid(grid, visc, beta, De)
    else:
        fluid = StokesFluid(grid, visc)

    func = '%g*sin(%g*x-%g*t)' % (amplitude, wavelength, waveVelocity)
    startPoint = Point(0., domainSize / 2.)
    periodicSwimmer = Swimmer(S1, S2, ds, func, 'x', (0, 1.), grid, True, startPoint)
    system = FluidStructure(fluid, periodicSwimmer, dt, 'Implicit')


    sim = SimulationManager(system, simulationName, finalTime, dt, .5, saveDict)
    sim.start()

if __name__ == '__main__':
#     test = int(sys.argv[1])
    test = 1
    if test == 1:
        # Amplitude test, same deborah
        S1 = 10000.0
        S2 = 5000.0
        domainSize = 1
        q = 0
        De = 1.
        for n in [64]:  # [32, 64, 128, 512]:
            for amplitude in [0.125, 0.0625, 0.03125]:
                finalTime = 6
                run_simulation(n, S1, S2, domainSize, q, De, amplitude, finalTime)
                run_simulation(n, S1, S2, domainSize, q, 0., amplitude, finalTime)

#     if test == 2:
#         # dt test
#         n = 256
#         S1 = 10000.0
#         S2 = 500.0
#         domainSize = 1
#         De = 1
#         amplitude = 0.03125
#         finalTime = 6.
#         for q in [0, 1, 2, 3]:
#             run_simulation(n, S1, S2, domainSize, q, De, amplitude, finalTime)

    if test == 3:
        # Deborah test, same amplitude
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        q = 0
        amplitude = 0.01 / (2 * pi)
        for n in [32, 64, 128, 256, 512]:
            for De in [8, 4, 2, 1, .5, 0.]:
                finalTime = De * 4 + 3
                run_simulation(n, S1, S2, domainSize, q, De, amplitude, finalTime)

    if test == 4:
        # resolution test
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        q = 0
        De = 1
        amplitude = 0.03125
        finalTime = 6.
        for n in [32, 64, 128, 256, 512]:
            run_simulation(n, S1, S2, domainSize, q, De, amplitude, finalTime)
            run_simulation(n, S1, S2, domainSize, q, 0, amplitude, finalTime)

