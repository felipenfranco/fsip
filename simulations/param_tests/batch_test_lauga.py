# -*- coding: utf-8 -*-
'''
Created on Mar 27, 2013

@author: felipe
'''
import matplotlib
matplotlib.use('Agg')

import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import PolimericStokesFluid, StokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import Grid, Point
from numpy import pi
from simulation_manager.manager import SimulationManager


def run_simulation(n, S1, S2, domainSize, q, De, amplitude, finalTime):
    assert(S1 >= S2)

    beta = 0.
    if De:
        beta = 0.5 / float(De)

    simulationName = "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g" % (n, S1, S2, domainSize, De, beta, q, amplitude)

    # grid parameters
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)
    ds = grid.dx / 2.
    wavelength = 2 * pi
    waveVelocity = 2 * pi
    dt = 0.5 * ds * (2 ** (-q))
    visc = 1.

    if De:
        saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always", "saveSxxSup":"always",
                    "saveSyySup":"always", "saveSxySup":"always", "saveMeanXSpeed":"always", "saveEnergyE1":"always",
                    "saveEnergyE2":"always", "savePolimer":"end"}
    else:
        saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always", "saveMeanXSpeed":"always",
                    "saveEnergyE1":"always", "saveEnergyE2":"always"}


    if De:
        fluid = PolimericStokesFluid(grid, visc, beta, De)
    else:
        fluid = StokesFluid(grid, visc)

    func = '%g*sin(%g*x-%g*t)' % (amplitude, wavelength, waveVelocity)
    startPoint = Point(0., domainSize / 2.)
    periodicSwimmer = Swimmer(S1, S2, ds, func, 'x', (0, 1.), grid, True, startPoint)
    system = FluidStructure(fluid, periodicSwimmer, dt, 'Implicit')


    sim = SimulationManager(system, simulationName, finalTime, dt, .5, saveDict)
    sim.start()

if __name__ == '__main__':
    # Lauga test
    # RODAR ESSE!
    S1 = 10000.0
    S2 = 500.0
    domainSize = 1
    q = 0
    k = 2. * pi
    n = 128

    bkList = [0.01, 0.02, 0.04, 0.06, 0.08, 0.1]

    for De in [.5, 8.]:
        if De == .5:
            finalTime = 5.
        elif De == 8:
            finalTime = 35.

        for bk in bkList:
            b = bk / k
            run_simulation(n, S1, S2, domainSize, q, De, b, finalTime)

