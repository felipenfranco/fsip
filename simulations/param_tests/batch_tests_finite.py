# -*- coding: utf-8 -*-
'''
Created on Mar 27, 2013

@author: felipe
'''
import matplotlib
matplotlib.use('Agg')

import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import PolimericStokesFluid, StokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import Grid, Point
from numpy import pi
from simulation_manager.manager import SimulationManager

def run_simulation(n, S1, S2, domainSize, q, De, amplitude):
    assert(S1 >= S2)

    beta = 0.
    if De:
        beta = 0.5 / De
    visc = 1.

    simulationName = "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g" % (n, S1, S2, domainSize, De, beta, q, amplitude)

    # grid parameters
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)
    ds = grid.dx / 2.
    swimmerLength = .6
    wavelength = 2 * pi / swimmerLength
    waveVelocity = 2 * pi

    dt = 0.5 * ds * (2 ** (-q))
    finalTime = 20.
    if De:
        saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always", "saveSxxSup":"always",
                    "saveSyySup":"always", "saveSxySup":"always", "saveMeanXSpeed":"always", "saveEnergyE1":"always",
                    "saveEnergyE2":"always", "savePolimer":"end"}
    else:
        saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always", "saveMeanXSpeed":"always",
                    "saveEnergyE1":"always", "saveEnergyE2":"always"}

    func = '-%g * (x-%g) * (%g**2) * sin(%g*x + %g*t)' % (amplitude, swimmerLength, wavelength, wavelength, waveVelocity)

    if De:
        fluid = PolimericStokesFluid(grid, visc, beta, De)
    else:
        fluid = StokesFluid(grid, visc)

    finiteSwimmer = Swimmer(S1, S2, ds, func, 'x', (0., swimmerLength), grid, False, Point(0., domainSize / 2.), isCurvature=True)
    system = FluidStructure(fluid, finiteSwimmer, dt, 'Implicit')


    sim = SimulationManager(system, simulationName, finalTime, dt, .5, saveDict)
    sim.start()

if __name__ == '__main__':
    # resolution test
    S1 = 10000.0
    domainSize = 1
    q = 0
    De = 5
    amplitude = 0.08
    for S2 in [50.0, 500.0]:
        for n in [32, 64, 128, 256]:
            run_simulation(n, S1, S2, domainSize, q, De, amplitude)

    for S2 in [50.0, 500.0]:
        for n in [512, 1024]:
            run_simulation(n, S1, S2, domainSize, q, De, amplitude)

