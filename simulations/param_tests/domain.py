# -*- coding: utf-8 -*-
'''
Created on Mar 27, 2013

@author: felipe
'''
import matplotlib
matplotlib.use('Agg')

import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import PolimericStokesFluid, StokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import Grid, Point
from numpy import pi
from simulation_manager.manager import SimulationManager

if __name__ == '__main__':
    if len(sys.argv) < 8:
        print '''
            Console Parameters:
            n, S1, S2, domain_size, q, De, amp
        '''
        sys.exit()
    else:
        n = int(sys.argv[1])
        S1 = float(sys.argv[2])
        S2 = float(sys.argv[3])
        domainSize = float(sys.argv[4])
        q = float(sys.argv[5])
        De = float(sys.argv[6])
        amplitude = float(sys.argv[7])
    assert(S1 >= S2)

    beta = 0.5
    visc = 1.

    simulationName = "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g" % (n, S1, S2, domainSize, De, beta, q, amplitude)

    # grid parameters
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)
    ds = grid.dx / 2.
    S1 , S2 = 10 ** S1, 10 ** S2
    swimmerLength = .6
    wavelength = 2 * pi / swimmerLength
    waveVelocity = 2 * pi

    dt = (grid.dx ** q) * .5
    if De:
        finalTime = 20.
    else:
        finalTime = 2.

    func = '-%g * (1.-x/%g) * (%g**2) * sin(%g*x + %g*t)' % (amplitude, swimmerLength, wavelength, wavelength, waveVelocity)

    if De:
        fluid = PolimericStokesFluid(grid, visc, beta, De)
    else:
        fluid = StokesFluid(grid, visc)

    finiteSwimmer = Swimmer(S1, S2, ds, func, 'x', (0., swimmerLength), grid, False, Point(0., .5), isCurvature=True)
    system = FluidStructure(fluid, finiteSwimmer, dt, 'Implicit')

    saveDict = {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always"}
    sim = SimulationManager(system, simulationName, finalTime, dt, .5, saveDict)
    sim.start()
