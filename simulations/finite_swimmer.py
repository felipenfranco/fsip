# -*- coding: utf-8 -*-
'''
Created on Mar 27, 2013

@author: felipe
'''
import matplotlib
matplotlib.use('Agg')

import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import PolimericStokesFluid, StokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import Grid, Point
from math_objects.lagrangian_points import LagrangianPoints
from numpy import pi

if __name__ == '__main__':
    n = int(sys.argv[1])
    S1 = float(sys.argv[2])
    S2 = float(sys.argv[3])
    domainSize = float(sys.argv[4])
    q = float(sys.argv[5])
    De = float(sys.argv[6])
    amplitude = float(sys.argv[7])

#     n = 64
#     S1 = 6
#     S2 = 4
#     domainSize = 2.
#     q = 1
#     De = 5.
#     amplitude = 0.1
    assert(S1 >= S2)

    beta = 0.5
    visc = 1.
#     De = 5.

    simulationName = "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g" % (n, S1, S2, domainSize, De, beta, q, amplitude)

    # grid parameters
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)

    ds = grid.dx / 2.
    S1 , S2 = 10 ** S1, 10 ** S2
    swimmerLength = .6
    wavelength = 2 * pi / swimmerLength
    waveVelocity = 2 * pi

    dt = (grid.dx ** q) * .5
    finalTime = 20.

    func = '-%g * (1.-x/%g) * (%g**2) * sin(%g*x + %g*t)' % (amplitude, swimmerLength, wavelength, wavelength, waveVelocity)
#     func = "-%g*(x-%g)*(%g**2)*sin(%g*x+%g*t)" % (amplitude, swimmerLength, wavelength, wavelength, waveVelocity)

    if De:
        fluid = PolimericStokesFluid(grid, visc, beta, De)
    else:
        fluid = StokesFluid(grid, visc)

    finiteSwimmer = Swimmer(S1, S2, ds, func, 'x', (0., swimmerLength), grid, False, Point(0., .5), isCurvature=True)
    finiteSwimmer.saveShape('initial_%s.png' % (simulationName))

    system = FluidStructure(fluid, finiteSwimmer, dt, 'Implicit')

    time = utils.Time(finalTime)
    meanX = LagrangianPoints(0)
    noConstDist = LagrangianPoints(0)
    noConstCurv = LagrangianPoints(0)
    Dist = LagrangianPoints(0)
    Curv = LagrangianPoints(0)
#     pylab.ion()
#     pylab.show()
#     snap = 0
    while system.fluid.currentTime < finalTime:
        time.start()
        print "Energy:", finiteSwimmer.computeEnergy(printTerms=True)
        system.advanceInTime(dt, withReport=False)
        time.stop()
        currTime = system.fluid.currentTime
        time.printTimeInfo(dt, currTime)

        ncd, d, ncc, c = finiteSwimmer.computeEnergy(printTerms=False, splitTerms=True)

        meanX.addPoint(currTime, finiteSwimmer.x.mean())
        noConstDist.addPoint(currTime, ncd)
        noConstCurv.addPoint(currTime, ncc)
        Dist.addPoint(currTime, d)
        Curv.addPoint(currTime, c)



#         if system.fluid.currentTime > snap:
#             pylab.clf()
#             pylab.axis([0, 1., 0, 1.])
#             pylab.plot(finiteSwimmer.x, finiteSwimmer.y, '.')
#             pylab.draw()
#             snap += .05

    meanX.save("meanx_%s" % (simulationName))
    noConstDist.save("noconstdist_%s" % (simulationName))
    noConstCurv.save("noconstcurv_%s" % (simulationName))
    Dist.save("dist_%s" % (simulationName))
    Curv.save("curv_%s" % (simulationName))
    finiteSwimmer.saveShape('final_%s.png' % (simulationName))
