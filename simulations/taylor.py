'''
Created on Mar 27, 2013

@author: felipe
'''
import sys
import os
import numpy
import pylab
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
import utils
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid, PolimericStokesFluid
from swimmer.swimmer_class import Swimmer
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints


'''
In this simulation, we try to reproduce the analitical results by Taylor for Re = 0.
He finds out that for an infinite sheet y = b*sin(kx-wt), and small amplitudes b, the following holds:

1- The points describe a path of '8 figures'
2- if U is the mean velocity, then U = V*(.5 b^2 k^2(1-(19/16)b^2 k^2)), where V = (w/k)
3 - The dissipation of energy, averaged over a period, is W = 2 b^2 w^2 k mu, where mu is the viscosity.
'''


if __name__ == '__main__':
    for n in [128, 64, 32]:
        # grid parameters
        domainSize = 1.
        grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)
        finalTime = .1

        ds = grid.dx / 2.
        S1 , S2 = 10000.0, 500.0
        wavelength = 2 * pi
        waveVelocity = 2 * pi

        velocityList = []
        amplitudeList = [0.125]  # [0.0005 * (2 ** i) for i in range(8)]
        amplitudeList.reverse()
        dt = .5 * ds

        rotation = 0
        pylab.ion()
        for amplitude in amplitudeList:
            func = '%g * sin(%g*x + %g*t)' % (amplitude, wavelength, waveVelocity)

            fluid = StokesFluid(grid)
            sheet = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, True, Point(0., 0.), rotation)
            system = FluidStructure(fluid, sheet, dt, 'Implicit')

            time = utils.Time(finalTime)
            vel = LagrangianPoints(0)

            while system.fluid.currentTime < finalTime:
                time.start()
                print "Energy:", sheet.computeEnergy(True)
                system.advanceInTime(dt, False)
                time.stop()
                time.printTimeInfo(dt, system.fluid.currentTime)

                if rotation == pi / 2:
                    vel.addPoint(system.fluid.currentTime, sheet.velocity.y.mean())
                else:
                    vel.addPoint(system.fluid.currentTime, sheet.velocity.x.mean())
                pylab.clf()
                pylab.plot(vel.x, vel.y)
                pylab.draw()
                print "Vel", vel.y[-1]
            vel.save("taylor_n%d_amp%g_S1%g_S2%g" % (n, amplitude, S1, S2))

#     taylorList = [.5 * (amplitude ** 2) * (wavelength ** 2) * (1. - (19. / 16.) * (amplitude ** 2) * (wavelength ** 2)) for amplitude in amplitudeList]

