'''
Created on Mar 27, 2013

@author: felipe
'''
# import matplotlib
# matplotlib.use('Agg')
#
# import pylab

import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
import utils
from physic_objects.ib_collection import IBCollection
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import NavierStokesFluid, StokesFluid
from swimmer.swimmer_class import Swimmer
from physic_objects.ib import Wall
from math_objects.cartesian_grid import *
from numpy import pi, savez
from math_objects.lagrangian_points import LagrangianPoints
from physic_objects.force import TheterForce


'''
In this simulation, we try to reproduce the analitical results by Katz for Re = (rho*w)/(mu*k^2).
He finds out that for an infinite sheet y = b*sin(kx-wt), inside a channel and with small amplitudes b,
the following holds:

1- if U is the mean velocity, then U = (3w/k)[(h/b)^2 +2]
'''


if __name__ == '__main__':
    dens = 1.
    wavelength = 2 * pi
    waveVelocity = 2 * pi

    for reynolds in [0, 0.02, 2.]:
        if reynolds != 0:
            visc = (dens * waveVelocity) / (reynolds * (wavelength ** 2))
            print "Reynolds number of the flow", (dens * waveVelocity) / (visc * (wavelength ** 2))
        else:
            print "Reynolds number of the flow: 0"

        for n in [128, 64, 32]:
            # grid parameters
            domainSize = 1.
            grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)


            ds = grid.dx / 2.
            S1 , S2 = 1e6, 1e4
            ellasticConstant = 1e8


            amplitude = 0.025
            channelList = numpy.arange(5, 16) * amplitude
            print channelList / amplitude
            dt = ds

            if reynolds == 0:
                finalTime = 1.  # 1 period
            else:
                finalTime = 4.  # 4 periods

            for channel in channelList:
                func = '%g * sin(%g*x + %g*t) + .5' % (amplitude, wavelength, waveVelocity)

                if reynolds == 0:
                    fluid = StokesFluid(grid)
                else:
                    fluid = NavierStokesFluid(grid, visc, dens)

                sheet = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, True)
                upperWall = Wall(ds, Point(0., .5 + channel), Point(1., .5 + channel), grid, TheterForce(ellasticConstant))
                bottomWall = Wall(ds, Point(0., .5 - channel), Point(1., .5 - channel), grid, TheterForce(ellasticConstant))

                upperWall.boundaryForce.setTheterPoints(upperWall.x, upperWall.y)
                bottomWall.boundaryForce.setTheterPoints(bottomWall.x, bottomWall.y)

                ibs = IBCollection([sheet, upperWall, bottomWall], ds)

                system = FluidStructure(fluid, ibs, dt, 'Implicit')
                vel = LagrangianPoints(0)

                time = utils.Time(finalTime)
                snapshot = dt
                while system.fluid.currentTime < finalTime:
                    time.start()
                    print "Energy:\n", sheet.computeEnergy()
                    system.advanceInTime(dt)
                    time.stop()
                    time.printTimeInfo(dt, system.fluid.currentTime)
                    vel.addPoint(system.fluid.currentTime, sheet.velocity.x.mean())
                vel.save("katz_re%g_n%d_amp%g_S1%g_S2%g_chan%g" % (reynolds, n, amplitude, S1, S2, channel))

    #     katzList = [3. / ((channel / amplitude) ** 2 + 2) for channel in channelList]
    #     pylab.plot(numpy.array(channelList) / amplitude, katzList, label='Katz Formula')
    #     pylab.legend(loc='upper right')
    #     pylab.ioff()
    #     pylab.show()
    #     pylab.savefig("katz.png")

