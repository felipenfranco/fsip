'''
Created on Jun 11, 2013

@author: felipe
'''
import utils
import numpy
# from numpy.fft import fft2, ifft2
# from scipy.fftpack import fft2, ifft2
import pyfftw
import fast_fft_poisson
from time import time
pi = numpy.pi

class FFTPoisson:
    def __init__(self, n, domain_size):
        self.n = n
        self.domain_size = domain_size
        self.h = domain_size / float(n)

        # this is to solve the resulting linear system
        self._create_coef_matrix_fortran()

        # seting up the fftw plan
        print "Initializing FFTW PLAN... ",
        self.a = pyfftw.n_byte_align_empty((n, n), 16, 'complex128')
        self.b = pyfftw.n_byte_align_empty((n, n), 16, 'complex128')

        self.fft = pyfftw.FFTW(self.a, self.b, axes=(0, 1), threads=64)
        self.ifft = pyfftw.FFTW(self.b, self.a, direction='FFTW_BACKWARD', axes=(0, 1), threads=64)
        print "done!"

    def _create_coef_matrix(self):
        W = numpy.exp(2 * pi * 1j / float(self.n), dtype=numpy.complex128)
        Wm = 1.
        Wn = 1.
        coefMatrix = numpy.zeros((self.n, self.n), dtype=numpy.complex128)
        h = self.h

        for m in range(self.n):
            for n in range(self.n):
                coefMatrix[m, n] = (complex(4.) - (Wm + Wn + 1. / Wm + 1. / Wn))
                Wn *= W
            Wm *= W

        coefMatrix[0, 0] = 1.
        self.coefMatrix = h * h / coefMatrix
        self.coefMatrix[0, 0] = 0.


    def _create_coef_matrix_fortran(self):
        coefMatrix = numpy.zeros((self.n, self.n), dtype=numpy.complex128, order='F')
        fast_fft_poisson.fft_poisson.create_coef_matrix(coefMatrix, self.h)
        self.coefMatrix = coefMatrix

    def solve(self, rho, withTimeReport=False):
        t = time()
        self.a[:] = rho[:]
        self.fft()
        fftTime = time() - t

        t = time()
        self.apply_stencil_fortran(self.b)
        stencilTime = time() - t

        t = time()
#         solution = ifft2(rho_freq).real
        solution = self.ifft()
        ifftTime = time() - t

        if withTimeReport:
            print '''
            -----------------------------------
            Time to solve

            FFT     : %g
            Stencil : %g
            IFFT    : %g
            -----------------------------------
            ''' % (fftTime, stencilTime, ifftTime)
        return solution.real

    def apply_stencil(self, rho_freq):
        coef = self.coefMatrix
        for m in range(self.n):
            for n in range(self.n):
                rho_freq[m, n] *= coef[m, n]

    def apply_stencil_fortran(self, rho_freq):
        fast_fft_poisson.fft_poisson.solve_poisson(rho_freq.T, self.coefMatrix)



if __name__ == '__main__':
    import sympy
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from time import time

    last = 0.
    info = []
    for n in [64, 128, 256, 512, 1024, 2048, 4096, 8192]:
        domain_size = 1.
        h = 1. / float(n)
        x = numpy.linspace(0., domain_size, n, endpoint=False)
        y = numpy.linspace(0., domain_size, n, endpoint=False)

        xx, yy = numpy.meshgrid(x, y)


#         # set up the rhs
        a, b = sympy.var('a b')
        func = sympy.sin(2 * pi * a + 2 * pi * b)  # + sympy.cos(2 * pi * a + 2 * pi * b)  # periodic with period 1
        rhs = -(func.diff(a, a) + func.diff(b, b))

        lfunc = sympy.lambdify([a, b], func, 'numpy')
        rhsfunc = sympy.lambdify([a, b], rhs, 'numpy')

        rho = rhsfunc(xx, yy)

        print "N = ", n
        t = time()
        solver = FFTPoisson(n, domain_size)
        print "Time to initialize = %g" % (time() - t)
        t = time()
        u = solver.solve(rho, withTimeReport=True)
        print "Time to solve = %g" % (time() - t)
        u_exact = lfunc(xx, yy)

#         fig = plt.figure()
#         ax = fig.add_subplot(111, projection='3d')
#         ax.plot_wireframe(xx, yy, u)
#         ax.plot_wireframe(xx, yy, u_exact)
#         plt.show()

        info.append([abs(u - u_exact).max(), 1. / n, last / abs(u - u_exact).max()])
        last = abs(u - u_exact).max()

    print "Convergence test:"
    for data in info:
        print "Max error = %g, Discretization error = %g, Ratio = %g" % (data[0], data[1], data[2])
