from os.path import realpath
import utils


# FORTRAN CODE
codeCreateCoefMatrix = '''!    -*- f90 -*-
subroutine create_coef_matrix(coef,d,h)
implicit none

integer, intent(in) :: d
COMPLEX(kind=8), dimension(0:d-1, 0:d-1) , intent(inout) :: coef
double precision , intent(in) :: h
integer :: m, n
COMPLEX(kind=8) :: w,wm,wn

!f2py intent(inplace) :: coef
!f2py intent(hide) :: d
!f2py intent(in) :: h
!f2py threadsafe

w = exp(2.0d0 * 4.0d0*ATAN(1.0d0) * (0.0d0,1.0d0) / dble(d))
wm = 1.0d0
wn = 1.0d0

do n = 0, d-1
    do m = 0, d-1
        coef(m, n) = (4.0d0 - (wm + wn + 1.0d0 / wm + 1.0d0 / wn))
        wm =wm*w
    end do
    wn =wn*w
end do

coef(0, 0) = 1.
coef = h * h / coef
coef(0, 0) = 0.

end subroutine create_coef_matrix
'''


codeApplyLaplacianStencil = '''!    -*- f90 -*-
subroutine solve_poisson(rho_freq,coef,d)
use omp_lib
implicit none

integer, intent(in) :: d
COMPLEX(kind=8), dimension(0:d-1, 0:d-1) , intent(inout) :: rho_freq
COMPLEX(kind=8), dimension(0:d-1, 0:d-1) , intent(inout) :: coef
integer :: m, n, num


!f2py intent(inplace) :: rho_freq, coef
!f2py intent(hide) :: d
!f2py threadsafe

!$OMP PARALLEL
!$OMP DO
do n = 0, d-1
    do m = 0, d-1
        rho_freq(m, n) = rho_freq(m, n) * coef(m,n)
    end do
end do
!$OMP END DO
!$OMP END PARALLEL

end subroutine solve_poisson
'''

codeFFTPoissonHeader = '''!    -*- f90 -*-
module fft_poisson
contains
'''


codeFFTPoisson = codeFFTPoissonHeader + codeApplyLaplacianStencil + codeCreateCoefMatrix + "end module"


def retrieveCode():
    name_of_current_file = realpath(__file__)
    codeInfo = [(codeFFTPoisson, "fast_fft_poisson", name_of_current_file)]
    return codeInfo
