'''
Created on Jun 6, 2013

@author: felipe
'''

class ProcessDispatcher:
    '''
        Given a list of SimulationManager objects, dispatch them according to specifications.
        If a given problem fails to execute due to divergence, detect that and reload the problem
        with a reduction in the time step parameter dt.

        There is room for improvement: dispatch the simulations if there is processors idle, etc...
    '''


    def __init__(self, simulationList):
        self.simulationList = simulationList
