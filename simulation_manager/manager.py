'''
Created on Jun 6, 2013

@author: felipe
'''
import utils
import os
from math_objects.lagrangian_points import LagrangianPoints
import numpy

class SimulationManager:
    '''
        Plugin a system and some specifications of the simulation.
        The manager runs the simulation and saves it periodically, according to specs.
        Can load and restart from savefile.

        Provides functions to common actions, like save the velocity field, immersed boundary, etc...
        If you need something to be saved, take one of the functions as a template and modify accordingly.
    '''

    def __init__(self, system, name, finalTime, dt, saveInterval=1., saveDict={}):
        '''
            system -> an object of the FluidStructure class
            name -> string representing the name of the simulation
            saveInterval = T -> save every T time ellapsed from last save
            saveDict = dictionary of string(function_name) : string('when' parameter)
                            when -> string:
                            * "end" : save at the end of the simulation
                            * "always" : save at every time step
                            * "begin and end" : save at the begin and end of simulation

                            Ex: saveDict = {"saveVelocity":"end", "saveEnergy":"always"}
                            look for the save#### functions to see whats available
        '''
        self.system = system
        self.name = name
        self.finalTime = finalTime
        self.dt = dt
        self.saveInterval = saveInterval
        self.saveTime = 0.
        self.saveDict = saveDict
        self._parseSaveDict()
        self.path = utils.rootPath + "//cached_simulations//"


    def _parseSaveDict(self):
        beginSave = []
        endSave = []
        alwaysSave = []
        alwaysSaveLagrangian = []

        for functionName in self.saveDict.iterkeys():
            func = self._getFunction(functionName)
            if func:
                if self.saveDict[functionName] == 'begin':
                    beginSave.append(func)
                elif self.saveDict[functionName] == 'end':
                    endSave.append(func)
                elif self.saveDict[functionName] == 'always':
                    alwaysSave.append(func)
                    alwaysSaveLagrangian.append(LagrangianPoints(0))
                    alwaysSaveLagrangian[-1].myName = functionName
                elif self.saveDict[functionName] == 'begin and end':
                    beginSave.append(func)
                    endSave.append(func)
        self.beginSave = beginSave
        self.endSave = endSave
        self.alwaysSave = alwaysSave
        self.alwaysSaveLagrangian = alwaysSaveLagrangian


    def _getFunction(self, name):
        if name == "saveEnergyE1": return self.saveEnergyE1
        elif name == "saveEnergyE2": return self.saveEnergyE2
        elif name == "saveMeanXPosition": return self.saveMeanXPosition
        elif name == "saveMeanXForce": return self.saveMeanXForce
        elif name == "saveMeanYForce": return self.saveMeanYForce
        elif name == "saveSxxSup": return self.saveSxxSup
        elif name == "saveSyySup": return self.saveSyySup
        elif name == "saveSxySup": return self.saveSxySup
        elif name == "saveMeanXSpeed": return self.saveMeanXSpeed
        elif name == "savePolimer" : return self.savePolimer

        else: sys.exit("The function:%s does not exist in class SimulationManager" % (name))
        return 0

    def _saveAlways(self):
        for m in range(len(self.alwaysSaveLagrangian)):
            self.alwaysSave[m](self.alwaysSaveLagrangian[m])

    def _checkForSave(self):
        if self.system.fluid.currentTime > self.saveTime:
            print "Saving Simulation....",
            self.save()
            self.saveTime += self.saveInterval
            print "Done."

    def _finalSaveAlways(self):
        path = self.path
        for i in range(len(self.alwaysSaveLagrangian)):
            myName = self.alwaysSaveLagrangian[i].myName
            self.alwaysSaveLagrangian[i].save(path + self.name + myName)

    def _finalSaveEnd(self):
        for func in self.endSave: func()

    def save(self):
        path = self.path
        open(path + self.name + "flag", 'w').close()
        for i in range(len(self.alwaysSaveLagrangian)):
            self.alwaysSaveLagrangian[i].save(path + self.name + str(i))
        self.system.saveState(path + self.name)


    def load(self):
        print "Loading Simulation....",
        path = self.path
        if os.path.exists(path + self.name + "flag"):
            for i in range(len(self.alwaysSaveLagrangian)):
                self.alwaysSaveLagrangian[i].load(path + self.name + str(i) + ".npz")

            self.system.loadState(path + self.name)
            self.saveTime = self.system.fluid.currentTime + self.saveInterval
            print "Done."
        else:
            print "dont exist!!!"

    def start(self):
        self.load()

        time = utils.Time(self.finalTime)
        while self.system.fluid.currentTime < self.finalTime:
            time.start()
            self.system.advanceInTime(self.dt, withReport=False)
            time.stop()
            time.printTimeInfo(self.dt, self.system.fluid.currentTime)
            self._saveAlways()
            self._checkForSave()

        self._finalSaveAlways()
        self._finalSaveEnd()


    def saveEnergyE1(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            E1, E2 = self.system.ib.computeEnergy(printTerms=False, splitTerms=True)
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, E1)

    def saveEnergyE2(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            E1, E2 = self.system.ib.computeEnergy(printTerms=False, splitTerms=True)
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, E2)

    def saveMeanXPosition(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, self.system.ib.x.mean())

    def saveMeanXForce(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, self.system.ib.force.x.mean())

    def saveMeanYForce(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, self.system.ib.force.y.mean())

    def saveSxxSup(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, abs(self.system.fluid.polimer.Sxx.data).max())

    def saveSyySup(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, abs(self.system.fluid.polimer.Syy.data).max())

    def saveSxySup(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, abs(self.system.fluid.polimer.Sxy.data).max())

    def saveMeanXSpeed(self, alwaysLagrangian=None):
        if alwaysLagrangian:
            alwaysLagrangian.addPoint(self.system.fluid.currentTime, self.system.ib.velocity.x.mean())

    def savePolimer(self):
        Sxx = self.system.fluid.polimer.Sxx.data
        Syy = self.system.fluid.polimer.Syy.data
        Sxy = self.system.fluid.polimer.Sxy.data
        numpy.savez(self.path + self.name + "savePolimer.npz", Sxx=Sxx, Syy=Syy, Sxy=Sxy)


if __name__ == '__main__':
    import sys
    from math_objects.cartesian_grid import Grid, Point
    import numpy
    from physic_objects.fluid import PolimericStokesFluid, StokesFluid
    from swimmer.swimmer_class import Swimmer
    from physic_objects.fluid_structure import FluidStructure

    pi = numpy.pi

    n = 32
    S1 = 6
    S2 = 4
    domainSize = 2.
    q = 1
    De = 5
    amplitude = 0.1
    assert(S1 >= S2)

    beta = 0.5
    visc = 1.
#     De = 5.

    simulationName = "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g" % (n, S1, S2, domainSize, De, beta, q, amplitude)

    # grid parameters
    grid = Grid(Point(0., 0.), domainSize, domainSize, n, n, 2)

    ds = grid.dx / 2.
    S1 , S2 = 10 ** S1, 10 ** S2
    swimmerLength = .6
    wavelength = 2 * pi / swimmerLength
    waveVelocity = 2 * pi

    dt = (grid.dx ** q) * .5
    finalTime = .5

    func = '-%g * (1.-x/%g) * (%g**2) * sin(%g*x + %g*t)' % (amplitude, swimmerLength, wavelength, wavelength, waveVelocity)
#     func = "-%g*(x-%g)*(%g**2)*sin(%g*x+%g*t)" % (amplitude, swimmerLength, wavelength, wavelength, waveVelocity)

    if De:
        fluid = PolimericStokesFluid(grid, visc, beta, De)
    else:
        fluid = StokesFluid(grid, visc)

    finiteSwimmer = Swimmer(S1, S2, ds, func, 'x', (0., swimmerLength), grid, False, Point(0., .5), isCurvature=True)
    finiteSwimmer.saveShape('initial_%s.png' % (simulationName))

    system = FluidStructure(fluid, finiteSwimmer, dt, 'Implicit')

    simman = SimulationManager(system, "teste", finalTime, dt, .2, {"saveMeanXPosition":"always", "saveMeanXForce":"always", "saveMeanYForce":"always"})

    simman.start()

