'''
Created on May 21, 2013

@author: felipe
'''
import utils
import sympy
from swimmer_class import Swimmer
from physic_objects.force import EnergyForce
from physic_objects.ib import IB
import sys
import numpy
from math_objects.cartesian_grid import Point, Grid
from math_objects.lagrangian_points import LagrangianPoints


class LaugaSwimmer(Swimmer):

    def __init__(self, S1, S2, ds, lenght, grid, startPoint=Point(0, 0)):
        self.ds = ds
        self.grid = grid
        self.isPeriodic = False
        self.orientation = 1

        # computing number of points of each part
        numberOfPoints = int(lenght / ds) + 1
        headNumberOfPoints = int(float(numberOfPoints) / 3.)
        if headNumberOfPoints % 2: headNumberOfPoints += 1
        tailNumberOfPoints = numberOfPoints - headNumberOfPoints


        self.headNumberOfPoints = headNumberOfPoints
        self.tailNumberOfPoints = tailNumberOfPoints

        self.S1 = numpy.array([S1 / ds for _ in range(numberOfPoints)])
        self.S2 = numpy.array([S2 * ds for _ in range(numberOfPoints)])
        self.S2[:tailNumberOfPoints] = (S2 / 100.) * ds  # the tail has small prescript curvature

        self.boundaryForce = EnergyForce()

        IB.__init__(self, numberOfPoints)
        self.ID = "LaugaSwimmer"

        self.paramSpace = numpy.arange(numberOfPoints) * ds
        self.x = self.paramSpace + startPoint.x
        self.y = numpy.array([startPoint.y for _ in range(numberOfPoints)])

        self.c = numpy.zeros(numberOfPoints)
        self.h = self.ds

        if reduce: self.reduceEnergy()
        self.checkDistance()
        sys.stdout.flush()


    def computeForce(self):
        self.boundaryForce.computeForce(self)
        self.addMagneticForce()


    def addMagneticForce(self, magnitude=1e+1):
        half = self.headNumberOfPoints / 2
        tnbp = self.tailNumberOfPoints
        self.force.y[tnbp:tnbp + half] += self.orientation * magnitude
        self.force.y[tnbp + half:] -= self.orientation * magnitude


    def changeOrientation(self):
        self.orientation *= -1


    def updateData(self, currentTime):
        self.c = numpy.zeros(self.numberOfPoints)


if __name__ == '__main__':
    import pylab
    from physic_objects.fluid import StokesFluid, PolimericStokesFluid
    from physic_objects.fluid_structure import FluidStructure

    n = 32
    grid = Grid(Point(0, 0), 1., 1., n, n, 2)
    s_til = 3
    S1, S2 = 10 ** (s_til + 2), 10 ** (s_til)
    ds = grid.dx / 2.
    dt = ds * .25

    l = LaugaSwimmer(S1, S2, ds, .3, grid, Point(0.1, .5))

    pylab.show()
    fluid = StokesFluid(grid)
#     fluid = PolimericStokesFluid(grid, 1., 5, 5)
    system = FluidStructure(fluid, l, dt, "Implicit", "Matrix")


#     pylab.plot(l.x, l.y)
#     pylab.show()

    pylab.ion()
    pylab.show()
    snap = .2
    meanx = LagrangianPoints(0)
    for i in range(1000):
        system.advanceInTime(dt, False)
        meanx.addPoint(fluid.currentTime, l.x.mean())

        if fluid.currentTime > snap:
            l.changeOrientation()
            snap += .2
        pylab.clf()
#         pylab.plot(meanx.x, meanx.y)
        pylab.axis([0, .5, 0.45, 0.55])
        pylab.plot(l.x[:l.tailNumberOfPoints], l.y[:l.tailNumberOfPoints], '.')
        pylab.plot(l.x[l.tailNumberOfPoints:], l.y[l.tailNumberOfPoints:], 'x')
        pylab.draw()

