'''
Created on Mar 24, 2012

@author: felipe
'''
import utils
from numpy import sum, zeros
import swimmer_diff
import timeit
import numpy

#=======================================================================================================================
# Energy Functions
#=======================================================================================================================

def _periodicDistanceSummation(X, ds, n, s1, xsize, ysize):
    '''
        Peridioc in the x (horizontal) component
    '''
    x , y = X[:n], X[n:]

    summation = _nonPeriodicDistanceSummation(X, ds, n, s1)

    distance = ((x[0] + xsize - x[-1]) ** 2 + (y[0] + ysize - y[-1]) ** 2) ** (.5)
    summation += s1[0] * (distance - ds) ** 2
    return summation


def _periodicCurvatureSummation(X, c, n, s2, xsize, ysize):
    '''
        Peridioc in the x (horizontal) component
    '''
    x, y = X[:n], X[n:]

    summation = _nonPeriodicCurvatureSummation(X, c, n, s2)

    k = 0
    cross_product = (x[k + 1] - x[k]) * (y[k] + ysize - y[k - 1]) - (y[k + 1] - y[k]) * (x[k] + xsize - x[k - 1])
    norm = ((x[k + 1] + xsize - x[k - 1]) ** 2 + (y[k + 1] + ysize - y[k - 1]) ** 2) ** (1.5)
    summation += s2[k] * (-8.0 * cross_product / norm - c[k]) ** 2

    k = n - 1
    cross_product = (x[(k + 1) % n] + xsize - x[k]) * (y[k] - y[k - 1]) - (y[(k + 1) % n] + ysize - y[k]) * (x[k] - x[k - 1])
    norm = ((x[(k + 1) % n] + xsize - x[k - 1]) ** 2 + (y[(k + 1) % n] + ysize - y[k - 1]) ** 2) ** (1.5)
    summation += s2[k] * (-8.0 * cross_product / norm - c[k]) ** 2

    return summation



def _nonPeriodicDistanceSummation(X, ds, n, s1):
    x , y = X[:n], X[n:]
    distance = ((x[1:n] - x[0:(n - 1)]) ** 2 + (y[1:n] - y[0:(n - 1)]) ** 2) ** (.5)
    summation = sum(s1[1:n] * (distance - ds) ** 2)
    return summation


def _nonPeriodicCurvatureSummation(X, c, n, s2):
    x, y = X[:n], X[n:]

    curvature = (-8.0 * ((x[2:n] - x[1:n - 1]) * (y[1:n - 1] - y[0:n - 2]) - (y[2:n] - y[1:n - 1]) * (x[1:n - 1] - x[0:n - 2])) / (((x[2:n] - x[0:n - 2]) ** 2 + (y[2:n] - y[0:n - 2]) ** 2) ** (1.5)))
    return sum(s2[1:n - 1] * (curvature - c[1:n - 1]) ** 2)



def energy(X, c, ds, n, xsize, ysize, s1, s2, isPeriodic, printTerms=False, splitTerms=False):
    if isPeriodic:
        # decides if its x or y periodic
        if abs(X[0] - X[n - 1]) > ds * 4:
            ysize = 0.
        else:
            xsize = 0.

        dist = _periodicDistanceSummation(X, ds, n, s1, xsize, ysize)
        curv = _periodicCurvatureSummation(X, c, n, s2, xsize, ysize)
    else:
        dist = _nonPeriodicDistanceSummation(X, ds, n, s1)
        curv = _nonPeriodicCurvatureSummation(X, c, n, s2)

    if printTerms:
        print "Distance  -> No const = %g, With const = %g" % (0.5 * dist / s1.mean(), .5 * dist)
        print "Curvature -> No const = %g, With const = %g" % (0.5 * curv / s2.mean(), .5 * curv)

    if splitTerms:
        E1 = (dist / s1.mean()) / ds
        E2 = (curv / s2.mean()) * ds
        return E1, E2
    else:
        return 0.5 * dist + 0.5 * curv


#=======================================================================================================================
# Hessian Functions
#=======================================================================================================================
from os.path import realpath, join, dirname, exists
fortranFile = join(dirname(realpath(__file__)), 'fortran_energy_hessian.f90')

if not exists(fortranFile):
    print 'Energy function hessian and gradient dont exists in fortran code. Generating...'
    symbEnergy = swimmer_diff.SymbolicEnergy()
    symbEnergy.createFortran(fortranFile)
    print 'Done!'

codeEnergy = open(fortranFile, 'r').read()

def retrieveCode():
    name_of_current_file = realpath(__file__)
    codeInfo = [(codeEnergy, 'fast_energy', name_of_current_file)]
    return codeInfo



if __name__ == '__main__':
    import fast_energy
    import sympy
    from math_objects.cartesian_grid import Grid, Point
    from numpy import pi
    from swimmer.swimmer_class import Swimmer
    n = 512

    # grid parameters
    sizeX, sizeY = 1., 1.
    nx, ny = int(n * sizeX), int(n * sizeY)
    grid = Grid(Point(0., 0.), sizeX, sizeY, nx, ny, 2)

    # swimmer parameter
    amplitude = 0.125
    ds = grid.dx / 2.
    S1 , S2 = 10e4, 10e4
    wavelength = 2 * pi
    waveVelocity = 2 * pi
    dt = ds
    func = '%g * sin(%g*x - %g*t) + 0.5' % (amplitude, wavelength, waveVelocity)

    # Newtonian
    swimmerAD = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, False, reduce=False)
    swimmerFortran = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, False, reduce=False)
    swimmerSparse = Swimmer(S1, S2, ds, func, 'x', (0., 1.), grid, False, reduce=False)
    swimmerAD.setComputationType('ad')
    swimmerFortran.setComputationType('fortran')
    swimmerSparse.setComputationType('sparse')

    swimmerAD.computeForce()
    swimmerFortran.computeForce()
    swimmerAD.computeHessian()
    swimmerFortran.computeHessian()
    swimmerSparse.computeForce()
    swimmerSparse.computeHessian()


    print "AD FORTRAN:"
    print "Max error in gradx:", abs(swimmerFortran.force.x - swimmerAD.force.x).max()
    print "Max error in grady:", abs(swimmerFortran.force.y - swimmerAD.force.y).max()
    print "gradx Min, Max, Mean:", swimmerAD.force.x.min(), swimmerAD.force.x.max(), swimmerAD.force.x.mean()
    print "Relative error gradx:", abs(swimmerFortran.force.x - swimmerAD.force.x).max() / abs(swimmerAD.force.x).max()
    print "Relative error grady:", abs(swimmerFortran.force.y - swimmerAD.force.y).max() / abs(swimmerAD.force.y).max()
    print
    print "Max error in Hessian:", abs(swimmerAD.hessian - swimmerFortran.hessian).max()
    print "Hessian Min, Max, Mean:", swimmerAD.hessian.min(), swimmerAD.hessian.max(), swimmerAD.hessian.mean()
    print "Relative error:", abs(swimmerAD.hessian - swimmerFortran.hessian).max() / abs(swimmerAD.hessian).max()
    print
    print
    print
    print "AD SPARSE:"
    print "Max error in Hessian:", abs(swimmerAD.hessian - swimmerSparse.hessian).max()
    print "Hessian Min, Max, Mean:", swimmerAD.hessian.min(), swimmerAD.hessian.max(), swimmerAD.hessian.mean()
    print "Relative error:", abs(swimmerAD.hessian - swimmerSparse.hessian).max() / abs(swimmerAD.hessian).max()
    print
    print
    print
    print "FORTRAN SPARSE:"
    print "Max error in Hessian:", abs(swimmerFortran.hessian - swimmerSparse.hessian).max()
    print "Hessian Min, Max, Mean:", swimmerAD.hessian.min(), swimmerAD.hessian.max(), swimmerAD.hessian.mean()
    print "Relative error:", abs(swimmerFortran.hessian - swimmerSparse.hessian).max() / abs(swimmerFortran.hessian).max()
    print
    print
    print
    print 'AD Hessian Time:', timeit.timeit('swimmerAD.computeHessian()', setup='from __main__ import swimmerAD', number=1)
    print 'Fortran Hessian Time:', timeit.timeit('swimmerFortran.computeHessian()', setup='from __main__ import swimmerFortran', number=1)
    print 'Sparse Hessian Time:', timeit.timeit('swimmerSparse.computeHessian()', setup='from __main__ import swimmerSparse', number=100) / 100.
