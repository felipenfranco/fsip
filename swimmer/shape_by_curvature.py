'''
Created on Apr 27, 2013

@author: felipe
'''
import utils
import sympy
import numpy
from scipy.integrate import quad
from math_objects.cartesian_grid import Point
ceil = numpy.ceil
floor = numpy.floor

def lenght(func, param, paramRange):
    '''compute the lenght of the function func from paramRange[0] to paramRange[1]'''
    func = sympy.sympify(func)
    param = sympy.sympify(param)
    lenghtFunc = sympy.lambdify([param, sympy.var('t')], sympy.sqrt(1 + func.diff(param) ** 2) , 'numpy')
    return quad(lenghtFunc, paramRange[0], paramRange[1], args=(0.))[0]


class ShapebyCurvature:
    def __init__(self, curvature, param, paramRange, startPoint, theta):
        self.curvature = sympy.sympify(curvature)
        self.param = sympy.var(param)
        self.paramRange = paramRange
        self.startPoint = startPoint
        self.theta = theta

        self.t = sympy.var('t')

        phi = sympy.integrate(self.curvature, self.param)
        self.dxds = sympy.lambdify([self.param, self.t], sympy.cos(phi), 'numpy')
        self.dyds = sympy.lambdify([self.param, self.t], sympy.sin(phi), 'numpy')
        self.lambdaCurvature = sympy.lambdify([self.param, self.t], self.curvature, 'numpy')


    def computeDiscretePoints(self, ds, time=0.):
        p0, p1 = self.paramRange
        self.numberPoints = int(floor(float(p1 - p0) / ds) + 1)
        self.paramSpace = numpy.array([i * ds for i in range(self.numberPoints)]) + p0
        self.x = numpy.array([quad(self.dxds, p0, p, args=(time))[0] for p in self.paramSpace])
        self.y = numpy.array([quad(self.dyds, p0, p, args=(time))[0] for p in self.paramSpace])


    def getInitialPoints(self):
        return self.x + self.startPoint.x, self.y + self.startPoint.y

    def targetCurvatureExact(self, x, y, time):
        return self.lambdaCurvature(self.paramSpace, time)

if __name__ == '__main__':
    import pylab

    A = 0.125
    pi = numpy.pi
    k = 2 * pi
    w = 2 * pi
    ds = 1. / 64


    tamanho = lenght("%g*sin(%g*s)" % (A, k), "s", [0, 1])

    k = k / tamanho
    curv = "-%g*(%g**2)*sin(%g*s-%g*t)" % (A, k, k, w)
    shp = ShapebyCurvature(curv, 's', [0, tamanho], Point(0, .5), 0)

    pylab.ion()
    for time in [i * 0.01 for i in range(1000)]:
        shp.computeDiscretePoints(ds, time)
        x, y = shp.getInitialPoints()
        dist = ((x[0:-1] - x[1:]) ** 2 + (y[0:-1] - y[1:]) ** 2) ** .5
        print dist.max(), dist.min(), dist.mean(), ds

        pylab.clf()
        pylab.axis([0, 1, 0, 1.2])
        pylab.plot(x, y)
        pylab.draw()
