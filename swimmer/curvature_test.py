'''
Created on Apr 27, 2013

@author: felipe
'''

if __name__ == '__main__':
    import sympy, numpy, pylab
    from scipy.integrate import quad
    pi = numpy.pi
    A = 0.05
    k = 2*pi
    w = 2*pi
    ds = .01
    paramRange = [ds * i for i in range(int(1. / ds))]
    inverseParam = [ds * i for i in range(int(1. / ds))]
    inverseParam.reverse()
    times = [0.01 * i for i in range(1000)]
    
    t, s = sympy.var('t s')
    curvature = -A * k * k * (s - 1) * sympy.sin(k * s + w * t)
    theta = sympy.integrate(curvature, s)

    dxds = sympy.lambdify([s, t], sympy.cos(theta))
    dyds = sympy.lambdify([s, t], sympy.sin(theta))

    pylab.ion()

    for time in times:
        xc = [quad(dxds, 0, p, args=(time))[0] for p in paramRange]
        yc = [quad(dyds, 0, p, args=(time))[0] + 0.5 for p in paramRange]

        x = numpy.array(paramRange)
        y = A * (1 - x) * numpy.cos(k * x + w * time) + 0.5

        pylab.clf()
        pylab.axis([0, 1, 0, 1])
        pylab.plot(xc, yc, 'red')
        pylab.plot(inverseParam, y, 'blue')
        pylab.draw()
