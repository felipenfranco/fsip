'''
Created on Mar 28, 2013

@author: felipe

Generates the functions necessary for the gradient and hessian of the swimmer energy function.

The goal here is to generate the symbolic gradient and hessian, and take advantage of the
sparsity of the hessian and it's patterns to generate the hessian as fast as possible in fortran
low level code, generated from the symbolic derivatives.

This part of the code (the fortran code generated here) only takes in consideration the inner part
of the hessian and gradient, that is, the boundary cases are ommited and are taken care of in the physics_objects.force
module, using autommatic differentiation of a small part of the swimmer energy (that has the first and last points).
This choice of design reduces complexity and dont add overhead in the computations, since the size of the hessian and
gradient computed by autommatic differentiation is very small (n ~ 10).

Heavily uses sympy and f2py to automate the process.
A template energy using only the points used in the derivatives is created using sympy,
the derivatives are computed symbolicaly, then the fortran code is generated.
Then a hard-coded fortran part calls the generated fortran functions and assemble the hessian and
gradient inner parts. The rest is taken care in the physics_objects.force module, as stated above.

NOTE: if you look ate the energy function equation, you can see that the derivatives (both first order and second order)
of the energy function at a given point xk are dependent only on the points: k-2, k-1, k, k+1, k+2. We use this information
to generate the symbolic energy, using only as many terms as needed to have all the terms that contains these variables.

NOTE: in generating the hessian, the fact that is is symmetric is used, so we only need to compute the derivatives to the
left of the center point, for the second derivatives involving only x's or y's componentes.
The derivatives involving x AND y componentes must ALL be computed.
'''

import utils
import swimmer
import sympy
import numpy
from os.path import exists

class SymbolicEnergy:
    def __init__(self):
        self.createVariables()
        self.createEnergy()
        self.createGrad()
        self.createHessian()

        self.assignVariables = '''
        x_l1 = x(i-1)
        x_l2 = x(i-2)
        x_k  = x(i)
        x_r1 = x(i+1)
        x_r2 = x(i+2)

        y_l1 = y(i-1)
        y_l2 = y(i-2)
        y_k  = y(i)
        y_r1 = y(i+1)
        y_r2 = y(i+2)

        c_l1 = c(i-1)
        c_l2 = c(i-2)
        c_k  = c(i)
        c_r1 = c(i+1)
        c_r2 = c(i+2)

        s1_l1 = s1(i-1)
        s1_l2 = s1(i-2)
        s1_k  = s1(i)
        s1_r1 = s1(i+1)
        s1_r2 = s1(i+2)

        s2_l1 = s2(i-1)
        s2_l2 = s2(i-2)
        s2_k  = s2(i)
        s2_r1 = s2(i+1)
        s2_r2 = s2(i+2)
        '''

        self.header = '''! -*- f90 -*-
        module hessian
        contains
        '''

        self.footer = '''
        end module
        '''

    def createVariables(self):
        # _k  -> the center element
        # _ln -> n'th element left of x_k
        # _rn -> n'th element right of x_k
        indexes = ['_l3', '_l2', '_l1', '_k', '_r1', '_r2', '_r3']
        self.x = [sympy.var('x%s' % idx) for idx in indexes]
        self.y = [sympy.var('y%s' % idx) for idx in indexes]
        self.X = numpy.concatenate([self.x, self.y])
        self.c = [sympy.var('c%s' % idx) for idx in indexes]
        self.ds = sympy.var('ds')
        self.s1 = [sympy.var('s1%s' % idx) for idx in indexes]
        self.s2 = [sympy.var('s2%s' % idx) for idx in indexes]
        self.centerIdx = 3  # index of the center element in the x,y,c arrays
        self.shiftVar = len(indexes)

    def createEnergy(self):
        print 'Computing symbolic Energy...',
        self.energy = swimmer.energy(self.X, self.c, self.ds, len(self.c), 0.0, 0.0, self.s1, self.s2, False)
        print ' Done.'

    def createGrad(self):
        print 'Computing symbolic Gradient...',
        xk = self.x[self.centerIdx]
        yk = self.y[self.centerIdx]
        self.gradx = self.energy.diff(xk)
        self.grady = self.energy.diff(yk)
        print ' Done.'

    def gradCode(self):
        gradxCode = sympy.fcode(self.gradx, assign_to='gradx(i)', source_format='free')
        gradyCode = sympy.fcode(self.grady, assign_to='grady(i)', source_format='free')
        code = '''
        subroutine compute_grad(x,y,c,gradx,grady,n,ds,s1,s2)
            double precision, dimension(n), intent(in) :: x,y,c,s1,s2
            double precision, dimension(n), intent(inout) :: gradx,grady
            integer, intent(in) :: n
            integer :: i
            double precision, intent(in) :: ds
            double precision :: x_l1, y_l1, c_l1, s1_l1, s2_l1, x_l2, y_l2, c_l2, s1_l2, s2_l2
            double precision :: x_r1, y_r1, c_r1, s1_r1, s2_r1, x_r2, y_r2, c_r2, s1_r2, s2_r2
            double precision :: x_k, y_k, c_k, s1_k, s2_k

            !f2py intent(inplace) :: gradx,grady,x,y,c,s1,s2
            !f2py intent(hide) :: n

            do i = 3, n-2
                %s
                %s
                %s

            end do
        end subroutine
        ''' % (self.assignVariables, gradxCode, gradyCode)

        return code.replace('-1.5d0', '(-1.5d0)')  # avoid the warning from gfortran

    def createHessian(self):
        print 'Computing symbolic Hessian...',
        self.hessian = sympy.hessian(self.energy, self.X)
        print ' Done.'

    def hessianCode(self):
        idx = self.centerIdx
        s = self.shiftVar
        xk_xk = sympy.fcode(self.hessian[idx, idx], assign_to='H(i,i)', source_format='free')
        xk_xl1 = sympy.fcode(self.hessian[idx, idx - 1], assign_to='H(i,i-1)', source_format='free')
        xk_xl2 = sympy.fcode(self.hessian[idx, idx - 2], assign_to='H(i,i-2)', source_format='free')

        yk_yk = sympy.fcode(self.hessian[s + idx, s + idx], assign_to='H(n+i,n+i)', source_format='free')
        yk_yl1 = sympy.fcode(self.hessian[s + idx, s + idx - 1], assign_to='H(n+i,n+i-1)', source_format='free')
        yk_yl2 = sympy.fcode(self.hessian[s + idx, s + idx - 2], assign_to='H(n+i,n+i-2)', source_format='free')

        yk_xk = sympy.fcode(self.hessian[s + idx, idx], assign_to='H(n+i,i)', source_format='free')
        yk_xl1 = sympy.fcode(self.hessian[s + idx, idx - 1], assign_to='H(n+i,i-1)', source_format='free')
        yk_xl2 = sympy.fcode(self.hessian[s + idx, idx - 2], assign_to='H(n+i,i-2)', source_format='free')
        yk_xr1 = sympy.fcode(self.hessian[s + idx, idx + 1], assign_to='H(n+i,i+1)', source_format='free')
        yk_xr2 = sympy.fcode(self.hessian[s + idx, idx + 2], assign_to='H(n+i,i+2)', source_format='free')


        code = '''
        subroutine compute_hessian(x,y,c,H,n,ds,s1,s2)
            double precision, dimension(n), intent(in) :: x,y,c,s1,s2
            double precision, dimension(2*n,2*n), intent(inout) :: H
            integer, intent(in) :: n
            integer :: i
            double precision, intent(in) :: ds
            double precision :: x_l1, y_l1, c_l1, s1_l1, s2_l1, x_l2, y_l2, c_l2, s1_l2, s2_l2
            double precision :: x_r1, y_r1, c_r1, s1_r1, s2_r1, x_r2, y_r2, c_r2, s1_r2, s2_r2
            double precision :: x_k, y_k, c_k, s1_k, s2_k

            !f2py intent(inplace) :: H,x,y,c,s1,s2
            !f2py intent(hide) :: n

            do i = 3, n-2
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
            end do
        end subroutine
        ''' % (self.assignVariables, xk_xk, xk_xl1, xk_xl2, yk_yk, yk_yl1, yk_yl2, yk_xk, yk_xl1, yk_xl2, yk_xr1, yk_xr2)

        return code.replace('-1.5d0', '(-1.5d0)').replace('-3.0d0', '(-3.0d0)')  # avoid the warning from gfortran

    def makeSymmetricCode(self):
        code = '''
        subroutine fill_upper_part(H, n)
            double precision , dimension(0:n-1,0:n-1) , intent(inout) :: H
            integer , intent(in) :: n
            integer :: row , col
            !f2py intent(inplace) :: H
            !f2py intent(hide) :: n
            do row = 0 , n-1
                do col = 0 , row
                    H(col,row) = H(row,col)
                end do
            end do
        end subroutine
        '''
        return code

    def assemble(self):
        print 'Assembling code...',
        gradCode = self.gradCode()
        hessianCode = self.hessianCode()
        symmetricCode = self.makeSymmetricCode()
        sparseHessianCode = self.sparseHessianCode()
        print ' Done.'
        return '\n '.join([self.header, gradCode, hessianCode, sparseHessianCode, symmetricCode, self.footer])


    def createFortran(self, pathToFile):
        code = self.assemble()
        fortranFile = open(pathToFile, 'w')
        fortranFile.write(code)
        fortranFile.close()


    def sparseHessianCode(self):
        idx = self.centerIdx
        s = self.shiftVar
        xk_xk = sympy.fcode(self.hessian[idx, idx], assign_to='val(k)', source_format='free')  # (i,i)
        xk_xl1 = sympy.fcode(self.hessian[idx, idx - 1], assign_to='val(k)', source_format='free')  # (i,i-1)
        xk_xl2 = sympy.fcode(self.hessian[idx, idx - 2], assign_to='val(k)', source_format='free')  # (i,i-2)

        yk_yk = sympy.fcode(self.hessian[s + idx, s + idx], assign_to='val(k)', source_format='free')  # (n+i,n+i)
        yk_yl1 = sympy.fcode(self.hessian[s + idx, s + idx - 1], assign_to='val(k)', source_format='free')  # (n+i,n+i-1)
        yk_yl2 = sympy.fcode(self.hessian[s + idx, s + idx - 2], assign_to='val(k)', source_format='free')  # (n+i,n+i-2)

        yk_xk = sympy.fcode(self.hessian[s + idx, idx], assign_to='val(k)', source_format='free')  # (n+i,i)
        yk_xl1 = sympy.fcode(self.hessian[s + idx, idx - 1], assign_to='val(k)', source_format='free')  # (n+i,i-1)
        yk_xl2 = sympy.fcode(self.hessian[s + idx, idx - 2], assign_to='val(k)', source_format='free')  # (n+i,i-2)
        yk_xr1 = sympy.fcode(self.hessian[s + idx, idx + 1], assign_to='val(k)', source_format='free')  # (n+i,i+1)
        yk_xr2 = sympy.fcode(self.hessian[s + idx, idx + 2], assign_to='val(k)', source_format='free')  # (n+i,i+2)

        code = '''
        subroutine compute_sparse_hessian(x,y,c,n,val,row,col,m,ds,s1,s2,last_index)
            double precision, dimension(n), intent(in) :: x,y,c,s1,s2
            double precision, dimension(m), intent(inout) :: val
            integer, dimension(m), intent(inout) :: row,col
            integer, intent(in) :: n,m
            integer, intent(out) :: last_index
            integer :: i, k
            double precision, intent(in) :: ds
            double precision :: x_l1, y_l1, c_l1, s1_l1, s2_l1, x_l2, y_l2, c_l2, s1_l2, s2_l2
            double precision :: x_r1, y_r1, c_r1, s1_r1, s2_r1, x_r2, y_r2, c_r2, s1_r2, s2_r2
            double precision :: x_k, y_k, c_k, s1_k, s2_k

            !f2py intent(inplace) :: val,row,col,x,y,c,s1,s2
            !f2py intent(hide) :: n,m
            !f2py intent(out) :: last_index

            k = 0
            !IMPORTANT: a C based index convention is used for rows and cols, because the matrix will be
            !assembled in python, thats the reason for -1 in all row, col indexes
            do i = 3, n-2
                %s

                k=k+1
                row(k)=i-1
                col(k)=i-1
                %s

                k=k+1
                row(k)=i-1
                col(k)=i-1-1
                %s

                k=k+1
                row(k)=i-1
                col(k)=i-2-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=n+i-1-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=n+i-2-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=i-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=i-1-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=i-2-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=i+1-1
                %s

                k=k+1
                row(k)=n+i-1
                col(k)=i+2-1
                %s


                !now the symmetric part
                k=k+1
                row(k)=i-1-1
                col(k)=i-1
                %s

                k=k+1
                row(k)=i-2-1
                col(k)=i-1
                %s

                k=k+1
                row(k)=n+i-1-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=n+i-2-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=i-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=i-1-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=i-2-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=i+1-1
                col(k)=n+i-1
                %s

                k=k+1
                row(k)=i+2-1
                col(k)=n+i-1
                %s

            end do
            last_index = k
        end subroutine
        ''' % (self.assignVariables, xk_xk, xk_xl1, xk_xl2, yk_yk, yk_yl1, yk_yl2, yk_xk, yk_xl1, yk_xl2, yk_xr1, yk_xr2, xk_xl1, xk_xl2, yk_yl1, yk_yl2, yk_xk, yk_xl1, yk_xl2, yk_xr1, yk_xr2)

        return code.replace('-1.5d0', '(-1.5d0)').replace('-3.0d0', '(-3.0d0)')  # avoid the warning from gfortran

if __name__ == '__main__':

    E = SymbolicEnergy()
    print 'Variables:', E.x, E.y, E.c, E.ds
    print 'Center element:', E.x[E.centerIdx], E.y[E.centerIdx], E.c[E.centerIdx]
    print 'Energy:', E.energy
    print 'Gradx:', E.gradx
    print 'Grady:', E.grady
    idx = E.centerIdx
    shift = E.shiftVar
    print 'Hessian xk xk:', E.hessian[idx, idx]
    print 'Hessian xk xl1:', E.hessian[idx, idx - 1]
    print 'Hessian xk xl2:', E.hessian[idx, idx - 2]
    print 'Hessian xk xl3:', E.hessian[idx, idx - 3]

    print 'Hessian yk yk:', E.hessian[shift + idx, shift + idx]
    print 'Hessian yk yl1:', E.hessian[shift + idx, shift + idx - 1]
    print 'Hessian yk yl2:', E.hessian[shift + idx, shift + idx - 2]
    print 'Hessian yk yl3:', E.hessian[shift + idx, shift + idx - 3]

    print 'Hessian xk yk:', E.hessian[shift + idx, idx]
    print 'Hessian xl1 yk:', E.hessian[shift + idx, idx - 1]
    print 'Hessian xl2 yk:', E.hessian[shift + idx, idx - 2]
    print 'Hessian xl3 yk:', E.hessian[shift + idx, idx - 3]
    print 'Hessian xr1 yk:', E.hessian[shift + idx, idx + 1]
    print 'Hessian xr2 yk:', E.hessian[shift + idx, idx + 2]
    print 'Hessian xr3 yk:', E.hessian[shift + idx, idx + 3]




