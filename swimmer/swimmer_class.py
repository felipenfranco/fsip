# -*- coding: utf-8 -*-
'''
Created on Mar 20, 2012

@author: felipe
'''
import sys
import sympy

sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
import utils

from physic_objects.ib import IB
from math_objects.cartesian_grid import Point
from physic_objects.force import EnergyForce
from scipy.integrate import quad
import scipy.optimize as optimize
from numpy import array, copy, savez, savetxt, dot
from sympy import var, lambdify, sin, cos, sqrt, sympify
import numpy
from cPickle import dump, load
import pylab
from math_objects.lagrangian_points import LagrangianPoints
from physic_objects.fluid import NavierStokesFluid, StokesFluid
from physic_objects.fluid_structure import FluidStructure
import sys
from swimmer.reparametrization import Reparametrization
from swimmer.shape_by_curvature import ShapebyCurvature, lenght
import swimmer



class Swimmer(IB):

    def __init__(self, S1, S2, ds, func, param, paramRange, grid, isPeriodic, startPoint=Point(0, 0), theta=0., isCurvature=False, reduce=True):


        self.ds = ds
        self.func = sympify(func)
        self.param = param
        self.paramRange = paramRange
        self.grid = grid
        self.isPeriodic = isPeriodic

        if isCurvature:
            rep = ShapebyCurvature(func, param, paramRange, startPoint, theta)
        else:
            rep = Reparametrization(func, param, paramRange, startPoint, theta)

        rep.computeDiscretePoints(ds)
        numberOfPoints = rep.numberPoints
        self.rep = rep

        self.S1 = numpy.array([S1 / ds for _ in range(numberOfPoints)])
        self.S2 = numpy.array([S2 * ds for _ in range(numberOfPoints)])
        self.boundaryForce = EnergyForce()

        IB.__init__(self, numberOfPoints)
        self.ID = "Swimmer"

        self.x, self.y = rep.getInitialPoints()

        self.c = rep.targetCurvatureExact(self.x, self.y, 0.0)
        self.h = self.ds
        if reduce:
            self.reduceEnergy()
        self.checkDistance()
        sys.stdout.flush()

    def setComputationType(self, computationType):
        self.boundaryForce = EnergyForce(computationType)

    def getHessian(self):
        return -1.*self.hessian

    def explicitUpdate(self, dt, currentTime):
        Swimmer.explicitUpdate(self, dt)
        self.updateCurvature(currentTime)

    def updateData(self, currentTime):
        self.c = self.rep.targetCurvatureExact(self.x, self.y, currentTime)

    def computeEnergy(self, printTerms=False, splitTerms=False):
        return self.boundaryForce.computeEnergy(self, printTerms, splitTerms)

    def reduceEnergy(self):
        # if the start energy is too big, perform a small step to minimize the terms
        # this is necessary if we want to use BIG S1,S2 constants
        # because, even though the distances are small, if the constants are VERY large
        # the energy will be large. When running a time step in the simulation, the newton
        # method takes care of this and minimize the distances.
        # we could have made a minimization before, in the initialization of the swimmer,
        # but this is very expensive using BFGS, this way is much faster and we have the same
        # results
        # a similar approach would be to run the simulation with small time steps first,
        # then increase then... thast valid, but this way requires less work, and is
        # more "local"
        if self.computeEnergy() > 100:
            dt = self.ds / 100
            fluid = StokesFluid(self.grid)
            print "\n\n\nEnergy too high... reducing initial energy..."
            print "Initial Energy before:\n", self.computeEnergy(printTerms=True)
            system = FluidStructure(fluid, self, dt, 'Implicit')
            system.advanceInTime(dt, True)
            print "Initial Energy after:\n", self.computeEnergy(printTerms=True)


    def saveShape(self, name):
        grid = self.grid
        pylab.axis([grid.origin.x, grid.endPoint.x, grid.origin.y, grid.endPoint.y])

        x, y = self.x.copy(), self.y.copy()
        x -= grid.origin.x
        y -= grid.origin.y
        x, y = self.getPeriodicPositions(grid.sizeX, grid.sizeY, x, y)
        pylab.plot(x + grid.origin.x, y + grid.origin.y, ',')

        if name[-3:] != "png":
            name += ".png"
        pylab.savefig(name)
        pylab.clf()


    def save(self, fileName, txt=False):
        if txt:
            savetxt(fileName + "_x.txt", self.x)
            savetxt(fileName + "_y.txt", self.y)
        else:
            savez(fileName, x=self.x, y=self.y)

    def saveState(self, stateName):
        data = {}
        data['x'] = self.x
        data['y'] = self.y
        data['c'] = self.c

        with open(stateName, 'w') as dataFile:
            dump(data, dataFile)

    def loadState(self, stateName):
        with open(stateName, 'r') as dataFile:
            data = load(dataFile)

        self.x = data['x']
        self.y = data['y']
        self.c = data['c']

    def checkDistance(self):
        print "\nChecking Distances..."
        distances = ((self.x[1:] - self.x[0:-1]) ** 2 + (self.y[1:] - self.y[0:-1]) ** 2) ** .5
        print "Max:", distances.max()
        print "Min:", distances.min()
        print "Mean:", distances.mean()
        print "Ds:", self.ds
        print "Lenght:", distances.sum()
#         print "First and last param:", self.rep.paramSpace[0], self.rep.paramSpace[-1]
        print "\n\n\n"

    def checkCurvature(self):
        print "\n Checking curvature..."
        x = self.x
        y = self.y

        curvature = -8.0 * ((x[2:n] - x[1:n - 1]) * (y[1:n - 1] - y[0:n - 2]) - (y[2:n] - y[1:n - 1]) * (x[1:n - 1] - x[0:n - 2])) / (((x[2:n] - x[0:n - 2]) ** 2 + (y[2:n] - y[0:n - 2]) ** 2) ** (1.5))
        print curvature

        if self.isPeriodic:
            xsize = self.grid.sizeX
            ysize = self.grid.sizeY
            if abs(x[0] - x[-1]) > ds * 4:
                ysize = 0.
            else:
                xsize = 0.

            k = 0
            cross_product = (x[k + 1] - x[k]) * (y[k] + ysize - y[k - 1]) - (y[k + 1] - y[k]) * (x[k] + xsize - x[k - 1])
            norm = ((x[k + 1] + xsize - x[k - 1]) ** 2 + (y[k + 1] + ysize - y[k - 1]) ** 2) ** (1.5)
            print -8.0 * cross_product / norm

            k = n - 1
            cross_product = (x[(k + 1) % n] + xsize - x[k]) * (y[k] - y[k - 1]) - (y[(k + 1) % n] + ysize - y[k]) * (x[k] - x[k - 1])
            norm = ((x[(k + 1) % n] + xsize - x[k - 1]) ** 2 + (y[(k + 1) % n] + ysize - y[k - 1]) ** 2) ** (1.5)
            print -8.0 * cross_product / norm
        print "\n\n\n"


if __name__ == '__main__':
    from math_objects.cartesian_grid import Point, Grid
    from numpy import pi, ceil
    from physic_objects.fluid_structure import FluidStructure
    from physic_objects.fluid import StokesFluid, PolimericStokesFluid
    from time import sleep
    from matplotlib.patches import Ellipse

    n = 256
    grid = Grid(Point(0., 0.), 1., 1., n, n, 2)
    S1, S2 = 10e5, 10e3

    ds = .5 * grid.dx
    L = lenght('0.125*sin(2*pi*x)', 'x', (0, 1))
    k = 2 * pi / L
    func = '-0.125*(%g**2)*sin(%g*x-2*pi*t)' % (k, k)
    swimmer = Swimmer(S1, S2, ds, func, 'x', (0., L), grid, True, Point(0., 0.5), 0., True, False)
    swimmer.checkCurvature()

    pylab.axis([0, 1, 0, 1])
    x, y = swimmer.x, swimmer.y
    pylab.plot(x, y, ',')
    print x
#     rx, ry = swimmer.rep.rotateToXAxis(x, y)
#     pylab.plot(rx, ry)
    pylab.show()
