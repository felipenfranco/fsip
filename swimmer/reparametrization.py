# -*- coding: utf-8 -*-
'''
Created on Mar 24, 2013

@author: felipe

this module has utility functions to find the arclength reparametrization of a curve x(q,t)=(q,f(q,t)), given by a function f

Given a function x(q,t) = (q, f(q,t)), q in [q0,qF]
'''

import utils
import numpy
from scipy.integrate import quad
from scipy.optimize import newton
# from scipy.interpolate import InterpolatedUnivariateSpline as Spline
import sympy
from math_objects.cartesian_grid import Point
import pylab
ceil = numpy.ceil

def norm2(curve):
        return (curve[0] ** 2 + curve[1] ** 2) ** 0.5

def lengthFunc(b, f, a, length):
    '''
    we want to know for which 'b' we have a root to this function
    '''
    return quad(f, a, b)[0] - length

class Reparametrization:

    def __init__(self, f, param, paramRange, startPoint, theta):
        '''
            curve = (param, f(param,t)) -> rotated by theta radians counter clockwise
                                           and translated to point startPoint
            f -> string that represents the parametric curve function
            param -> string with the parameter symbol
            paramRange -> tuple (a,b) of the range of the parameter
            theta -> counter clockwise rotation angle in radians
            startPoint -> Point(x,y) that translate the graph
        '''
        self.f = sympy.sympify(f)
        self.param = sympy.var(param)
        self.t = sympy.var('t')
        self.paramRange = paramRange
        self.startPoint = startPoint
        self.theta = theta

        self.df = self.f.diff(self.param)
        self.d2f = self.f.diff(self.param, self.param)

        self.curve = (self.param, self.f)
        self.dcurve = (1., self.df)

        self.lambdaexactCurvature = sympy.lambdify([param, self.t], self.d2f / ((1 + self.df ** 2)) ** (1.5), 'numpy')

        self.setTime(0)


    def setTime(self, time):
        print "Setting time:", time
        self.lambdaf = sympy.lambdify(self.param, self.f.subs({self.t:time}), 'numpy')
        self.lambdadf = sympy.lambdify(self.param, self.df.subs({self.t:time}), 'numpy')
        self.lambdanormf = sympy.lambdify(self.param, norm2(self.dcurve).subs({self.t:time}), 'numpy')
        self.lambdad2f = sympy.lambdify(self.param, self.d2f.subs({self.t:time}), 'numpy')

    def findQ(self, sk, initialGuess):
        return newton(lengthFunc, initialGuess, args=(self.lambdanormf, self.paramRange[0], sk,))

    def computeDiscretePoints(self, ds, numberSegments=None):
        if not numberSegments:
            numberSegments = int(ceil(quad(self.lambdanormf, self.paramRange[0], self.paramRange[1])[0] / ds))
            self.numberSegments = numberSegments

        numberPoints = numberSegments
        self.numberPoints = numberPoints
        print 'Number of points of reparametrization:', numberPoints

        s = numpy.arange(numberPoints) * ds
        q = numpy.zeros_like(s)

        dq = float(self.paramRange[1] - self.paramRange[0]) / numberPoints
        q0 = self.paramRange[0]
        self.dq = dq
        # now we want to find wich param value qk give us the length s[k]
        # for this we employ newtons method


        for k in xrange(len(s)):
            initialGuess = q0 + dq * k
            q[k] = self.findQ(s[k], initialGuess)

        self.ds = ds
        self.s = s  # the arclength parameter
        self.q = q  # q(s) -> the points in the x coordinate that correspond to arc-length

#         self.spline = Spline(s, q, k=3)  # spline approximation of (s,q(s))


#     def targetCurvatureSpline(self, time):
# #         computing the target curvature expression by the spline
#         self.setTime(time)
#         self.computeDiscretePoints(self.ds)
#         return self.lambdad2f(self.q) * (self.spline(self.s, 1)) ** 3

    def targetCurvatureExact(self, x, y, time):
        # computing the target curvature expression by the exact formula

        if False:  # self.theta != 0:
            # NOT WORKING WITH THE ROTATE AXIS!!!!!
            # NOT WORKING WITH THE ROTATE AXIS!!!!!
            # NOT WORKING WITH THE ROTATE AXIS!!!!!
            rx, ry = self.rotateToXAxis(x, y)
            c = self.lambdaexactCurvature(rx - rx[0], time)
        else:
            # this is the fast way, somewhat accurate with small amps
            # because of the low speed
#             c = self.lambdaexactCurvature(x, time)

            # this is the right way
            self.setTime(time)
            self.computeDiscretePoints(self.ds, self.numberSegments)
            c = self.lambdaexactCurvature(self.q, time)
#             c = self.targetCurvatureSpline(time)
#         self.checkAdherence(x, y, time)
        return c


    def getInitialPoints(self):
        x = self.q.copy()
        y = self.lambdaf(self.q)
        rx, ry = self.rotate(x, y, self.theta)
        return rx + self.startPoint.x, ry + self.startPoint.y

    def rotate(self, x, y, theta):
        '''
            Rotate the points (x,y) by theta radians counter clockwise,
            taking the first point to be the origin
        '''
        # put the first point at the origin
        x0 = x[0]
        y0 = y[0]
        ct = numpy.cos(theta)
        st = numpy.sin(theta)

        rx = (x - x0) * ct - (y - y0) * st
        ry = (x - x0) * st + (y - y0) * ct
        return rx + x0, ry + y0

    def rotateToXAxis(self, x, y):
        '''
            rotate the points (x,y) back to the X axis
        '''
        # first compute the slope of the line that connects the first and last points
        dy = y[-1] - y[0]
        dx = x[-1] - x[0]
        theta = numpy.arctan2(dy, dx)
#         print "Dy,dx", dy, dx

        q0 = x[0]
        initialGuess = q0 + self.dq * self.numberPoints
        q1 = self.findQ(self.s[-1], initialGuess)
        dy = self.lambdaf(q1) - self.lambdaf(q0)
        dx = q1 - q0
#         print "Dy,dx", dy, dx
        theta2 = numpy.arctan2(dy, dx)
        print "Rotation:", -(theta - theta2)
        return self.rotate(x, y, -(theta - theta2))

    def checkAdherence(self, x, y, time):
        '''
            check the adherence of the (x,y) points to the target function
        '''
        rx, ry = self.rotateToXAxis(x, y)
        self.setTime(time)


        fx = self.lambdaf(rx - rx[0]) + self.startPoint.y
#         fx = self.lambdaf(self.q) + self.startPoint.y


        print "Max error in adhering to the function:", abs((fx - y) ** 2).max() ** .5
        pylab.ion()
        pylab.clf()

        pylab.plot(x, y, 'r.')
        pylab.plot(rx, ry - ry[-1] + self.startPoint.y, '.')
#         pylab.plot(self.q, fx)
        pylab.plot(x, fx)
        pylab.draw()
        pylab.ion()


def randomFunction(_sin=True, _cos=True, period='(2*pi)', numTerms=4):
    '''
    Assume that the origin is (0,0)
    '''
    function = []
    if period != None:
        for _ in range(numTerms):
            if(numpy.random.randint(2)):
                signal = '+'
            else:
                signal = '-'
            c = numpy.random.random()  # * numpy.random.random_integers(5)
            w = numpy.random.randint(4)
            function.append('%s %s*sin(%s*(2*pi/%s)*x)' % (signal, c, w, period))

            if(numpy.random.randint(2)):
                signal = '+'
            else:
                signal = '-'
            c = numpy.random.random()  # * numpy.random.random_integers(5)
            w = numpy.random.randint(4)
            function.append('%s %s*cos(%s*(2*pi/%s)*x)' % (signal, c, w, period))
    return ''.join(function)






if __name__ == '__main__':
    import swimmer, pylab
    from numpy import sin, cos, pi
    import timeit
#     f = '.5*cos(x)'
#     f1 = '0.01*sin(2*pi*(x-t))'
#     f2 = '0.01*sin(2*pi*(x-0.1))'
#     f = randomFunction()

    xsize = 1.0
    ds = xsize / 512
    rep = Reparametrization(f1, 'x', (0.0, xsize), Point(0.5, 0.), pi / 2)
    rep.computeDiscretePoints(ds)

    rep2 = Reparametrization(f2, 'x', (0.0, xsize), Point(0., 0.), 0.)
    rep2.computeDiscretePoints(ds)

    x1, y1 = rep.getInitialPoints()
    x2, y2 = rep2.getInitialPoints()

    rx1, ry1 = rep.rotateToXAxis(x1, y1)
    rx2, ry2 = rep2.rotateToXAxis(x2, y2)
    pylab.plot(rx1, ry1, 'green')
    pylab.plot(rx2, ry2, 'black')
    pylab.show()
    c1 = rep.targetCurvatureExact(x1, y1, 0.1)
    c2 = rep2.targetCurvatureExact(x2, y2, 1.)

    pylab.plot(c1 - c2)
#     pylab.plot(c2)
    pylab.show()
    print abs(c1 - c2).max()


#     n = rep.numberPoints
#     x = numpy.arange(n) / float(n) * xsize
#     pylab.plot(x, rep.lambdaf(x), 'green')
#
#     x, y = rep.getXY()
#     rx, ry = rep.rotateToXAxis(x, y)
#     pylab.plot(x, y, 'black')
#     pylab.plot(rx, ry, 'red')

#     pylab.plot(rep.rq, rep.ry)
#     pylab.plot(rep.spline(rep.s), rep.lambdaf(rep.spline(rep.s)))
#     pylab.show()

#     X = numpy.concatenate([rep.q, rep.lambdaf(rep.q)])
#
#     print rep.targetCurvatureExact(rep.q, 0.0) - rep.targetCurvatureExact(rep.s, 0.0)


