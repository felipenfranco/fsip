'''
Created on Jun 14, 2013

@author: felipe
'''

import matplotlib
matplotlib.use('Agg')
import pylab
import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils

root = utils.rootPath

def resolution_plot(nn, S1, S2, domainSize, De, beta, q, amplitude):
    if isinstance(nn, list):
        i = 0
        for save in ["saveMeanXForce", "saveSxxSup", "saveMeanYForce", "saveMeanXPosition" ]:
            pylab.clf()
            for n in nn:
                if n == 1024:
                    simulationName = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g%s.npz" % (n, 3, 1, domainSize, De, beta, q, amplitude, save)
                else:
                    simulationName = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%g%s.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude, save)
                temp = numpy.load(simulationName)

                if save[-5:] == "Force":
                    pylab.plot(temp['x'][:-6], temp['y'][:-6], '.', label=str(n))
                else:
                    pylab.plot(temp['x'][:-6], temp['y'][:-6], label=str(n))

            pylab.title(save)
            pylab.legend(loc="upper left")
            pylab.savefig("%s.png" % (save))
            i += 1
    
if __name__ == '__main__':
    resolution_plot([128,256,512,1024], 8, 6, 2, 5, 0.5, 1, 0.08)
