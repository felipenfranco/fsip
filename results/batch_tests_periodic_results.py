'''
Created on Jun 14, 2013

@author: felipe
'''

# import matplotlib
# matplotlib.use('Agg')
import pylab
import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from numpy import pi

root = utils.rootPath


def loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXSpeed.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadMeanXForce(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXForce.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadMeanYForce(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanYForce.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSxxSup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSxxSup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSyySup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSyySup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSxySup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSxySup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def laugaSpeed(de, alpha, b, k):
    if isinstance(de, int):
        if de == 0:
            return newtonSpeed(b, k)
    temp = 1. + (alpha) * (de ** 2)
    temp = temp / (1. + de ** 2)
    return temp * .5 * (b * b * k * k)


def newtonSpeed(b, k):
    return .5 * b * b * k * k * (1. - (19. / 16.) * b * b * k * k)

def firstOrderNewtonSpeed(b, k):
    return .5 * (b * b * k * k)

if __name__ == '__main__':
#     check for the results folder
    resultsPath = root + "//results//periodic_results//"
    if not os.path.isdir(resultsPath):
        os.makedirs(resultsPath)

    test = "new test"

    if test == 1:
        # Amplitude test, same deborah
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        q = 0
        De = 1.
        n = 256
        beta = .5 / De
        pylab.axis([0.05, 6, .6, 1])
        nty = [-0.174, -0.062, -0.0162]
        kk = 0
        for amplitude in [0.125, 0.0625, 0.03125]:
            obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude)
#             ntx, nty = loadMeanXSpeed(n, S1, S2, domainSize, 0, 0, q, amplitude)
#             nty = -firstOrderNewtonSpeed(amplitude, 2.*pi)
#             ratio = oby / nty[-1]
            ratio = oby / nty[kk]
            kk += 1
            pylab.plot(obx, ratio, label="%g" % (amplitude))
            pylab.legend(loc="upper right")
        pylab.show()
#         pylab.savefig(resultsPath + "fig1_inset_n%d_s1_%g_s2_%g.png" % (n, S1, S2))

    if test == "new test":
        # Amplitude test, same deborah
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        q = 0
        De = 1.
        beta = .5 / De
        alpha = 2. / 3.
#         pylab.axis([0, 6, .6, 1])
        for amplitude in [0.125, 0.0625, 0.03125]:
            for n in [32, 64, 128, 256]:
                obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude)

                print oby[-1], laugaSpeed(De, alpha, amplitude, 2 * pi), 2 * pi * amplitude
                pylab.plot(obx, -oby, label="%d" % (n))

            ntx, nty = loadMeanXSpeed(n, S1, S2, domainSize, 0, 0, q, amplitude)
            pylab.plot(ntx, -nty, label="Newtonian")
            pylab.plot(obx, [laugaSpeed(De, alpha, amplitude, 2 * pi) for _ in range(len(obx))], '--', label="Lauga Velocity")
            pylab.xlabel("Time")
            pylab.ylabel("Velocity")
            pylab.title("Amplitude = %g" % (amplitude))
            pylab.legend(loc="lower center")
            pylab.savefig(resultsPath + "amp_%g_s1_%g_s2_%g.png" % (amplitude, S1, S2))
            pylab.show()

    if test == 2:
        # dt test
        n = 256
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        De = 1
        amplitude = 0.03125
        finalTime = 6.
        beta = .5 / De
        last = 0.
        lastdiff = 0
        for q in [0, 1, 2, 3]:
            obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude)
            current = oby[-1]
            print lastdiff / (current - last)
            lastdiff = current - last
            last = current
            pylab.plot(obx, oby)
        pylab.show()

    if test == 3:
        # Lauga formula, same amplitude
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        q = 0
        amplitude = 0.01 / (2 * pi)
        n = 128
#         ntx, nty = loadMeanXSpeed(n, S1, S2, domainSize, 0, 0., q, amplitude)

        deRange = numpy.array([0.1 * i for i in range(100)])
#         laugaFormula = ((1. + (2. / 3.) * deRange ** 2) / (1. + deRange ** 2))
#         pylab.plot(deRange, laugaFormula, label="Lauga Formula")
#         pylab.axis([0, 8, 0, 0.0001])
        for De in [8, 4, 2, 1, .5, 0.]:  # [0., .5, 1, 2, 4, 8]:
            finalTime = De * 4 + 2
            beta = 0.
            if De:
                beta = .5 / De
            obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude)
            ratio = oby[-1] / -newtonSpeed(amplitude, 2.*pi)
            print ratio
            print De, -oby[-1], laugaSpeed(De, 2. / 3., amplitude, 2.*pi)
#             pylab.plot(De, -oby[-1], 's', label="%g" % (De))
            pylab.plot(De, ratio, 's', label="%g" % (De))
#         pylab.plot(deRange, laugaSpeed(deRange, 2. / 3., amplitude, 2 * pi), label="lauga %g" % (De))

        pylab.legend(loc='upper right')
        pylab.show()
        pylab.savefig(resultsPath + "fig1_n%d_s1_%g_s2_%g.png" % (n, S1, S2))

        pylab.clf()

        for De in [0., .5, 1, 2, 4, 8]:
            finalTime = De * 3 + 3
            beta = 0.
            if De:
                beta = .5 / De
            x, y = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude)
            pylab.plot(x, y / nty[-1], label="%g" % (De))
        pylab.legend(loc='upper right')
        pylab.savefig(resultsPath + "lauga_speed_n%d_s1_%g_s2_%g.png" % (n, S1, S2))



    if test == 4:
        # resolution test
        S1 = 10000.0
        S2 = 500.0
        domainSize = 1
        q = 0
        De = 1
        amplitude = 0.03125
        finalTime = 6.
        beta = .5 / De
        nlist = [32, 64 , 128, 256, 512]
        for n in nlist:
            obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, .5, q, amplitude)
#             ntx, nty = loadMeanXSpeed(n, S1, S2, domainSize, 0, 0, q, amplitude)
#             ratio = oby / nty
            pylab.plot(obx, -oby, label="%d" % (n))
            pylab.legend(loc="lower center")
        pylab.show()
        pylab.savefig(resultsPath + "resolution_amp%g_s1_%g_s2_%g.png" % (amplitude, S1, S2))

        pylab.clf()

        for n in nlist:
            fx, fy = loadMeanXForce(n, S1, S2, domainSize, De, .5, q, amplitude)
            pylab.plot(fx, fy, ',', label="%d" % (n))
            pylab.legend(loc="upper right")
#         pylab.show()
        pylab.savefig(resultsPath + "resolution_amp%g_s1_%g_s2_%g_FORCEX.png" % (amplitude, S1, S2))

        pylab.clf()

        for n in nlist:
            fx, fy = loadSxxSup(n, S1, S2, domainSize, De, .5, q, amplitude)
            pylab.axis([-0.1, 6, 1, 1.5])
            pylab.plot(fx, fy, label="%d" % (n))
            pylab.legend(loc="upper right")
        pylab.savefig(resultsPath + "resolution_amp%g_s1_%g_s2_%g_Sxx.png" % (amplitude, S1, S2))

        pylab.clf()

        for n in nlist:
            fx, fy = loadSyySup(n, S1, S2, domainSize, De, .5, q, amplitude)
            pylab.axis([-0.1, 6, 1, 1.25])
            pylab.plot(fx, fy, label="%d" % (n))
            pylab.legend(loc="upper right")
        pylab.savefig(resultsPath + "resolution_amp%g_s1_%g_s2_%g_Syy.png" % (amplitude, S1, S2))

        pylab.clf()

        for n in nlist:
            fx, fy = loadSxySup(n, S1, S2, domainSize, De, .5, q, amplitude)
            pylab.axis([-0.1, 6, 0, .25])
            pylab.plot(fx, fy, label="%d" % (n))
            pylab.legend(loc="upper right")
        pylab.savefig(resultsPath + "resolution_amp%g_s1_%g_s2_%g_Sxy.png" % (amplitude, S1, S2))

        pylab.clf()

        for n in nlist:
            fx, fy = loadMeanYForce(n, S1, S2, domainSize, De, .5, q, amplitude)
            pylab.plot(fx, fy, ',', label="%d" % (n))
            pylab.legend(loc="upper right")
        pylab.savefig(resultsPath + "resolution_amp%g_s1_%g_s2_%g_FORCEY.png" % (amplitude, S1, S2))
