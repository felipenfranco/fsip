'''
Created on Jun 14, 2013

@author: felipe
'''

# import matplotlib
# matplotlib.use('Agg')
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif')
import pylab
import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from numpy import pi

root = utils.rootPath


def loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXSpeed.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadMeanXForce(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXForce.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadMeanYForce(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanYForce.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSxxSup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSxxSup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSyySup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSyySup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSxySup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "periodic_n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSxySup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def laugaSpeed(de, alpha, b, k):
    if isinstance(de, int):
        if de == 0:
            return newtonSpeed(b, k)
    temp = 1. + (alpha) * (de ** 2)
    temp = temp / (1. + de ** 2)
#     return temp * .5 * (b * b * k * k)
    return temp * newtonSpeed(b, k)


def newtonSpeed(b, k):
    return .5 * b * b * k * k * (1. - (19. / 16.) * b * b * k * k)

def firstOrderNewtonSpeed(b, k):
    return .5 * (b * b * k * k)

if __name__ == '__main__':
#     check for the results folder
    resultsPath = root + "//results//periodic_results//"
    if not os.path.isdir(resultsPath):
        os.makedirs(resultsPath)

    # Lauga test
    S1 = 10000.0
    S2 = 500.0
    domainSize = 1
    q = 0
    De = 1.
    beta = .5 / De
    n = 128
    bkList = [0.01, 0.02, 0.04, 0.06, 0.08, 0.1, 0.2]
    smallList = [0.01, 0.02, 0.04, 0.06]
    h = 0.001
    contbkList = [i * h for i in range(int((bkList[-1]) / h))]
    contbkListsmall = [i * h for i in range(int(smallList[-1] / h))]
    finalTime = 12
    k = 2. * pi


    Lauga = []
    for bk in bkList:
        b = bk / k
        print bk
        obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, b)
        pylab.plot(bk, -oby[-1], 'ro')

    for bk in contbkList:
        b = bk / k
        Lauga.append(laugaSpeed(De, 2. / 3., b, k))
    pylab.plot(contbkList, Lauga, label='Lauga Formula')
    pylab.title("De=%g" % (De))
    pylab.xlabel("bk")
    pylab.ylabel("U/V")
    pylab.legend()
    pylab.show()



#     Lauga = []
#     for bk in smallList:
#         b = bk / k
#         obx, oby = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, b)
#         pylab.plot(bk, -oby[-1], 'ro')
#
#     for bk in contbkListsmall:
#         b = bk / k
#         Lauga.append(laugaSpeed(De, 2. / 3., b, k))
#
#     pylab.plot(contbkListsmall, Lauga, label='Lauga Formula')
#     pylab.xlabel("bk")
#     pylab.ylabel("U/V")
#     pylab.legend()
#     pylab.show()
#
#     pylab.show()
