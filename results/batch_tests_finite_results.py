'''
Created on Jun 14, 2013

@author: felipe
'''

# import matplotlib
# matplotlib.use('Agg')
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif')

import pylab
import sys
import os
import numpy
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/fnfranco/fsip/')
import utils
from numpy import pi

root = utils.rootPath

def loadPolimer(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsavePolimer.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['Sxx'], temp['Syy'], temp['Sxy']


def loadE1(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveEnergyE1.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']

def loadE2(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveEnergyE2.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']

def loadMeanXPosition(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXPosition.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']

def loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXSpeed.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadMeanXForce(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanXForce.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadMeanYForce(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveMeanYForce.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSxxSup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSxxSup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSyySup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSyySup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


def loadSxySup(n, S1, S2, domainSize, De, beta, q, amplitude):
    name = root + "//cached_simulations//" + "n%d_s1%g_s2%g_dom%g_de%g_beta%g_q%g_amp%gsaveSxySup.npz" % (n, S1, S2, domainSize, De, beta, q, amplitude)
    temp = numpy.load(name)
    return temp['x'], temp['y']


if __name__ == '__main__':
#     check for the results folder

    resultsPath = root + "//results//finite_results//"
    if not os.path.isdir(resultsPath):
        os.makedirs(resultsPath)


    S1 = 10000.0
    S2 = 500.0
    domainSize = 1
    q = 0
    De = 5.
    beta = .5 / De
    amplitude = 0.08
    nlist = [32, 64, 128, 256, 512]
    print "Start..."
    # mean position plot
    for n in nlist:
        x, y = loadMeanXPosition(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
        pylab.legend(loc="upper left")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"Mean X Position")
    pylab.savefig(resultsPath + "mean_x_position_s1_%g_s2_%g.png" % (S1, S2))
#     pylab.show()
    pylab.clf()

    # e1 plot
    for n in nlist:
        x, y = loadE1(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
    pylab.legend(loc="upper right")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"E1")
    pylab.savefig(resultsPath + "e1_s1_%g_s2_%g.png" % (S1, S2))
#     pylab.show()
    pylab.clf()

    # e2 plot
    for n in nlist:
        x, y = loadE2(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
    pylab.legend(loc="upper right")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"E2")
#     pylab.show()
    pylab.savefig(resultsPath + "e2_s1_%g_s2_%g.png" % (S1, S2))

    pylab.clf()

    # sxx plot
    for n in nlist:
        x, y = loadSxxSup(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
    pylab.legend(loc="upper left")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"$sup(S_{xx})$")
#     pylab.show()
    pylab.savefig(resultsPath + "sxxsup_s1_%g_s2_%g.png" % (S1, S2))

    pylab.clf()

    # sxx plot
    for n in nlist:
        x, y = loadSyySup(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
    pylab.legend(loc="upper left")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"$sup(S_{yy})$")
#     pylab.show()
    pylab.savefig(resultsPath + "syysup_s1_%g_s2_%g.png" % (S1, S2))


    pylab.clf()

    # sxx plot
    for n in nlist:
        x, y = loadSxySup(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
    pylab.legend(loc="upper left")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"$sup(S_{xy})$")
#     pylab.show()
    pylab.savefig(resultsPath + "sxysup_s1_%g_s2_%g.png" % (S1, S2))

    pylab.clf()

    # mean position plot
    for n in nlist:
        x, y = loadMeanXSpeed(n, S1, S2, domainSize, De, beta, q, amplitude)
        pylab.plot(x, y, label="%g" % (n))
        pylab.legend(loc="upper left")
    pylab.xlabel(r"Time")
    pylab.ylabel(r"Mean X Speed")
    pylab.savefig(resultsPath + "mean_x_speed_s1_%g_s2_%g.png" % (S1, S2))
    pylab.show()
    pylab.clf()
