'''
Created on Mar 1, 2012

@author: felipe
'''
from numpy import array, sqrt, floor, ceil, mod

class Point(object):
    '''
    A Point in the space
    '''

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distanceTo(self, point):
        return sqrt(((self.x - point.x) ** 2 + (self.y - point.y) ** 2))

    def getTuple(self):
        return (self.x, self.y)

    def __getitem__(self, key):
        if key == 0: return self.x
        else: return self.y

    def __add__(self, other):
        return Point(self.x + other[0], self.y + other[1])

    def __sub__(self, other):
        return Point(self.x - other[0], self.y - other[1])


class Grid(object):
    '''
        Implements the representation of a Cartesian Grid with Ghost Cells
    '''


    def __init__(self, origin, sizeX, sizeY, nx, ny, ngc):
        '''
            origin = Point
        '''
        self.origin = origin
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.nx = nx
        self.ny = ny
        self.ngc = ngc

        self.sx = ngc
        self.sy = ngc
        self.ex = nx + ngc
        self.ey = ny + ngc

        self.endPoint = Point(origin.x + sizeX, origin.y + sizeY)

        self.dx = sizeX / float(nx)
        self.dy = sizeY / float(ny)

        self.dimX = nx + 2 * ngc
        self.dimY = ny + 2 * ngc

        self.X = array([-ngc * self.dx + origin.x + self.dx * i for i in range(self.dimX)], order='F')
        self.Y = array([-ngc * self.dy + origin.y + self.dy * i for i in range(self.dimY)], order='F')

        self.nodeX = self.X + self.dx
        self.nodeY = self.Y.copy()

        self._cached_X = dict()
        self._cached_Y = dict()
        self._cached_grids = dict()
        self.primary_grid = True

    def getX(self, scalarField=None):
        '''
            If receives a scalarField, it makes the shifting and stores.
        '''
        if scalarField:
            if self._cached_X.has_key(scalarField.shiftX):
                return self._cached_X[scalarField.shiftX]
            else:
                self._cached_X[scalarField.shiftX] = self.X + scalarField.shiftX
                return self._cached_X[scalarField.shiftX]
        else:
            return self.X

    def getY(self, scalarField=None):
        '''
            If receives a scalarField, it makes the shifting and stores.
        '''
        if scalarField:
            if self._cached_Y.has_key(scalarField.shiftY):
                return self._cached_Y[scalarField.shiftY]
            else:
                self._cached_Y[scalarField.shiftY] = self.Y + scalarField.shiftY
                return self._cached_Y[scalarField.shiftY]
        else:
            return self.Y

    def getSpacing(self):
        return self.dx, self.dy

    def getBounds(self, Fortran=False):
        if Fortran:
            return self.sx + 1, self.ex, self.sy + 1, self.ey
        else:
            return self.sx, self.ex, self.sy, self.ey

    def inside(self, x, y):
        xinside = (x - self.origin.x) % self.sizeX + self.origin.x
        yinside = (y - self.origin.y) % self.sizeY + self.origin.y
        return xinside, yinside

    def getIndex(self, x, y):
        A0, B0 = self.origin.getTuple()
        i = int(ceil((x - A0) / self.dx)) - 1 + self.sx
        j = mod(int(floor((y - B0) / self.dy)) + self.sy , self.ey)
        return i, j

    def generateCoarseGrid(self, nx, ny):
        '''
        Generates a Coarse Grid. Only the original Grid must generate aditional grids.
        '''
        if self._cached_grids.has_key((nx, ny)):
            return self._cached_grids[(nx, ny)]
        else:
            if self.primary_grid:
                if nx >= 8 and ny >= 8:
                    self._cached_grids[(nx, ny)] = Grid(self.origin, self.sizeX, self.sizeY, nx , ny, self.ngc)
                    self._cached_grids[(nx, ny)].primary_grid = False
                    return self._cached_grids[(nx, ny)]
                else:
                    return NotImplementedError("Only Grids with nx,ny >= 8 are allowed.")
            else:
                raise NotImplementedError("Only the primary Grid should generate coarse Grids.")


if __name__ == '__main__':
    g = Grid(Point(-1., -1.), 2., 2., 128, 128, 2)

    sx, ex, sy, ey = g.getBounds()

    print g.sizeX
    print g.inside(.9, .9)
    print g.inside(-.9, -.9)
    print g.inside(1.5, 1.5)
    print g.inside(-1.5, -1.5)
