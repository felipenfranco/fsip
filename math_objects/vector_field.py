'''
Created on Mar 1, 2012

@author: felipe
'''
import utils
from scalar_field import RightScalarField, BottomScalarField

class VectorField(object):
    '''
        Implements the Mathematical model of a Vector Field over a Cartesian 
        Grid.
    '''

    def __init__(self, grid):
        self.u = RightScalarField(grid)
        self.v = BottomScalarField(grid)
        self.ID = "VectorField"

    def updateGhosts(self):
        self.u.updateGhosts()
        self.v.updateGhosts()

    def initialize(self, fu, fv, t = 0.):
        self.u.initialize(fu, t)
        self.v.initialize(fv, t)

