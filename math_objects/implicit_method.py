'''
Created on Mar 16, 2012

@author: felipe
'''
import sys
sys.path.insert(0, '/afs/labmap.ime.usp.br/usr/fnfranco/fsip/')
sys.path.insert(0, '/home/felipe/workspace/fsip/')

import utils
import numpy
import fast_matrix
from physic_objects.fluid import PolimericStokesFluid, NavierStokesFluid, PolimericNavierStokesFluid
from linear_algebra.biconjugate_gradient import bicgstab
from math_objects.scalar_field import NodeScalarField
from math_objects.projection_method import computeConvection

from scipy.linalg import solve
from numpy import dot, concatenate
import pylab
from quadtree.far_field_expansion import FarFieldExpansion
from physic_objects.ib import RandomPoints


# import pyximport; pyximport.install()
# import fast_implicit


class FluidStructureInteraction:
    def __init__(self, lookupTables, computationType, numLevels=4, numberExpansionTerms=10):
        self.lookupTables = lookupTables
        self.computationType = computationType
        if computationType == "Quadtree":
            self.ffe = FarFieldExpansion(numLevels, lookupTables, numberExpansionTerms)

        tempNode = NodeScalarField(lookupTables.grid)
        self.nodeX = tempNode.getX()
        self.nodeY = tempNode.getY()

        fluid = lookupTables.fluid
        if fluid.ID == "PolimericStokesFluid":
            self.tempFluid = PolimericStokesFluid(fluid.grid, fluid.visc, fluid.beta, fluid.de)

        elif fluid.ID == "NavierStokesFluid":
            self.tempFluid = NavierStokesFluid(fluid.grid, fluid.visc, fluid.dens)

        elif fluid.ID == "PolimericNavierStokesFluid":
            self.tempFluid = PolimericNavierStokesFluid(fluid.grid, fluid.visc, fluid.dens, fluid.beta, fluid.de)
            self.tempFluid.setReynolds(fluid.Re)


    def performPreComputations(self, fluidIn, ibIn, dt):
        self.computeExplicitTerm(fluidIn, ibIn, dt)

        if self.computationType == 'Matrix':
            self.makeInteractionMatrix(ibIn)
        elif self.computationType == "Quadtree":
            from quadtree.quadtree import QuadTree

            self.makeInteractionMatrix(ibIn)
            self.quadtree = QuadTree(fluidIn.grid, 10, self.ffe)
            self.quadtree.create_tree(ibIn.x, ibIn.y)
#             self.quadtree.draw()
#             pylab.show()
            self.quadtree.pre_eval(ibIn.x, ibIn.y, self.M)
        else:
            raise TypeError("Computation Type:%s does not exist." % (self.computationType))


    def computeInteraction(self, A, ib=None):
        if self.computationType == 'Matrix':
            return dot(self.M, A)
        elif self.computationType == "Quadtree":
            if len(A.shape) == 1:
                fx = A[:ib.numberOfPoints]
                fy = A[ib.numberOfPoints:]
                rx, ry = self.quadtree.eval(fx, fy, ib.h)

#                 print dot(self.M, A) - numpy.concatenate([rx, ry])

                return numpy.concatenate([rx, ry])
            else:
                return dot(self.M, A)



    def makeInteractionMatrix(self , ib, big=True, opt=False):
        self.M = numpy.empty((2 * ib.numberOfPoints, 2 * ib.numberOfPoints), order='F')
        x, y = ib.getX()
        grid = ib.grid
        dx, dy = grid.getSpacing()
        u_fx, v_fx = self.lookupTables.fxLookup.u , self.lookupTables.fxLookup.v
        u_fy, v_fy = self.lookupTables.fyLookup.u , self.lookupTables.fyLookup.v
        sx, ex, sy, ey = grid.getBounds(Fortran=False)  # <- this False is correct!!!
        A0, B0 = grid.origin.x, grid.origin.y
        A1, B1 = A0 + grid.sizeX , B0 + grid.sizeY
        hb = ib.h

        nodeX = self.nodeX
        nodeY = self.nodeY

        # the Matrix gets multiplied by hb in this routine!
        if opt:  # opt takes precedence
            fast_matrix.matrix.make_interaction_matrix_opt(self.M, u_fx.data, v_fx.data, u_fy.data, v_fy.data, x, y,
                                                           nodeX, nodeY, A0, A1, B0, B1, dx, dy, sx, sy, ey, hb)
        elif big:
            fast_matrix.matrix.make_interaction_matrix_big(self.M, u_fx.data, v_fx.data, u_fy.data, v_fy.data, x, y,
                                                           nodeX, nodeY, A0, A1, B0, B1, dx, dy, sx, sy, ey, hb)
        else:
            fast_matrix.matrix.make_interaction_matrix_new(self.M, u_fx.data, v_fx.data, u_fy.data, v_fy.data, x, y,
                                                           nodeX, nodeY, A0, A1, B0, B1, dx, dy, sx, sy, ey, hb)





    def computeExplicitTerm(self, fluid, ib, dt):
        self.explicitTerm = numpy.concatenate(ib.getX())
        old_vel_x = ib.velocity.x.copy()
        old_vel_y = ib.velocity.y.copy()

        if fluid.ID == "PolimericStokesFluid":
            tempFluid = self.tempFluid
            tempFluid.clearRhs()
            fluid.polimer.copyTo(tempFluid.polimer)
            tempFluid.velocity.u.data[:] = fluid.velocity.u.data[:]
            tempFluid.velocity.v.data[:] = fluid.velocity.v.data[:]
            tempFluid.pressure.data[:] = fluid.pressure.data[:]

            if fluid.polimer.started:
                tempFluid.advanceInTime(dt)
                ib.interpolateVelocity(tempFluid.velocity)
                polimerExplicit = numpy.concatenate(ib.velocity.getX())
                self.explicitTerm += dt * polimerExplicit

        if fluid.ID == "NavierStokesFluid":
            tempFluid = self.tempFluid
            tempFluid.clearRhs()
            tempFluid.velocity.u.data = fluid.velocity.u.data.copy()
            tempFluid.velocity.v.data = fluid.velocity.v.data.copy()
            tempFluid.pressure.data = fluid.pressure.data.copy()
            tempFluid.advanceInTime(dt)
            ib.interpolateVelocity(tempFluid.velocity)
            nsExplicit = numpy.concatenate(ib.velocity.getX())
            self.explicitTerm += dt * nsExplicit

#            pylab.clf()
#            pylab.plot(dt * nsExplicit)
#            pylab.draw()
#            pylab.clf()
#            pylab.contourf(tempFluid.velocity.u.data.T)
#            pylab.colorbar()
#            pylab.draw()

        if fluid.ID == "PolimericNavierStokesFluid":
            tempFluid = self.tempFluid
            tempFluid.clearRhs()
            tempFluid.velocity.u.data = fluid.velocity.u.data.copy()
            tempFluid.velocity.v.data = fluid.velocity.v.data.copy()
            tempFluid.pressure.data = fluid.pressure.data.copy()
            fluid.polimer.copyTo(tempFluid.polimer)

            tempFluid.advanceInTime(dt)
            ib.interpolateVelocity(tempFluid.velocity)
            nsExplicit = numpy.concatenate(ib.velocity.getX())
            self.explicitTerm += dt * nsExplicit

        # restore the ib velocity, since we changed it
        ib.velocity.x[:] = old_vel_x
        ib.velocity.y[:] = old_vel_y


def F(Xk, Fk, dt, FSI, ib=None):
    return Xk - FSI.explicitTerm - FSI.computeInteraction(Fk, ib)

def NormSquared(function_being_minimized):
    return numpy.dot(function_being_minimized.T, function_being_minimized)

def newton(ib, FSI, dt, withReport=False):
    '''
        Globally convergente Newton method, the search direction is multiplied by a lmb which ensures
        that the squared norm of the function decreases.

        We proceed until we reach discretization error dt**2
    '''
    if withReport:
        print "\n\n Minimization Info:"
        print "Before minimization:"
        if ib.ID == "Swimmer" or "IBCollection":
            print "    Energy:" , ib.computeEnergy()
    nbp = ib.numberOfPoints
    startX = numpy.copy(concatenate(ib.getX()))

    nsf = 10e10
    dX = numpy.zeros_like(startX) + 1.

    for newton_iterations in range(1, 50):

        ib.computeForce()
        ib.computeHessian()

        Hk = ib.getHessian()
        Xk = concatenate(ib.getX())
        Fk = concatenate(ib.force.getX())
        function_being_minimized = F(Xk, Fk, dt, FSI, ib)
        nsf = NormSquared(function_being_minimized)

        if withReport: print "Newton iter: %d , Function Norm: %g " % (newton_iterations , nsf)

        if nsf < 10e-15 or abs(dX).max() < 10e-14:
            if newton_iterations >= 4:
#         if nsf < dt ** 2 or abs(dX).max() < 10e-15:
                print "BREAK", newton_iterations
                print "Function Norm: %g" % (nsf)
                print "Max dx: %g" % (abs(dX).max())
                break

        Jacobian = numpy.identity(2 * nbp) - FSI.computeInteraction(Hk, ib)
        b = function_being_minimized

        dX = solve(Jacobian, b)
        # chosing the integers j that satisfy the inequality below
        lmbds = [2.**(-j) for j in range(8)]
        selected_lmb = .1
        selected_improv = -1.
        for lmb in lmbds:
            newX = Xk - lmb * dX
            Fk1 = ib.computeForce(newX[:nbp], newX[nbp:])
            after_step = NormSquared(F(newX, Fk1, dt, FSI, ib))
            amount_improved = nsf - after_step
#             print "nsf, after step, Amount improved, lmb:", nsf, after_step, amount_improved, lmb
            if amount_improved > 0:
#                 print "AQUI", amount_improved, selected_improv
                if amount_improved > selected_improv:
                    selected_improv = amount_improved
                    selected_lmb = lmb

        lmb = selected_lmb

        if withReport:
            print "DX max,min:(%g,%g)" % (dX.max(), dX.min())
            print "Lambda: %g" % (lmb)
            if ib.ID == "Swimmer":
                print "Current Energy:", ib.computeEnergy(printTerms=True)

        ib.updateX(-lmb * dX)



    velocity = (concatenate(ib.getX()) - startX) / dt
    ib.setVelocity(velocity)

    print "\n\n\nNumber of Newton Interactions: %d  Function Norm: %g Lambda: %g" % (newton_iterations , nsf, lmb)
    print "After minimization:"
    if ib.ID == "Swimmer":
            print "Current Energy:", ib.computeEnergy()
    ib.computeForce()
    fx, fy = ib.force.getX()
    print "Max Force:                ", abs(fx).max() , abs(fy).max()
#     if abs(fx).max() > 1000 or abs(fy).max() > 1000:
#         numpy.save("fx_%g" % (numpy.random.random(1)), fx)
#         numpy.save("fy_%g" % (numpy.random.random(1)), fy)


if __name__ == '__main__':
    from math_objects.lookup_table import LookupTable
    from physic_objects.fluid import StokesFluid
    from physic_objects.ib import Ellipse, RandomPoints
    from physic_objects.force import EllasticForce
    from math_objects.cartesian_grid import *
    from time import time as tic

    nbp = 2000  # int(sys.argv[1])

    nbps = []
    makeTimes = []
    makeTimesBig = []
    computeTimes = []

    for n in (512,):  # range(2, 20):
        grid = Grid(Point(0., 0.), 1., 1., n, n, 2)
        fluid = StokesFluid(grid)
        lp = LookupTable(fluid, 1.0)
        fsi = FluidStructureInteraction(lp, 'Matrix')
        fsiBig = FluidStructureInteraction(lp, 'Matrix')
#         nbp = 3000
        for _ in range(1):  # range(100, 1000, 50):
    #         ib = Ellipse(nbp, Point(.5, .5), 0.5, 0.2, grid, EllasticForce(1000.0))
            ib = RandomPoints(0., 1., 0., 1., nbp, grid, EllasticForce(1000.0))
            ib.computeForce()
            ib.force.x[:] = numpy.random.random(nbp) * (20) - 10
            ib.force.y[:] = numpy.random.random(nbp) * (20) - 10

            t = tic()
            print "BEGIN"
            fsi.makeInteractionMatrix(ib, True, False)
            makeTime = tic() - t
            print "Time to generate Matrix Big Method (%d): %g" % (nbp, makeTime)

            t = tic()
            fsiBig.makeInteractionMatrix(ib, True, True)
            makeTimeBig = tic() - t
            print "Time to generate Matrix Opt Method (%d): %g" % (nbp, makeTimeBig)

#             assert abs(fsiBig.M - fsi.M).max() < 10e-10
            print fsiBig.M[0:10, 0]
            print fsi.M[0:10, 0]

#             A = numpy.concatenate([ib.force.x, ib.force.y])

#             t = tic()
#             matrixInteraction = fsi.computeInteraction(A)
#             computeTime = tic() - t
#             print "Time to compute interaction (%d): %g \n\n" % (nbp, computeTime)

            nbps.append(nbp)
            makeTimes.append(makeTime)
            makeTimesBig.append(makeTimeBig)
#             computeTimes.append(computeTime)

#             print abs(fsi.M - fsi.M.T).max()
#             assert (numpy.sign(fsi.M) - numpy.sign(fsi.M.T)).sum() == 0

    makeTimes = numpy.array(makeTimes)
    makeTimesBig = numpy.array(makeTimesBig)

#     pylab.plot(nbps, makeTimes, label='Time to make matrix (seconds)')
#     pylab.plot(nbps, makeTimesBig, label='Time to make matrix Big method (seconds)')
#     pylab.plot(nbps, makeTimes - makeTimesBig, label='Diff')
#     pylab.plot(nbps, computeTimes, label='Time to compute (seconds)')
#     pylab.legend(loc='upper left')
#     pylab.show()

#    t = tic()
#    matrixFree = fsi.computeDirectInteraction(ib)
#    print "Matrix Free:", tic() - t
#
#    print abs(matrixInteraction - matrixFree).max()
