'''
Created on Mar 1, 2012

@author: felipe
'''
import utils
from inspect import getargspec
import numpy
import math_objects
from math_objects.cartesian_grid import Grid, Point
import fast_scalar_field

class ScalarField(object):
    '''
        Abstract Interface of a ScalarField.
        Implements the Mathematical model of a Scalar Field over a Cartesian 
        Grid.
    '''


    def __init__(self, grid, shiftX, shiftY):
        '''
            shiftX and shiftY = padding in relation to bottom left corner of a
            cell
            
            GhostCells = cells that help to set boundary conditions
        '''
        self.grid = grid
        self.shiftX = shiftX
        self.shiftY = shiftY

        self.dimx = self.getX().size
        self.dimy = self.getY().size
        self.data = numpy.zeros((self.dimx, self.dimy), order = 'F')
        self.ID = "ScalarField"


    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, data):
        self.data[key] = data

    def __add__(self, other):
        return (self.data + other.data)

    def __sub__(self, other):
        return (self.data - other.data)

    def __mul__(self, other):
        return self.data * other

    def __rmul__(self, other):
        return self.data * other

    def __iadd__(self, other):
        self.data += other.data
        return self

    def __isub__(self, other):
        self.data -= other.data
        return self

    def getX(self):
        '''
            Returns the X positions of the ScalarField, with the shiffiting and
            ghost cells.
        '''
        return self.grid.getX(self)

    def getY(self):
        '''
            Returns the X positions of the ScalarField, with the shiffiting and
            ghost cells.
        '''
        return self.grid.getY(self)

    def updateGhosts(self):
        '''
            Updates the value of the ghost cells.
            Assume that the domain is periodic.
        '''
        sx, ex, sy, ey = self.grid.getBounds(Fortran = True)
        fast_scalar_field.scalar_field.update_ghosts(self.data, sx, ex, sy, ey, self.grid.ngc)

    def initialize(self, function, t = 0.):
        numArgs = len(getargspec(function)[0])
        x, y = self.getX(), self.getY()
        if numArgs == 2:
            for j in range(self.dimy):
                self.data[:, j] = function(x[:], y[j])
        else:
            for j in range(self.dimy):
                self.data[:, j] = function(x[:], y[j], t)
        self.updateGhosts()

    def generateNewScalarField(self):
        raise NotImplementedError("This method should be called only by a derived class.")

    def l2(self):
        sx, ex, sy, ey = self.grid.getBounds()
        dx, dy = self.grid.getSpacing()
        summation = 0.
        for i in range(sx, ex):
            for j in range(sy, ey):
                summation += (self.data[i, j]) ** 2 
        return numpy.sqrt(summation * dx * dy)
    
class CenterScalarField(ScalarField):
    def __init__(self, grid):
        dx , dy = grid.getSpacing()
        ScalarField.__init__(self, grid, dx / 2., dy / 2.)

    def generateNewScalarField(self, grid):
        return CenterScalarField(grid)

    def __str__(self):
        return "Center"


class RightScalarField(ScalarField):
    def __init__(self, grid):
        dx , dy = grid.getSpacing()
        ScalarField.__init__(self, grid, dx, dy / 2.)

    def generateNewScalarField(self, grid):
        return RightScalarField(grid)

    def toNode(self):
        node = NodeScalarField(self.grid)
        node.data = self.data
        return node

    def __str__(self):
        return "Right"


class BottomScalarField(ScalarField):
    def __init__(self, grid):
        dx , dy = grid.getSpacing()
        ScalarField.__init__(self, grid, dx / 2., 0.)

    def generateNewScalarField(self, grid):
        return BottomScalarField(grid)

    def toNode(self):
        node = NodeScalarField(self.grid)
        node.data = self.data
        return node

    def __str__(self):
        return "Bottom"


class NodeScalarField(ScalarField):
    def __init__(self, grid):
        dx , dy = grid.getSpacing()
        ScalarField.__init__(self, grid, dx, 0.)

    def generateNewScalarField(self, grid):
        return NodeScalarField(grid)

    def __str__(self):
        return "Node"

if __name__ == '__main__':
    from numpy import sin, cos, pi
    for n in (32, 64, 128, 256, 512, 1024, 2048):
        grid = Grid(Point(0., 0.), 1., 1., n, n, 2)

        #tests that the node representation is correct
        func = lambda x, y:sin(2 * pi * x) * cos(2 * pi * y)

        u = RightScalarField(grid)
        v = RightScalarField(grid)
        u.initialize(func)
        v.initialize(func)

        node = NodeScalarField(grid)
        node.initialize(func)

        u.updateGhosts()
        v.updateGhosts()
        node.updateGhosts()

        diffu = u.toNode().data - node.data
        diffv = u.toNode().data - node.data

        print n, 'u', abs(diffu).max()
        print n, 'v', abs(diffv).max()
        print



