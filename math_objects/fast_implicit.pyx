# cython: profile=False

import numpy as np
cimport numpy as np
import cython
from libc.math cimport ceil,floor,int

ctypedef np.double_t DOUBLE_t
ctypedef np.int_t INT_t

@cython.boundscheck(False)
cpdef np.ndarray[DOUBLE_t] computeInteraction(fsi,ib,int point,np.ndarray[np.int_t] listPoints):
    cdef np.ndarray[DOUBLE_t] x, y, nodeX,nodeY,fx,fy, result
    cdef np.ndarray[DOUBLE_t,ndim=2] u_fx,v_fx,u_fy,v_fy
    cdef int sx,ex,sy,ey,nbp,fiber_out,i,j
    cdef double A0,B0,A1,B1,hb,x_size,y_size,origin_x,origin_y,distx,disty,temp_ux,temp_uy,temp_vx,temp_vy
    cdef double w1,w2,w3,w4,delta1,delta2,dx,dy
    
    x, y = ib.getX()
    grid = ib.grid
    dx, dy = grid.getSpacing()
    u_fx, v_fx = fsi.lookupTables.fxLookup.u.data , fsi.lookupTables.fxLookup.v.data
    u_fy, v_fy = fsi.lookupTables.fyLookup.u.data , fsi.lookupTables.fyLookup.v.data
    
    sx, ex, sy, ey = grid.getBounds()
    A0, B0 = grid.origin.x, grid.origin.y
    A1, B1 = A0 + grid.sizeX , B0 + grid.sizeY
    hb = ib.h
    nodeX = fsi.nodeX
    nodeY = fsi.nodeY
    nbp = ib.numberOfPoints
    
    fx, fy = ib.force.x, ib.force.y
    
    x_size = A1 - A0
    y_size = B1 - B0
    origin_x = nodeX[sx]
    origin_y = nodeY[sy]
    
    result = np.zeros(2)
    for fiber_out in listPoints:
        distx = ( x[fiber_out] - x[point] + origin_x + x_size) % x_size
        disty = ( y[fiber_out] - y[point] + origin_y + y_size) % y_size
        
#            get the interpolation data
        i = <unsigned int>(ceil((distx-A0)/dx)) - 1 + sx
        j = (<unsigned int>(floor((disty-B0)/dy))  + sy) % ey
        w1 = abs(nodeY[j] - disty)
        w2 = dy - w1
        w4 = abs(distx - nodeX[i])
        w3 = dx - w4
        
        
        delta1 = (w1*u_fx[i-1,j+1] + w2*u_fx[i-1,j])/dy
        delta2 = (w1*u_fx[i,j+1] + w2*u_fx[i,j])/dy
        temp_ux = (w3*delta2 + w4*delta1)/dx
        
        delta1 = (w1*u_fy[i-1,j+1] + w2*u_fy[i-1,j])/dy
        delta2 = (w1*u_fy[i,j+1] + w2*u_fy[i,j])/dy
        temp_uy = (w3*delta2 + w4*delta1)/dx
        
        delta1 = (w1*v_fx[i-1,j+1] + w2*v_fx[i-1,j])/dy
        delta2 = (w1*v_fx[i,j+1] + w2*v_fx[i,j])/dy
        temp_vx = (w3*delta2 + w4*delta1)/dx
        
        delta1 = (w1*v_fy[i-1,j+1] + w2*v_fy[i-1,j])/dy
        delta2 = (w1*v_fy[i,j+1] + w2*v_fy[i,j])/dy
        temp_vy = (w3*delta2 + w4*delta1)/dx
        
        result[0] += hb*(temp_ux*fx[fiber_out] + temp_uy*fy[fiber_out])
        result[1] += hb*(temp_vx*fx[fiber_out] + temp_vy*fy[fiber_out])
    return result
    
def computeAllInteraction(fsi,ib):
    cdef int nbp, i
    nbp = ib.numberOfPoints
    
    cdef np.ndarray[DOUBLE_t] result = np.zeros(2*nbp)
    cdef np.ndarray[DOUBLE_t] temp
    cdef np.ndarray[INT_t] allPoints = np.array(range(nbp))
    
    
    for i in range(nbp):
        temp = computeInteraction(fsi,ib,i,allPoints)
        result[i] = temp[0]
        result[i+nbp] = temp[1]
    return result
        

#def computeAllInteraction(fsi,ib):
#    cdef np.ndarray[DOUBLE_t] x, y, nodeX,nodeY,fx,fy,result
#    cdef np.ndarray[DOUBLE_t,ndim=2] u_fx,v_fx,u_fy,v_fy
#    cdef int sx,ex,sy,ey,nbp,fiber_in,fiber_out,i,j
#    cdef double A0,B0,A1,B1,hb,x_size,y_size,origin_x,origin_y,distx,disty,temp_ux,temp_uy,temp_vx,temp_vy
#    cdef double w1,w2,w3,w4,delta1,delta2,dx,dy
#    
#    x, y = ib.getX()
#    grid = ib.grid
#    dx, dy = grid.getSpacing()
#    u_fx, v_fx = fsi.lookupTables.fxLookup.u.data , fsi.lookupTables.fxLookup.v.data
#    u_fy, v_fy = fsi.lookupTables.fyLookup.u.data , fsi.lookupTables.fyLookup.v.data
#    
#    sx, ex, sy, ey = grid.getBounds()
#    A0, B0 = grid.origin.x, grid.origin.y
#    A1, B1 = A0 + grid.sizeX , B0 + grid.sizeY
#    hb = ib.h
#    nodeX = fsi.nodeX
#    nodeY = fsi.nodeY
#    nbp = ib.numberOfPoints
#    
#    fx, fy = ib.force.x, ib.force.y
#    result = np.zeros(2*nbp)
#    
#    x_size = A1 - A0
#    y_size = B1 - B0
#    origin_x = nodeX[sx]
#    origin_y = nodeY[sy]
#    
#    for fiber_in in range(nbp):
#        for fiber_out in range(nbp):
#            distx = ( x[fiber_out] - x[fiber_in] + origin_x + x_size) % x_size
#            disty = ( y[fiber_out] - y[fiber_in] + origin_y + y_size) % y_size
#            
##            get the interpolation data
#            i = <unsigned int>(ceil((distx-A0)/dx)) - 1 + sx
#            j = (<unsigned int>(floor((disty-B0)/dy))  + sy) % ey
#            w1 = abs(nodeY[j] - disty)
#            w2 = dy - w1
#            w4 = abs(distx - nodeX[i])
#            w3 = dx - w4
#            
#            
#            delta1 = (w1*u_fx[i-1,j+1] + w2*u_fx[i-1,j])/dy
#            delta2 = (w1*u_fx[i,j+1] + w2*u_fx[i,j])/dy
#            temp_ux = (w3*delta2 + w4*delta1)/dx
#            
#            delta1 = (w1*u_fy[i-1,j+1] + w2*u_fy[i-1,j])/dy
#            delta2 = (w1*u_fy[i,j+1] + w2*u_fy[i,j])/dy
#            temp_uy = (w3*delta2 + w4*delta1)/dx
#            
#            delta1 = (w1*v_fx[i-1,j+1] + w2*v_fx[i-1,j])/dy
#            delta2 = (w1*v_fx[i,j+1] + w2*v_fx[i,j])/dy
#            temp_vx = (w3*delta2 + w4*delta1)/dx
#            
#            delta1 = (w1*v_fy[i-1,j+1] + w2*v_fy[i-1,j])/dy
#            delta2 = (w1*v_fy[i,j+1] + w2*v_fy[i,j])/dy
#            temp_vy = (w3*delta2 + w4*delta1)/dx
#            
#            result[fiber_in] += hb*(temp_ux*fx[fiber_out] + temp_uy*fy[fiber_out])
#            result[fiber_in+nbp] += hb*(temp_vx*fx[fiber_out] + temp_vy*fy[fiber_out])
#    return result
#        