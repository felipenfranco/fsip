'''
Created on Apr 24, 2012

@author: fnfranco
'''
from linear_algebra.biconjugate_gradient import grad, div
from linear_algebra import multigrid
import fast_laplace

class TempVarsProjection:
    def __init__(self):
        self.initialized = False

    def initialize(self, fluid):
        if not self.initialized:
            self.initialized = True
            grid = fluid.grid

            self.oldPressure = fluid.pressure.generateNewScalarField(grid)
            self.gx = fluid.velocity.u.generateNewScalarField(grid)
            self.gy = fluid.velocity.v.generateNewScalarField(grid)
            self.convectionU = fluid.velocity.u.generateNewScalarField(grid)
            self.convectionV = fluid.velocity.v.generateNewScalarField(grid)

            self.multigridU = multigrid.Multigrid(fluid.systemU)
            self.multigridV = multigrid.Multigrid(fluid.systemV)
            self.multigridP = multigrid.Multigrid(fluid.systemP)

            self.nx, self.ny = grid.nx, grid.ny
        else:
            nx, ny = fluid.grid.nx, fluid.grid.ny
            if self.nx != nx or self.ny != ny:
                self.initialized = False
                self.initialize(fluid)


def computeConvection(u, v, convectionU, convectionV):
    sx, ex, sy, ey = u.grid.getBounds(Fortran=True)
    ngc = u.grid.ngc
    dx, dy = u.grid.getSpacing()
    fast_laplace.laplace.compute_convection(u.data, v.data, convectionU.data, convectionV.data, sx, ex, sy, ey, ngc, dx, dy)


def projectionMethod(fluid, dt):
    tempVars = fluid.tempVars
    tempVars.initialize(fluid)

    multigridU = tempVars.multigridU
    multigridV = tempVars.multigridV
    multigridP = tempVars.multigridP

    dx, dy = fluid.grid.getSpacing()
    tolerance = dx * dy
    visc = fluid.visc
    dens = fluid.dens
    rhsU = fluid.systemU.rhs
    rhsV = fluid.systemV.rhs
    rhsP = fluid.systemP.rhs
    gx = tempVars.gx
    gy = tempVars.gy
    oldPressure = tempVars.oldPressure


#    print "Computing grad(P)"
    grad(fluid.pressure, gx, gy)

#    print "Computing Convection term in time n"
    convectionU, convectionV = fluid.getConvection()

    rhsU.data *= (dt / dens)
    rhsV.data *= (dt / dens)

    rhsU.data += fluid.velocity.u.data - (dt / dens) * gx.data - dt * convectionU
    rhsV.data += fluid.velocity.v.data - (dt / dens) * gy.data - dt * convectionV

#    print "Solving for u*"
    multigridU.solve(visc, dens, dt, tolerance, False)
    multigridV.solve(visc, dens, dt, tolerance, False)

#    print "rhsP = div(u*)/dt"
    oldPressure.data[:] = fluid.pressure.data[:]
    rhsP[:] = 0.
    div(fluid.velocity.u, fluid.velocity.v, rhsP)
    rhsP.data *= (dens / dt)

#    print "Solving for p(n+1)"
    fluid.pressure.data[:] = 0.
    multigridP.solve(-1., 0., dt, tolerance)

#    print "u(n+1) = u* - (dt/dens)*grad(p(n+1))"
    grad(fluid.pressure, gx, gy)
    fluid.velocity.u.data -= (dt / dens) * gx.data
    fluid.velocity.v.data -= (dt / dens) * gy.data

    fluid.pressure.data += oldPressure.data

