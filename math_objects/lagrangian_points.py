'''
Created on Mar 9, 2012

@author: felipe
'''
import utils
import numpy


class LagrangianPoints(object):
    '''
    classdocs
    '''


    def __init__(self, numberOfPoints):
        '''
        Constructor
        '''
        self.numberOfPoints = numberOfPoints
        self.x = numpy.zeros(numberOfPoints)
        self.y = numpy.zeros(numberOfPoints)
        self._firstplot = True

    def __get__(self, key):
        return self.x[key], self.y[key]

    def __set__(self, key, data):
        self.x[key] = data[0]
        self.y[key] = data[1]
        
    def __div__(self, other):
        size = min(self.numberOfPoints, other.numberOfPoints)
        temp = LagrangianPoints(size)
        temp.x = self.x[:size] / other.x[:size]
        temp.y = self.y[:size] / other.y[:size]
        return temp
        

    def getX(self):
        return self.x, self.y

    def addPoint(self, x, y):
        self.x = numpy.append(self.x, x)
        self.y = numpy.append(self.y, y)
        if numpy.isscalar(x):
            self.numberOfPoints += 1
        else:
            self.numberOfPoints += len(x)

    def save(self, fileName, txt = False):
        if txt:
            numpy.savetxt(fileName + "_x.txt", self.x)
            numpy.savetxt(fileName + "_y.txt", self.y)
        else:
            numpy.savez(fileName, x = self.x, y = self.y)
            
    def load(self, fileName, txt = False):
        if txt:
            self.x = numpy.loadtxt(fileName + "_x.txt")
            self.y = numpy.loadtxt(fileName + "_y.txt")
        else:
            temp = numpy.load(fileName)
            self.x = temp['x']
            self.y = temp['y']
        self.numberOfPoints = len(self.x)
