from os.path import realpath
import utils


# FORTRAN CODE
updateGhostsCode = '''!    -*- f90 -*-
subroutine update_ghosts(array,dimx,dimy,sx,ex,sy,ey,ngc)
integer, intent(in) :: dimx, dimy,sx,ex,sy,ey,ngc
double precision, dimension(dimx,dimy) , intent(inout) :: array
!f2py intent(inplace) :: array
!f2py threadsafe

!ghosts da esquerda
array(1:ngc,:) = array(ex-ngc+1:ex,:)

!ghosts da direita
array(ex+1:ex+ngc,:) = array(sx:sx+ngc-1,:)

!ghosts de cima
array(:,ey+1:ey+ngc) = array(:,sy:sy+ngc-1)

!ghosts de baixo
array(:,1:ngc) = array(:,ey-ngc+1:ey)

end subroutine update_ghosts
'''


restrictRight = '''!    -*- f90 -*-
!the bounds sx,ex,sy,ey are from the coarse
subroutine restrict_right(fine,dimx_fine,dimy_fine,coarse,dimx_coarse,dimy_coarse,sx,ex,sy,ey)
integer, intent(in) :: dimx_fine, dimy_fine, dimx_coarse, dimy_coarse, sx, ex, sy, ey
double precision, dimension(dimx_fine,dimy_fine) , intent(inout) :: fine
double precision, dimension(dimx_coarse,dimy_coarse) , intent(inout) :: coarse
integer :: i, j
double precision :: r1, r2
!f2py intent(inplace) :: fine, coarse
!f2py threadsafe

coarse = 0.0
do j = sy , ey
    do i = sx , ex
        r1 = fine (2*i-2 , 2*j-2)
        r2 = fine (2*i-2 , 2*j-3)
        coarse(i,j) = (r1+r2)/2.0
    end do
end do
end subroutine restrict_right
'''

restrictBottom = '''!    -*- f90 -*-
!the bounds sx,ex,sy,ey are from the coarse
subroutine restrict_bottom(fine,dimx_fine,dimy_fine,coarse,dimx_coarse,dimy_coarse,sx,ex,sy,ey)
integer, intent(in) :: dimx_fine, dimy_fine, dimx_coarse, dimy_coarse, sx, ex, sy, ey
double precision, dimension(dimx_fine,dimy_fine) , intent(inout) :: fine
double precision, dimension(dimx_coarse,dimy_coarse) , intent(inout) :: coarse
integer :: i, j
double precision :: r1, r2
!f2py intent(inplace) :: fine, coarse
!f2py threadsafe

coarse = 0.0
do j = sy , ey
    do i = sx , ex
        r1 = fine(2*i-3 , 2*j-3)
        r2 = fine(2*i-2 , 2*j-3)
        coarse(i,j) = (r1+r2)/2.0
    end do
end do
end subroutine restrict_bottom
'''

restrictCenter = '''!    -*- f90 -*-
!the bounds sx,ex,sy,ey are from the coarse
subroutine restrict_center(fine,dimx_fine,dimy_fine,coarse,dimx_coarse,dimy_coarse,sx,ex,sy,ey)
integer, intent(in) :: dimx_fine, dimy_fine, dimx_coarse, dimy_coarse, sx, ex, sy, ey
double precision, dimension(dimx_fine,dimy_fine) , intent(inout) :: fine
double precision, dimension(dimx_coarse,dimy_coarse) , intent(inout) :: coarse
integer :: i, j
double precision :: r1, r2, r3, r4
!f2py intent(inplace) :: fine, coarse
!f2py threadsafe

coarse = 0.0
do j = sy , ey
    do i = sx , ex
        r1 = fine(2*i-3 , 2*j-2)
        r2 = fine(2*i-3 , 2*j-3)
        r3 = fine(2*i-2 , 2*j-2)
        r4 = fine(2*i-2 , 2*j-3)
        coarse(i,j) = (r1+r2+r3+r4)/4.0
    end do
end do
end subroutine restrict_center
'''

interpolateRight = '''!    -*- f90 -*-
!the bounds sx,ex,sy,ey are from the coarse
subroutine interpolate_correct_right(fine,dimx_fine,dimy_fine,coarse,dimx_coarse,dimy_coarse,sx,ex,sy,ey)
integer, intent(in) :: dimx_fine, dimy_fine, dimx_coarse, dimy_coarse, sx, ex, sy, ey
double precision, dimension(dimx_fine,dimy_fine) , intent(inout) :: fine
double precision, dimension(dimx_coarse,dimy_coarse) , intent(inout) :: coarse
integer :: i, j
double precision :: c1, c2, c3, c4
!f2py intent(inplace) :: fine, coarse
!f2py threadsafe

do j = sy , ey
    do i = sx , ex
        c1 = (3.0/4.0)*coarse(i,j) + (1.0/4.0)*coarse(i,j-1)
        c2 = (3.0/4.0)*coarse(i,j) + (1.0/4.0)*coarse(i,j+1)
        c3 = (3.0/8.0)*(coarse(i,j)+coarse(i-1,j)) + (1.0/8.0)*(coarse(i-1,j-1)+coarse(i,j-1))
        c4 = (3.0/8.0)*(coarse(i,j)+coarse(i-1,j)) + (1.0/8.0)*(coarse(i-1,j+1)+coarse(i,j+1))

        fine(2*i-2,2*j-3)  = fine(2*i-2,2*j-3) + c1
        fine(2*i-2,2*j-2)  = fine(2*i-2,2*j-2) + c2
        fine(2*i-3,2*j-3)  = fine(2*i-3,2*j-3) + c3
        fine(2*i-3,2*j-2)  = fine(2*i-3,2*j-2) + c4
    end do
end do
end subroutine interpolate_correct_right
'''

interpolateBottom = '''!    -*- f90 -*-
!the bounds sx,ex,sy,ey are from the coarse
subroutine interpolate_correct_bottom(fine,dimx_fine,dimy_fine,coarse,dimx_coarse,dimy_coarse,sx,ex,sy,ey)
integer, intent(in) :: dimx_fine, dimy_fine, dimx_coarse, dimy_coarse, sx, ex, sy, ey
double precision, dimension(dimx_fine,dimy_fine) , intent(inout) :: fine
double precision, dimension(dimx_coarse,dimy_coarse) , intent(inout) :: coarse
integer :: i, j
double precision :: c1, c2, c3, c4
!f2py intent(inplace) :: fine, coarse
!f2py threadsafe

do j = sy , ey
    do i = sx , ex
        c1 = (3.0/4.0)*coarse(i,j) + (1.0/4.0)*coarse(i+1,j)
        c2 = (3.0/4.0)*coarse(i,j) + (1.0/4.0)*coarse(i-1,j)
        c3 = (3.0/8.0)*(coarse(i,j)+coarse(i,j+1)) + (1.0/8.0)*(coarse(i-1,j)+coarse(i-1,j+1))
        c4 = (3.0/8.0)*(coarse(i,j)+coarse(i,j+1)) + (1.0/8.0)*(coarse(i+1,j)+coarse(i+1,j+1))

        fine(2*i-2,2*j-3)  = fine(2*i-2,2*j-3) + c1
        fine(2*i-3,2*j-3)  = fine(2*i-3,2*j-3) + c2
        fine(2*i-3,2*j-2)  = fine(2*i-3,2*j-2) + c3
        fine(2*i-2,2*j-2)  = fine(2*i-2,2*j-2) + c4
    end do
end do
end subroutine interpolate_correct_bottom
'''

interpolateCenter = '''!    -*- f90 -*-
!the bounds sx,ex,sy,ey are from the coarse
subroutine interpolate_correct_center(fine,dimx_fine,dimy_fine,coarse,dimx_coarse,dimy_coarse,sx,ex,sy,ey)
integer, intent(in) :: dimx_fine, dimy_fine, dimx_coarse, dimy_coarse, sx, ex, sy, ey
double precision, dimension(dimx_fine,dimy_fine) , intent(inout) :: fine
double precision, dimension(dimx_coarse,dimy_coarse) , intent(inout) :: coarse
integer :: i, j
double precision :: c1, c2, c3, c4, w1, w2, w3
!f2py intent(inplace) :: fine, coarse
!f2py threadsafe

w1 = 9.0/16.0
w2 = 3.0/16.0
w3 = 1.0/16.0

do j = sy , ey
    do i = sx , ex
        c1 = w1*coarse(i,j) + w2*(coarse(i,j-1)+coarse(i-1,j)) + w3*coarse(i-1,j-1)
        c2 = w1*coarse(i,j) + w2*(coarse(i,j-1)+coarse(i+1,j)) + w3*coarse(i+1,j-1)
        c3 = w1*coarse(i,j) + w2*(coarse(i,j+1)+coarse(i+1,j)) + w3*coarse(i+1,j+1)
        c4 = w1*coarse(i,j) + w2*(coarse(i-1,j)+coarse(i,j+1)) + w3*coarse(i-1,j+1)


        fine(2*i-3,2*j-3)  = fine(2*i-3,2*j-3) + c1
        fine(2*i-2,2*j-3)  = fine(2*i-2,2*j-3) + c2
        fine(2*i-2,2*j-2)  = fine(2*i-2,2*j-2) + c3
        fine(2*i-3,2*j-2)  = fine(2*i-3,2*j-2) + c4
    end do
end do
end subroutine interpolate_correct_center
'''

interpolateAllCode = '''!    -*- f90 -*-
subroutine interpolate_all(lookup,grid,n,m,grid_x,k,grid_y,l,dx,dy,sx,ex,sy,ey)
    implicit none
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: lookup , grid
    double precision , dimension(0:(k-1)) , intent(inout) :: grid_x
    double precision , dimension(0:(l-1)) , intent(inout) :: grid_y
    double precision , intent(in) :: dx , dy
    integer , intent(in) :: n , m , l , sx , ex , sy , ey, k
    double precision :: interpolated_result , distance_x , distance_y , delta
    integer :: i , j , ii , jj
    double precision :: pi = 4.0d0*datan(1.0d0)

    !f2py intent(in) :: dx , dy , sx , ex , sy , ey , hb
    !f2py intent(inplace) :: lookup , grid_x , grid_y , grid
    !f2py intent(hide) :: n , m , l, k

    do j = sy-1 , ey-1
        do i = sx-1 , ex-1
                interpolated_result = 0.0d0
                do jj = j-2 , j+2
                    do ii = i-2 , i+2
                        distance_x = abs(grid_x(i) - grid_x(ii))
                        distance_y = abs(grid_y(j) - grid_y(jj))
                        call compute_delta(distance_x , distance_y , dx , dy , delta , pi)
                        interpolated_result = interpolated_result + grid(ii,jj)*delta
                    end do
                end do
                lookup(i,j) = interpolated_result*dx*dy
        end do
    end do
end subroutine
'''

# codeInterpolateEulerianU = '''!    -*- f90 -*-
# subroutine interpolate_eulerian_u(lookup,n,m,x,y,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
#    implicit none
#    double precision , intent(in) :: distx , disty , A0 , B0 , dx, dy
#    integer , intent(in) :: sx , sy , ey , n , m , l
#    double precision :: interpolated_value , w1 , w2 , w3 , w4 , a , b , c , d , delta1 , delta2
#    integer :: i , j
#    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: lookup
#    double precision , dimension(0:(l-1)) , intent(inout) :: x , y
#    !f2py intent(in) :: distx , disty , A0 , B0 , dx, dy , sx , sy , ey
#    !f2py intent(out) :: interpolated_value
#    !f2py intent(inplace) :: lookup , x , y
#    !f2py intent(hide) :: m , n  , l
#    i = int(ceiling((distx-A0)/dx)) - 1 + sx
#    j = modulo(int(floor((disty-B0)/dy))  + sy , ey)
#
#    w1 = abs(x(i) - distx)
#    w2 = dx - w1
#    w3 = abs(disty - y(j))
#    w4 = dy - w3
#
#    if (disty > y(j)) then
#        a = lookup(i,j)
#        b = lookup(i-1,j)
#        c = lookup(i-1,j+1)
#        d = lookup(i,j+1)
#    else
#        a = lookup(i,j)
#        b = lookup(i-1,j)
#        c = lookup(i-1,j-1)
#        d = lookup(i,j-1)
#    end if
#
#    delta1 = (w1*b + w2*a)/dx
#    delta2 = (w1*c + w2*d)/dx
#    interpolated_value = (w3*delta2 + w4*delta1)/dy
# end subroutine
# '''
# codeInterpolateEulerianV = '''!    -*- f90 -*-
# subroutine interpolate_eulerian_v(lookup,n,m,x,y,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
#    implicit none
#    double precision , intent(in) :: distx , disty , A0 , B0 , dx, dy
#    integer , intent(in) :: sx , sy , ey , n , m , l
#    double precision :: interpolated_value , w1 , w2 , w3 , w4 , a , b , c , d , delta1 , delta2
#    integer :: i , j
#    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: lookup
#    double precision , dimension(0:(l-1)) , intent(inout) :: x , y
#    !f2py intent(in) :: distx , disty , A0 , B0 , dx, dy , sx , sy , ey
#    !f2py intent(out) :: interpolated_value
#    !f2py intent(inplace) :: lookup , x , y
#    !f2py intent(hide) :: m , n  , l
#    i = int(ceiling((distx-A0)/dx)) - 1 + sx
#    j = modulo(int(floor((disty-B0)/dy))  + sy , ey)
#
#    w1 = abs(y(j) - disty)
#    w2 = dy - w1
#    w3 = abs(distx - x(i))
#    w4 = dx - w3
#
#    if (distx > x(i)) then
#        a = lookup(i,j)
#        b = lookup(i,j+1)
#        c = lookup(i+1,j+1)
#        d = lookup(i+1,j)
#    else
#        a = lookup(i,j)
#        b = lookup(i,j+1)
#        c = lookup(i-1,j+1)
#        d = lookup(i-1,j)
#    end if
#
#    delta1 = (w1*b + w2*a)/dy
#    delta2 = (w1*c + w2*d)/dy
#    interpolated_value = (w3*delta2 + w4*delta1)/dx
# end subroutine
# '''

codeInterpolateNode = '''!    -*- f90 -*-
subroutine interpolate_node(G,n,m,x,y,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
    implicit none
    double precision , intent(in) :: distx , disty , A0 , B0 , dx, dy
    integer , intent(in) :: sx , sy , ey , n , m , l
    double precision :: interpolated_value , w1 , w2 , w3 , w4 , a , b , c , d , delta1 , delta2
    integer :: i , j
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: G
    double precision , dimension(0:(l-1)) , intent(inout) :: x , y
    !f2py intent(inplace) :: G , x , y
    !f2py intent(in) :: distx , disty , A0 , B0 , dx, dy , sx , sy , ey
    !f2py intent(out) :: interpolated_value
    !f2py intent(hide) :: m , n  , l

    !compute the ij index of the point
    i = int(ceiling((distx-A0)/dx)) - 1 + sx
    j = modulo(int(floor((disty-B0)/dy))  + sy , ey)

    !compute the interpolation weight for each point
    w1 = abs(y(j) - disty)
    w2 = dy - w1
    w4 = abs(distx - x(i))
    w3 = dx - w4

    a = G(i-1,j)
    b = G(i-1,j+1)
    c = G(i,j+1)
    d = G(i,j)

    delta1 = (w1*b + w2*a)/dy
    delta2 = (w1*c + w2*d)/dy
    interpolated_value = (w3*delta2 + w4*delta1)/dx
end subroutine
'''


codeMakeMatrixNew = '''!    -*- f90 -*-
subroutine make_interaction_matrix_new(Matrix,dimx,dimy,ufx,vfx,ufy,vfy,n,m,x,y,nbp,x_node,l,y_node,&
                                    k,A0,A1,B0,B1,dx,dy,sx,sy,ey,hb)
    implicit none
    double precision , intent(in) :: A0 , A1 , B0 , B1 , dx, dy, hb
    integer , intent(in) :: n , m , l , nbp , dimx , dimy , sx , sy , ey , k
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: ufx , vfx , ufy , vfy
    double precision , dimension(0:(dimx-1),0:(dimy-1)) , intent(inout) :: Matrix
    double precision , dimension(0:(nbp-1)) , intent(inout) :: x , y !x and y of the fiber
    double precision , dimension(0:(l-1)) , intent(inout) :: x_node
    double precision , dimension(0:(k-1)) , intent(inout) :: y_node
    integer :: fiber_in , fiber_out
    double precision :: x_size , y_size , distx , disty , interpolated_value
    double precision :: lpu_origin_x, lpu_origin_y, lpv_origin_x, lpv_origin_y

    !f2py intent(in) :: A0 , B0 , dx, dy , sx , sy , ey, hb
    !f2py intent(inplace) :: Matrix, ufx , vfx , ufy , vfy , x , y , x_node, y_node
    !f2py intent(hide) :: m , n  , l , nbp , dimx , dimy, k

    x_size = A1 - A0
    y_size = B1 - B0

    lpu_origin_x = x_node(sx)
    lpu_origin_y = y_node(sy)
    lpv_origin_x = x_node(sx)
    lpv_origin_y = y_node(sy)


    do fiber_in = 0 , nbp-1
        do fiber_out = 0 , nbp-1
            distx = modulo( x(fiber_out) - x(fiber_in) + lpu_origin_x + x_size, x_size)
            disty = modulo( y(fiber_out) - y(fiber_in) + lpu_origin_y + y_size, y_size)

            call interpolate_node(ufx,n,m,x_node,y_node,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
            Matrix(fiber_in , fiber_out)      = hb*interpolated_value
            call interpolate_node(ufy,n,m,x_node,y_node,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
            Matrix(fiber_in , nbp+fiber_out)  = hb*interpolated_value


            distx = modulo( x(fiber_out) - x(fiber_in) + lpv_origin_x + x_size, x_size)
            disty = modulo( y(fiber_out) - y(fiber_in) + lpv_origin_y + y_size, y_size)

            call interpolate_node(vfx,n,m,x_node,y_node,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
            Matrix(nbp+fiber_in , fiber_out)      = hb*interpolated_value
            call interpolate_node(vfy,n,m,x_node,y_node,l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
            Matrix(nbp+fiber_in , nbp+fiber_out)  = hb*interpolated_value
        end do
    end do
end subroutine
'''



codeMakeMatrixBig = '''!    -*- f90 -*-
subroutine make_interaction_matrix_big(Matrix,dimx,dimy,ufx,vfx,ufy,vfy,n,m,x,y,nbp,x_node,l,y_node,&
                                    k,A0,A1,B0,B1,dx,dy,sx,sy,ey,hb)
    implicit none
    double precision , intent(in) :: A0 , A1 , B0 , B1 , dx, dy, hb
    integer , intent(in) :: n , m , l , nbp , dimx , dimy , sx , sy , ey , k
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: ufx , vfx , ufy , vfy
    double precision , dimension(0:(dimx-1),0:(dimy-1)) , intent(inout) :: Matrix
    double precision , dimension(0:(nbp-1)) , intent(inout) :: x , y !x and y of the fiber
    double precision , dimension(0:(l-1)) , intent(inout) :: x_node
    double precision , dimension(0:(k-1)) , intent(inout) :: y_node
    integer :: fiber_in , fiber_out, i ,j
    double precision :: x_size , y_size , distx , disty , distx2, disty2, interpolated_value
    double precision :: lp_origin_x, lp_origin_y, w1, w4

    !f2py intent(in) :: A0 , B0 , dx, dy , sx , sy , ey, hb
    !f2py intent(inplace) :: Matrix, ufx , vfx , ufy , vfy , x , y , x_node, y_node
    !f2py intent(hide) :: m , n  , l , nbp , dimx , dimy, k

    x_size = A1 - A0
    y_size = B1 - B0

    lp_origin_x = x_node(sx)
    lp_origin_y = y_node(sy)

    do fiber_in = 0 , nbp-1
        do fiber_out = 0 , fiber_in
            distx = modulo( x(fiber_out) - x(fiber_in) + lp_origin_x + x_size, x_size)
            disty = modulo( y(fiber_out) - y(fiber_in) + lp_origin_y + y_size, y_size)

            !compute the ij index of the point
            i = int(ceiling((distx-A0)/dx)) - 1 + sx
            j = modulo(int(floor((disty-B0)/dy))  + sy , ey)

            !compute the interpolation weight for each point
            w1 = abs(y_node(j) - disty)
            w4 = abs(distx - x_node(i))

            Matrix(fiber_in , fiber_out)         = hb*((dx-w4)*((w1*ufx(i,j+1) + (dy-w1)*ufx(i,j))/dy) + w4*((w1*ufx(i-1,j+1) + (dy-w1)*ufx(i-1,j))/dy))/dx
            Matrix(nbp+fiber_in , fiber_out)     = hb*((dx-w4)*((w1*vfx(i,j+1) + (dy-w1)*vfx(i,j))/dy) + w4*((w1*vfx(i-1,j+1) + (dy-w1)*vfx(i-1,j))/dy))/dx
            Matrix(nbp+fiber_in , nbp+fiber_out) = hb*((dx-w4)*((w1*vfy(i,j+1) + (dy-w1)*vfy(i,j))/dy) + w4*((w1*vfy(i-1,j+1) + (dy-w1)*vfy(i-1,j))/dy))/dx



            distx = modulo( x(fiber_in) - x(fiber_out) + lp_origin_x + x_size, x_size)
            disty = modulo( y(fiber_in) - y(fiber_out) + lp_origin_y + y_size, y_size)
            i = int(ceiling((distx-A0)/dx)) - 1 + sx
            j = modulo(int(floor((disty-B0)/dy))  + sy , ey)
            w1 = abs(y_node(j) - disty)
            w4 = abs(distx - x_node(i))

            Matrix(nbp+fiber_out , fiber_in)      = hb*((dx-w4)*((w1*vfx(i,j+1) + (dy-w1)*vfx(i,j))/dy) + w4*((w1*vfx(i-1,j+1) + (dy-w1)*vfx(i-1,j))/dy))/dx



            Matrix(fiber_out, fiber_in) = Matrix(fiber_in , fiber_out)
            Matrix(fiber_out, nbp+fiber_in) = Matrix(nbp+fiber_in , fiber_out)
            Matrix(nbp+fiber_out, nbp+fiber_in) = Matrix(nbp+fiber_in , nbp+fiber_out)
            Matrix(fiber_in, nbp+fiber_out) = Matrix(nbp+fiber_out , fiber_in)
        end do
    end do

end subroutine
'''


codeMakeMatrixOpt = '''!    -*- f90 -*-
subroutine make_interaction_matrix_opt(Matrix,dimx,dimy,ufx,vfx,ufy,vfy,n,m,x,y,nbp,x_node,l,y_node,&
                                    k,A0,A1,B0,B1,dx,dy,sx,sy,ey,hb)
    Use omp_lib
    implicit none
    double precision , intent(in) :: A0 , A1 , B0 , B1 , dx, dy, hb
    integer , intent(in) :: n , m , l , nbp , dimx , dimy , sx , sy , ey , k
    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: ufx , vfx , ufy , vfy
    double precision , dimension(0:(dimx-1),0:(dimy-1)) , intent(inout) :: Matrix
    double precision , dimension(0:(nbp-1)) , intent(inout) :: x , y !x and y of the fiber
    double precision , dimension(0:(l-1)) , intent(inout) :: x_node
    double precision , dimension(0:(k-1)) , intent(inout) :: y_node
    double precision , dimension(0:(nbp-1),0:(nbp-1)) :: distx_m , disty_m
    double precision , dimension(0:(nbp-1),0:(nbp-1)) :: w1_m , w2_m, w3_m, w4_m
    integer , dimension(0:(nbp-1),0:(nbp-1)) :: i_m, j_m
    integer :: fiber_in , fiber_out, i , j, i2, j2
    double precision :: x_size , y_size,distx,disty,distx2,disty2
    double precision :: lp_origin_x, lp_origin_y, w1,w2,w3,w4

    !f2py threadsafe
    !f2py intent(in) :: A0 , B0 , dx, dy , sx , sy , ey, hb
    !f2py intent(inplace) :: Matrix, ufx , vfx , ufy , vfy , x , y , x_node, y_node
    !f2py intent(hide) :: m , n  , l , nbp , dimx , dimy, k

    x_size = A1 - A0
    y_size = B1 - B0

    lp_origin_x = x_node(sx)
    lp_origin_y = y_node(sy)


    print *, "Number of threads:", OMP_GET_NUM_THREADS()


    !$OMP PARALLEL SHARED(distx_m, disty_m, x, y)
    print *, "Number of threads:", OMP_GET_NUM_THREADS()
    !$OMP WORKSHARE
    forall(fiber_in=0:nbp-1, fiber_out=0:nbp-1)
            distx_m(fiber_in, fiber_out) = x(fiber_out) - x(fiber_in)
            disty_m(fiber_in, fiber_out) = y(fiber_out) - y(fiber_in)
    end forall
    !$OMP END WORKSHARE
    !$OMP END PARALLEL



    !$OMP PARALLEL SHARED(distx_m, disty_m, i_m, j_m)
    !$OMP WORKSHARE
        distx_m = modulo(distx_m + lp_origin_x + x_size, x_size)
        disty_m = modulo(disty_m + lp_origin_y + y_size, y_size)
    !$OMP END WORKSHARE

    !$OMP WORKSHARE
        i_m = int(ceiling((distx_m-A0)/dx)) - 1 + sx
        j_m = modulo(int(floor((disty_m-B0)/dy))  + sy , ey)
    !$OMP END WORKSHARE
    !$OMP END PARALLEL


    !$OMP PARALLEL SHARED(w1_m, w2_m, w3_m, w4_m)
    !$OMP WORKSHARE
    forall(fiber_in=0:nbp-1, fiber_out=0:nbp-1)
            w1_m(fiber_in,fiber_out) = abs(y_node(j_m(fiber_in,fiber_out)) - disty_m(fiber_in,fiber_out))
            w4_m(fiber_in,fiber_out) = abs(distx_m(fiber_in,fiber_out) - x_node(i_m(fiber_in,fiber_out)))
    end forall
    !$OMP END WORKSHARE
    !$OMP END PARALLEL



    !$OMP PARALLEL DEFAULT(SHARED), PRIVATE(fiber_out, fiber_in, i, j, w1, w4)
    !$OMP DO
    do fiber_out = 0, nbp-1
        do fiber_in = 0, nbp-1
            i = i_m(fiber_in,fiber_out)
            j = j_m(fiber_in,fiber_out)
            w1 = w1_m(fiber_in,fiber_out)
            !w2 = w2_m(fiber_in,fiber_out)
            !w3 = w3_m(fiber_in,fiber_out)
            w4 = w4_m(fiber_in,fiber_out)


            Matrix(fiber_in , fiber_out)         = hb*((dx-w4)*((w1*ufx(i,j+1) + (dy-w1)*ufx(i,j))/dy) + w4*((w1*ufx(i-1,j+1) + (dy-w1)*ufx(i-1,j))/dy))/dx
            Matrix(fiber_in , nbp+fiber_out)     = hb*((dx-w4)*((w1*ufy(i,j+1) + (dy-w1)*ufy(i,j))/dy) + w4*((w1*ufy(i-1,j+1) + (dy-w1)*ufy(i-1,j))/dy))/dx
            Matrix(nbp+fiber_in , fiber_out)     = hb*((dx-w4)*((w1*vfx(i,j+1) + (dy-w1)*vfx(i,j))/dy) + w4*((w1*vfx(i-1,j+1) + (dy-w1)*vfx(i-1,j))/dy))/dx
            Matrix(nbp+fiber_in , nbp+fiber_out) = hb*((dx-w4)*((w1*vfy(i,j+1) + (dy-w1)*vfy(i,j))/dy) + w4*((w1*vfy(i-1,j+1) + (dy-w1)*vfy(i-1,j))/dy))/dx

        end do
    end do
    !$OMP END DO
    !$OMP END PARALLEL


end subroutine
'''



codeScalarFieldHeader = '''!    -*- f90 -*-
module scalar_field
!include "omp_lib.h"
contains
'''

codeLookupHeader = '''!    -*- f90 -*-
module lookup
!include "omp_lib.h"
contains
'''

codeMatrixHeader = '''!    -*- f90 -*-
module matrix
include "omp_lib.h"
contains
'''


codeScalarField = codeScalarFieldHeader + updateGhostsCode + restrictRight + restrictBottom + restrictCenter
codeScalarField += interpolateRight + interpolateBottom + interpolateCenter + "end module"

codeMatrix = codeMatrixHeader + codeInterpolateNode + codeMakeMatrixNew + codeMakeMatrixBig + codeMakeMatrixOpt + "end module"

from physic_objects import diracDelta
codeLookup = codeLookupHeader + diracDelta + interpolateAllCode + "end module"


def retrieveCode():
    name_of_current_file = realpath(__file__)
    codeInfo = [(codeScalarField, "fast_scalar_field", name_of_current_file)]
    codeInfo.append((codeMatrix, "fast_matrix", name_of_current_file))
    codeInfo.append((codeLookup, "fast_lookup", name_of_current_file))
    return codeInfo
