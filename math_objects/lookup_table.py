'''
Created on Mar 15, 2012

@author: felipe
'''
import utils
from math_objects.vector_field import VectorField
from physic_objects.fluid import StokesFluid, NavierStokesFluid
from physic_objects.ib import IB
from numpy import savez, load
from utils import rootPath
from os.path import exists
import fast_lookup
import numpy
import pylab

class LookupTable:
    '''
    Contains the 2 VectorFields that represent the lookup tables.
        fxLookup: the (x,y)-velocity for a force (1,0)
        fyLookup: the (x,y)-velocity for a force (0,1)

        The force for the x components are centered on the Right Face
        The force for the y components are centered on the Bottom Face
    '''


    def __init__(self, fluid, dt):
        self.grid = fluid.grid
        self.fluid = fluid
        self.dt = dt

        self.fxLookup = VectorField(self.grid)
        self.fyLookup = VectorField(self.grid)

        if fluid.ID is "StokesFluid" or fluid.ID is "PolimericStokesFluid":
            self.workFluid = StokesFluid(fluid.grid, fluid.visc)
            self.alpha = dt

        elif fluid.ID is "NavierStokesFluid" or fluid.ID is "PolimericNavierStokesFluid":
            self.workFluid = NavierStokesFluid(fluid.grid, fluid.visc, fluid.dens)
#            self.alpha = (dt ** 2) / fluid.dens
            # AHA! o problema estava aqui, muito cuidado!
            self.alpha = (dt) / fluid.dens

        else:
            raise TypeError("Fluid type %s not supported!" % (fluid.ID))

        self.workIb = IB(1)

        self.name = "%slookup_nx_ny_%d_%d_visc_%g_dens_%g_dt_%g" % \
        (fluid.ID, self.grid.nx, self.grid.ny, fluid.visc, fluid.dens, self.dt)

        self.filePath = rootPath + "//cached_arrays//%s" % (self.name)
        if exists(self.filePath):
            self.load()
        else:
            self.generateAllLookups()
            self.save()


    def save(self):
        fileCache = open(self.filePath, 'w')
        savez(fileCache, fxLookupU=self.fxLookup.u.data, fxLookupV=self.fxLookup.v.data,
              fyLookupU=self.fyLookup.u.data, fyLookupV=self.fyLookup.v.data)

    def load(self):
        cachedArrays = load(self.filePath)
        self.fxLookup.u.data = cachedArrays['fxLookupU']
        self.fxLookup.v.data = cachedArrays['fxLookupV']
        self.fyLookup.u.data = cachedArrays['fyLookupU']
        self.fyLookup.v.data = cachedArrays['fyLookupV']

    def interpolate_all(self, scalarField, lookupField):
        """
            For all the eulerian points in the velocity field contained in grid, interpolate
            the surrounding velocities and store the result in the lookuptable.
            The hb multiplying the result is to account for the number of fiber points in the
            problem Immersed Boundary.
        """
        sx, ex, sy, ey = self.grid.getBounds(Fortran=True)
        dx, dy = self.grid.getSpacing()
        x, y = scalarField.getX(), scalarField.getY()
        fast_lookup.lookup.interpolate_all(lookupField.data, scalarField.data, x, y, dx, dy,
                                            sx, ex, sy, ey)

        lookupField.data *= self.alpha
        lookupField.updateGhosts()

    def generateLookup(self, velocityComponent, lookupComponent, force):
        fluid = self.workFluid
        ib = self.workIb
        sx, ex, sy, ey = self.grid.getBounds()

        ib.x[0] = velocityComponent.getX()[sx]
        ib.y[0] = velocityComponent.getY()[sy]
        ib.force.x[0] , ib.force.y[0] = force

        # spread the force
        fluid.clear()
        ib.spreadForce(fluid.systemU.rhs, fluid.systemV.rhs)
#        print numpy.max(numpy.abs(fluid.systemU.rhs[:])) , numpy.max(numpy.abs(fluid.systemV.rhs[:]))
        # solve for the velocity
        fluid.advanceInTime(self.dt)
        # interpolate for all eulerian points
        self.interpolate_all(velocityComponent , lookupComponent)


    def generateAllLookups(self):
        '''
            Since the equation is Au = rhs, and the force on the right hand side is with the negative signal
            the correct force is (-1,0) and (0,-1)
        '''
        fx = (1., 0.)
        fy = (0., 1.)
        self.generateLookup(self.workFluid.velocity.u, self.fxLookup.u, fx)
        self.generateLookup(self.workFluid.velocity.v, self.fxLookup.v, fx)
        self.generateLookup(self.workFluid.velocity.u, self.fyLookup.u, fy)
        self.generateLookup(self.workFluid.velocity.v, self.fyLookup.v, fy)



if __name__ == '__main__':
    from physic_objects.fluid import StokesFluid
    from physic_objects.force import EllasticForce
    from math_objects.cartesian_grid import *
    from time import sleep

    n = 128
    nbp = n * 2
    grid = Grid(Point(0., 0.), 1.0, 1.0, n, n, 2)

    visc = 1.
    dens = 1.

#    fluid = StokesFluid(grid, visc)
    fluid = NavierStokesFluid(grid, visc, dens)

    dt = grid.dx
    lookup = LookupTable(fluid, dt)
    print lookup.fxLookup.v.data

    pylab.ioff()
    pylab.pcolor(lookup.fxLookup.u.data.T)
    pylab.colorbar()
    pylab.show()


