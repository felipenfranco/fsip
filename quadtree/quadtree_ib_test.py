'''
Created on May 19, 2013

@author: felipe
'''
import utils
from math_objects.cartesian_grid import Grid, Point
from physic_objects.force import EllasticForce
from physic_objects.ib import Ellipse
from physic_objects.fluid import StokesFluid
from physic_objects.fluid_structure import FluidStructure
import pylab

if __name__ == '__main__':
    import pylab
    from time import time

    n = 64
    nbp = 2 * n
    ellasticConst = 10.

    grid = Grid(Point(0., 0.), 1., 1., n, n, 2)
    ib = Ellipse(nbp, Point(.5, .5), .2, .1, grid, EllasticForce(ellasticConst))
    ibQuad = Ellipse(nbp, Point(.5, .5), .2, .1, grid, EllasticForce(ellasticConst))

#     pylab.axis([0, 1, 0, 1])
#     pylab.plot(ib.x, ib.y)
#     pylab.show()

    fluid = StokesFluid(grid)
    fluidQuad = StokesFluid(grid)
    dt = grid.dx

    systemQuad = FluidStructure(fluidQuad, ibQuad, dt, "Implicit", "Quadtree")
    system = FluidStructure(fluid, ib, dt, "Implicit", "Matrix")
    pylab.ion()
    pylab.show()
    for i in range(100):
        system.advanceInTime(dt, False)
        systemQuad.advanceInTime(dt, False)
        print abs(ib.x - ibQuad.x).max(), abs(ib.y - ibQuad.y).max()

        pylab.clf()
        pylab.plot(ib.x, ib.y, 'bo')
        pylab.plot(ibQuad.x, ibQuad.y, 'ro')
        pylab.draw()

