'''
Created on Apr 30, 2012

@author: fnfranco
'''
import utils
import numpy
from numpy import fft, real, array
from utils import rootPath
from os.path import exists
from math_objects.cartesian_grid import Point
import fast_treecode
from math_objects.scalar_field import NodeScalarField





def Convolve(xhat, y):
    """Convolve the scalar field y with x. xhat is the Fourier transform of x."""

    yhat = fft.fftn(y)
    return real(fft.ifftn(xhat * yhat))


class Decomposition:
    def __init__(self, grid, depth, numberExpansionTerms, referenceName):
        self.decomposition_set = False
        self.grid = grid
        self.depth = depth
        self.numberExpansionTerms = numberExpansionTerms
        name = "%s_%d_%d" % (referenceName, self.depth, numberExpansionTerms)
        self.filePath = rootPath + "//cached_arrays//%s" % (name)

        center_i = (grid.ex - grid.sx) / 2 + grid.ngc
        center_j = (grid.ey - grid.sy) / 2 + grid.ngc
        self.center = Point(grid.getX()[center_i], grid.getY()[center_j])

        mySizeX = grid.sizeX / (2.** (depth - 1))
        mySizey = grid.sizeY / (2.** (depth - 1))

        # separate the domain
        # for future use WITH THE SCALAR FIELD representation of A and B
        IndexWidth = int(mySizeX / grid.dx)
        IndexHeight = int(mySizey / grid.dy)

        # the +- 2 are to account for boundary cases
        # Outgoing region (inside the panel)
        # [B_x1,B_x2] x [B_y1,B_y2]
        self.B_x1 = center_i - IndexWidth / 2 - 2
        self.B_x2 = center_i + IndexWidth / 2 + 2
        self.B_y1 = center_j - IndexHeight / 2 - 2
        self.B_y2 = center_j + IndexHeight / 2 + 2

        # Complement of Incoming region (well separated from the panel)
        # Incoming region = Domain \ [A_x1,A_x2] x [A_y1,A_y2]
        Vwidth = 3 * IndexWidth
        Vheight = 3 * IndexHeight
        self.A_x1 = center_i - Vwidth / 2 + 2
        self.A_x2 = center_i + Vwidth / 2 - 2
        self.A_y1 = center_j - Vheight / 2 + 2
        self.A_y2 = center_j + Vheight / 2 - 2

    def setDecomposition(self, expansions):
        self.ufx = expansions[0]
        self.vfx = expansions[1]
        self.ufy = expansions[2]
        self.vfy = expansions[3]
        self.decomposition_set = True

    def save(self):
        if self.decomposition_set:
            numpy.savez(self.filePath,
                        ufxA=self.ufx[0], ufxB=self.ufx[1],
                        vfxA=self.vfx[0], vfxB=self.vfx[1],
                        ufyA=self.ufy[0], ufyB=self.ufy[1],
                        vfyA=self.vfy[0], vfyB=self.vfy[1])

    def load(self):
        if exists(self.filePath + ".npz"):
            temp = numpy.load(self.filePath + ".npz")
            ufxA, ufxB = temp['ufxA'], temp['ufxB']
            vfxA, vfxB = temp['vfxA'], temp['vfxB']
            ufyA, ufyB = temp['ufyA'], temp['ufyB']
            vfyA, vfyB = temp['vfyA'], temp['vfyB']
            self.ufx = (ufxA, ufxB)
            self.vfx = (vfxA, vfxB)
            self.ufy = (ufyA, ufyB)
            self.vfy = (vfyA, vfyB)
            self.decomposition_set = True

        return self.decomposition_set




class FarFieldExpansion:
    def __init__(self, numLevels, lookupTable, numberExpansionTerms):
        self.numLevels = numLevels
        self.maxDepth = numLevels + 2
        self.lookupTable = lookupTable
        self.numberExpansionTerms = numberExpansionTerms
        self.grid = lookupTable.grid
        tempNode = NodeScalarField(lookupTable.grid)
        self.nodeX = tempNode.getX()
        self.nodeY = tempNode.getY()

        self._createDecompositionGroup()
        self.load()  # tries to load the decomposition from disk. If any dont exists, compute it.

    def _createDecompositionGroup(self):
        self.decompositions = []
        for depth in range(3, self.numLevels + 3):
            self.decompositions.append(Decomposition(self.grid, depth, self.numberExpansionTerms, self.lookupTable.name))

    def _computeDecomposition(self, decomposition, powerIterations=20):
        ngc = self.grid.ngc
        sx, ex, sy, ey = self.grid.getBounds()

        A_x1, A_x2, A_y1, A_y2 = decomposition.A_x1 - ngc, decomposition.A_x2 - ngc, decomposition.A_y1 - ngc, decomposition.A_y2 - ngc
        B_x1, B_x2, B_y1, B_y2 = decomposition.B_x1 - ngc, decomposition.B_x2 - ngc, decomposition.B_y1 - ngc, decomposition.B_y2 - ngc

        # defines the indicator functions
        def indicatorA(A):
            A[A_x1:A_x2, A_y1:A_y2] *= 0
            return A

        def indicatorB(B):
            hold = B[B_x1:B_x2, B_y1:B_y2].copy()
            B *= 0
            B[B_x1:B_x2, B_y1:B_y2] = hold
            return B

        # compute the 4 expansions for each of the lookup tables and for each decomposition
        fxLookup = self.lookupTable.fxLookup
        fyLookup = self.lookupTable.fyLookup

        expansions = []  # holds [(Au,Bu)_fx,(Av,Bv)_fx,(Au,Bu)_fy,(Av,Bv)_fy]

        for lookup in [fxLookup, fyLookup]:
            print "\nLookup."
            Au, Bu = [], []
            Av, Bv = [], []

            for G, A, B in zip([lookup.u, lookup.v], [Au, Av], [Bu, Bv]):
                print '*',
                phi = G[sx:ex, sy:ey]
                phiHat = real(fft.fftn(phi))

                # create and initialize the expansion terms with noise
                for l in range(self.numberExpansionTerms):
                    A.append(numpy.random.random(phi.shape))
                    B.append(numpy.random.random(phi.shape))

                B[0] = indicatorB(B[0])
#                singularValues = []

                for l in range(self.numberExpansionTerms):
                    print '.',
                    A[l] = indicatorA(A[l])

                    for i in range(powerIterations):
                        # compute the convolution for B
                        less = 0
                        for _l in range(l):
                            less += B[_l] * (A[_l] * A[l]).sum()
                        B[l] = (Convolve(phiHat, A[l]) - less) / (A[l] ** 2).sum()
                        B[l] = indicatorB(B[l])

                        # renormalize
                        norm1 = (B[l] ** 2).sum() ** .5
                        norm2 = (A[l] ** 2).sum() ** .5
                        B[l] *= (norm2 / norm1) ** .5
                        A[l] *= (norm1 / norm2) ** .5

                        # compute the convolution for A
                        less = 0
                        for _l in range(l):
                            less += B[_l] * (A[_l] * B[l]).sum()
                        A[l] = (Convolve(phiHat, B[l]) - less) / (B[l] ** 2).sum()
                        A[l] = indicatorA(A[l])

                        # renormalize
                        norm1 = (B[l] ** 2).sum() ** .5
                        norm2 = (A[l] ** 2).sum() ** .5
                        B[l] *= (norm2 / norm1) ** .5
                        A[l] *= (norm1 / norm2) ** .5

#                    singularValues.append(((B[l] ** 2).sum() * (A[l] ** 2).sum()) ** .5)
#                print "Singular Values:", singularValues
#                singularValues.sort()
#                singularValues.reverse()
#                pylab.yscale('log')
#                pylab.plot(singularValues, label = str(G.grid.nx))

                # convert the results to ScalarFields
                for l in range(self.numberExpansionTerms):
                    tempA = G.generateNewScalarField(G.grid)
                    tempB = G.generateNewScalarField(G.grid)
                    tempA[sx:ex, sy:ey] = A[l]
                    tempB[sx:ex, sy:ey] = B[l]
                    tempA.updateGhosts()
                    tempB.updateGhosts()

                    A[l] = array(tempA.data, order='F')
                    B[l] = array(tempB.data, order='F')
                A = array(A, order='F')
                B = array(B, order='F')
                expansions.append((A, B))

        decomposition.setDecomposition(expansions)
        decomposition.save()

    def computeFFE(self):
        i = 1
        for decomp in self.decompositions:
            print "\nComputing for decomposition %d of %d" % (i, len(self.decompositions))
            i += 1
            self._computePanelFFE(decomp)

    def load(self):
        for decomp in self.decompositions:
            if not decomp.load():
                self._computeDecomposition(decomp)

    def getBaseDecomposition(self, depth):
        assert(3 <= depth <= self.maxDepth), "%d,%d" % (depth, self.maxDepth)
        return self.decompositions[depth - 3]


    def computeSummation(self, _panel, x, y, inside, outside, lp):
        basePanel = self.getBaseDecomposition(_panel.depth)
        # make the shift to the base panel position
        shiftX = basePanel.center.x - _panel.center.x
        shiftY = basePanel.center.y - _panel.center.y

        if lp == 'ufx':
            A, B = basePanel.ufx
        if lp == 'ufy':
            A, B = basePanel.ufy
        if lp == 'vfx':
            A, B = basePanel.vfx
        if lp == 'vfy':
            A, B = basePanel.vfy

        xout, yout = self.grid.inside(x[outside] + shiftX, y[outside] + shiftY)
        xin, yin = self.grid.inside(x[inside] + shiftX, y[inside] + shiftY)

        temp = 0.
        from quadtree.panel import interpolate
        for l in range(self.numberExpansionTerms):
            a = interpolate(A[l], xout, yout, self.grid)
            b = interpolate(B[l], xin, yin, self.grid)
            temp += a * b
        return temp


    def computePole(self, _panel, fx, fy):
        pass

    def computeInteraction(self, panelWellSeparated, x, y, i):
        pass

#     def directSummation(self, panelDirectSummation, fsi, ib, i):
#         import pyximport; pyximport.install()
#         import math_objects.fast_implicit as fast_implicit
#         return fast_implicit.computeInteraction(fsi, ib, i, panelDirectSummation.pointsInsidePanel)


if __name__ == '__main__':
    import pylab
    from math_objects.cartesian_grid import *
    from physic_objects.fluid import StokesFluid
    from math_objects.lookup_table import LookupTable
    from time import time as tic
    from quadtree import panel
#=======================================================================================================================
#
# FFE test for random points and grid points
#
#=======================================================================================================================
    n = 64
    numLevels = 1
    diffsfxu = []
    diffsfxv = []
    diffsfyu = []
    diffsfyv = []

    seed = numpy.random.randint(10, 10000)
    t = tic()
    for numberExpansionTerms in range(1, 16):
        sizex = 1.0
        sizey = 1.0
        grid = Grid(Point(0., 0.), sizex, sizey, n, n, 2)
        dx, dy = grid.getSpacing()
        dt = dx
        fluid = StokesFluid(grid)
        lp = LookupTable(fluid, dt)
        ffe = FarFieldExpansion(numLevels, lp, numberExpansionTerms)

# For random points
        nbp = 100
        numpy.random.seed(int(seed))
        x = numpy.random.random(nbp)
        y = numpy.random.random(nbp)
# For grid Points
#         sx, ex, sy, ey = grid.getBounds()
#         x = fluid.velocity.u.getX()[sx:ex]
#         y = fluid.velocity.u.getY()[sy:ey]

        root = panel.Panel(Point(0., 0.), 1., 1., None)
        root.selectPoints(x, y, 1., 1.)
        _panel = panel.Panel(Point(0.5 - 0.125, 0.5 - 0.125), 0.25, 0.25, root)
        _panel.depth = 3
        _panel.selectPoints(x, y, 1., 1.)

        diffs = diffs = [[], [], [], []]
        for pointIn in _panel.points_inside:
            for pointOut in _panel.pointsWellSeparated:
                ufxffe = ffe.computeSummation(_panel, x, y, pointIn, pointOut, 'ufx')
                ufyffe = ffe.computeSummation(_panel, x, y, pointIn, pointOut, 'ufy')
                vfxffe = ffe.computeSummation(_panel, x, y, pointIn, pointOut, 'vfx')
                vfyffe = ffe.computeSummation(_panel, x, y, pointIn, pointOut, 'vfy')

                distx = mod(x[pointOut] - x[pointIn] + dx + sizex, sizex)
                disty = mod(y[pointOut] - y[pointIn] + dy / 2. + sizey, sizey)
                i, j = grid.getIndex(distx, disty)

                ufxlp = lp.fxLookup.u.data[i, j]
                ufylp = lp.fyLookup.u.data[i, j]

                distx = mod(x[pointIn] - x[pointOut] + dx / 2. + sizex, sizex)
                disty = mod(y[pointIn] - y[pointOut] + sizey, sizey)
                i, j = grid.getIndex(distx, disty)

                vfxlp = lp.fxLookup.v.data[i, j]
                vfylp = lp.fyLookup.v.data[i, j]

                diffs[0].append(ufxffe - ufxlp)
                diffs[1].append(ufyffe - ufylp)
                diffs[2].append(vfxffe - vfxlp)
                diffs[3].append(vfyffe - vfylp)

        diffs = [abs(numpy.array(d)) for d in diffs]
        print "\n\n\n"
        for d in diffs:
            print d.max()
        diffsfxu.append(diffs[0].max())
        diffsfxv.append(diffs[1].max())
        diffsfyu.append(diffs[2].max())
        diffsfyv.append(diffs[3].max())

    pylab.plot(range(len(diffsfxu)), diffsfxu, label="fxu")
    pylab.plot(range(len(diffsfxu)), diffsfxv, label="fxv")
    pylab.plot(range(len(diffsfxu)), diffsfyu, label="fyu")
    pylab.plot(range(len(diffsfxu)), diffsfyv, label="fyv")
    pylab.legend()
    pylab.yscale('log')
    print tic() - t
    pylab.show()
