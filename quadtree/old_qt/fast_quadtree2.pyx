# cython: profile=True
import pylab
import numpy
cimport numpy
from libc.math cimport floor, ceil, abs
from libc.stdlib cimport malloc, free


DOUBLE = numpy.double
INT = numpy.int
mod = numpy.mod
ctypedef numpy.int_t INT_t
ctypedef numpy.double_t DOUBLE_t


cdef int NOT_WELL_SEPARATED_NO_CHILD = -1
cdef int NOT_WELL_SEPARATED_HAS_CHILD = 0
cdef int WELL_SEPARATED = 1


cdef struct InterpolationData
cdef struct Panel
cdef struct PointData
cdef struct Carray
cdef struct Cgrid




#=======================================================================================================================
# Auxiliary functions
#=======================================================================================================================


cdef Carray toCarray(numpy.ndarray[DOUBLE_t,ndim=2] arr):
    return Carray(<double*>arr.data, <int>(arr.strides[0]/arr.dtype.itemsize), <int>(arr.strides[1]/arr.dtype.itemsize))


cdef inline double get(Carray* arr, int i, int j):
    return arr.data[i*arr.stride0+j*arr.stride1]


cdef inline int inside_box(double x, double y, double start_x, double start_y, double width, double height, 
                    double domain_size_x, double domain_size_y):
    '''
        Tests if the point is inside the box.
        By default the box is seen as if it start point is at the origin.
        That is, the points (x,y) is shifted by (sx,sy), the bottom left of the box.
    '''
    if (x-start_x) % domain_size_x > width:
        return 0

    if (y-start_y) % domain_size_y >= height:
        return 0
    
    return 1


cdef double interpolate_node(numpy.ndarray[DOUBLE_t,ndim=2] G, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t]  y,
                                    double distx, double disty, int sx, int sy, int ey, double A0, double B0, double dx, double dy):
    cdef int i,j
    cdef double w1,w2,w3,w4,a,b,c,d,delta1,delta2
    
    #compute the ij index of the point
    i = int(ceil((distx-A0)/dx)) - 1 + sx
    j = int(floor((disty-B0)/dy)) + sy

    #compute the interpolation weight for each point
    w1 = abs(y[j] - disty)
    w2 = dy - w1
    w4 = abs(distx - x[i])
    w3 = dx - w4

    a = G[i-1,j]
    b = G[i-1,j+1]
    c = G[i,j+1]
    d = G[i,j]

    delta1 = (w1*b + w2*a)/dy
    delta2 = (w1*c + w2*d)/dy
    return (w3*delta2 + w4*delta1)/dx


cdef InterpolationData* get_interpolation_data( double* x, double*  y, double distx, double disty,
                                     int sx, int sy, int ey, double A0, double B0, double dx, double dy):
    cdef int i,j
    cdef double w1,w2,w3,w4
    cdef InterpolationData* interp_data
    
    #compute the ij index of the point
    i = int(ceil((distx-A0)/dx)) - 1 + sx
    j = int(floor((disty-B0)/dy)) + sy

    #compute the interpolation weight for each point
    w1 = abs(y[j] - disty)
    w2 = dy - w1
    w4 = abs(distx - x[i])
    w3 = dx - w4
    
    interp_data=new_interpolation_data(i,j,w1,w2,w3,w4)

    return interp_data

cdef double apply_interpolation_data(Carray* G, InterpolationData* id, double dx, double dy):
    cdef int i, j
    cdef double a,b,c,d,delta1,delta2
    
    #compute the ij index of the point
    i = id.i
    j = id.j

    a = get(G,i-1,j)
    b = get(G,i-1,j+1)
    c = get(G,i,j+1)
    d = get(G,i,j)

    delta1 = (id.w1*b + id.w2*a)/dy
    delta2 = (id.w1*c + id.w2*d)/dy
    return (id.w3*delta2 + id.w4*delta1)/dx

#=======================================================================================================================
# STRUCTS
#=======================================================================================================================

cdef struct Cgrid:
    double x_size, y_size
    double dx, dy
    double A0, B0, origin_x, origin_y
    int sx,ex,sy,ey
    double* xnode
    double* ynode
    
    
cdef struct Carray:
    double* data
    int stride0
    int stride1
    

cdef struct InterpolationData:
    int i,j #reference indexes i,j
    double w1,w2,w3,w4  #weight list


cdef struct Panel:
    Panel* parent
    Panel* children[4]
    int* points_inside
    int has_child, depth, num_points_inside
                    
    double start_x, start_y
    double end_x, end_y
    double center_x, center_y
    double height, width
    
    double shift_x, shift_y #the shift to the base decomposition


cdef struct PointData:
    Panel** well_separated_panels
    InterpolationData** well_separated_interpolation
    int num_well_separated
    
    int* close_points
    InterpolationData** close_points_interpolation
    int num_close_points
    
    InterpolationData** all_panels_interpolation
    int num_panels


#=======================================================================================================================
# Interpolation Data Functions
#=======================================================================================================================
cdef InterpolationData* new_interpolation_data(int i, int j, double w1, double w2, double w3, double w4):
    cdef InterpolationData* id = <InterpolationData*>malloc( sizeof(InterpolationData) )
    id.i=i
    id.j=j
    id.w1=w1
    id.w2=w2
    id.w3=w3
    id.w4=w4
    return id

#=======================================================================================================================
# Cgrid Functions
#=======================================================================================================================
cdef Cgrid* new_cgrid(grid):
    cdef Cgrid* g = <Cgrid*>malloc(sizeof(Cgrid))
    cdef numpy.ndarray[DOUBLE_t] nodex
    cdef numpy.ndarray[DOUBLE_t] nodey
    
    nodex=grid.nodeX
    nodey=grid.nodeY
    
    g.x_size = grid.sizeX
    g.y_size = grid.sizeY
    g.dx, g.dy = grid.getSpacing()
    g.A0, g.B0 = grid.origin.x, grid.origin.y
    g.sx,g.sy,g.ey = grid.sx, grid.sy, grid.ey
    g.origin_x = grid.nodeX[grid.sx]
    g.origin_y = grid.nodeY[grid.sy]
    g.xnode = <double*>(nodex.data)
    g.ynode = <double*>(nodey.data)
    
    return g
    
    

#=======================================================================================================================
# Panel Functions
#=======================================================================================================================


cdef draw_panel(Panel* p, color = None):
        x, y = p.start_x, p.start_y
        if color:
            rectangle = pylab.Rectangle((x, y), p.width, p.height, facecolor = color)
        else:
            rectangle = pylab.Rectangle((x, y), p.width, p.height, fill = False)
        ax = pylab.gca()
        ax.add_patch(rectangle)


cdef Panel* new_panel(double start_x, double start_y, double height, double width, Panel* parent):
    cdef Panel* p = <Panel*>malloc( sizeof(Panel) )
    p.start_x = start_x
    p.start_y = start_y
    p.height = height
    p.width = width

    p.center_x = start_x + width/2.
    p.center_y = start_y + height/2.
    p.end_x = start_x + width
    p.end_y = start_y + height
    
    p.has_child = 0
    p.parent = parent
    if parent is not NULL: 
        p.depth = parent.depth + 1
    else: 
        p.depth = 1

    return p


cdef free_tree(Panel* root):
    if root.has_child:
        free_tree(root.children[0])
        free_tree(root.children[1])
        free_tree(root.children[2])
        free_tree(root.children[3])
    free(root.points_inside)
    free(root)


cdef create_childs(Panel* p):
    cdef:
        double c_height = p.height / 2.
        double c_width = p.width / 2.
        double sx = p.start_x
        double sy = p.start_y
        Panel* c1 = new_panel(sx,           sy,             c_height, c_width, p)
        Panel* c2 = new_panel(sx+c_width,   sy,             c_height, c_width, p)
        Panel* c3 = new_panel(sx+c_width,   sy+c_height,    c_height, c_width, p)
        Panel* c4 = new_panel(sx,           sy+c_height,    c_height, c_width, p)
    
    p.children[0] = c1
    p.children[1] = c2
    p.children[2] = c3
    p.children[3] = c4
    p.has_child = 1
    

cdef select_points_inside(Panel* p, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, double size_x, double size_y):
        cdef int i, n, point, num_points_inside
        cdef int* points_inside
        cdef int* inside_parent
        cdef numpy.ndarray[INT_t] temp
        
        if p.parent is not NULL:
            inside_parent = p.parent.points_inside
            n = p.parent.num_points_inside
            
            num_points_inside = 0
            temp = numpy.empty(n, dtype=INT)
            for i in xrange(n):
                point = inside_parent[i]
                if inside_box(x[point], y[point], p.start_x, p.start_y, p.width, p.height, size_x, size_y):
                    temp[num_points_inside] = point
                    num_points_inside += 1
        else:
            num_points_inside = x.shape[0]
            temp = numpy.array(range(num_points_inside), dtype=INT)
            
        points_inside = <int*>malloc( num_points_inside * sizeof(int) )
        for i in range(num_points_inside):
            points_inside[i] = temp[i]
        
        p.points_inside = points_inside
        p.num_points_inside = num_points_inside




#=======================================================================================================================
# PointData Functions
#=======================================================================================================================


cdef PointData* new_pointdata(int num_panels, int num_points):
    cdef PointData* pd = <PointData*>malloc( sizeof(PointData) )
    
    pd.well_separated_panels = <Panel**>malloc( num_panels * sizeof(Panel*) )
    pd.well_separated_interpolation = <InterpolationData**>malloc( num_panels * sizeof(InterpolationData*) )

    pd.all_panels_interpolation = <InterpolationData**>malloc( num_panels * sizeof(InterpolationData*) )
    
    pd.close_points = <int*>malloc( num_points * sizeof(int) )
    pd.close_points_interpolation = <InterpolationData**>malloc( num_points * sizeof(InterpolationData*) )
    
    pd.num_close_points = 0
    pd.num_well_separated = 0
    pd.num_panels = 0
    
    return pd


cdef free_pointdata(PointData* pd):
    free(pd.well_separated_panels)
    free(pd.well_separated_interpolation)
    free(pd.all_panels_interpolation)
    free(pd.close_points)
    free(pd.close_points_interpolation)
    free(pd)




#=======================================================================================================================
# Quad Tree
#=======================================================================================================================


cdef divide_panel(numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, int max_points, int max_depth, double size_x, double size_y, Panel* root, int* num_panels):
    select_points_inside(root, x, y, size_x, size_y)

    if root.num_points_inside > max_points and root.depth < max_depth:
        create_childs(root)
        c1 = root.children[0]
        c2 = root.children[1]
        c3 = root.children[2]
        c4 = root.children[3]
        divide_panel(x, y, max_points, max_depth, size_x, size_y, c1, num_panels)
        divide_panel(x, y, max_points, max_depth, size_x, size_y, c2, num_panels)
        divide_panel(x, y, max_points, max_depth, size_x, size_y, c3, num_panels)
        divide_panel(x, y, max_points, max_depth, size_x, size_y, c4, num_panels)
        num_panels[0] += 4


cdef set_panel_list(Panel** all_panels, Panel* root, int* iterator):
    all_panels[iterator[0]] = root
    iterator[0] += 1
    if root.has_child:
        set_panel_list(all_panels, root.children[0], iterator)
        set_panel_list(all_panels, root.children[1], iterator)
        set_panel_list(all_panels, root.children[2], iterator)
        set_panel_list(all_panels, root.children[3], iterator)
    
  
cdef int check_panel(double x, double y, Panel* p, double domain_size_x, double domain_size_y):
    cdef:
        double start_x, start_y, width, height
    
    #defines the start and size of the big box that means not well separated
    start_x = p.center_x - 1.5*p.width
    start_y = p.center_y - 1.5*p.height
    width = 3*p.width
    height = 3*p.height
    
    if inside_box(x, y, start_x, start_y, width, height, domain_size_x, domain_size_y):
        if p.has_child:
            return NOT_WELL_SEPARATED_HAS_CHILD
        else:
            return NOT_WELL_SEPARATED_NO_CHILD
    return WELL_SEPARATED


cdef sort_panels(Panel* root, PointData* pd, double xpoint, double ypoint, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, Cgrid* grid):
    cdef int panel_type, i
    cdef double domain_size_x, domain_size_y
    
    cdef double dx,dy,A0,B0,distx,disty
    cdef double* xnode
    cdef double* ynode
    
    
    x_size = grid.x_size
    y_size = grid.y_size
    dx, dy = grid.dx, grid.dy
    A0,B0 = grid.A0, grid.B0
    sx,sy,ey = grid.sx, grid.sy, grid.ey
    origin_x = grid.origin_x
    origin_y = grid.origin_y
    xnode = grid.xnode
    ynode = grid.ynode
    
    
    panel_type = check_panel(xpoint, ypoint, root, x_size, y_size)
    
    if root.num_points_inside != 0:
        if panel_type == WELL_SEPARATED:
            pd.well_separated_panels[pd.num_well_separated] = root
            pd.num_well_separated += 1
            
        if panel_type == NOT_WELL_SEPARATED_NO_CHILD:
            for point in range(root.num_points_inside):
                i = root.points_inside[point]
                pd.close_points[pd.num_close_points] = i
                
                distx = (x[i] - xpoint + origin_x + x_size)% x_size
                disty = (y[i] - ypoint + origin_y + y_size)% y_size
                pd.close_points_interpolation[pd.num_close_points] = get_interpolation_data(xnode, ynode, distx, disty, sx, sy, ey, A0, B0, dx, dy)
                
                pd.num_close_points += 1
                
        if panel_type == NOT_WELL_SEPARATED_HAS_CHILD:
            sort_panels(root.children[0], pd, xpoint, ypoint, x, y, grid)
            sort_panels(root.children[1], pd, xpoint, ypoint, x, y, grid)
            sort_panels(root.children[2], pd, xpoint, ypoint, x, y, grid)
            sort_panels(root.children[3], pd, xpoint, ypoint, x, y, grid)
        
        
cdef class QuadTree:
    cdef:
        int max_points, max_depth, num_panels, num_points
        double domain_size_x, domain_size_y
        Panel* root
        Panel** all_panels
        PointData** pd
        Cgrid* c_grid
        numpy.ndarray result_x, result_y
        grid
        ffe
        
        
    def __init__(self, max_points, max_depth, grid, ffe):
        self.max_points = max_points
        self.max_depth = max_depth
        self.grid = grid
        self.ffe = ffe
        self.domain_size_x = grid.sizeX
        self.domain_size_y = grid.sizeY
        self.c_grid = new_cgrid(grid)


    cpdef make_tree(self, x, y):
        cdef int num_panels = 1
        cdef int iterator = 0
        self.root = new_panel(self.grid.origin.x, self.grid.origin.y, self.grid.sizeY, self.grid.sizeY, NULL)
        divide_panel(x, y, self.max_points, self.max_depth, self.root.width, self.root.height, self.root, &num_panels)
        
        self.all_panels = <Panel**>malloc( num_panels * sizeof(Panel*) )
        set_panel_list(self.all_panels, self.root, &iterator)
        self.num_panels = num_panels
        
        
    cpdef delete_tree(self):
        cdef int i
        cdef PointData** pd = self.pd
        
        free_tree(self.root)
        for i in range(self.num_points): free_pointdata(pd[i])
        free(pd)
        
        
    cpdef pre_eval(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y):
        '''
        for each point, sort which panels are well separated, select the close points 
        and put this into PointData pd
        '''
        
        cdef int i, num_panels, num_points
        cdef PointData** pd
        num_points = x.shape[0]
        num_panels = self.num_panels
        self.num_points = num_points
        pd = <PointData**>malloc( num_points * sizeof(PointData*) )
        self.result_x = numpy.zeros(num_points)
        self.result_y = numpy.zeros(num_points)
        
        
        for i in range(num_points):
            pd[i] = new_pointdata(num_panels, num_points)
            sort_panels(self.root, pd[i], x[i], y[i],x, y, self.c_grid)

        self.pd = pd
    
    
    cpdef eval(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, numpy.ndarray[DOUBLE_t] fx, numpy.ndarray[DOUBLE_t] fy, double h):
        cdef int point
        
        for point in range(self.num_points):
            self.panel_summation(x,y,fx,fy,point,h)
            self.direct_summation(x,y,fx,fy,point,h)
    
    
    
    cpdef panel_summation(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, numpy.ndarray[DOUBLE_t] fx, numpy.ndarray[DOUBLE_t] fy, int point, double h):
        '''
            for each well separated panel from the point, compute its interaction 
        '''
        pass

    cdef direct_summation(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, numpy.ndarray[DOUBLE_t] fx, numpy.ndarray[DOUBLE_t] fy, int point, double h):
        cdef int i, k
        cdef double xpoint, ypoint, x_size, y_size, origin_x, origin_y, temp_x, temp_y
        cdef double dx,dy,A0,B0,distx,disty
        cdef numpy.ndarray result_x, result_y, xnode, ynode
        cdef Carray fxu, fxv, fyu, fyv
        
        
        xpoint = x[point]
        ypoint = y[point]
        x_size = self.grid.sizeX
        y_size = self.grid.sizeY
        dx, dy = self.grid.getSpacing()
        A0,B0 = self.grid.origin.x, self.grid.origin.y
        sx,sy,ey = self.grid.sx,self.grid.sy,self.grid.ey
        origin_x = self.grid.nodeX[sx]
        origin_y = self.grid.nodeY[sy]
        xnode = self.grid.nodeX
        ynode = self.grid.nodeY
        result_x = self.result_x
        result_y = self.result_y
        
        fxu = toCarray(self.ffe.lookupTable.fxLookup.u.data)
        fxv = toCarray(self.ffe.lookupTable.fxLookup.v.data)
        fyu = toCarray(self.ffe.lookupTable.fyLookup.u.data)
        fyv = toCarray(self.ffe.lookupTable.fyLookup.v.data)
        #for each close point, compute the diff and search the lookuptable for the proper value
#         for k in range(self.pd[point].num_close_points):
#             i = self.pd[point].close_points[k]
#             distx = (x[i] - xpoint + origin_x + x_size)% x_size
#             disty = (y[i] - ypoint + origin_y + y_size)% y_size
#             distx = mod((x[i] - xpoint + origin_x + x_size), x_size)
#             disty = mod((y[i] - ypoint + origin_y + y_size), y_size)
#              
#             temp_x = interpolate_node(fxu, xnode, ynode, distx, disty, sx, sy, ey, A0, B0, dx, dy)
#             temp_y = interpolate_node(fyu, xnode, ynode, distx, disty, sx, sy, ey, A0, B0, dx, dy)
#             result_x[point] += temp_x*h*fx[i] + temp_y*h*fy[i]
#              
#             temp_x = interpolate_node(fxv, xnode, ynode, distx, disty, sx, sy, ey, A0, B0, dx, dy)
#             temp_y = interpolate_node(fyv, xnode, ynode, distx, disty, sx, sy, ey, A0, B0, dx, dy)
#             result_y[point] += temp_x*h*fx[i] + temp_y*h*fy[i]
        for k in range(self.pd[point].num_close_points):
            i = self.pd[point].close_points[k]
            id = self.pd[point].close_points_interpolation[k]
              
            temp_x = apply_interpolation_data(&fxu, id, dx, dy)
            temp_y = apply_interpolation_data(&fyu, id, dx, dy)
            result_x[point] += temp_x*h*fx[i] + temp_y*h*fy[i]
               
            temp_x = apply_interpolation_data(&fxv, id, dx, dy)
            temp_y = apply_interpolation_data(&fyv, id, dx, dy)
            result_y[point] += temp_x*h*fx[i] + temp_y*h*fy[i]
    
    
    cpdef compute_panel_H(self):
        pass
    
    cpdef compute_all_panels_H(self):
        pass
    
    
    def get_result(self):
        return self.result_x, self.result_y
    
    def draw_tree(self, color=None):
        for i in range(self.num_panels):
            draw_panel(self.all_panels[i], color)


    def draw_well_separated(self, int point, color=None):
        for i in range(self.pd[point].num_well_separated):
            draw_panel(self.pd[point].well_separated_panels[i], color)
        
        
    def draw_close_points(self, int point, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, mark='b.'):
        cdef int i,k
        for k in range(self.pd[point].num_close_points):
            i = self.pd[point].close_points[k]
            pylab.plot(x[i], y[i], mark)
    


