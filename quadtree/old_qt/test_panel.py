'''
Created on Jun 8, 2012

@author: fnfranco
'''
import utils
import pyximport; pyximport.install()
import fast_quadtree2


if __name__ == '__main__':
    import numpy
    import pylab
    from time import time as tic
    from quadtree.far_field_expansion import FarFieldExpansion
    from math_objects.lookup_table import LookupTable
    from physic_objects.fluid import StokesFluid
    from math_objects.cartesian_grid import Grid, Point
    from physic_objects.ib import Ellipse
    from physic_objects.force import EllasticForce
    from physic_objects.fluid_structure import FluidStructure

    grid = Grid(Point(0., 0.), 1., 1., 128, 128, 2)
    fluid = StokesFluid(grid)
    dt = grid.dx
    lp = LookupTable(fluid, dt)

    nbp = 63
    ib = Ellipse(nbp, Point(0.5, 0.5), 0.3, 0.1, grid, EllasticForce(10000.))
    ib.computeForce()

    system = FluidStructure(fluid, ib, dt, 'Implicit')


    x = numpy.random.random(nbp)
    y = numpy.random.random(nbp)
    x[0] = .01
    y[0] = .01

    x = ib.x
    y = ib.y

    px, py = ib.getPeriodicPositions(1., 1., x, y)

    size = 1.
    ffe = FarFieldExpansion(6, lp, 5)
    max_points = 5
    max_depth = 1
    tree = fast_quadtree2.QuadTree(max_points, max_depth, grid, ffe)

#     tree.make_tree(x, y)
#     tree.pre_eval(x, y)
#     pylab.plot(x, y, 'g.')
#     tree.draw_tree()
#     point = 0
#     tree.draw_close_points(point, x, y)
#     tree.draw_well_separated(point, color='yellow')
#     pylab.plot(x[point], y[point], 'r.')
#     pylab.show()
#     tree.delete_tree()


    t = tic()
    system.FSInteraction.makeInteractionMatrix(ib)
    M = system.FSInteraction.M
    result = numpy.dot(M, numpy.concatenate(ib.force.getX()))
    print "Time for making the matrix:", tic() - t
#
#     t = tic()
#     tree.make_tree(x, y)
#     tree.pre_eval(x, y)
#     tree.delete_tree()
#     print "Time for making the tree, peforming pre computations and deleting:", tic() - t


    tree.make_tree(x, y)
    tree.pre_eval(x, y)
    for point in range(nbp):
        tree.direct_summation(x, y, ib.force.x, ib.force.y, point, ib.h)
    rx, ry = tree.get_result()
    print "error fx:", abs(rx - result[:nbp]).max()
    print "error fy:", abs(ry - result[nbp:]).max()
    tree.delete_tree()

