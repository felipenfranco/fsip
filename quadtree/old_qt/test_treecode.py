'''
Created on Jun 8, 2012

@author: fnfranco
'''
import utils
import pyximport; pyximport.install()
import fast_quadtree2


if __name__ == '__main__':
    import numpy
    import pylab
    from time import time as tic
    from quadtree.far_field_expansion import FarFieldExpansion
    from math_objects.lookup_table import LookupTable
    from physic_objects.fluid import StokesFluid
    from math_objects.cartesian_grid import Grid, Point
    from physic_objects.ib import RandomPoints
    from physic_objects.force import EllasticForce
    from physic_objects.fluid_structure import FluidStructure

    grid = Grid(Point(0., 0.), 1., 1., 128, 128, 2)
    fluid = StokesFluid(grid)
    dt = grid.dx
    lp = LookupTable(fluid, dt)

    ellasticConstant = 1e10
    nbp = 1024
    ib = RandomPoints(0.25, 0.5, 0.25, 0.5, nbp, grid, EllasticForce(ellasticConstant))
    ib.computeForce()

    system = FluidStructure(fluid, ib, dt, 'Implicit')

#     ib.x[0] = 0.8
#     ib.y[0] = 0.8

    x, y = ib.getX()
    px, py = ib.getPeriodicPositions(1., 1., x, y)

    size = 1.
    ffe = FarFieldExpansion(6, lp, 5)
    max_points = 5
    max_depth = 3
    tree = fast_quadtree2.QuadTree(max_points, max_depth, grid, ffe)

#     tree.make_tree(x, y)
#     tree.pre_eval(x, y)
#     pylab.plot(x, y, 'g.')
#     tree.draw_tree()
#     point = 0
#     tree.draw_close_points(point, x, y)
#     tree.draw_well_separated(point, color='yellow')
#     pylab.plot(x[point], y[point], 'r.')
#     pylab.show()
#     tree.delete_tree()


    t = tic()
    system.FSInteraction.makeInteractionMatrix(ib)
    M = system.FSInteraction.M
    result = numpy.dot(M, numpy.concatenate(ib.force.getX()))
    print "Time for making the matrix:", tic() - t
#
#     t = tic()
#     tree.make_tree(x, y)
#     tree.pre_eval(x, y)
#     tree.delete_tree()
#     print "Time for making the tree, peforming pre computations and deleting:", tic() - t

    t = tic()
    tree.make_tree(x, y)
    print "Time for making the tree:", tic() - t

    t = tic()
    tree.pre_eval(x, y)
    print "Time for pre eval:", tic() - t

    t = tic()
    tree.eval(x, y, ib.force.x, ib.force.y, ib.h)
    print "Time for eval:", tic() - t

    rx, ry = tree.get_result()
#     print "error fx:\n", abs(rx - result[:nbp])
#     print "error fy:\n", abs(ry - result[nbp:])
    print "max relative error fx:", abs(rx - result[:nbp]).max() / ellasticConstant
    print "max relative error fy:", abs(ry - result[nbp:]).max() / ellasticConstant
    tree.delete_tree()

