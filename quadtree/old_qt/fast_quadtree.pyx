# cython: profile=True
import numpy
cimport numpy
from libc.math cimport floor, ceil, abs

from math_objects.cartesian_grid import Point
from quadtree.far_field_expansion import FarFieldExpansion
import pylab

DOUBLE = numpy.double
INT = numpy.int
ctypedef numpy.int_t INT_t
ctypedef numpy.double_t DOUBLE_t



cdef int inside_box(double x, double y, double sx, double sy, double width, double height, 
                    double domain_size_x, double domain_size_y):
    '''
        Tests if the point is inside the box.
        By default the box is seen as if it start point is at the origin.
        That is, the points (x,y) is shifted by (sx,sy), the bottom left of the box.
    '''
    if (x-sx) % domain_size_x > width:
        return 0

    if (y-sy) % domain_size_y >= height:
        return 0
    
    return 1




#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
# INTERPOLATION DATA
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================

cdef struct InterpolationData:
        int i,j #reference indexes i,j
        double w[4]  #weight list
        double dxdy
        
#    def __cinit__(self, i, j, list w, double dxdy):
#        self.i = i
#        self.j = j
#        self.w = w
#        self.dxdy = dxdy
#        
#    cdef inline double bilinear(self, numpy.ndarray[DOUBLE_t,ndim=2] M):
#        cdef double delta1, delta2, dxdy
#        cdef list w
#        cdef int i,j
#        i, j, w = self.i, self.j, self.w
#        dxdy = self.dxdy
#        
#        delta1 = (w[0]*M[i-1,j+1] + w[1]*M[i-1,j])
#        delta2 = (w[0]*M[i,j+1] + w[1]*M[i,j])
#        return (w[2]*delta2 + w[3]*delta1)/dxdy
    
    
    
    
    
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
# POINT TEMPLATE DATA
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================

cdef class PointTemplateData:
    cdef:
        public numpy.ndarray separated_panels, A_interpolation
        public numpy.ndarray not_separated_points, not_separated_interpolation
        public numpy.ndarray all_panels, B_interpolation
        
        double size_x, size_y, dx, dy, origin_x, origin_y, start_node_x, start_node_y
        numpy.ndarray x_node, y_node
        int sx, sy
        
    def __cinit__(self,double origin_x, double origin_y, double size_x, double size_y, double dx, double dy, int sx, int sy,
                   numpy.ndarray[DOUBLE_t] x_node, numpy.ndarray[DOUBLE_t] y_node):
        self.origin_x = origin_x
        self.origin_y = origin_y
        self.size_x = size_x
        self.size_y = size_y
        self.dx = dx
        self.dy = dy
        self.sx = sx
        self.sy = sy
        self.x_node = x_node
        self.y_node = y_node
        self.start_node_x = x_node[sx]
        self.start_node_y = y_node[sy]
        
#    cdef inline double inside_domain_x(self,double x):
#        return (x-self.origin_x+self.size_x)%self.size_x + self.origin_x
#    
#    cdef inline double inside_domain_y(self,double y):
#        return (y-self.origin_y+self.size_y)%self.size_y + self.origin_y
#    
#    cdef inline int get_i(self, double x):
#        return <int>(ceil(  (x-self.origin_x) /self.dx)) - 1 + self.sx
#
#    cdef inline int get_j(self, double y):
#        return <int>(floor( (y-self.origin_y) /self.dy))  + self.sy 
    
    
#    cpdef InterpolationData compute_panel_interpolation(self, Panel p, double x, double y):
#        cdef double shifted_x, shifted_y, w1, w2, w3, w4
#        cdef int i, j
#        cdef InterpolationData inter_data
#        #make the shift to the center of the decomposition
#        shifted_x = self.inside_domain_x(x + p.shift_x)
#        shifted_y = self.inside_domain_y(y + p.shift_y)
#        #the indexes        
#        i = self.get_i(shifted_x)
#        j = self.get_j(shifted_y)
#        
#        w1 = abs(self.y_node[j] - shifted_y)
#        w2 = self.dy - w1
#        w4 = abs(shifted_x - self.x_node[i])
#        w3 = self.dx - w4
#        
#        inter_data = InterpolationData(i, j, [w1, w2, w3, w4], self.dx*self.dy)
#        return inter_data
        
        
    cpdef compute_points_interpolation(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, 
                                       numpy.ndarray[INT_t] not_separated_points, int point):
        #point is the index of the point we are considering and point_list are the points not well separated
        #Types definitions
        cdef double dist_x, dist_y, fiber_x, fiber_y, dxdy
        cdef double snx,sny,origin_x,origin_y,size_x,size_y,dx,dy
        cdef int i,j,k,z
        cdef double w[4]
        cdef int sx,sy
        cdef numpy.ndarray[DOUBLE_t] x_node, y_node
        cdef numpy.ndarray[InterpolationData] not_separated_interpolation
        
        #assignments
        fiber_x = x[point]
        fiber_y = y[point]
        snx = self.start_node_x
        sny = self.start_node_y
        origin_x = self.origin_x
        origin_y = self.origin_y
        size_x = self.size_x
        size_y = self.size_y
        dx = self.dx
        dy = self.dy
        sx = self.sx
        sy = self.sy
        x_node = self.x_node
        y_node = self.y_node
        dxdy = self.dx*self.dy
        not_separated_interpolation = numpy.empty(not_separated_points.shape[0])
        
        #actual code
        for z in xrange(not_separated_points.shape[0]):
            k = not_separated_points[z]
            #compute the distance vector
            dist_x = ((x[k]-fiber_x + snx)-origin_x+size_x)%size_x + origin_x
            dist_y = ((y[k]-fiber_y + sny)-origin_y+size_y)%size_y + origin_y
            
            #find out in which cell the distance vector is
            i = <int>( ceil((dist_x-origin_x) /dx)) + sx -1 
            j = <int>(floor((dist_y-origin_y) /dy)) + sy 
            
            #compute the weights
            w[0] = abs(y_node[j] - dist_y)
            w[1] = dy - w[0]
            w[3] = abs(dist_x - x_node[i])
            w[2] = dx - w[3]
        
            #store the data
            not_separated_interpolation[k] = InterpolationData(i, j, w, dxdy)
        
        self.not_separated_points = not_separated_points
        self.not_separated_interpolation = not_separated_interpolation
        
        
        
        
        
        
#=======================================================================================================================        
#=======================================================================================================================
#=======================================================================================================================
# PANEL
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================

cdef class Panel:
    cdef:
        public Panel parent
        public double start_x, start_y
        public double end_x, end_y
        public double center_x, center_y
        public double height, width
        public int has_child, depth
        public list children
        public numpy.ndarray points_inside
        public int num_points_inside
        
        public numpy.ndarray ufxA, ufxB
        public numpy.ndarray ufyA, ufyB
        public numpy.ndarray vfxA, vfxB
        public numpy.ndarray vfyA, vfyB
        public double shift_x, shift_y #the shift to the base decomposition
        

    def __cinit__(self, double start_x, double start_y, double height, double width, Panel parent):
        '''
        startPoint = Point() : bottom left corner of the panel.
        sizeX, sizeY = numbers
        '''
        #geometry parameters
        self.start_x = start_x
        self.start_y = start_y
        self.height = height
        self.width = width
        self.parent = parent

        self.center_x = start_x + width/2.
        self.center_y = start_y + height/2.
        self.end_x = start_x + width
        self.end_y = start_y + height

        self.parent = parent
        self.has_child = 0
        if parent is not None: 
            self.depth = parent.depth + 1
        else: 
            self.depth = 1
    
    
    cdef createChilds(self):
        cdef double childHeight = self.height / 2.
        cdef double childWidth = self.width / 2.
        cdef double sx = self.start_x
        cdef double sy = self.start_y

        cdef list csx = [sx, sx+childWidth, sx+childWidth,  sx]
        cdef list csy = [sy, sy,            sy+childHeight, sy+childHeight]
        cdef list startChildren = zip(csx, csy)
        
        self.children = [Panel(x, y, childHeight, childWidth, self) for x,y in startChildren]
        self.has_child = 1
        
    
    cpdef select_points_inside(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, double size_x, double size_y):
        cdef int i, num_points, point
        cdef double sx, sy, width, height
        cdef list points_inside = []
        cdef numpy.ndarray[INT_t] points_inside_parent
        
        if self.parent:
            sx, sy = self.start_x, self.start_y
            width, height = self.width, self.height
            points_inside_parent = self.parent.points_inside
            num_points = points_inside_parent.shape[0]
            for i in xrange(num_points):
                point = points_inside_parent[i]
                if inside_box(x[point], y[point], sx, sy, width, height, size_x, size_y):
                    points_inside.append(point)
            
            self.points_inside = numpy.array(points_inside, dtype=INT)
        else:
            self.points_inside = numpy.array(range(x.shape[0]), dtype=INT)

        self.num_points_inside = self.points_inside.shape[0]
        
    
    def drawPanel(self, color = None):
        x, y = self.start_x, self.start_y
        if color:
            rectangle = pylab.Rectangle((x, y), self.width, self.height, facecolor = color)
        else:
            rectangle = pylab.Rectangle((x, y), self.width, self.height, fill = False)
        ax = pylab.gca()
        ax.add_patch(rectangle)
        


cdef compute_interaction(int fiber_in, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, 
                         numpy.ndarray[DOUBLE_t] fx, numpy.ndarray[DOUBLE_t] fy, 
                         lp, PointTemplateData template, 
                         numpy.ndarray[DOUBLE_t] interaction_x, numpy.ndarray[DOUBLE_t] interaction_y):
    
    not_separated_points = template.not_separated_points
    not_separated_interpolation = template.not_separated_interpolation
    
    ufx = lp.fxLookup.u.data
    vfx = lp.fxLookup.v.data 
    ufy = lp.fyLookup.u.data 
    vfy = lp.fyLookup.v.data 
    
#    for k in xrange(not_separated_points.shape[0]):
#        interaction_x[fiber_in] += 
#        interaction_y[fiber_in] += 






#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
# QUADTREE
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================

cdef class QuadTree:
    cdef:
        int max_points
        Panel root
        int size_x, size_y
        numpy.ndarray template
        
    def __cinit__(self, max_points, root):
        self.max_points = max_points
        self.root = root
        self.size_x = root.width
        self.size_y = root.height
        
    cpdef makeTree(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, int max_depth, decompositions):
        self._dividePanel(x, y, self.root, max_depth, decompositions)
        
    cpdef _dividePanel(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, Panel root, int max_depth, decompositions):
        root.select_points_inside(x, y, self.size_x, self.size_y)
        if root.num_points_inside > self.max_points and root.depth < max_depth:
            root.createChilds()
            for child in root.children:
                #associate the base decomposition
                if child.depth >= 3:
                    decomp = decompositions[child.depth-3]
                    child.ufxA, child.ufxB = decomp.ufx
                    child.ufyA, child.ufyB = decomp.ufy
                    child.vfxA, child.vfxB = decomp.vfx
                    child.vfyA, child.vfyB = decomp.vfy
                    child.shift_x = decomp.center.x-child.center_x
                    child.shift_y = decomp.center.y-child.center_y
                self._dividePanel(x, y, child, max_depth, decompositions)
                
    cpdef do_pre_evaluation(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, grid):
        cdef numpy.ndarray[DOUBLE_t] x_node, y_node
        cdef numpy.ndarray template
        cdef double ox,oy,sizex,sizey,dx,dy
        cdef int sx,sy, num_points
        cdef int fiber_in, fiber_out
        
        x_node = grid.getX() + grid.dx
        y_node = grid.getY()
        
        ox, oy = grid.origin.x, grid.origin.y
        sizex, sizey = grid.sizeX, grid.sizeY
        dx, dy = grid.dx, grid.dy
        sx, sy = grid.sx, grid.sy
        
        num_points = x.shape[0]
        
        template = numpy.array([PointTemplateData(ox, oy, sizex, sizey, dx, dy, sx, sy, x_node, y_node) for i in range(x.shape[0])] , dtype = PointTemplateData)
        all_points = numpy.array(range(x.shape[0]))
        
        for fiber_in in xrange(num_points):
            #define the template for each point
            template[fiber_in].compute_points_interpolation(x,y,all_points,fiber_in)
    
    
    cpdef evaluate(self, numpy.ndarray[DOUBLE_t] x, numpy.ndarray[DOUBLE_t] y, numpy.ndarray[DOUBLE_t] fx, numpy.ndarray[DOUBLE_t] fy, lp):
        cdef PointTemplateData template
        cdef numpy.ndarray[DOUBLE_t] interaction_x, interaction_y
        
        template = self.template
        interaction_x = numpy.zeros(x.shape[0])
        interaction_y = numpy.zeros(x.shape[0])
        
        for fiber_in in range(x.shape[0]):
            compute_interaction(fiber_in, x, y, fx, fy, lp, template, interaction_x, interaction_y)
            
            
    def _drawTree(self, rootPanel):
        rootPanel.drawPanel()
        if rootPanel.has_child:
            for child in rootPanel.children:
                self._drawTree(child)

    def plot(self):
        self._drawTree(self.root)
        pylab.show()
