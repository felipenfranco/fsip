from os.path import realpath
import utils
from math_objects import codeInterpolateNode

#FORTRAN CODE
codeInsidePanel = '''!    -*- f90 -*-
subroutine points_inside_panel(x,y,nbp,inside_parent,n,blx,bly,trx,try,size_x,size_y,inside,number_points_inside)
    implicit none
    integer, intent(in) :: nbp, n
    double precision, dimension(0:(nbp-1)), intent(inout) :: x,y
    integer (kind=8), dimension(n), intent(inout) :: inside_parent
    integer (kind=8), dimension(n), intent(out) :: inside
    double precision, intent(in) :: blx,bly,trx,try,size_x,size_y
    double precision :: blx_0,bly_0,trx_0,try_0,x_0,y_0
    integer (kind=8) :: point, i, range
    integer (kind=8) , intent(out) :: number_points_inside
    
    !f2py intent(inplace) :: x,y,inside_parent
    !f2py intent(hide) :: nbp,n
    !f2py intent(out) :: inside, number_points_inside
    
    inside = 0
    number_points_inside = 0
    blx_0 = 0.
    bly_0 = 0.
    trx_0 = trx - blx
    try_0 = try - bly
    
    do i = 1, n
        point = inside_parent(i)
        x_0 = modulo(x(point)-blx,size_x)
        y_0 = modulo(y(point)-bly,size_y)
        if (x_0 > blx_0 .and. x_0 <= trx_0 .and. y_0 >= bly_0 .and. y_0 < try_0) then
                number_points_inside = number_points_inside + 1
                inside(number_points_inside) = point
        end if
    end do
    
end subroutine points_inside_panel
'''


codeWellSeparatedPanel = '''!    -*- f90 -*-
subroutine points_well_separated(x,y,nbp,blx,bly,trx,try,size_x,size_y,well_separated,number_points_well_separated)
    implicit none
    integer, intent(in) :: nbp
    double precision, dimension(0:(nbp-1)), intent(inout) :: x,y
    integer (kind=8), dimension(nbp), intent(out) :: well_separated
    double precision, intent(in) :: blx,bly,trx,try,size_x,size_y
    double precision :: blx_0,bly_0,trx_0,try_0,x_0,y_0
    integer (kind=8) :: point, i, range
    integer (kind=8) , intent(out) :: number_points_well_separated
    
    !f2py intent(inplace) :: x,y
    !f2py intent(hide) :: nbp
    !f2py intent(out) :: well_separated, number_points_well_separated
    
    well_separated = 0
    number_points_well_separated = 0
    blx_0 = 0.
    bly_0 = 0.
    trx_0 = trx - blx
    try_0 = try - bly
    
    do i = 0, nbp-1
        x_0 = modulo(x(i)-blx,size_x)
        y_0 = modulo(y(i)-bly,size_y)
        if (.not.(x_0 > blx_0 .and. x_0 <= trx_0 .and. y_0 >= bly_0 .and. y_0 < try_0)) then
            number_points_well_separated = number_points_well_separated + 1
            well_separated(number_points_well_separated) = i
        end if
    end do
    
end subroutine points_well_separated
'''

#codeComputeInterpolationData = '''!    -*- f90 -*-
#subroutine compute_interpolation_data(x,y,nbp,ux,vx,dimx,uy,vy,dimy,sx,ex,sy,ey,A0,B0,dx,dy,iiu,jju,wwu,iiv,jjv,wwv)
#implicit none
#integer, intent(in) :: nbp,dimx,dimy,sx,ex,sy,ey
#double precision, intent(in) :: A0,B0,dx,dy
#double precision, dimension(0:(dimx-1)) , intent(inout) :: ux,vx
#double precision, dimension(0:(dimy-1)) , intent(inout) :: uy,vy
#double precision, dimension(0:(nbp-1)) , intent(inout) :: x, y
#double precision, dimension(0:(nbp-1), 4), intent(out) :: iiu, jju , wwu, iiv, jjv, wwv
#double precision :: a1,a2,a3,a4,b1,b2,b3,b4,temp
#integer :: i, j, point
#
#!f2py threadsafe
#!f2py intent(inplace) :: ux,uy,vx,vy,x,y
#!f2py intent(out) :: iiu,jju,wwu,iiv,jjv,wwv
#
#do point = 0, nbp-1
#    i = int(ceiling((x(point)-A0)/dx)) - 1 + sx
#    j = modulo(int(floor((y(point)-B0)/dy))  + sy , ey)
#
#    !###############################
#    !for U
#    !###############################
#    iiu(point,1)=i
#    iiu(point,2)=i-1
#    iiu(point,3)=i
#    iiu(point,4)=i-1
#    
#    jju(point,1)=j
#    jju(point,2)=j
#    if (y(point) > uy(j)) then
#        jju(point,3) = j+1
#        jju(point,4) = j+1
#    else
#        jju(point,3) = j-1
#        jju(point,4) = j-1
#    end if
#    
#    temp = abs(ux(i)-x(point))
#    a1 = abs(dx-temp)
#    a2 = abs(dx-a1)
#    a3 = a1
#    a4 = a2
#    
#    temp = abs(uy(j)-y(point))
#    b1 = abs(dy-temp)
#    b2 = b1
#    b3 = abs(dy-b1)
#    b4 = b3
#       
#    wwu(point,1)=a1*b1/(dx*dy)
#    wwu(point,2)=a2*b2/(dx*dy)
#    wwu(point,3)=a3*b3/(dx*dy)
#    wwu(point,4)=a4*b4/(dx*dy)
#    
#    !###############################
#    !for V
#    !###############################
#    jjv(point,1)=j
#    jjv(point,2)=j+1
#    jjv(point,3)=j
#    jjv(point,4)=j+1
#    
#    iiv(point,1)=i
#    iiv(point,2)=i
#    
#    if (x(point) > vx(i)) then
#        iiv(point,3) = i+1
#        iiv(point,4) = i+1
#    else
#        iiv(point,3) = i-1
#        iiv(point,4) = i-1
#    end if
#    
#    temp = abs(vx(i)-x(point))
#    a1 = abs(dx-temp)
#    a2 = a1
#    a3 = abs(dx-a1)
#    a4 = a3
#    
#    temp = abs(vy(j)-y(point))
#    b1 = abs(dy-temp)
#    b2 = abs(dy-b1)
#    b3 = b1
#    b4 = b2
#       
#    wwv(point,1)=a1*b1/(dx*dy)
#    wwv(point,2)=a2*b2/(dx*dy)
#    wwv(point,3)=a3*b3/(dx*dy)
#    wwv(point,4)=a4*b4/(dx*dy)
#end do
#end subroutine compute_interpolation_data
#'''

#codeComputePole = '''!    -*- f90 -*-
#subroutine compute_pole(cx,cy,bcx,bcy,Bx,By,numterms,dimx,dimy,ii,jj,ww,nbp,n,points_inside,m,fx,fy,dx,dy,Hx,Hy)
#    implicit none
#    integer, intent(in) :: m,n,dimx,dimy,nbp,numterms
#    double precision, intent(in) :: cx,cy !panel center
#    double precision, intent(in) :: bcx, bcy !base panel center
#    double precision, dimension(numterms,0:(dimx-1),0:(dimy-1)), intent(inout) :: Bx,By
#    double precision, dimension(0:(nbp-1),n), intent(inout) :: ii, jj, ww
#    double precision, dimension(0:(nbp-1)), intent(inout) :: fx,fy
#    integer (kind=8), dimension(m), intent(inout) :: points_inside
#    double precision, intent(in) :: dx,dy
#    double precision, intent(out) :: Hx,Hy
#    integer :: point, i1,i2,i3,i4, j1,j2,j3,j4,l, k, shifti,shiftj
#
#    !f2py intent(inplace) :: Bx,By, ii, jj, ww, fx,fy, points_inside
#    !f2py intent(hide) :: nbp,n,m,dimx,dimy,numterms
#    !f2py intent(out) :: Hx,Hy
#    
#    shifti = int((bcx-cx)/dx)
#    shiftj = int((bcy-cy)/dy)
#    Hx = 0.
#    Hy = 0.
#
#    do k = 1, m
#        point = points_inside(k)
#        i1 = ii(point,1)+shifti
#        i2 = ii(point,2)+shifti
#        i3 = ii(point,3)+shifti
#        i4 = ii(point,4)+shifti
#        j1 = jj(point,1)+shiftj
#        j2 = jj(point,2)+shiftj
#        j3 = jj(point,3)+shiftj
#        j4 = jj(point,4)+shiftj
#        do l = 1, numterms
#            Hx = Hx + (Bx(l,i1,j1)*ww(point,1) + Bx(l,i2,j2)*ww(point,2) + Bx(l,i3,j3)*ww(point,3) + Bx(l,i4,j4)*ww(point,4))*fx(point)
#            Hy = Hy + (By(l,i1,j1)*ww(point,1) + By(l,i2,j2)*ww(point,2) + By(l,i3,j3)*ww(point,3) + By(l,i4,j4)*ww(point,4))*fy(point)
#        end do
#    end do
#end subroutine compute_pole
#'''
#
#
#codeComputeInteraction = '''!    -*- f90 -*-
#subroutine compute_interaction(cx,cy,bcx,bcy,Ax,Ay,numterms,dimx,dimy,ii,jj,ww,nbp,n,dx,dy,Hx,Hy,point,tempx,tempy)
#    implicit none
#    integer, intent(in) :: n,dimx,dimy,nbp,numterms,point
#    double precision, intent(in) :: cx,cy !panel center
#    double precision, intent(in) :: bcx, bcy !base panel center
#    double precision, intent(in) :: dx,dy,Hx,Hy
#    double precision, intent(out) :: tempx, tempy
#    
#    double precision, dimension(numterms,0:(dimx-1),0:(dimy-1)), intent(inout) :: Ax,Ay
#    double precision, dimension(0:(nbp-1),n), intent(inout) :: ii, jj, ww
#
#    integer :: i1,i2,i3,i4,j1,j2,j3,j4,l,k,shifti,shiftj
#    double precision :: Axtemp, Aytemp
#
#    !f2py intent(inplace) :: Ax,Ay, ii, jj, ww
#    !f2py intent(hide) :: nbp,n,m,dimx,dimy,numterms
#    !f2py intent(out) :: tempx, tempy
#    
#    shifti = int((bcx-cx)/dx)
#    shiftj = int((bcy-cy)/dy)
#
#    i1 = ii(point,1)+shifti
#    i2 = ii(point,2)+shifti
#    i3 = ii(point,3)+shifti
#    i4 = ii(point,4)+shifti
#    j1 = jj(point,1)+shiftj
#    j2 = jj(point,2)+shiftj
#    j3 = jj(point,3)+shiftj
#    j4 = jj(point,4)+shiftj
#    
#    tempx = 0.
#    tempy = 0.
#    do l = 1, numterms
#        Axtemp = Ax(l,i1,j1)*ww(point,1) + Ax(l,i2,j2)*ww(point,2) + Ax(l,i3,j3)*ww(point,3) + Ax(l,i4,j4)*ww(point,4)
#        Aytemp = Ay(l,i1,j1)*ww(point,1) + Ay(l,i2,j2)*ww(point,2) + Ay(l,i3,j3)*ww(point,3) + Ay(l,i4,j4)*ww(point,4)
#        tempx = tempx + Axtemp*Hx
#        tempy = tempy + Aytemp*Hy
#    end do
#end subroutine compute_interaction
#'''
#
#codeComputeDirectSummation = '''!    -*- f90 -*-
#subroutine compute_direct_summation(fiber_in,ufx,vfx,ufy,vfy,n,m,x,y,fx,fy,nbp,x_ufx,x_vfx,l,y_ufx,y_vfx,k,&
#                                    A0,A1,B0,B1,dx,dy,sx,sy,ey,points,num_points,tempx,tempy)
#    implicit none
#    double precision , intent(in) :: A0 , A1 , B0 , B1 , dx, dy
#    integer , intent(in) :: n , m , l , nbp , sx , sy , ey , k, num_points, fiber_in
#    double precision , dimension(0:(n-1),0:(m-1)) , intent(inout) :: ufx , vfx , ufy , vfy
#    double precision , dimension(0:(nbp-1)) , intent(inout) :: x , y, fx, fy !x and y of the fiber
#    double precision , dimension(0:(l-1)) , intent(inout) :: x_ufx, x_vfx
#    double precision , dimension(0:(k-1)) , intent(inout) :: y_ufx , y_vfx
#    integer(kind=8), dimension(num_points), intent(inout) :: points
#    double precision :: x_size , y_size , distx , disty, interpolated_value
#    integer :: fiber_out, i
#    double precision, intent(out) :: tempx, tempy
#    
#    !f2py intent(in) :: A0 , B0 , dx, dy , sx , sy , ey, fiber_in
#    !f2py intent(inplace) :: ufx , vfx , ufy , vfy , x , y , fx, fy, x_ufx, y_ufx , x_vfx , y_vfx, points
#    !f2py intent(hide) :: m , n  , l , nbp , k, num_points
#    !f2py intent(out) :: tempx, tempy
#    
#    x_size = A1 - A0
#    y_size = B1 - B0
#    
#    tempx = 0.
#    tempy = 0.
#    do i = 1 , num_points
#        fiber_out = points(i)
#        distx = modulo( x(fiber_out) - x(fiber_in) + x_ufx(sx) + x_size, x_size)
#        disty = modulo( y(fiber_out) - y(fiber_in) + y_ufx(sy) + y_size, y_size)
#                    
#        call interpolate_eulerian_u(ufx,n,m, x_ufx , y_ufx , l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
#        tempx = tempx + interpolated_value*fx(fiber_out)
#        call interpolate_eulerian_u(ufy,n,m, x_ufx , y_ufx , l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
#        tempy = tempy + interpolated_value*fy(fiber_out)
#        
#        
#        distx = modulo( x(fiber_out) - x(fiber_in) + x_vfx(sx) + x_size, x_size)
#        disty = modulo( y(fiber_out) - y(fiber_in) + y_vfx(sy) + y_size, y_size)
#        
#        call interpolate_eulerian_v(vfx,n,m, x_vfx , y_vfx , l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
#        tempx = tempx + interpolated_value*fx(fiber_out)
#        call interpolate_eulerian_v(vfy,n,m, x_vfx , y_vfx , l,distx,disty,sx,sy,ey,A0,B0,dx,dy,interpolated_value)
#        tempy = tempy + interpolated_value*fy(fiber_out)
#    end do
#end subroutine compute_direct_summation
#'''


codeTreeHeader = '''!    -*- f90 -*-
module treecode
include "omp_lib.h"
contains
'''

codeTree = codeTreeHeader + codeInterpolateNode + codeInsidePanel + codeWellSeparatedPanel + "end module"

def retrieveCode():
    name_of_current_file = realpath(__file__)
    codeInfo = [(codeTree, "fast_treecode", name_of_current_file)]
    return codeInfo
