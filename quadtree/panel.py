'''
Created on Apr 30, 2012

@author: fnfranco
'''
import utils
import numpy
import pylab
ceil = numpy.ceil
floor = numpy.floor

def interpolate(G, x, y, grid):
    dx, dy = grid.getSpacing()
    A0, B0 = grid.origin.getTuple()
    sx, sy = grid.sx, grid.sy
    # put in the center of the node
    x += grid.nodeX[sx]
    y += grid.nodeY[sy]

    i = int(ceil((x - A0) / dx)) - 1 + sx
    j = int(floor((y - B0) / dy)) + sy

    # compute the interpolation weight for each point
    w1 = abs(grid.nodeY[j] - y)
    w2 = dy - w1
    w4 = abs(x - grid.nodeX[i])
    w3 = dx - w4

    a = G[i - 1, j]
    b = G[i - 1, j + 1]
    c = G[i, j + 1]
    d = G[i, j]

    delta1 = (w1 * b + w2 * a) / dy
    delta2 = (w1 * c + w2 * d) / dy
    return (w3 * delta2 + w4 * delta1) / dx


class Panel:

    def __init__(self, center, width, height, parent=None):
        self.center = center
        self.width = width
        self.height = height
        self.parent = parent
        self.children = None
        self.computed_pole = False

        self.left_corner_x = center[0] - width / 2.
        self.left_corner_y = center[1] - height / 2.

        self.points_inside = []

        if parent == None:
            self.depth = 1
        else:
            self.depth = parent.depth + 1


    def is_root(self):
        if self.parent == None: return True
        return False


    def has_children(self):
        if self.children == None: return False
        return True


    def create_children(self):
        cw = self.width / 2.
        ch = self.height / 2.
        lx, rx = self.center[0] - cw / 2., self.center[0] + cw / 2.
        by, ty = self.center[1] - ch / 2., self.center[1] + ch / 2.
        c1 = (lx, by)
        c2 = (rx, by)
        c3 = (rx, ty)
        c4 = (lx, ty)
        self.children = []
        for c in [c1, c2, c3, c4]:
            self.children.append(Panel(c, cw, ch, self))


    def its_inside(self, x, y):
        c, w, h = self.center, self.width, self.height
        if (c[0] - w / 2. < x <= c[0] + w / 2.) and (c[1] - h / 2. <= y < c[1] + h / 2.): return True
        return False


    def count_points(self, x, y):
        counter = 0
        for i in range(len(x)):
            if self.its_inside(x[i], y[i]):
                counter += 1
                self.points_inside.append(i)
        return counter


    def is_well_separated(self, x, y, domain_size_x, domain_size_y):
        sx = self.left_corner_x - self.width
        sy = self.left_corner_y - self.height

        if (x - sx) % domain_size_x > 3 * self.width: return 1
        if (y - sy) % domain_size_y > 3 * self.height: return 1
        return 0


    def set_base_decomposition(self, ffe):
        dec = ffe.getBaseDecomposition(self.depth)
        self.base_center = dec.center
        self.ufx = dec.ufx
        self.ufy = dec.ufy
        self.vfx = dec.vfx
        self.vfy = dec.vfy

        self.shiftx = (self.base_center[0] - self.center[0])
        self.shifty = (self.base_center[1] - self.center[1])



    def compute_pole(self, x, y, fx, fy, grid):
        if not self.computed_pole:
            ufxB = self.ufx[1]
            ufyB = self.ufy[1]
            vfxB = self.vfx[1]
            vfyB = self.vfy[1]
            numTerms = len(ufxB)
            self.Hux = numpy.zeros(numTerms)
            self.Huy = numpy.zeros(numTerms)
            self.Hvx = numpy.zeros(numTerms)
            self.Hvy = numpy.zeros(numTerms)
            for point in self.points_inside:
                xp, yp = x[point], y[point]
                # relocate the point to the base panel center
                xp += self.shiftx
                yp += self.shifty

                # make the interpolation for all terms B
                for w in range(numTerms):
                    self.Hux[w] = interpolate(ufxB[w], xp, yp, grid) * fx[point]
                    self.Huy[w] = interpolate(ufyB[w], xp, yp, grid) * fy[point]
                    self.Hvx[w] = interpolate(vfxB[w], xp, yp, grid) * fx[point]
                    self.Hvy[w] = interpolate(vfyB[w], xp, yp, grid) * fy[point]
#                     self.Hx[w] = interpolate(ufxB[w], xp, yp, grid) + interpolate(ufyB[w], xp, yp, grid)
#                     self.Hy[w] = interpolate(vfxB[w], xp, yp, grid) + interpolate(vfyB[w], xp, yp, grid)

                self.computed_pole = True



    def draw(self, color=None):
        c, w, h = self.center, self.width, self.height
        ax = pylab.gca()
        if color:
            ax.add_patch(pylab.Rectangle((c[0] - w / 2., c[1] - h / 2.), w, h, facecolor=color))
        else:
            ax.add_patch(pylab.Rectangle((c[0] - w / 2., c[1] - h / 2.), w, h, fill=False))


    def selectPoints(self, x, y, xsize, ysize):
        self.count_points(x, y)
        self.pointsWellSeparated = []
        for i in range(len(x)):
            if self.is_well_separated(x[i], y[i], xsize, ysize):
                self.pointsWellSeparated.append(i)

if __name__ == '__main__':
    root = Panel((.5, .5), 1., 1.)
    root.create_children()

    c1, c2, c3, c4 = root.children
    assert(c1.its_inside(.25, .1) == True)
    assert(c1.its_inside(.25, .51) == False)
    assert(c1.its_inside(.5, 0.) == True)
    assert(c1.its_inside(.5, .5) == False)
    assert(c4.its_inside(.5, .5) == True)

