'''
Created on Apr 30, 2012

@author: fnfranco
'''
import utils
from panel import Panel, interpolate
import numpy
from math_objects.cartesian_grid import Grid, Point
from physic_objects.ib import RandomPoints
from physic_objects.force import EllasticForce
from physic_objects.fluid_structure import FluidStructure
from physic_objects.fluid import StokesFluid


class QuadTree:

    def __init__(self, grid, max_points, ffe):
        center = grid.origin + (grid.sizeX / 2., grid.sizeY / 2.)
        self.root = Panel(center, grid.sizeX, grid.sizeY)
        self.max_points = max_points
        self.grid = grid
        self.ffe = ffe


    def create_tree(self, x, y):
        self.x = x.copy()
        self.y = y.copy()
        self._recur_create_tree(x, y, self.root)


    def draw(self, color=None):
        self._recur_draw(self.root, color)


    def sort_panels(self, x, y):
        domain_size_x, domain_size_y = self.grid.sizeX, self.grid.sizeY
        well_separated = []
        close_panels = []
        self._recur_sort_panels(x, y, domain_size_x, domain_size_y, self.root, well_separated, close_panels)

        close_points = []
        for cp in close_panels:
            close_points += cp.points_inside

        return well_separated, close_points


    def pre_eval(self, x, y, M):
        nbp = len(x)
        well_separated_list = [None for _ in range(nbp)]
        self.Aux_list = [None for _ in range(nbp)]
        self.Auy_list = [None for _ in range(nbp)]
        self.Avx_list = [None for _ in range(nbp)]
        self.Avy_list = [None for _ in range(nbp)]
        close_points_list = [None for _ in range(nbp)]
        vec_list = [None for _ in range(nbp)]  # list of vectors that are made of the M entries for the close points

        for i in range(nbp):
            well_separated_list[i], close_points_list[i] = self.sort_panels(x[i], y[i])

            ncp = len(close_points_list[i])
            temp = numpy.zeros((2, 2 * ncp))
            for l in range(ncp):
                point = close_points_list[i][l]

                temp[0, l] = M[i, point]
                temp[0, l + ncp] = M[i, point + nbp]
                temp[1, l] = M[i + nbp, point]
                temp[1, l + ncp] = M[i + nbp, point + nbp]

            vec_list[i] = temp

        self.well_separated_list = well_separated_list
        self.close_points_list = close_points_list
        self.vec_list = vec_list
        self._pre_eval_well_separated()


    def eval(self, fx, fy, h):
        nbp = len(fx)
        rx, ry = numpy.zeros_like(fx), numpy.zeros_like(fy)

        for w in self.well_separated_list:
            for k in w:
                k.computed_pole = False

        for i in range(nbp):
            # close points first
            self._eval_close_points(i, fx, fy, rx, ry)
            self._eval_well_separated(i, fx, fy, rx, ry, h)
        return rx, ry


    def _eval_close_points(self, i, fx, fy, rx, ry):
        close_points = self.close_points_list[i]
        vec = self.vec_list[i]
        ncp = len(close_points)
        vec_f = numpy.concatenate([numpy.array([fx[k] for k in close_points]), numpy.array([fy[k] for k in close_points])])
        rx[i] += vec[0, :].dot(vec_f)
        ry[i] += vec[1, :].dot(vec_f)


    def _eval_well_separated(self, i, fx, fy, rx, ry, h):
        well_separated = self.well_separated_list[i]

        if well_separated:
            num_ws = len(well_separated)
            numTerms = len(well_separated[0].ufx)
            Auxs = self.Aux_list[i]
            Auys = self.Auy_list[i]
            Avxs = self.Avx_list[i]
            Avys = self.Avy_list[i]

            for z in range(num_ws):
                if well_separated[z].points_inside:
                    well_separated[z].compute_pole(self.x, self.y, fx, fy, self.grid)

                    Aux = Auxs[z]
                    Auy = Auys[z]
                    Avx = Avxs[z]
                    Avy = Avys[z]

                    for w in range(numTerms):
                        rx[i] += (well_separated[z].Hux[w] * Aux[w] + well_separated[z].Huy[w] * Auy[w]) * h
                        ry[i] += (well_separated[z].Hvx[w] * Avx[w] + well_separated[z].Hvy[w] * Avy[w]) * h


    def _pre_eval_well_separated(self):
        for i in range(len(self.x)):
            well_separated = self.well_separated_list[i]
            num_ws = len(well_separated)
            grid = self.grid
            Auxs = [None for _ in range(num_ws)]
            Auys = [None for _ in range(num_ws)]
            Avxs = [None for _ in range(num_ws)]
            Avys = [None for _ in range(num_ws)]
            for z in range(num_ws):
                p = well_separated[z]
                xp = ((self.x[i] + p.shiftx) - grid.origin.x) % grid.sizeX + grid.origin.x
                yp = ((self.y[i] + p.shifty) - grid.origin.y) % grid.sizeY + grid.origin.y

                ufxA = p.ufx[0]
                ufyA = p.ufy[0]
                vfxA = p.vfx[0]
                vfyA = p.vfy[0]
                numTerms = len(ufxA)
                Aux = numpy.zeros(numTerms)
                Auy = numpy.zeros(numTerms)
                Avx = numpy.zeros(numTerms)
                Avy = numpy.zeros(numTerms)
                for w in range(numTerms):
                    Aux[w] = interpolate(ufxA[w], xp, yp, grid)
                    Auy[w] = interpolate(ufyA[w], xp, yp, grid)
                    Avx[w] = interpolate(vfxA[w], xp, yp, grid)
                    Avy[w] = interpolate(vfyA[w], xp, yp, grid)
                Auxs[z] = Aux
                Auys[z] = Auy
                Avxs[z] = Avx
                Avys[z] = Avy

            self.Aux_list[i] = Auxs
            self.Auy_list[i] = Auys
            self.Avx_list[i] = Avxs
            self.Avy_list[i] = Avys




    def _recur_sort_panels(self, x, y, domain_size_x, domain_size_y, root, well_separated, close_panels):
        if root.is_well_separated(x, y, domain_size_x, domain_size_y):
            well_separated.append(root)
        elif root.has_children():
            for c in root.children:
                self._recur_sort_panels(x, y, domain_size_x, domain_size_y, c, well_separated, close_panels)
        else:
            close_panels.append(root)


    def _recur_create_tree(self, x, y, root):
        if root.count_points(x, y) > self.max_points and root.depth < self.ffe.numLevels:
            root.create_children()
            for c in root.children:
                if c.depth >= 3:
                    c.set_base_decomposition(self.ffe)
                self._recur_create_tree(x, y, c)


    def _recur_draw(self, root, color=None):
        root.draw(color)
        if root.has_children():
            for c in root.children:
                self._recur_draw(c, color)



if __name__ == '__main__':
    import pylab
    from time import time
    from physic_objects.ib import Ellipse


    nbp = 100
    grid = Grid(Point(0., 0.), 1., 1., 64, 64, 2)
#     ib = RandomPoints(0.51, .6, 0.51, .6, nbp, grid, EllasticForce(1))
#     ib = RandomPoints(0., 1., 0., 1., nbp, grid, EllasticForce(1))
    ib = Ellipse(nbp, Point(.5, .5), .2, .1, grid, EllasticForce(10e6))
#     ib.x[0] = .2
#     ib.y[0] = .2
    ib.computeForce()
    fx, fy = ib.force.x, ib.force.y
#     fx[:] = 1.0
#     fy[:] = 1.0


    fluid = StokesFluid(grid)
    dt = .01
    systemQuad = FluidStructure(fluid, ib, dt, "Implicit", "Quadtree")
    ffe = systemQuad.FSInteraction.ffe

    system = FluidStructure(fluid, ib, dt, "Implicit", "Matrix")
    system.FSInteraction.performPreComputations(fluid, ib, dt)
    M = system.FSInteraction.M
    rM = M.dot(numpy.concatenate([fx, fy]))

    max_points = 0
    q = QuadTree(grid, max_points, ffe)

    t = time()
    q.create_tree(ib.x, ib.y)
    q.draw()
    print "Time to create tree:", time() - t

    t = time()
    well_separated, close_points = q.sort_panels(ib.x[0], ib.y[0])
    print "Time to separate panels for ONE point:", time() - t

    pylab.plot(ib.x, ib.y, '.')
    for p in well_separated: p.draw("yellow")
    for point in close_points: pylab.plot(ib.x[point], ib.y[point], "go")
    pylab.plot(ib.x[0], ib.y[0], 'ro')
    pylab.show()

    t = time()
    q.pre_eval(ib.x, ib.y, M)
    print "Time to pre eval:", time() - t

    t = time()
    rx, ry = q.eval(fx, fy, ib.h)
    print "Time to eval:", time() - t

    print abs(rM[:nbp] - rx).max() / 10e6
    print abs(rM[nbp:] - ry).max() / 10e6
#     print rM[:nbp], rx
#     print M


