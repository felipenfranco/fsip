# -*- coding: utf-8 -*-
'''
Created on Mar 3, 2012

@author: felipe
'''
from os.path import realpath, getmtime, split, exists, isdir
from os import chdir, curdir, makedirs
from numpy import f2py
import sys

# try:
#    import sympy
# except:
#    raise ImportError('You need sympy installed to use this package! Goto: http://sympy.org/')

# try:
#    import pylab
# except:
#    raise ImportError('''You need pylab (matplotlib) installed to use this package! Either install it, or comment the lines
#    where a pylab/matplotlib statement appears in the code used. Goto: www.scipy.org/PyLab''')


# try:
#    import pycppad
# except:
#    raise ImportError('''You need pycppad installed to use this package! First install cppAD, then pycppad.
#    Goto: www.coin-or.org/CppAD/  and  http://www.seanet.com/~bradbell/pycppad/index.xml''')


__debug = False

#=======================================================================================================================
# Set up the needed directory
#=======================================================================================================================
rootPath = split(realpath(__file__))[0]
sys.path.insert(0, rootPath)
sys.path.insert(0, rootPath + "//fortran_binary")
sys.path.insert(0, rootPath + "//cached_objects")
sys.path.insert(0, rootPath + "//cached_simulations")

if not isdir(rootPath + "//fortran_binary"):
    makedirs(rootPath + "//fortran_binary")

if not isdir(rootPath + "//cached_objects"):
    makedirs(rootPath + "//cached_objects")

if not isdir(rootPath + "//cached_simulations"):
    makedirs(rootPath + "//cached_simulations")

#=======================================================================================================================
# Fortran compile function
#=======================================================================================================================
def compileFortran(code, module_name, source_module):
    '''
    Compiles the Fortran code if the source_module of the code has suffered modifications.
    '''

    optmizationArgs = '''--fcompiler=gnu95 --f90flags="-O3 -ffree-form -ffree-line-length-none -fopenmp"'''
    debugArgs = '''--fcompiler=gnu95 --f90flags="-O0 -g -fimplicit-none -fbounds-check -fbacktrace -ffree-form -ffree-line-length-none"'''

    if __debug:
        compilationArgs = debugArgs
    else:
        compilationArgs = optmizationArgs

    reportArgs = ''' -DF2PY_REPORT_ON_ARRAY_COPY=10000 -lgomp'''
    pathFortranBinary = rootPath + "//fortran_binary"
    originalDir = realpath(curdir)

    # change to the fortran binaries directory so the compiled file is located there
    chdir(pathFortranBinary)
    compiledFile = pathFortranBinary + "//%s.so" % (module_name)

    if exists(compiledFile):
        if getmtime(source_module) > getmtime(compiledFile):
            f2py.compile(code, module_name, compilationArgs + reportArgs, verbose=0)
    else:
        f2py.compile(code, module_name, compilationArgs + reportArgs, verbose=0)

    # change back to the original dir, before changing to the fortran binaries dir
    chdir(originalDir)


#===================================================================================================
# Compiling All Fortran Code
#===================================================================================================
import math_objects
import linear_algebra
import physic_objects
import quadtree
import swimmer
import fft_solver


allCode = []
modules = [math_objects, linear_algebra, physic_objects, quadtree, swimmer, fft_solver]
for module in modules:
    modCode = module.retrieveCode()
    if modCode:
        allCode += modCode

for info in allCode:
    code , module_name, source_module = info
    compileFortran(code , module_name, source_module)


#===================================================================================================
# Time Class
#===================================================================================================
from time import time

class Time:
    def __init__(self, simulationFinalTime):
        self.simulationFinalTime = simulationFinalTime
        self.currentTime = 0.
        self.elapsedTime = 0.

    def start(self):
        self.currentTime = time()

    def stop(self):
        self.elapsedTime = time() - self.currentTime

    def getElapsedTime(self):
        return self.elapsedTime

    def remainingSimulationTime(self, dt, simulationCurrentTime):
        return ((self.simulationFinalTime - simulationCurrentTime) / dt) * self.elapsedTime / 60.

    def printTimeInfo(self, dt, simulationCurrentTime):
        print "\t\t\t\t Step time:(%g) Estimated remaining time:(%g minutes)" % (self.elapsedTime, self.remainingSimulationTime(dt, simulationCurrentTime))


#===============================================================================
# Email sender
#===============================================================================
import smtplib


def mailMe(msg):
    fromaddr = 'felipenfranco.research@gmail.com'
    toaddrs = 'felipenfranco@gmail.com'

    # Credentials (if needed)
    username = 'felipenfranco.research@gmail.com'
    password = '2172987415'

    # The actual mail send
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username, password)
    print "Sending mail with msg: %s" % (msg)
    server.sendmail(fromaddr, toaddrs, msg)
    server.quit()
