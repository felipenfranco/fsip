'''
Created on Mar 4, 2012

@author: felipe
'''
import utils
from warnings import warn
from numpy.linalg import norm
from math_objects.scalar_field import CenterScalarField, RightScalarField, BottomScalarField, \
    ScalarField
from math_objects.cartesian_grid import Grid, Point
import linear_algebra
import fast_laplace
import numpy



class LinearSystem(object):
    '''
    This is the implementation of a Linear System of equations mathematical model.
    Ax = rhs
    The matrix A is given in the form of a function which receives a x^n (i.e. the value of x
    in the iteration number n) and returns the updated x^n+1.
    '''


    def __init__(self, updateFunction, residualFunction, ScalarField):
        '''
            updateFunction: given x^n updates to x^n+1, where x is a ScalarField
            residualFunction: returns Ax-rhs, as a ScalarField
        '''
        self.updateFunction = updateFunction
        self.residualFunction = residualFunction

        self.x = ScalarField
        self.residual = ScalarField.generateNewScalarField(self.x.grid)
        self.rhs = ScalarField.generateNewScalarField(self.x.grid)
        self.sx, self.ex, self.sy, self.ey = ScalarField.grid.getBounds(Fortran = True)
        self.ngc = ScalarField.grid.ngc
        self.dx, self.dy = ScalarField.grid.getSpacing()

    def computeIteration(self, visc, dens, dt, numberOfIterations = 1):
        for i in xrange(numberOfIterations):
            self.updateFunction(self.x.data, self.rhs.data, self.sx, self.ex, self.sy, self.ey, self.ngc, self.dx, self.dy, visc, dens, dt)

    def computeResidual(self, visc, dens, dt):
        self.residualFunction(self.x.data, self.rhs.data, self.residual.data, self.sx, self.ex, self.sy, self.ey, self.ngc, self.dx, self.dy, visc, dens, dt)

    def solveSystem(self, visc, dens, dt, tol = 10e-10, printResidual = False):
        residualNorm = 1.
        numIterations = 0
        while residualNorm > tol:
            self.computeIteration(visc, dens, dt)
            self.computeResidual(visc, dens, dt)
            residualNorm = numpy.abs(self.residual.data).max()
            if printResidual:
                print residualNorm
            numIterations += 1
            if numIterations > 20000:
                warn("Too much iterations. Not converging.")
                exit
        print "Number of Iterations: ", numIterations

    def enforce_compatibility_condition(self):
        sx, ex, sy, ey = self.rhs.grid.getBounds()
        dx, dy = self.dx, self.dy
        sizeX, sizeY = self.rhs.grid.sizeX, self.rhs.grid.sizeY
        integral = self.rhs.data[sx:ex, sy:ey].sum() * dx * dy / (sizeX * sizeY)
        self.rhs.data[sx:ex, sy:ey] -= integral

    def generateNewLinearSystem(self, ScalarField):
        return LinearSystem(self.updateFunction, self.residualFunction, ScalarField)


if __name__ == '__main__':
    from sympy import var, diff, sin, cos, lambdify
    from numpy import pi, inf
    import numpy
    dimensions = (16, 32, 64, 128, 256)
    visc = pi
    dens = 0.
    dt = 0.1
    for n in dimensions:
        origin = Point(0., 0.)
        nx, ny = n, n
        grid = Grid(origin, 1., 1., nx, ny, 2)

        sx, ex, sy, ey = grid.getBounds()

        p = CenterScalarField(grid)
        u = RightScalarField(grid)
        v = BottomScalarField(grid)
        pExact = CenterScalarField(grid)
        uExact = RightScalarField(grid)
        vExact = BottomScalarField(grid)


        x, y = var("x y")
        function = sin(2.*pi * x) * cos(2.*pi * y)

        laplacian = lambdify([x, y], -visc * (diff(function, x, x) + diff(function, y, y)), numpy)
        function = lambdify([x, y], function, numpy)


        systemP = LinearSystem(fast_laplace.laplace.laplace_update_function_stokes, fast_laplace.laplace.laplace_residual_function_stokes, p)
        systemU = LinearSystem(fast_laplace.laplace.laplace_update_function_stokes, fast_laplace.laplace.laplace_residual_function_stokes, u)
        systemV = LinearSystem(fast_laplace.laplace.laplace_update_function_stokes, fast_laplace.laplace.laplace_residual_function_stokes, v)

        systemP.rhs.initialize(laplacian)
        systemP.solveSystem(visc, dens, dt)

        systemU.rhs.initialize(laplacian)
        systemU.solveSystem(visc, dens, dt)

        systemV.rhs.initialize(laplacian)
        systemV.solveSystem(visc, dens, dt)

        #check the results
        pExact.initialize(function)
        uExact.initialize(function)
        vExact.initialize(function)
        print "Max norm of the difference (%d,%d):" % (nx, ny)
        print "Center: ", norm((pExact.data - p.data).flatten(), inf)
        print "Right:  ", norm((uExact.data - u.data).flatten(), inf)
        print "Bottom: ", norm((vExact.data - v.data).flatten(), inf)
        print "\n\n"
