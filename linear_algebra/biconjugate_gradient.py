'''
Created on Mar 8, 2012

@author: felipe
In numerical linear algebra, the biconjugate gradient stabilized method, often abbreviated
as BiCGSTAB, is an iterative method developed by H. A. van der Vorst for the numerical solution
of nonsymmetric linear systems. It is a variant of the biconjugate gradient method (BiCG) and has
faster and smoother convergence than the original BiCG as well as other variants such as the
conjugate gradient squared method (CGS). It is a Krylov subspace method.
 (Wikipedia)
'''
import utils
from math_objects.scalar_field import ScalarField
import multigrid
import numpy
import fast_bicgstab
from fft_solver.poisson import FFTPoisson

class TempVarsBicgstab:
    def __init__(self):
        self.initialized = False

    def initialize(self, fluid):
        if not self.initialized:
            self.initialized = True
            pressure = fluid.pressure
            velocity = fluid.velocity
            grid = pressure.grid
            self.z1u = velocity.u.generateNewScalarField(grid)
            self.z2u = velocity.u.generateNewScalarField(grid)

            self.z1v = velocity.v.generateNewScalarField(grid)
            self.z2v = velocity.v.generateNewScalarField(grid)

            self.q = pressure.generateNewScalarField(grid)
            self.q_old = pressure.generateNewScalarField(grid)
            self.s = pressure.generateNewScalarField(grid)

            self.w = ScalarField(grid, 0., 0.)
            self.w_old = ScalarField(grid, 0., 0.)
            self.t = ScalarField(grid, 0., 0.)
            self.r = ScalarField(grid, 0., 0.)
            self.r_til = ScalarField(grid, 0., 0.)

            self.multigridU = multigrid.Multigrid(fluid.systemU)
            self.multigridV = multigrid.Multigrid(fluid.systemV)
            self.multigridP = multigrid.Multigrid(fluid.systemP)

            baseSystem = fluid.systemU
            self.systemZ1U = baseSystem.generateNewLinearSystem(self.z1u)
            self.systemZ2U = baseSystem.generateNewLinearSystem(self.z2u)
            self.systemZ1V = baseSystem.generateNewLinearSystem(self.z1v)
            self.systemZ2V = baseSystem.generateNewLinearSystem(self.z2v)

            self.multigridZ1U = multigrid.Multigrid(self.systemZ1U)
            self.multigridZ2U = multigrid.Multigrid(self.systemZ2U)
            self.multigridZ1V = multigrid.Multigrid(self.systemZ1V)
            self.multigridZ2V = multigrid.Multigrid(self.systemZ2V)
            self.nx, self.ny = grid.nx, grid.ny

            assert self.nx == self.ny
            assert grid.sizeX == grid.sizeY
            self.poisson_solver = FFTPoisson(self.nx, grid.sizeX)

        else:
            nx, ny = fluid.pressure.grid.nx, fluid.pressure.grid.ny
            if self.nx != nx or self.ny != ny:
                self.initialized = False
                self.initialize(fluid)


def div(varx, vary, res):
    sx, ex, sy, ey = varx.grid.getBounds(Fortran=True)
    dx, dy = varx.grid.getSpacing()
    fast_bicgstab.bicgstab.div(varx.data, vary.data, res.data, sx, ex, sy, ey, dx, dy)

def grad(var, gx, gy):
    sx, ex, sy, ey = var.grid.getBounds(Fortran=True)
    ngc = var.grid.ngc
    dx, dy = var.grid.getSpacing()
    fast_bicgstab.bicgstab.grad(var.data, gx.data, gy.data, sx, ex, sy, ey, ngc, dx, dy)

def innerProduct(a, b):
    sx, ex, sy, ey = a.grid.getBounds(Fortran=True)
    return fast_bicgstab.bicgstab.inner_product(a.data, b.data, sx, ex, sy, ey)

def bicgstab(fluid, visc, dens, dt):
    tempVars = fluid.tempVars
    tempVars.initialize(fluid)
    u, v, p = fluid.velocity.u, fluid.velocity.v, fluid.pressure
    z1u, z2u = tempVars.z1u, tempVars.z2u
    z1v, z2v = tempVars.z1v     , tempVars.z2v
    q, q_old = tempVars.q         , tempVars.q_old
    s, t = tempVars.s         , tempVars.t
    w, w_old = tempVars.w         , tempVars.w_old
    r, r_til = tempVars.r        , tempVars.r_til

    multigridU = tempVars.multigridU
    multigridV = tempVars.multigridV

    multigridZ1U = tempVars.multigridZ1U
    multigridZ2U = tempVars.multigridZ2U
    multigridZ1V = tempVars.multigridZ1V
    multigridZ2V = tempVars.multigridZ2V

    poisson_solver = tempVars.poisson_solver

    rhsZ1U, rhsZ2U = tempVars.systemZ1U.rhs, tempVars.systemZ2U.rhs
    rhsZ1V, rhsZ2V = tempVars.systemZ1V.rhs, tempVars.systemZ2V.rhs

    grid = u.grid
    u.data[:, :] = 0.
    v.data[:, :] = 0.
    p.data[:, :] = 0.
    dx, dy = grid.getSpacing()
    sx, ex, sy, ey = grid.getBounds()

    tolerance = dx * dy
#     multigridU.solve(visc, dens, dt, tolerance)
#     multigridV.solve(visc, dens, dt, tolerance)

    u.data[sx:ex, sy:ey] = poisson_solver.solve(multigridU.systems[0].rhs.data[sx:ex, sy:ey])
    v.data[sx:ex, sy:ey] = poisson_solver.solve(multigridV.systems[0].rhs.data[sx:ex, sy:ey])
    u.updateGhosts()
    v.updateGhosts()

    r[:, :] = 0.
    div(u, v, r)
    r_til.data[:, :] = r.data[:, :]

    for i in range(1, 10):
        rho = innerProduct(r_til, r)

        if rho == 0:
            print "Erro Uzawa!"
            exit()
        if i == 1:
            q.data[:, :] = r.data[:, :]
        else:
            beta = (rho / rho_old) * (alpha_old / omega_old)  # @UndefinedVariable
            q.data[:, :] = r.data[:, :] + beta * (q_old.data[:, :] - omega_old * w_old.data[:, :])  # @UndefinedVariable

        q.updateGhosts()
        grad(q, rhsZ1U, rhsZ1V)
        z1u.data[:, :] = 0.
        z1v.data[:, :] = 0.
        # multigridZ1U.solve(visc, dens, dt, tolerance)
        # multigridZ1V.solve(visc, dens, dt, tolerance)
        z1u.data[sx:ex, sy:ey] = poisson_solver.solve(multigridZ1U.systems[0].rhs.data[sx:ex, sy:ey])
        z1v.data[sx:ex, sy:ey] = poisson_solver.solve(multigridZ1V.systems[0].rhs.data[sx:ex, sy:ey])
        z1u.updateGhosts()
        z1v.updateGhosts()

        div(z1u, z1v, w)

        alpha = rho / innerProduct(r_til, w)

        s.data[:, :] = r.data[:, :] - alpha * w.data[:, :]
        s.updateGhosts()
        grad(s, rhsZ2U, rhsZ2V)
        z2u.data[:, :] = 0.
        z2v.data[:, :] = 0.

#         multigridZ2U.solve(visc, dens, dt, tolerance)
#         multigridZ2V.solve(visc, dens, dt, tolerance)
        z2u.data[sx:ex, sy:ey] = poisson_solver.solve(multigridZ2U.systems[0].rhs.data[sx:ex, sy:ey])
        z2v.data[sx:ex, sy:ey] = poisson_solver.solve(multigridZ2V.systems[0].rhs.data[sx:ex, sy:ey])
        z2u.updateGhosts()
        z2v.updateGhosts()

        div(z2u, z2v, t)

        omega = innerProduct(t , s) / innerProduct(t, t)

        p.data[:, :] = p.data[:, :] + alpha * q.data[:, :] + omega * s.data[:, :]
        u.data[:, :] = u.data[:, :] - alpha * z1u.data[:, :] - omega * z2u.data[:, :]
        v.data[:, :] = v.data[:, :] - alpha * z1v.data[:, :] - omega * z2v.data[:, :]
        r.data[:, :] = s.data[:, :] - omega * t.data[:, :]

        alpha_old = alpha  # @UnusedVariable
        rho_old = rho  # @UnusedVariable
        omega_old = omega  # @UnusedVariable
        w_old.data[:, :] = w.data[:, :]
        q_old.data[:, :] = q.data[:, :]

        residual = numpy.max(numpy.abs(r.data[:, :]))
        if residual < tolerance:
            break
    u.updateGhosts()
    v.updateGhosts()
    p.updateGhosts()



