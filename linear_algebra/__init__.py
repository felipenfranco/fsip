from os.path import realpath
import utils
from math_objects import updateGhostsCode

##################
# FORTRAN SNIPPETS#
##################
# Laplacian Gauss Seidel
laplacianStencil = "-visc* ((x(i-1,j)+x(i+1,j)-2.0*x(i,j))/(dx*dx) + (x(i,j-1)+x(i,j+1)-2.0*x(i,j))/(dy*dy))"
parabolicStencil = "x(i,j) - dt*(visc/dens) * ((x(i-1,j)+x(i+1,j)-2.0*x(i,j))/(dx*dx) + (x(i,j-1)+x(i,j+1)-2.0*x(i,j))/(dy*dy))"

laplacianGaussSeidelStokes = "rhs(i,j) + visc*((x(i-1,j)+x(i+1,j))/(dx*dx) + (x(i,j-1)+x(i,j+1))/(dy*dy))"
laplacianConstStokes = "(2.0*visc)/(dx*dx) + (2.0*visc)/(dy*dy)"

laplacianGaussSeidelNavierStokes = "rhs(i,j) + dt*(visc/dens)*((x(i-1,j)+x(i+1,j))/(dx*dx) + (x(i,j-1)+x(i,j+1))/(dy*dy))"
laplacianConstNavierStokes = "1.0d0 +(2.0*dt*(visc/dens))/(dx*dx) +(2.0*dt*(visc/dens))/(dy*dy)"



##############
# FORTRAN CODE#
##############
_laplaceEquationUpdateFunctionCode = '''!    -*- f90 -*-
subroutine laplace_update_function_%s(x,rhs,dimx,dimy,sx,ex,sy,ey,ngc,dx,dy,visc,dens,dt)
implicit none
integer :: i, j
integer, intent(in) :: dimx,dimy,sx,ex,sy,ey,ngc
double precision, intent(in) :: dx, dy, visc, dens, dt
double precision, dimension(dimx,dimy) , intent(inout) :: x, rhs
!f2py intent(inplace) :: x, rhs
!f2py threadsafe

call update_ghosts(x,dimx,dimy,sx,ex,sy,ey,ngc)
!!!!!!!!!!!!!
!red
!!!!!!!!!!!!!
do j=sy,ey,2
    do i=sx,ex,2
        x(i,j) = (%s) / (%s)
    end do
end do

do j=sy+1,ey,2
    do i=sx+1,ex,2
        x(i,j) = (%s) / (%s)
    end do
end do

call update_ghosts(x,dimx,dimy,sx,ex,sy,ey,ngc)

!!!!!!!!!!!!!
!black
!!!!!!!!!!!!!
do j=sy,ey,2
    do i=sx+1,ex,2
        x(i,j) = (%s) / (%s)
    end do
end do

do j=sy+1,ey,2
    do i=sx,ex,2
        x(i,j) = (%s) / (%s)
    end do
end do

call update_ghosts(x,dimx,dimy,sx,ex,sy,ey,ngc)

end subroutine laplace_update_function_%s
'''

laplaceEquationUpdateFunctionCodeStokes = _laplaceEquationUpdateFunctionCode % (("stokes",) + (laplacianGaussSeidelStokes, laplacianConstStokes) * 4 + ("stokes",))
laplaceEquationUpdateFunctionCodeNavierStokes = _laplaceEquationUpdateFunctionCode % (("ns",) + (laplacianGaussSeidelNavierStokes, laplacianConstNavierStokes) * 4 + ("ns",))

_laplaceEquationResidualFunctionCode = '''!    -*- f90 -*-
subroutine laplace_residual_function_%s(x,rhs,residual,dimx,dimy,sx,ex,sy,ey,ngc,dx,dy,visc,dens,dt)
implicit none
integer :: i,j
integer, intent(in) :: dimx, dimy,sx,ex,sy,ey,ngc
double precision, intent(in) :: dx, dy, visc, dens, dt
double precision, dimension(dimx,dimy) , intent(inout) :: x, rhs, residual
!f2py intent(inplace) :: x, rhs, residual

residual = 0.0
do j=sy,ey
    do i=sx,ex
        residual(i,j) = rhs(i,j) - (%s)
    end do
end do
call update_ghosts(residual,dimx,dimy,sx,ex,sy,ey,ngc)
end subroutine laplace_residual_function_%s
'''

laplaceEquationResidualFunctionCodeStokes = _laplaceEquationResidualFunctionCode % ("stokes", laplacianStencil, "stokes")
laplaceEquationResidualFunctionCodeNavierStokes = _laplaceEquationResidualFunctionCode % ("ns", parabolicStencil, "ns")

bicgstabGrad = '''!    -*- f90 -*-
subroutine grad(var,gx,gy,n,m,sx,ex,sy,ey,ngc,dx,dy)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: var , gx , gy
    double precision , intent(in) :: dx , dy
    integer , intent(in) :: sx , ex , sy , ey , n , m , ngc
    integer :: i , j
    !f2py intent(in) :: sx , ex , sy , ey , ngc , dx , dy
    !f2py intent(inplace) :: var , gx , gy
    !f2py intent(hide) :: n , m

    do j = sy-1 , ey+ngc
        do i = sx-1 , ex+ngc
            gx(i-1 , j)    = (var(i,j) - var(i-1,j))/dx
            gy(i , j)        = (var(i,j) - var(i,j-1))/dy
        end do
    end do

end subroutine
'''

bicgstabDiv = '''!    -*- f90 -*-
subroutine div(varx,vary,res,n,m,sx,ex,sy,ey,dx,dy)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: varx , vary , res
    double precision , intent(in) :: dx , dy
    integer , intent(in) :: sx , ex , sy , ey , n , m
    integer :: i , j
    !f2py intent(in) :: sx , ex , sy , ey , dx , dy
    !f2py intent(inplace) :: varx , vary , res
    !f2py intent(hide) :: n , m

    do j = sy-1 , ey+1
        do i = sx-1 , ex+1
            res(i,j) = (varx(i,j)-varx(i-1,j))/dx + (vary(i,j+1)-vary(i,j))/dy
        end do
    end do
end subroutine
'''

bicgstabInnerProduct = '''!    -*- f90 -*-
subroutine inner_product(a,b,n,m,sx,ex,sy,ey,summation)
    implicit none
    double precision , dimension(n,m) , intent(inout) :: a , b
    integer , intent(in) :: sx , ex , sy , ey , n , m
    integer :: i , j
    double precision , intent(out) :: summation
    !f2py intent(in) :: sx , ex , sy , ey
    !f2py intent(inplace) :: a , b
    !f2py intent(hide) :: n , m
    !f2py intent(out) :: summation


    summation = 0.0d0
    do j = sy , ey
        do i = sx , ex
            summation = summation + a(i,j)*b(i,j)
        end do
    end do
end subroutine
'''

convectionCode = '''!    -*- f90 -*-
subroutine compute_convection(u,v,convection_u,convection_v,dimx,dimy,sx,ex,sy,ey,ngc,dx,dy)
    implicit none
    integer, intent(in) :: dimx,dimy,sx,ex,sy,ey,ngc
    integer :: i,j
    double precision, intent(in) :: dx, dy
    double precision, dimension(dimx,dimy), intent(inout) :: u,v,convection_u,convection_v
    double precision :: v_mean, u_mean, u_dx, u_dy, v_dx, v_dy
    !f2py intent(in) :: dx,dy,sx,ex,sy,ey,ngc
    !f2py intent(hide) :: dimx,dimy
    !f2py intent(inplace) :: u,v,convection_u,convection_v

    convection_u = 0.0
    convection_v = 0.0

    do j = sy,ey
        do i = sx,ex
            u_mean = (u(i,j)+u(i-1,j)+u(i,j-1)+u(i-1,j-1))/4.0d0
            v_mean = (v(i,j)+v(i+1,j)+v(i,j+1)+v(i+1,j+1))/4.0d0

            u_dx = (u(i+1,j)-u(i-1,j))/(2.0d0*dx)
            u_dy = (u(i,j+1)-u(i,j-1))/(2.0d0*dy)

            v_dx = (v(i+1,j)-v(i-1,j))/(2.0d0*dx)
            v_dy = (v(i,j+1)-v(i,j-1))/(2.0d0*dy)

            convection_u(i,j) = u(i,j)*u_dx + v_mean*u_dy
            convection_v(i,j) = u_mean*v_dx + v(i,j)*v_dy
        end do
    end do

    call update_ghosts(convection_u,dimx,dimy,sx,ex,sy,ey,ngc)
    call update_ghosts(convection_v,dimx,dimy,sx,ex,sy,ey,ngc)
end subroutine compute_convection
'''

codeLaplaceHeader = '''!    -*- f90 -*-
module laplace
include "omp_lib.h"
contains
'''

codeBicgstabHeader = '''!    -*- f90 -*-
module bicgstab
include "omp_lib.h"
contains
'''

########################
# Compiling Fortran Code#
########################
codeLaplace = codeLaplaceHeader + updateGhostsCode + laplaceEquationUpdateFunctionCodeStokes + laplaceEquationResidualFunctionCodeStokes
codeLaplace += laplaceEquationUpdateFunctionCodeNavierStokes + laplaceEquationResidualFunctionCodeNavierStokes + convectionCode + "end module"

codeBicgstab = codeBicgstabHeader + bicgstabGrad + bicgstabDiv + bicgstabInnerProduct + "end module"


def retrieveCode():
    name_of_current_file = realpath(__file__)
    codeInfo = [(codeLaplace, "fast_laplace", name_of_current_file)]
    codeInfo.append((codeBicgstab, "fast_bicgstab", name_of_current_file))
    return codeInfo

