'''
Created on Mar 4, 2012

@author: felipe
'''
import utils
import numpy
import linear_system
from math_objects.scalar_field import RightScalarField, BottomScalarField, CenterScalarField
from math_objects.cartesian_grid import *
import fast_scalar_field
from time import time
import warnings
import pylab

class Multigrid(object):
    '''
    Solve a LinearSystem with the Multigrid method.
    '''


    def __init__(self, finestSystem):
        '''
        Constructor
        '''
        if finestSystem:
            grid = finestSystem.x.grid
            self.systems = [finestSystem]
            nx, ny = grid.nx, grid.ny
            coarseGrid = grid
            while nx >= 8 and ny >= 8:
                coarseGrid = grid.generateCoarseGrid(nx, ny)
                coarseCorrection = finestSystem.x.generateNewScalarField(coarseGrid)
                coarseSystem = linear_system.LinearSystem(finestSystem.updateFunction, finestSystem.residualFunction, coarseCorrection)
                self.systems.append(coarseSystem)
                nx /= 2 ; ny /= 2



    def interpolate(self, coarseScalarField, fineArray):
        '''
        Interpolate the correction from the coarse scalar field to the fineArray.
        '''
        sx, ex, sy, ey = coarseScalarField.grid.getBounds(Fortran=True)

        if isinstance(coarseScalarField, RightScalarField):
            fast_scalar_field.scalar_field.interpolate_correct_right(fineArray, coarseScalarField.data, sx, ex, sy, ey)

        if isinstance(coarseScalarField, BottomScalarField):
            fast_scalar_field.scalar_field.interpolate_correct_bottom(fineArray, coarseScalarField.data, sx, ex, sy, ey)

        if isinstance(coarseScalarField, CenterScalarField):
            fast_scalar_field.scalar_field.interpolate_correct_center(fineArray, coarseScalarField.data, sx, ex, sy, ey)

    def restrict(self, fineScalarField, coarseArray):
        '''
        Restrict the residual from the fine scalar field to the coarse array.
        '''
        sx, ex, sy, ey = fineScalarField.grid.getBounds(Fortran=True)
        # converts the fine bounds to the coarse bounds
        ex = ex / 2 + 1 ; ey = ey / 2 + 1

        if isinstance(fineScalarField, RightScalarField):
            fast_scalar_field.scalar_field.restrict_right(fineScalarField.data, coarseArray, sx, ex, sy, ey)

        if isinstance(fineScalarField, BottomScalarField):
            fast_scalar_field.scalar_field.restrict_bottom(fineScalarField.data, coarseArray, sx, ex, sy, ey)

        if isinstance(fineScalarField, CenterScalarField):
            fast_scalar_field.scalar_field.restrict_center(fineScalarField.data, coarseArray, sx, ex, sy, ey)

    def vCycle(self, visc, dens, dt, printResidual=False):
        finestSystem = self.systems[0]
        finestSystem.computeResidual(visc, dens, dt)

        self.systems[1].rhs.data[:, :] = finestSystem.residual.data[:, :]

        vCycleRange = range(1, len(self.systems) - 1)
        for i in vCycleRange:
            fineSystem = self.systems[i]
            coarseSystem = self.systems[i + 1]
            fineSystem.x.data[:, :] = 0.
            fineSystem.computeIteration(visc, dens, dt)
            fineSystem.computeResidual(visc, dens, dt)
            self.restrict(fineSystem.residual, coarseSystem.rhs.data)

        coarsestSystem = self.systems[-1]
        coarsestSystem.x.data[:, :] = 0.
        coarsestSystem.computeIteration(visc, dens, dt, 50)

        vCycleRange.reverse()
        for i in vCycleRange:
            fineSystem = self.systems[i]
            coarseSystem = self.systems[i + 1]
            self.interpolate(coarseSystem.x, fineSystem.x.data)
            fineSystem.computeIteration(visc, dens, dt)

        finestSystem.x.data += self.systems[1].x.data

        maxNorm = numpy.abs(finestSystem.residual.data).max()
        if printResidual:
            print "Max Norm of residual:", maxNorm
        return maxNorm

    def solve(self, visc, dens, dt, tolerance, enforce_compatibility=True):
        maxNorm = 1.
        if enforce_compatibility:
            self.systems[0].enforce_compatibility_condition()
        numIters = 0
        maxIters = 500
        while maxNorm > tolerance and numIters < maxIters:
            numIters += 1
            maxNorm = self.vCycle(visc, dens, dt)

        if numIters == maxIters:
            print "RHS (|min|,|max|): (%g,%g):" % (abs(self.systems[0].rhs.data).min(), abs(self.systems[0].rhs.data).max())
#            raise RuntimeError("Multigrid made more then %d iters!!! Max Norm:%g" % (maxIters, maxNorm))


if __name__ == '__main__':
    import numpy
    from sympy import var, cos, sin, lambdify, diff
    from numpy.linalg import norm
    from numpy import inf, pi
    import fast_laplace
    grids = [Grid(Point(0., 0.), 1., 1., (2 ** i), (2 ** i), 2) for i in range(5, 10)]

    M = Multigrid(None)

#    def test_interpolation(ScalarFieldClass, gridCoarse, gridFine):
#        fieldCoarse = ScalarFieldClass(gridCoarse)
#        fieldFine = ScalarFieldClass(gridFine)
#        fieldFineExact = ScalarFieldClass(gridFine)
#
#        x, y = var("x y")
#        function = lambdify([x, y], cos(2 * pi * x) * sin(2 * pi * y), numpy)
#
#        fieldCoarse.initialize(function)
#        fieldFineExact.initialize(function)
#
#        M.interpolate(fieldCoarse, fieldFine.data)
#        sx, ex, sy, ey = fieldFine.grid.getBounds()
#        return norm((fieldFine.data - fieldFineExact.data)[sx:ex, sy:ey].flatten(), inf)
#
#
#    def test_restriction(ScalarFieldClass, gridCoarse, gridFine):
#        fieldCoarse = ScalarFieldClass(gridCoarse)
#        fieldCoarseExact = ScalarFieldClass(gridCoarse)
#        fieldFine = ScalarFieldClass(gridFine)
#
#
#        x, y = var("x y")
#        function = lambdify([x, y], cos(2 * pi * x) * sin(2 * pi * y), numpy)
#
#        fieldFine.initialize(function)
#        fieldCoarseExact.initialize(function)
#
#        M.restrict(fieldFine, fieldCoarse.data)
#        sx, ex, sy, ey = fieldCoarse.grid.getBounds()
#        return norm((fieldCoarse.data - fieldCoarseExact.data)[sx:ex, sy:ey].flatten(), inf)
#
#    #######################################
#    #Testing Interpolation and Restriction#
#    #######################################
#    for ScalarFieldType in (RightScalarField, BottomScalarField, CenterScalarField):
#
#        print "\nInterpolation test:"
#        old_max = 0.
#        for i in range(len(grids) - 1):
#            coarse = grids[i]
#            fine = grids[i + 1]
#            new_max = test_interpolation(ScalarFieldType, coarse, fine)
#            nxc, nyc = coarse.nx, coarse.ny
#            nxf, nyf = fine.nx, fine.ny
#            print "\n(%d,%d) -> (%d,%d) Norm, ratio: (%g,%g)" % (nxc, nyc, nxf, nyf, new_max, old_max / new_max)
#            old_max = new_max
#
#        old_max = 0.
#        print "\nRestriction test:"
#        for i in range(len(grids) - 1):
#            coarse = grids[i]
#            fine = grids[i + 1]
#            new_max = test_restriction(ScalarFieldType, coarse, fine)
#            nxc, nyc = coarse.nx, coarse.ny
#            nxf, nyf = fine.nx, fine.ny
#            print "\n(%d,%d) -> (%d,%d) Norm, ratio: (%g,%g)" % (nxf, nyf, nxc, nyc, new_max, old_max / new_max)
#            old_max = new_max


    #####################
    # Testing the V-Cycle#
    #####################
    t = time()
    x, y = var("x y")
    function = sin(2 * pi * x) * cos(2 * pi * y)
    laplacian = lambdify([x, y], diff(function, x, x) + diff(function, y, y), numpy)
    function = lambdify([x, y], function, numpy)
    grid = Grid(Point(0., 0.), 1., 1., 1024, 1024, 2)
    visc, dens, dt = 1., 0., 0.1

    for ScalarFieldType in (BottomScalarField, RightScalarField, CenterScalarField):
        x = ScalarFieldType(grid)
        finestSystem = linear_system.LinearSystem(fast_laplace.laplace.laplace_update_function_stokes, fast_laplace.laplace.laplace_residual_function_stokes, x)
        finestSystem.rhs.initialize(laplacian)
        M = Multigrid(finestSystem)

        iterations = []
        residuals = []
        t = time()
        for i in range(30):
            M.vCycle(visc, dens, dt, False)
            iterations.append(i)
            res = numpy.abs(finestSystem.residual.data).max()
            residuals.append(res)

            if res < grid.dx * grid.dy:
                break
        print time() - t
        pylab.plot(iterations, residuals)
        pylab.yscale('log')
        pylab.title(str(ScalarFieldType))
        pylab.show()
